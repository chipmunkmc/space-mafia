using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Discord
{
	public class AchievementManager
	{
		internal struct FFIEvents
		{
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void UserAchievementUpdateHandler(IntPtr ptr, ref UserAchievement userAchievement);

			internal UserAchievementUpdateHandler OnUserAchievementUpdate;
		}

		internal struct FFIMethods
		{
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void SetUserAchievementCallback(IntPtr ptr, Result result);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void SetUserAchievementMethod(IntPtr methodsPtr, long achievementId, long percentComplete, IntPtr callbackData, SetUserAchievementCallback callback);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void FetchUserAchievementsCallback(IntPtr ptr, Result result);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void FetchUserAchievementsMethod(IntPtr methodsPtr, IntPtr callbackData, FetchUserAchievementsCallback callback);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void CountUserAchievementsMethod(IntPtr methodsPtr, ref int count);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate Result GetUserAchievementMethod(IntPtr methodsPtr, long userAchievementId, ref UserAchievement userAchievement);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate Result GetUserAchievementAtMethod(IntPtr methodsPtr, int index, ref UserAchievement userAchievement);

			internal SetUserAchievementMethod SetUserAchievement;

			internal FetchUserAchievementsMethod FetchUserAchievements;

			internal CountUserAchievementsMethod CountUserAchievements;

			internal GetUserAchievementMethod GetUserAchievement;

			internal GetUserAchievementAtMethod GetUserAchievementAt;
		}

		public delegate void SetUserAchievementHandler(Result result);

		public delegate void FetchUserAchievementsHandler(Result result);

		public delegate void UserAchievementUpdateHandler(ref UserAchievement userAchievement);

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass14_0
		{
			public SetUserAchievementHandler callback;

			internal void _003CSetUserAchievement_003Eb__0(IntPtr ptr, Result result)
			{
				Utility.Release(ptr);
				callback(result);
			}
		}

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass15_0
		{
			public FetchUserAchievementsHandler callback;

			internal void _003CFetchUserAchievements_003Eb__0(IntPtr ptr, Result result)
			{
				Utility.Release(ptr);
				callback(result);
			}
		}

		private IntPtr MethodsPtr;

		private object MethodsStructure;

		private FFIMethods Methods
		{
			get
			{
				if (MethodsStructure == null)
				{
					MethodsStructure = Marshal.PtrToStructure(MethodsPtr, typeof(FFIMethods));
				}
				return (FFIMethods)MethodsStructure;
			}
		}

		public event UserAchievementUpdateHandler OnUserAchievementUpdate;

		internal AchievementManager(IntPtr ptr, IntPtr eventsPtr, ref FFIEvents events)
		{
			if (eventsPtr == IntPtr.Zero)
			{
				throw new ResultException(Result.InternalError);
			}
			InitEvents(eventsPtr, ref events);
			MethodsPtr = ptr;
			if (MethodsPtr == IntPtr.Zero)
			{
				throw new ResultException(Result.InternalError);
			}
		}

		private void InitEvents(IntPtr eventsPtr, ref FFIEvents events)
		{
			events.OnUserAchievementUpdate = _003CInitEvents_003Eb__13_0;
			Marshal.StructureToPtr(events, eventsPtr, false);
		}

		public void SetUserAchievement(long achievementId, long percentComplete, SetUserAchievementHandler callback)
		{
			_003C_003Ec__DisplayClass14_0 _003C_003Ec__DisplayClass14_ = new _003C_003Ec__DisplayClass14_0();
			_003C_003Ec__DisplayClass14_.callback = callback;
			FFIMethods.SetUserAchievementCallback setUserAchievementCallback = _003C_003Ec__DisplayClass14_._003CSetUserAchievement_003Eb__0;
			Methods.SetUserAchievement(MethodsPtr, achievementId, percentComplete, Utility.Retain(setUserAchievementCallback), setUserAchievementCallback);
		}

		public void FetchUserAchievements(FetchUserAchievementsHandler callback)
		{
			_003C_003Ec__DisplayClass15_0 _003C_003Ec__DisplayClass15_ = new _003C_003Ec__DisplayClass15_0();
			_003C_003Ec__DisplayClass15_.callback = callback;
			FFIMethods.FetchUserAchievementsCallback fetchUserAchievementsCallback = _003C_003Ec__DisplayClass15_._003CFetchUserAchievements_003Eb__0;
			Methods.FetchUserAchievements(MethodsPtr, Utility.Retain(fetchUserAchievementsCallback), fetchUserAchievementsCallback);
		}

		public int CountUserAchievements()
		{
			int count = 0;
			Methods.CountUserAchievements(MethodsPtr, ref count);
			return count;
		}

		public UserAchievement GetUserAchievement(long userAchievementId)
		{
			UserAchievement userAchievement = default(UserAchievement);
			Result result = Methods.GetUserAchievement(MethodsPtr, userAchievementId, ref userAchievement);
			if (result != 0)
			{
				throw new ResultException(result);
			}
			return userAchievement;
		}

		public UserAchievement GetUserAchievementAt(int index)
		{
			UserAchievement userAchievement = default(UserAchievement);
			Result result = Methods.GetUserAchievementAt(MethodsPtr, index, ref userAchievement);
			if (result != 0)
			{
				throw new ResultException(result);
			}
			return userAchievement;
		}

		[CompilerGenerated]
		private void _003CInitEvents_003Eb__13_0(IntPtr ptr, ref UserAchievement userAchievement)
		{
			if (this.OnUserAchievementUpdate != null)
			{
				this.OnUserAchievementUpdate(ref userAchievement);
			}
		}
	}
}
