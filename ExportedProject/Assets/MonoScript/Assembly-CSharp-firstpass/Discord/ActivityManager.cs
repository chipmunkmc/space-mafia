using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Discord
{
	public class ActivityManager
	{
		internal struct FFIEvents
		{
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void ActivityJoinHandler(IntPtr ptr, [MarshalAs(UnmanagedType.LPStr)] string secret);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void ActivitySpectateHandler(IntPtr ptr, [MarshalAs(UnmanagedType.LPStr)] string secret);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void ActivityJoinRequestHandler(IntPtr ptr, ref User user);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void ActivityInviteHandler(IntPtr ptr, ActivityActionType type, ref User user, ref Activity activity);

			internal ActivityJoinHandler OnActivityJoin;

			internal ActivitySpectateHandler OnActivitySpectate;

			internal ActivityJoinRequestHandler OnActivityJoinRequest;

			internal ActivityInviteHandler OnActivityInvite;
		}

		internal struct FFIMethods
		{
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate Result RegisterCommandMethod(IntPtr methodsPtr, [MarshalAs(UnmanagedType.LPStr)] string command);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate Result RegisterSteamMethod(IntPtr methodsPtr, uint steamId);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void UpdateActivityCallback(IntPtr ptr, Result result);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void UpdateActivityMethod(IntPtr methodsPtr, ref Activity activity, IntPtr callbackData, UpdateActivityCallback callback);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void ClearActivityCallback(IntPtr ptr, Result result);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void ClearActivityMethod(IntPtr methodsPtr, IntPtr callbackData, ClearActivityCallback callback);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void SendRequestReplyCallback(IntPtr ptr, Result result);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void SendRequestReplyMethod(IntPtr methodsPtr, long userId, ActivityJoinRequestReply reply, IntPtr callbackData, SendRequestReplyCallback callback);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void SendInviteCallback(IntPtr ptr, Result result);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void SendInviteMethod(IntPtr methodsPtr, long userId, ActivityActionType type, [MarshalAs(UnmanagedType.LPStr)] string content, IntPtr callbackData, SendInviteCallback callback);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void AcceptInviteCallback(IntPtr ptr, Result result);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void AcceptInviteMethod(IntPtr methodsPtr, long userId, IntPtr callbackData, AcceptInviteCallback callback);

			internal RegisterCommandMethod RegisterCommand;

			internal RegisterSteamMethod RegisterSteam;

			internal UpdateActivityMethod UpdateActivity;

			internal ClearActivityMethod ClearActivity;

			internal SendRequestReplyMethod SendRequestReply;

			internal SendInviteMethod SendInvite;

			internal AcceptInviteMethod AcceptInvite;
		}

		public delegate void UpdateActivityHandler(Result result);

		public delegate void ClearActivityHandler(Result result);

		public delegate void SendRequestReplyHandler(Result result);

		public delegate void SendInviteHandler(Result result);

		public delegate void AcceptInviteHandler(Result result);

		public delegate void ActivityJoinHandler(string secret);

		public delegate void ActivitySpectateHandler(string secret);

		public delegate void ActivityJoinRequestHandler(ref User user);

		public delegate void ActivityInviteHandler(ActivityActionType type, ref User user, ref Activity activity);

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass32_0
		{
			public UpdateActivityHandler callback;

			internal void _003CUpdateActivity_003Eb__0(IntPtr ptr, Result result)
			{
				Utility.Release(ptr);
				callback(result);
			}
		}

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass33_0
		{
			public ClearActivityHandler callback;

			internal void _003CClearActivity_003Eb__0(IntPtr ptr, Result result)
			{
				Utility.Release(ptr);
				callback(result);
			}
		}

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass34_0
		{
			public SendRequestReplyHandler callback;

			internal void _003CSendRequestReply_003Eb__0(IntPtr ptr, Result result)
			{
				Utility.Release(ptr);
				callback(result);
			}
		}

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass35_0
		{
			public SendInviteHandler callback;

			internal void _003CSendInvite_003Eb__0(IntPtr ptr, Result result)
			{
				Utility.Release(ptr);
				callback(result);
			}
		}

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass36_0
		{
			public AcceptInviteHandler callback;

			internal void _003CAcceptInvite_003Eb__0(IntPtr ptr, Result result)
			{
				Utility.Release(ptr);
				callback(result);
			}
		}

		private IntPtr MethodsPtr;

		private object MethodsStructure;

		private FFIMethods Methods
		{
			get
			{
				if (MethodsStructure == null)
				{
					MethodsStructure = Marshal.PtrToStructure(MethodsPtr, typeof(FFIMethods));
				}
				return (FFIMethods)MethodsStructure;
			}
		}

		public event ActivityJoinHandler OnActivityJoin;

		public event ActivitySpectateHandler OnActivitySpectate;

		public event ActivityJoinRequestHandler OnActivityJoinRequest;

		public event ActivityInviteHandler OnActivityInvite;

		public void RegisterCommand()
		{
			RegisterCommand(null);
		}

		internal ActivityManager(IntPtr ptr, IntPtr eventsPtr, ref FFIEvents events)
		{
			if (eventsPtr == IntPtr.Zero)
			{
				throw new ResultException(Result.InternalError);
			}
			InitEvents(eventsPtr, ref events);
			MethodsPtr = ptr;
			if (MethodsPtr == IntPtr.Zero)
			{
				throw new ResultException(Result.InternalError);
			}
		}

		private void InitEvents(IntPtr eventsPtr, ref FFIEvents events)
		{
			events.OnActivityJoin = _003CInitEvents_003Eb__29_0;
			events.OnActivitySpectate = _003CInitEvents_003Eb__29_1;
			events.OnActivityJoinRequest = _003CInitEvents_003Eb__29_2;
			events.OnActivityInvite = _003CInitEvents_003Eb__29_3;
			Marshal.StructureToPtr(events, eventsPtr, false);
		}

		public void RegisterCommand(string command)
		{
			Result result = Methods.RegisterCommand(MethodsPtr, command);
			if (result != 0)
			{
				throw new ResultException(result);
			}
		}

		public void RegisterSteam(uint steamId)
		{
			Result result = Methods.RegisterSteam(MethodsPtr, steamId);
			if (result != 0)
			{
				throw new ResultException(result);
			}
		}

		public void UpdateActivity(Activity activity, UpdateActivityHandler callback)
		{
			_003C_003Ec__DisplayClass32_0 _003C_003Ec__DisplayClass32_ = new _003C_003Ec__DisplayClass32_0();
			_003C_003Ec__DisplayClass32_.callback = callback;
			FFIMethods.UpdateActivityCallback updateActivityCallback = _003C_003Ec__DisplayClass32_._003CUpdateActivity_003Eb__0;
			Methods.UpdateActivity(MethodsPtr, ref activity, Utility.Retain(updateActivityCallback), updateActivityCallback);
		}

		public void ClearActivity(ClearActivityHandler callback)
		{
			_003C_003Ec__DisplayClass33_0 _003C_003Ec__DisplayClass33_ = new _003C_003Ec__DisplayClass33_0();
			_003C_003Ec__DisplayClass33_.callback = callback;
			FFIMethods.ClearActivityCallback clearActivityCallback = _003C_003Ec__DisplayClass33_._003CClearActivity_003Eb__0;
			Methods.ClearActivity(MethodsPtr, Utility.Retain(clearActivityCallback), clearActivityCallback);
		}

		public void SendRequestReply(long userId, ActivityJoinRequestReply reply, SendRequestReplyHandler callback)
		{
			_003C_003Ec__DisplayClass34_0 _003C_003Ec__DisplayClass34_ = new _003C_003Ec__DisplayClass34_0();
			_003C_003Ec__DisplayClass34_.callback = callback;
			FFIMethods.SendRequestReplyCallback sendRequestReplyCallback = _003C_003Ec__DisplayClass34_._003CSendRequestReply_003Eb__0;
			Methods.SendRequestReply(MethodsPtr, userId, reply, Utility.Retain(sendRequestReplyCallback), sendRequestReplyCallback);
		}

		public void SendInvite(long userId, ActivityActionType type, string content, SendInviteHandler callback)
		{
			_003C_003Ec__DisplayClass35_0 _003C_003Ec__DisplayClass35_ = new _003C_003Ec__DisplayClass35_0();
			_003C_003Ec__DisplayClass35_.callback = callback;
			FFIMethods.SendInviteCallback sendInviteCallback = _003C_003Ec__DisplayClass35_._003CSendInvite_003Eb__0;
			Methods.SendInvite(MethodsPtr, userId, type, content, Utility.Retain(sendInviteCallback), sendInviteCallback);
		}

		public void AcceptInvite(long userId, AcceptInviteHandler callback)
		{
			_003C_003Ec__DisplayClass36_0 _003C_003Ec__DisplayClass36_ = new _003C_003Ec__DisplayClass36_0();
			_003C_003Ec__DisplayClass36_.callback = callback;
			FFIMethods.AcceptInviteCallback acceptInviteCallback = _003C_003Ec__DisplayClass36_._003CAcceptInvite_003Eb__0;
			Methods.AcceptInvite(MethodsPtr, userId, Utility.Retain(acceptInviteCallback), acceptInviteCallback);
		}

		[CompilerGenerated]
		private void _003CInitEvents_003Eb__29_0(IntPtr ptr, string secret)
		{
			if (this.OnActivityJoin != null)
			{
				this.OnActivityJoin(secret);
			}
		}

		[CompilerGenerated]
		private void _003CInitEvents_003Eb__29_1(IntPtr ptr, string secret)
		{
			if (this.OnActivitySpectate != null)
			{
				this.OnActivitySpectate(secret);
			}
		}

		[CompilerGenerated]
		private void _003CInitEvents_003Eb__29_2(IntPtr ptr, ref User user)
		{
			if (this.OnActivityJoinRequest != null)
			{
				this.OnActivityJoinRequest(ref user);
			}
		}

		[CompilerGenerated]
		private void _003CInitEvents_003Eb__29_3(IntPtr ptr, ActivityActionType type, ref User user, ref Activity activity)
		{
			if (this.OnActivityInvite != null)
			{
				this.OnActivityInvite(type, ref user, ref activity);
			}
		}
	}
}
