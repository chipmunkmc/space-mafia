using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

namespace Discord
{
	public class LobbyManager
	{
		internal struct FFIEvents
		{
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void LobbyUpdateHandler(IntPtr ptr, long lobbyId);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void LobbyDeleteHandler(IntPtr ptr, long lobbyId, uint reason);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void MemberConnectHandler(IntPtr ptr, long lobbyId, long userId);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void MemberUpdateHandler(IntPtr ptr, long lobbyId, long userId);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void MemberDisconnectHandler(IntPtr ptr, long lobbyId, long userId);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void LobbyMessageHandler(IntPtr ptr, long lobbyId, long userId, IntPtr dataPtr, int dataLen);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void SpeakingHandler(IntPtr ptr, long lobbyId, long userId, bool speaking);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void NetworkMessageHandler(IntPtr ptr, long lobbyId, long userId, byte channelId, IntPtr dataPtr, int dataLen);

			internal LobbyUpdateHandler OnLobbyUpdate;

			internal LobbyDeleteHandler OnLobbyDelete;

			internal MemberConnectHandler OnMemberConnect;

			internal MemberUpdateHandler OnMemberUpdate;

			internal MemberDisconnectHandler OnMemberDisconnect;

			internal LobbyMessageHandler OnLobbyMessage;

			internal SpeakingHandler OnSpeaking;

			internal NetworkMessageHandler OnNetworkMessage;
		}

		internal struct FFIMethods
		{
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate Result GetLobbyCreateTransactionMethod(IntPtr methodsPtr, ref IntPtr transaction);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate Result GetLobbyUpdateTransactionMethod(IntPtr methodsPtr, long lobbyId, ref IntPtr transaction);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate Result GetMemberUpdateTransactionMethod(IntPtr methodsPtr, long lobbyId, long userId, ref IntPtr transaction);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void CreateLobbyCallback(IntPtr ptr, Result result, ref Lobby lobby);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void CreateLobbyMethod(IntPtr methodsPtr, IntPtr transaction, IntPtr callbackData, CreateLobbyCallback callback);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void UpdateLobbyCallback(IntPtr ptr, Result result);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void UpdateLobbyMethod(IntPtr methodsPtr, long lobbyId, IntPtr transaction, IntPtr callbackData, UpdateLobbyCallback callback);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void DeleteLobbyCallback(IntPtr ptr, Result result);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void DeleteLobbyMethod(IntPtr methodsPtr, long lobbyId, IntPtr callbackData, DeleteLobbyCallback callback);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void ConnectLobbyCallback(IntPtr ptr, Result result, ref Lobby lobby);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void ConnectLobbyMethod(IntPtr methodsPtr, long lobbyId, [MarshalAs(UnmanagedType.LPStr)] string secret, IntPtr callbackData, ConnectLobbyCallback callback);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void ConnectLobbyWithActivitySecretCallback(IntPtr ptr, Result result, ref Lobby lobby);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void ConnectLobbyWithActivitySecretMethod(IntPtr methodsPtr, [MarshalAs(UnmanagedType.LPStr)] string activitySecret, IntPtr callbackData, ConnectLobbyWithActivitySecretCallback callback);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void DisconnectLobbyCallback(IntPtr ptr, Result result);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void DisconnectLobbyMethod(IntPtr methodsPtr, long lobbyId, IntPtr callbackData, DisconnectLobbyCallback callback);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate Result GetLobbyMethod(IntPtr methodsPtr, long lobbyId, ref Lobby lobby);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate Result GetLobbyActivitySecretMethod(IntPtr methodsPtr, long lobbyId, StringBuilder secret);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate Result GetLobbyMetadataValueMethod(IntPtr methodsPtr, long lobbyId, [MarshalAs(UnmanagedType.LPStr)] string key, StringBuilder value);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate Result GetLobbyMetadataKeyMethod(IntPtr methodsPtr, long lobbyId, int index, StringBuilder key);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate Result LobbyMetadataCountMethod(IntPtr methodsPtr, long lobbyId, ref int count);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate Result MemberCountMethod(IntPtr methodsPtr, long lobbyId, ref int count);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate Result GetMemberUserIdMethod(IntPtr methodsPtr, long lobbyId, int index, ref long userId);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate Result GetMemberUserMethod(IntPtr methodsPtr, long lobbyId, long userId, ref User user);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate Result GetMemberMetadataValueMethod(IntPtr methodsPtr, long lobbyId, long userId, [MarshalAs(UnmanagedType.LPStr)] string key, StringBuilder value);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate Result GetMemberMetadataKeyMethod(IntPtr methodsPtr, long lobbyId, long userId, int index, StringBuilder key);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate Result MemberMetadataCountMethod(IntPtr methodsPtr, long lobbyId, long userId, ref int count);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void UpdateMemberCallback(IntPtr ptr, Result result);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void UpdateMemberMethod(IntPtr methodsPtr, long lobbyId, long userId, IntPtr transaction, IntPtr callbackData, UpdateMemberCallback callback);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void SendLobbyMessageCallback(IntPtr ptr, Result result);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void SendLobbyMessageMethod(IntPtr methodsPtr, long lobbyId, byte[] data, int dataLen, IntPtr callbackData, SendLobbyMessageCallback callback);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate Result GetSearchQueryMethod(IntPtr methodsPtr, ref IntPtr query);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void SearchCallback(IntPtr ptr, Result result);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void SearchMethod(IntPtr methodsPtr, IntPtr query, IntPtr callbackData, SearchCallback callback);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void LobbyCountMethod(IntPtr methodsPtr, ref int count);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate Result GetLobbyIdMethod(IntPtr methodsPtr, int index, ref long lobbyId);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void ConnectVoiceCallback(IntPtr ptr, Result result);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void ConnectVoiceMethod(IntPtr methodsPtr, long lobbyId, IntPtr callbackData, ConnectVoiceCallback callback);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void DisconnectVoiceCallback(IntPtr ptr, Result result);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void DisconnectVoiceMethod(IntPtr methodsPtr, long lobbyId, IntPtr callbackData, DisconnectVoiceCallback callback);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate Result ConnectNetworkMethod(IntPtr methodsPtr, long lobbyId);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate Result DisconnectNetworkMethod(IntPtr methodsPtr, long lobbyId);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate Result FlushNetworkMethod(IntPtr methodsPtr);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate Result OpenNetworkChannelMethod(IntPtr methodsPtr, long lobbyId, byte channelId, bool reliable);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate Result SendNetworkMessageMethod(IntPtr methodsPtr, long lobbyId, long userId, byte channelId, byte[] data, int dataLen);

			internal GetLobbyCreateTransactionMethod GetLobbyCreateTransaction;

			internal GetLobbyUpdateTransactionMethod GetLobbyUpdateTransaction;

			internal GetMemberUpdateTransactionMethod GetMemberUpdateTransaction;

			internal CreateLobbyMethod CreateLobby;

			internal UpdateLobbyMethod UpdateLobby;

			internal DeleteLobbyMethod DeleteLobby;

			internal ConnectLobbyMethod ConnectLobby;

			internal ConnectLobbyWithActivitySecretMethod ConnectLobbyWithActivitySecret;

			internal DisconnectLobbyMethod DisconnectLobby;

			internal GetLobbyMethod GetLobby;

			internal GetLobbyActivitySecretMethod GetLobbyActivitySecret;

			internal GetLobbyMetadataValueMethod GetLobbyMetadataValue;

			internal GetLobbyMetadataKeyMethod GetLobbyMetadataKey;

			internal LobbyMetadataCountMethod LobbyMetadataCount;

			internal MemberCountMethod MemberCount;

			internal GetMemberUserIdMethod GetMemberUserId;

			internal GetMemberUserMethod GetMemberUser;

			internal GetMemberMetadataValueMethod GetMemberMetadataValue;

			internal GetMemberMetadataKeyMethod GetMemberMetadataKey;

			internal MemberMetadataCountMethod MemberMetadataCount;

			internal UpdateMemberMethod UpdateMember;

			internal SendLobbyMessageMethod SendLobbyMessage;

			internal GetSearchQueryMethod GetSearchQuery;

			internal SearchMethod Search;

			internal LobbyCountMethod LobbyCount;

			internal GetLobbyIdMethod GetLobbyId;

			internal ConnectVoiceMethod ConnectVoice;

			internal DisconnectVoiceMethod DisconnectVoice;

			internal ConnectNetworkMethod ConnectNetwork;

			internal DisconnectNetworkMethod DisconnectNetwork;

			internal FlushNetworkMethod FlushNetwork;

			internal OpenNetworkChannelMethod OpenNetworkChannel;

			internal SendNetworkMessageMethod SendNetworkMessage;
		}

		public delegate void CreateLobbyHandler(Result result, ref Lobby lobby);

		public delegate void UpdateLobbyHandler(Result result);

		public delegate void DeleteLobbyHandler(Result result);

		public delegate void ConnectLobbyHandler(Result result, ref Lobby lobby);

		public delegate void ConnectLobbyWithActivitySecretHandler(Result result, ref Lobby lobby);

		public delegate void DisconnectLobbyHandler(Result result);

		public delegate void UpdateMemberHandler(Result result);

		public delegate void SendLobbyMessageHandler(Result result);

		public delegate void SearchHandler(Result result);

		public delegate void ConnectVoiceHandler(Result result);

		public delegate void DisconnectVoiceHandler(Result result);

		public delegate void LobbyUpdateHandler(long lobbyId);

		public delegate void LobbyDeleteHandler(long lobbyId, uint reason);

		public delegate void MemberConnectHandler(long lobbyId, long userId);

		public delegate void MemberUpdateHandler(long lobbyId, long userId);

		public delegate void MemberDisconnectHandler(long lobbyId, long userId);

		public delegate void LobbyMessageHandler(long lobbyId, long userId, byte[] data);

		public delegate void SpeakingHandler(long lobbyId, long userId, bool speaking);

		public delegate void NetworkMessageHandler(long lobbyId, long userId, byte channelId, byte[] data);

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass54_0
		{
			public CreateLobbyHandler callback;

			internal void _003CCreateLobby_003Eb__0(IntPtr ptr, Result result, ref Lobby lobby)
			{
				Utility.Release(ptr);
				callback(result, ref lobby);
			}
		}

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass55_0
		{
			public UpdateLobbyHandler callback;

			internal void _003CUpdateLobby_003Eb__0(IntPtr ptr, Result result)
			{
				Utility.Release(ptr);
				callback(result);
			}
		}

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass56_0
		{
			public DeleteLobbyHandler callback;

			internal void _003CDeleteLobby_003Eb__0(IntPtr ptr, Result result)
			{
				Utility.Release(ptr);
				callback(result);
			}
		}

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass57_0
		{
			public ConnectLobbyHandler callback;

			internal void _003CConnectLobby_003Eb__0(IntPtr ptr, Result result, ref Lobby lobby)
			{
				Utility.Release(ptr);
				callback(result, ref lobby);
			}
		}

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass58_0
		{
			public ConnectLobbyWithActivitySecretHandler callback;

			internal void _003CConnectLobbyWithActivitySecret_003Eb__0(IntPtr ptr, Result result, ref Lobby lobby)
			{
				Utility.Release(ptr);
				callback(result, ref lobby);
			}
		}

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass59_0
		{
			public DisconnectLobbyHandler callback;

			internal void _003CDisconnectLobby_003Eb__0(IntPtr ptr, Result result)
			{
				Utility.Release(ptr);
				callback(result);
			}
		}

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass71_0
		{
			public UpdateMemberHandler callback;

			internal void _003CUpdateMember_003Eb__0(IntPtr ptr, Result result)
			{
				Utility.Release(ptr);
				callback(result);
			}
		}

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass72_0
		{
			public SendLobbyMessageHandler callback;

			internal void _003CSendLobbyMessage_003Eb__0(IntPtr ptr, Result result)
			{
				Utility.Release(ptr);
				callback(result);
			}
		}

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass74_0
		{
			public SearchHandler callback;

			internal void _003CSearch_003Eb__0(IntPtr ptr, Result result)
			{
				Utility.Release(ptr);
				callback(result);
			}
		}

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass77_0
		{
			public ConnectVoiceHandler callback;

			internal void _003CConnectVoice_003Eb__0(IntPtr ptr, Result result)
			{
				Utility.Release(ptr);
				callback(result);
			}
		}

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass78_0
		{
			public DisconnectVoiceHandler callback;

			internal void _003CDisconnectVoice_003Eb__0(IntPtr ptr, Result result)
			{
				Utility.Release(ptr);
				callback(result);
			}
		}

		private IntPtr MethodsPtr;

		private object MethodsStructure;

		private FFIMethods Methods
		{
			get
			{
				if (MethodsStructure == null)
				{
					MethodsStructure = Marshal.PtrToStructure(MethodsPtr, typeof(FFIMethods));
				}
				return (FFIMethods)MethodsStructure;
			}
		}

		public event LobbyUpdateHandler OnLobbyUpdate;

		public event LobbyDeleteHandler OnLobbyDelete;

		public event MemberConnectHandler OnMemberConnect;

		public event MemberUpdateHandler OnMemberUpdate;

		public event MemberDisconnectHandler OnMemberDisconnect;

		public event LobbyMessageHandler OnLobbyMessage;

		public event SpeakingHandler OnSpeaking;

		public event NetworkMessageHandler OnNetworkMessage;

		internal LobbyManager(IntPtr ptr, IntPtr eventsPtr, ref FFIEvents events)
		{
			if (eventsPtr == IntPtr.Zero)
			{
				throw new ResultException(Result.InternalError);
			}
			InitEvents(eventsPtr, ref events);
			MethodsPtr = ptr;
			if (MethodsPtr == IntPtr.Zero)
			{
				throw new ResultException(Result.InternalError);
			}
		}

		private void InitEvents(IntPtr eventsPtr, ref FFIEvents events)
		{
			events.OnLobbyUpdate = _003CInitEvents_003Eb__50_0;
			events.OnLobbyDelete = _003CInitEvents_003Eb__50_1;
			events.OnMemberConnect = _003CInitEvents_003Eb__50_2;
			events.OnMemberUpdate = _003CInitEvents_003Eb__50_3;
			events.OnMemberDisconnect = _003CInitEvents_003Eb__50_4;
			events.OnLobbyMessage = _003CInitEvents_003Eb__50_5;
			events.OnSpeaking = _003CInitEvents_003Eb__50_6;
			events.OnNetworkMessage = _003CInitEvents_003Eb__50_7;
			Marshal.StructureToPtr(events, eventsPtr, false);
		}

		public LobbyTransaction GetLobbyCreateTransaction()
		{
			LobbyTransaction result = default(LobbyTransaction);
			Result result2 = Methods.GetLobbyCreateTransaction(MethodsPtr, ref result.MethodsPtr);
			if (result2 != 0)
			{
				throw new ResultException(result2);
			}
			return result;
		}

		public LobbyTransaction GetLobbyUpdateTransaction(long lobbyId)
		{
			LobbyTransaction result = default(LobbyTransaction);
			Result result2 = Methods.GetLobbyUpdateTransaction(MethodsPtr, lobbyId, ref result.MethodsPtr);
			if (result2 != 0)
			{
				throw new ResultException(result2);
			}
			return result;
		}

		public LobbyMemberTransaction GetMemberUpdateTransaction(long lobbyId, long userId)
		{
			LobbyMemberTransaction result = default(LobbyMemberTransaction);
			Result result2 = Methods.GetMemberUpdateTransaction(MethodsPtr, lobbyId, userId, ref result.MethodsPtr);
			if (result2 != 0)
			{
				throw new ResultException(result2);
			}
			return result;
		}

		public void CreateLobby(LobbyTransaction transaction, CreateLobbyHandler callback)
		{
			_003C_003Ec__DisplayClass54_0 _003C_003Ec__DisplayClass54_ = new _003C_003Ec__DisplayClass54_0();
			_003C_003Ec__DisplayClass54_.callback = callback;
			FFIMethods.CreateLobbyCallback createLobbyCallback = _003C_003Ec__DisplayClass54_._003CCreateLobby_003Eb__0;
			Methods.CreateLobby(MethodsPtr, transaction.MethodsPtr, Utility.Retain(createLobbyCallback), createLobbyCallback);
			transaction.MethodsPtr = IntPtr.Zero;
		}

		public void UpdateLobby(long lobbyId, LobbyTransaction transaction, UpdateLobbyHandler callback)
		{
			_003C_003Ec__DisplayClass55_0 _003C_003Ec__DisplayClass55_ = new _003C_003Ec__DisplayClass55_0();
			_003C_003Ec__DisplayClass55_.callback = callback;
			FFIMethods.UpdateLobbyCallback updateLobbyCallback = _003C_003Ec__DisplayClass55_._003CUpdateLobby_003Eb__0;
			Methods.UpdateLobby(MethodsPtr, lobbyId, transaction.MethodsPtr, Utility.Retain(updateLobbyCallback), updateLobbyCallback);
			transaction.MethodsPtr = IntPtr.Zero;
		}

		public void DeleteLobby(long lobbyId, DeleteLobbyHandler callback)
		{
			_003C_003Ec__DisplayClass56_0 _003C_003Ec__DisplayClass56_ = new _003C_003Ec__DisplayClass56_0();
			_003C_003Ec__DisplayClass56_.callback = callback;
			FFIMethods.DeleteLobbyCallback deleteLobbyCallback = _003C_003Ec__DisplayClass56_._003CDeleteLobby_003Eb__0;
			Methods.DeleteLobby(MethodsPtr, lobbyId, Utility.Retain(deleteLobbyCallback), deleteLobbyCallback);
		}

		public void ConnectLobby(long lobbyId, string secret, ConnectLobbyHandler callback)
		{
			_003C_003Ec__DisplayClass57_0 _003C_003Ec__DisplayClass57_ = new _003C_003Ec__DisplayClass57_0();
			_003C_003Ec__DisplayClass57_.callback = callback;
			FFIMethods.ConnectLobbyCallback connectLobbyCallback = _003C_003Ec__DisplayClass57_._003CConnectLobby_003Eb__0;
			Methods.ConnectLobby(MethodsPtr, lobbyId, secret, Utility.Retain(connectLobbyCallback), connectLobbyCallback);
		}

		public void ConnectLobbyWithActivitySecret(string activitySecret, ConnectLobbyWithActivitySecretHandler callback)
		{
			_003C_003Ec__DisplayClass58_0 _003C_003Ec__DisplayClass58_ = new _003C_003Ec__DisplayClass58_0();
			_003C_003Ec__DisplayClass58_.callback = callback;
			FFIMethods.ConnectLobbyWithActivitySecretCallback connectLobbyWithActivitySecretCallback = _003C_003Ec__DisplayClass58_._003CConnectLobbyWithActivitySecret_003Eb__0;
			Methods.ConnectLobbyWithActivitySecret(MethodsPtr, activitySecret, Utility.Retain(connectLobbyWithActivitySecretCallback), connectLobbyWithActivitySecretCallback);
		}

		public void DisconnectLobby(long lobbyId, DisconnectLobbyHandler callback)
		{
			_003C_003Ec__DisplayClass59_0 _003C_003Ec__DisplayClass59_ = new _003C_003Ec__DisplayClass59_0();
			_003C_003Ec__DisplayClass59_.callback = callback;
			FFIMethods.DisconnectLobbyCallback disconnectLobbyCallback = _003C_003Ec__DisplayClass59_._003CDisconnectLobby_003Eb__0;
			Methods.DisconnectLobby(MethodsPtr, lobbyId, Utility.Retain(disconnectLobbyCallback), disconnectLobbyCallback);
		}

		public Lobby GetLobby(long lobbyId)
		{
			Lobby lobby = default(Lobby);
			Result result = Methods.GetLobby(MethodsPtr, lobbyId, ref lobby);
			if (result != 0)
			{
				throw new ResultException(result);
			}
			return lobby;
		}

		public string GetLobbyActivitySecret(long lobbyId)
		{
			StringBuilder stringBuilder = new StringBuilder(128);
			Result result = Methods.GetLobbyActivitySecret(MethodsPtr, lobbyId, stringBuilder);
			if (result != 0)
			{
				throw new ResultException(result);
			}
			return stringBuilder.ToString();
		}

		public string GetLobbyMetadataValue(long lobbyId, string key)
		{
			StringBuilder stringBuilder = new StringBuilder(4096);
			Result result = Methods.GetLobbyMetadataValue(MethodsPtr, lobbyId, key, stringBuilder);
			if (result != 0)
			{
				throw new ResultException(result);
			}
			return stringBuilder.ToString();
		}

		public string GetLobbyMetadataKey(long lobbyId, int index)
		{
			StringBuilder stringBuilder = new StringBuilder(256);
			Result result = Methods.GetLobbyMetadataKey(MethodsPtr, lobbyId, index, stringBuilder);
			if (result != 0)
			{
				throw new ResultException(result);
			}
			return stringBuilder.ToString();
		}

		public int LobbyMetadataCount(long lobbyId)
		{
			int count = 0;
			Result result = Methods.LobbyMetadataCount(MethodsPtr, lobbyId, ref count);
			if (result != 0)
			{
				throw new ResultException(result);
			}
			return count;
		}

		public int MemberCount(long lobbyId)
		{
			int count = 0;
			Result result = Methods.MemberCount(MethodsPtr, lobbyId, ref count);
			if (result != 0)
			{
				throw new ResultException(result);
			}
			return count;
		}

		public long GetMemberUserId(long lobbyId, int index)
		{
			long userId = 0L;
			Result result = Methods.GetMemberUserId(MethodsPtr, lobbyId, index, ref userId);
			if (result != 0)
			{
				throw new ResultException(result);
			}
			return userId;
		}

		public User GetMemberUser(long lobbyId, long userId)
		{
			User user = default(User);
			Result result = Methods.GetMemberUser(MethodsPtr, lobbyId, userId, ref user);
			if (result != 0)
			{
				throw new ResultException(result);
			}
			return user;
		}

		public string GetMemberMetadataValue(long lobbyId, long userId, string key)
		{
			StringBuilder stringBuilder = new StringBuilder(4096);
			Result result = Methods.GetMemberMetadataValue(MethodsPtr, lobbyId, userId, key, stringBuilder);
			if (result != 0)
			{
				throw new ResultException(result);
			}
			return stringBuilder.ToString();
		}

		public string GetMemberMetadataKey(long lobbyId, long userId, int index)
		{
			StringBuilder stringBuilder = new StringBuilder(256);
			Result result = Methods.GetMemberMetadataKey(MethodsPtr, lobbyId, userId, index, stringBuilder);
			if (result != 0)
			{
				throw new ResultException(result);
			}
			return stringBuilder.ToString();
		}

		public int MemberMetadataCount(long lobbyId, long userId)
		{
			int count = 0;
			Result result = Methods.MemberMetadataCount(MethodsPtr, lobbyId, userId, ref count);
			if (result != 0)
			{
				throw new ResultException(result);
			}
			return count;
		}

		public void UpdateMember(long lobbyId, long userId, LobbyMemberTransaction transaction, UpdateMemberHandler callback)
		{
			_003C_003Ec__DisplayClass71_0 _003C_003Ec__DisplayClass71_ = new _003C_003Ec__DisplayClass71_0();
			_003C_003Ec__DisplayClass71_.callback = callback;
			FFIMethods.UpdateMemberCallback updateMemberCallback = _003C_003Ec__DisplayClass71_._003CUpdateMember_003Eb__0;
			Methods.UpdateMember(MethodsPtr, lobbyId, userId, transaction.MethodsPtr, Utility.Retain(updateMemberCallback), updateMemberCallback);
			transaction.MethodsPtr = IntPtr.Zero;
		}

		public void SendLobbyMessage(long lobbyId, byte[] data, SendLobbyMessageHandler callback)
		{
			_003C_003Ec__DisplayClass72_0 _003C_003Ec__DisplayClass72_ = new _003C_003Ec__DisplayClass72_0();
			_003C_003Ec__DisplayClass72_.callback = callback;
			FFIMethods.SendLobbyMessageCallback sendLobbyMessageCallback = _003C_003Ec__DisplayClass72_._003CSendLobbyMessage_003Eb__0;
			Methods.SendLobbyMessage(MethodsPtr, lobbyId, data, data.Length, Utility.Retain(sendLobbyMessageCallback), sendLobbyMessageCallback);
		}

		public LobbySearchQuery GetSearchQuery()
		{
			LobbySearchQuery result = default(LobbySearchQuery);
			Result result2 = Methods.GetSearchQuery(MethodsPtr, ref result.MethodsPtr);
			if (result2 != 0)
			{
				throw new ResultException(result2);
			}
			return result;
		}

		public void Search(LobbySearchQuery query, SearchHandler callback)
		{
			_003C_003Ec__DisplayClass74_0 _003C_003Ec__DisplayClass74_ = new _003C_003Ec__DisplayClass74_0();
			_003C_003Ec__DisplayClass74_.callback = callback;
			FFIMethods.SearchCallback searchCallback = _003C_003Ec__DisplayClass74_._003CSearch_003Eb__0;
			Methods.Search(MethodsPtr, query.MethodsPtr, Utility.Retain(searchCallback), searchCallback);
			query.MethodsPtr = IntPtr.Zero;
		}

		public int LobbyCount()
		{
			int count = 0;
			Methods.LobbyCount(MethodsPtr, ref count);
			return count;
		}

		public long GetLobbyId(int index)
		{
			long lobbyId = 0L;
			Result result = Methods.GetLobbyId(MethodsPtr, index, ref lobbyId);
			if (result != 0)
			{
				throw new ResultException(result);
			}
			return lobbyId;
		}

		public void ConnectVoice(long lobbyId, ConnectVoiceHandler callback)
		{
			_003C_003Ec__DisplayClass77_0 _003C_003Ec__DisplayClass77_ = new _003C_003Ec__DisplayClass77_0();
			_003C_003Ec__DisplayClass77_.callback = callback;
			FFIMethods.ConnectVoiceCallback connectVoiceCallback = _003C_003Ec__DisplayClass77_._003CConnectVoice_003Eb__0;
			Methods.ConnectVoice(MethodsPtr, lobbyId, Utility.Retain(connectVoiceCallback), connectVoiceCallback);
		}

		public void DisconnectVoice(long lobbyId, DisconnectVoiceHandler callback)
		{
			_003C_003Ec__DisplayClass78_0 _003C_003Ec__DisplayClass78_ = new _003C_003Ec__DisplayClass78_0();
			_003C_003Ec__DisplayClass78_.callback = callback;
			FFIMethods.DisconnectVoiceCallback disconnectVoiceCallback = _003C_003Ec__DisplayClass78_._003CDisconnectVoice_003Eb__0;
			Methods.DisconnectVoice(MethodsPtr, lobbyId, Utility.Retain(disconnectVoiceCallback), disconnectVoiceCallback);
		}

		public void ConnectNetwork(long lobbyId)
		{
			Result result = Methods.ConnectNetwork(MethodsPtr, lobbyId);
			if (result != 0)
			{
				throw new ResultException(result);
			}
		}

		public void DisconnectNetwork(long lobbyId)
		{
			Result result = Methods.DisconnectNetwork(MethodsPtr, lobbyId);
			if (result != 0)
			{
				throw new ResultException(result);
			}
		}

		public void FlushNetwork()
		{
			Result result = Methods.FlushNetwork(MethodsPtr);
			if (result != 0)
			{
				throw new ResultException(result);
			}
		}

		public void OpenNetworkChannel(long lobbyId, byte channelId, bool reliable)
		{
			Result result = Methods.OpenNetworkChannel(MethodsPtr, lobbyId, channelId, reliable);
			if (result != 0)
			{
				throw new ResultException(result);
			}
		}

		public void SendNetworkMessage(long lobbyId, long userId, byte channelId, byte[] data)
		{
			Result result = Methods.SendNetworkMessage(MethodsPtr, lobbyId, userId, channelId, data, data.Length);
			if (result != 0)
			{
				throw new ResultException(result);
			}
		}

		public IEnumerable<User> GetMemberUsers(long lobbyID)
		{
			int num = MemberCount(lobbyID);
			List<User> list = new List<User>();
			for (int i = 0; i < num; i++)
			{
				list.Add(GetMemberUser(lobbyID, GetMemberUserId(lobbyID, i)));
			}
			return list;
		}

		public void SendLobbyMessage(long lobbyID, string data, SendLobbyMessageHandler handler)
		{
			SendLobbyMessage(lobbyID, Encoding.UTF8.GetBytes(data), handler);
		}

		[CompilerGenerated]
		private void _003CInitEvents_003Eb__50_0(IntPtr ptr, long lobbyId)
		{
			if (this.OnLobbyUpdate != null)
			{
				this.OnLobbyUpdate(lobbyId);
			}
		}

		[CompilerGenerated]
		private void _003CInitEvents_003Eb__50_1(IntPtr ptr, long lobbyId, uint reason)
		{
			if (this.OnLobbyDelete != null)
			{
				this.OnLobbyDelete(lobbyId, reason);
			}
		}

		[CompilerGenerated]
		private void _003CInitEvents_003Eb__50_2(IntPtr ptr, long lobbyId, long userId)
		{
			if (this.OnMemberConnect != null)
			{
				this.OnMemberConnect(lobbyId, userId);
			}
		}

		[CompilerGenerated]
		private void _003CInitEvents_003Eb__50_3(IntPtr ptr, long lobbyId, long userId)
		{
			if (this.OnMemberUpdate != null)
			{
				this.OnMemberUpdate(lobbyId, userId);
			}
		}

		[CompilerGenerated]
		private void _003CInitEvents_003Eb__50_4(IntPtr ptr, long lobbyId, long userId)
		{
			if (this.OnMemberDisconnect != null)
			{
				this.OnMemberDisconnect(lobbyId, userId);
			}
		}

		[CompilerGenerated]
		private void _003CInitEvents_003Eb__50_5(IntPtr ptr, long lobbyId, long userId, IntPtr dataPtr, int dataLen)
		{
			if (this.OnLobbyMessage != null)
			{
				byte[] array = new byte[dataLen];
				Marshal.Copy(dataPtr, array, 0, dataLen);
				this.OnLobbyMessage(lobbyId, userId, array);
			}
		}

		[CompilerGenerated]
		private void _003CInitEvents_003Eb__50_6(IntPtr ptr, long lobbyId, long userId, bool speaking)
		{
			if (this.OnSpeaking != null)
			{
				this.OnSpeaking(lobbyId, userId, speaking);
			}
		}

		[CompilerGenerated]
		private void _003CInitEvents_003Eb__50_7(IntPtr ptr, long lobbyId, long userId, byte channelId, IntPtr dataPtr, int dataLen)
		{
			if (this.OnNetworkMessage != null)
			{
				byte[] array = new byte[dataLen];
				Marshal.Copy(dataPtr, array, 0, dataLen);
				this.OnNetworkMessage(lobbyId, userId, channelId, array);
			}
		}
	}
}
