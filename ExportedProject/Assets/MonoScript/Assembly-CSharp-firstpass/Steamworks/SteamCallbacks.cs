using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using AOT;

namespace Steamworks
{
	public static class SteamCallbacks
	{
		public static class CallbackHub<T> where T : struct
		{
			public delegate void DispatchDelegate(T param);

			public delegate void APIDispatchDelegate(T param, bool bIOFailure);

			private static CCallbackBaseVTable m_CallbackBaseVTable;

			private static IntPtr m_pVTable;

			private static CCallbackBase m_CCallbackBase;

			private static GCHandle m_pCCallbackBase;

			private static Dictionary<ulong, APIDispatchDelegate> callResultFuncs;

			private static readonly int m_size;

			private static int CallbackCount
			{
				get
				{
					return default(int);
				}
			}

			public static void RegisterCallback(DispatchDelegate func)
			{
			}

			public static void UnregisterCallback(DispatchDelegate func)
			{
			}

			public static void RegisterCallResult(APIDispatchDelegate func, SteamAPICall_t hApiCall)
			{
			}

			public static void UnregisterCallResult(SteamAPICall_t hApiCall)
			{
			}

			private static void BuildCCallbackBase()
			{
			}

			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}

			private static event DispatchDelegate m_Func;
		}

		internal static class VTables
		{
			internal static readonly Dictionary<Type, CCallbackBaseVTable> vtables;
		}

		public static class SteamAppInstalled_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class SteamAppUninstalled_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class DlcInstalled_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class RegisterActivationCodeResponse_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class NewLaunchQueryParameters_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class AppProofOfPurchaseKeyResponse_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class FileDetailsResult_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class PersonaStateChange_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class GameOverlayActivated_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class GameServerChangeRequested_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class GameLobbyJoinRequested_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class AvatarImageLoaded_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class ClanOfficerListResponse_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class FriendRichPresenceUpdate_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class GameRichPresenceJoinRequested_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class GameConnectedClanChatMsg_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class GameConnectedChatJoin_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class GameConnectedChatLeave_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class DownloadClanActivityCountsResult_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class JoinClanChatRoomCompletionResult_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class GameConnectedFriendChatMsg_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class FriendsGetFollowerCount_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class FriendsIsFollowing_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class FriendsEnumerateFollowingList_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class SetPersonaNameResponse_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class GCMessageAvailable_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class GCMessageFailed_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class GSClientApprove_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class GSClientDeny_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class GSClientKick_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class GSClientAchievementStatus_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class GSPolicyResponse_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class GSGameplayStats_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class GSClientGroupStatus_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class GSReputation_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class AssociateWithClanResult_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class ComputeNewPlayerCompatibilityResult_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class GSStatsReceived_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class GSStatsStored_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class GSStatsUnloaded_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class HTML_BrowserReady_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class HTML_NeedsPaint_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class HTML_StartRequest_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class HTML_CloseBrowser_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class HTML_URLChanged_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class HTML_FinishedRequest_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class HTML_OpenLinkInNewTab_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class HTML_ChangedTitle_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class HTML_SearchResults_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class HTML_CanGoBackAndForward_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class HTML_HorizontalScroll_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class HTML_VerticalScroll_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class HTML_LinkAtPosition_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class HTML_JSAlert_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class HTML_JSConfirm_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class HTML_FileOpenDialog_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class HTML_NewWindow_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class HTML_SetCursor_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class HTML_StatusText_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class HTML_ShowToolTip_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class HTML_UpdateToolTip_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class HTML_HideToolTip_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class HTTPRequestCompleted_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class HTTPRequestHeadersReceived_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class HTTPRequestDataReceived_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class SteamInventoryResultReady_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class SteamInventoryFullUpdate_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCBDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class SteamInventoryDefinitionUpdate_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.GetCallbackSizeBytesDel))]
			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class SteamInventoryEligiblePromoItemDefIDs_t
		{
			[MonoPInvokeCallback(typeof(CCallbackBaseVTable.RunCRDel))]
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class FavoritesListChanged_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class LobbyInvite_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class LobbyEnter_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class LobbyDataUpdate_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class LobbyChatUpdate_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class LobbyChatMsg_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class LobbyGameCreated_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class LobbyMatchList_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class LobbyKicked_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class LobbyCreated_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class FavoritesListAccountsUpdated_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class PlaybackStatusHasChanged_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class VolumeHasChanged_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class MusicPlayerRemoteWillActivate_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class MusicPlayerRemoteWillDeactivate_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class MusicPlayerRemoteToFront_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class MusicPlayerWillQuit_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class MusicPlayerWantsPlay_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class MusicPlayerWantsPause_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class MusicPlayerWantsPlayPrevious_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class MusicPlayerWantsPlayNext_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class MusicPlayerWantsShuffled_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class MusicPlayerWantsLooped_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class MusicPlayerWantsVolume_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class MusicPlayerSelectsQueueEntry_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class MusicPlayerSelectsPlaylistEntry_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class MusicPlayerWantsPlayingRepeatStatus_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class P2PSessionRequest_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class P2PSessionConnectFail_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class SocketStatusCallback_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class RemoteStorageAppSyncedClient_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class RemoteStorageAppSyncedServer_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class RemoteStorageAppSyncProgress_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class RemoteStorageAppSyncStatusCheck_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class RemoteStorageFileShareResult_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class RemoteStoragePublishFileResult_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class RemoteStorageDeletePublishedFileResult_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class RemoteStorageEnumerateUserPublishedFilesResult_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class RemoteStorageSubscribePublishedFileResult_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class RemoteStorageEnumerateUserSubscribedFilesResult_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class RemoteStorageUnsubscribePublishedFileResult_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class RemoteStorageUpdatePublishedFileResult_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class RemoteStorageDownloadUGCResult_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class RemoteStorageGetPublishedFileDetailsResult_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class RemoteStorageEnumerateWorkshopFilesResult_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class RemoteStorageGetPublishedItemVoteDetailsResult_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class RemoteStoragePublishedFileSubscribed_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class RemoteStoragePublishedFileUnsubscribed_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class RemoteStoragePublishedFileDeleted_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class RemoteStorageUpdateUserPublishedItemVoteResult_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class RemoteStorageUserVoteDetails_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class RemoteStorageEnumerateUserSharedWorkshopFilesResult_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class RemoteStorageSetUserPublishedFileActionResult_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class RemoteStorageEnumeratePublishedFilesByUserActionResult_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class RemoteStoragePublishFileProgress_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class RemoteStoragePublishedFileUpdated_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class RemoteStorageFileWriteAsyncComplete_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class RemoteStorageFileReadAsyncComplete_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class ScreenshotReady_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class ScreenshotRequested_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class SteamUGCQueryCompleted_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class SteamUGCRequestUGCDetailsResult_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class CreateItemResult_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class SubmitItemUpdateResult_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class ItemInstalled_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class DownloadItemResult_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class UserFavoriteItemsListChanged_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class SetUserItemVoteResult_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class GetUserItemVoteResult_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class StartPlaytimeTrackingResult_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class StopPlaytimeTrackingResult_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class SteamServersConnected_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class SteamServerConnectFailure_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class SteamServersDisconnected_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class ClientGameServerDeny_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class IPCFailure_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class LicensesUpdated_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class ValidateAuthTicketResponse_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class MicroTxnAuthorizationResponse_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class EncryptedAppTicketResponse_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class GetAuthSessionTicketResponse_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class GameWebCallback_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class StoreAuthURLResponse_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class UserStatsReceived_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class UserStatsStored_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class UserAchievementStored_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class LeaderboardFindResult_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class LeaderboardScoresDownloaded_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class LeaderboardScoreUploaded_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class NumberOfCurrentPlayers_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class UserStatsUnloaded_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class UserAchievementIconFetched_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class GlobalAchievementPercentagesReady_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class LeaderboardUGCSet_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class GlobalStatsReceived_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class IPCountry_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class LowBatteryPower_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class SteamAPICallCompleted_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class SteamShutdown_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class CheckFileSignature_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class GamepadTextInputDismissed_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class BroadcastUploadStart_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class BroadcastUploadStop_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}

		public static class GetVideoURLResult_t
		{
			public static void OnRunCallback(IntPtr pvParam)
			{
			}

			public static void OnRunCallResult(IntPtr pvParam, bool bFailed, ulong hSteamAPICall)
			{
			}

			public static int OnGetCallbackSizeBytes()
			{
				return default(int);
			}
		}
	}
}
