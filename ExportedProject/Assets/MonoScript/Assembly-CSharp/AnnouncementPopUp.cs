using System;
using System.Collections;
using System.Net;
using Hazel;
using Hazel.Udp;
using UnityEngine;

public class AnnouncementPopUp : MonoBehaviour
{
	private enum AnnounceState
	{
		Fetching = 0,
		Failed = 1,
		Success = 2,
		Cached = 3
	}

	private const uint AnnouncementVersion = 2u;

	private UdpClientConnection connection;

	private AnnounceState AskedForUpdate;

	public TextRenderer AnnounceText;

	private Announcement announcementUpdate;

	private static bool IsSuccess(AnnounceState state)
	{
		if (state != AnnounceState.Success)
		{
			return state == AnnounceState.Cached;
		}
		return true;
	}

	public IEnumerator Init()
	{
		if (AskedForUpdate != 0)
		{
			yield break;
		}
		yield return DestroyableSingleton<ServerManager>.Instance.WaitForServers();
		Debug.Log("Requesting announcement");
		connection = new UdpClientConnection(new IPEndPoint(IPAddress.Parse(DestroyableSingleton<ServerManager>.Instance.OnlineNetAddress), 22024));
		connection.ResendTimeout = 1000;
		connection.ResendPingMultiplier = 1f;
		connection.DataReceived += Connection_DataReceived;
		connection.Disconnected += Connection_Disconnected;
		try
		{
			Announcement lastAnnouncement = SaveManager.LastAnnouncement;
			MessageWriter messageWriter = MessageWriter.Get();
			messageWriter.WritePacked(2u);
			messageWriter.WritePacked(lastAnnouncement.Id);
			messageWriter.WritePacked(SaveManager.LastLanguage);
			connection.ConnectAsync(messageWriter.ToByteArray(true));
			messageWriter.Recycle();
		}
		catch
		{
			AskedForUpdate = AnnounceState.Failed;
		}
	}

	private void Connection_Disconnected(object sender, DisconnectedEventArgs e)
	{
		AskedForUpdate = AnnounceState.Failed;
		connection.Dispose();
		connection = null;
	}

	private void Connection_DataReceived(DataReceivedEventArgs e)
	{
		MessageReader message = e.Message;
		try
		{
			while (message.Position < message.Length)
			{
				MessageReader messageReader = message.ReadMessage();
				switch (messageReader.Tag)
				{
				case 0:
					AskedForUpdate = AnnounceState.Cached;
					break;
				case 1:
					announcementUpdate = default(Announcement);
					announcementUpdate.DateFetched = DateTime.UtcNow;
					announcementUpdate.Id = messageReader.ReadPackedUInt32();
					announcementUpdate.AnnounceText = messageReader.ReadString();
					AskedForUpdate = ((announcementUpdate.Id == 0) ? AnnounceState.Cached : AnnounceState.Success);
					break;
				case 2:
					SaveManager.IsFreeWeekend = (FreeWeekendState)messageReader.ReadByte();
					break;
				}
			}
		}
		finally
		{
			message.Recycle();
		}
		try
		{
			connection.Dispose();
			connection = null;
		}
		catch
		{
		}
	}

	public IEnumerator Show()
	{
		float timer = 0f;
		while (AskedForUpdate == AnnounceState.Fetching && timer < 6f)
		{
			timer += Time.deltaTime;
			yield return null;
		}
		if (!IsSuccess(AskedForUpdate))
		{
			Announcement lastAnnouncement = SaveManager.LastAnnouncement;
			if (lastAnnouncement.Id == 0)
			{
				AnnounceText.Text = "Couldn't get announcement.";
			}
			else
			{
				AnnounceText.Text = "Couldn't get announcement. Last Known:\r\n" + lastAnnouncement.AnnounceText;
			}
		}
		else if (announcementUpdate.Id != SaveManager.LastAnnouncement.Id)
		{
			if (AskedForUpdate != AnnounceState.Cached)
			{
				base.gameObject.SetActive(true);
			}
			if (announcementUpdate.Id == 0)
			{
				announcementUpdate = SaveManager.LastAnnouncement;
				announcementUpdate.DateFetched = DateTime.UtcNow;
			}
			SaveManager.LastAnnouncement = announcementUpdate;
			AnnounceText.Text = announcementUpdate.AnnounceText;
		}
		while (base.gameObject.activeSelf)
		{
			yield return null;
		}
	}

	public void Close()
	{
		base.gameObject.SetActive(false);
	}

	private void OnDestroy()
	{
		if (connection != null)
		{
			connection.Dispose();
			connection = null;
		}
	}
}
