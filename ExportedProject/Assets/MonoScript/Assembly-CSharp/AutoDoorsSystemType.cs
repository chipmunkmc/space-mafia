using System;
using System.Linq;
using System.Runtime.CompilerServices;
using Hazel;

public class AutoDoorsSystemType : ISystemType, IActivatable, ISystemTimer, IDoorSystem
{
	[Serializable]
	[CompilerGenerated]
	private sealed class _003C_003Ec
	{
		public static readonly _003C_003Ec _003C_003E9 = new _003C_003Ec();

		public static Func<PlainDoor, bool> _003C_003E9__2_0;

		internal bool _003Cget_IsActive_003Eb__2_0(PlainDoor b)
		{
			return !b.Open;
		}
	}

	private uint dirtyBits;

	public bool IsActive
	{
		get
		{
			return ShipStatus.Instance.AllDoors.Any(_003C_003Ec._003C_003E9__2_0 ?? (_003C_003Ec._003C_003E9__2_0 = _003C_003Ec._003C_003E9._003Cget_IsActive_003Eb__2_0));
		}
	}

	public bool Detoriorate(float deltaTime)
	{
		for (int i = 0; i < ShipStatus.Instance.AllDoors.Length; i++)
		{
			if (ShipStatus.Instance.AllDoors[i].DoUpdate(deltaTime))
			{
				dirtyBits |= (uint)(1 << i);
			}
		}
		return dirtyBits != 0;
	}

	public void RepairDamage(PlayerControl player, byte amount)
	{
	}

	public void Serialize(MessageWriter writer, bool initialState)
	{
		if (initialState)
		{
			for (int i = 0; i < ShipStatus.Instance.AllDoors.Length; i++)
			{
				ShipStatus.Instance.AllDoors[i].Serialize(writer);
			}
			return;
		}
		writer.WritePacked(dirtyBits);
		for (int j = 0; j < ShipStatus.Instance.AllDoors.Length; j++)
		{
			if ((dirtyBits & (uint)(1 << j)) != 0)
			{
				ShipStatus.Instance.AllDoors[j].Serialize(writer);
			}
		}
		dirtyBits = 0u;
	}

	public void Deserialize(MessageReader reader, bool initialState)
	{
		if (initialState)
		{
			for (int i = 0; i < ShipStatus.Instance.AllDoors.Length; i++)
			{
				ShipStatus.Instance.AllDoors[i].Deserialize(reader);
			}
			return;
		}
		uint num = reader.ReadPackedUInt32();
		for (int j = 0; j < ShipStatus.Instance.AllDoors.Length; j++)
		{
			if ((num & (uint)(1 << j)) != 0)
			{
				ShipStatus.Instance.AllDoors[j].Deserialize(reader);
			}
		}
	}

	public void SetDoor(AutoOpenDoor door, bool open)
	{
		door.SetDoorway(open);
		dirtyBits |= (uint)(1 << ShipStatus.Instance.AllDoors.IndexOf(door));
	}

	public void CloseDoorsOfType(SystemTypes room)
	{
		for (int i = 0; i < ShipStatus.Instance.AllDoors.Length; i++)
		{
			PlainDoor plainDoor = ShipStatus.Instance.AllDoors[i];
			if (plainDoor.Room == room)
			{
				plainDoor.SetDoorway(false);
				dirtyBits |= (uint)(1 << i);
			}
		}
	}

	public float GetTimer(SystemTypes room)
	{
		for (int i = 0; i < ShipStatus.Instance.AllDoors.Length; i++)
		{
			PlainDoor plainDoor = ShipStatus.Instance.AllDoors[i];
			if (plainDoor.Room == room)
			{
				return ((AutoOpenDoor)plainDoor).CooldownTimer;
			}
		}
		return 0f;
	}
}
