using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using InnerNet;
using UnityEngine;

public class BanMenu : MonoBehaviour
{
	[CompilerGenerated]
	private sealed class _003C_003Ec__DisplayClass8_0
	{
		public ClientData client;

		internal bool _003CUpdate_003Eb__1(BanButton b)
		{
			return b.TargetClientId == client.Id;
		}
	}

	[Serializable]
	[CompilerGenerated]
	private sealed class _003C_003Ec
	{
		public static readonly _003C_003Ec _003C_003E9 = new _003C_003Ec();

		public static Func<int, bool> _003C_003E9__8_0;

		internal bool _003CUpdate_003Eb__8_0(int c)
		{
			return c != 0;
		}
	}

	public BanButton BanButtonPrefab;

	public SpriteRenderer Background;

	public SpriteRenderer BanButton;

	public SpriteRenderer KickButton;

	public GameObject ContentParent;

	public int selected = -1;

	[HideInInspector]
	public List<BanButton> allButtons = new List<BanButton>();

	public void SetVisible(bool show)
	{
		show &= (bool)PlayerControl.LocalPlayer && PlayerControl.LocalPlayer.Data != null && !PlayerControl.LocalPlayer.Data.IsDead;
		show &= AmongUsClient.Instance.CanKick();
		show &= (bool)MeetingHud.Instance || !ShipStatus.Instance;
		BanButton.gameObject.SetActive(AmongUsClient.Instance.CanBan());
		KickButton.gameObject.SetActive(AmongUsClient.Instance.CanKick());
		GetComponent<SpriteRenderer>().enabled = show;
		GetComponent<PassiveButton>().enabled = show;
	}

	private void Update()
	{
		if (!AmongUsClient.Instance)
		{
			return;
		}
		for (int i = 0; i < AmongUsClient.Instance.allClients.Count; i++)
		{
			try
			{
				_003C_003Ec__DisplayClass8_0 _003C_003Ec__DisplayClass8_ = new _003C_003Ec__DisplayClass8_0();
				_003C_003Ec__DisplayClass8_.client = AmongUsClient.Instance.allClients[i];
				if (_003C_003Ec__DisplayClass8_.client == null)
				{
					break;
				}
				int[] value;
				if (VoteBanSystem.Instance.HasMyVote(_003C_003Ec__DisplayClass8_.client.Id) && VoteBanSystem.Instance.Votes.TryGetValue(_003C_003Ec__DisplayClass8_.client.Id, out value))
				{
					int num = value.Count(_003C_003Ec._003C_003E9__8_0 ?? (_003C_003Ec._003C_003E9__8_0 = _003C_003Ec._003C_003E9._003CUpdate_003Eb__8_0));
					BanButton banButton = allButtons.FirstOrDefault(_003C_003Ec__DisplayClass8_._003CUpdate_003Eb__1);
					if ((bool)banButton && banButton.numVotes != num)
					{
						banButton.SetVotes(num);
					}
				}
			}
			catch
			{
				break;
			}
		}
	}

	public void Show()
	{
		if (ContentParent.activeSelf)
		{
			Hide();
			return;
		}
		selected = -1;
		KickButton.color = Color.gray;
		BanButton.color = Color.gray;
		ContentParent.SetActive(true);
		int num = 0;
		if ((bool)AmongUsClient.Instance)
		{
			List<ClientData> allClients = AmongUsClient.Instance.allClients;
			for (int i = 0; i < allClients.Count; i++)
			{
				ClientData clientData = allClients[i];
				if (clientData.Id != AmongUsClient.Instance.ClientId && (bool)clientData.Character)
				{
					GameData.PlayerInfo data = clientData.Character.Data;
					if (!string.IsNullOrWhiteSpace(data.PlayerName))
					{
						BanButton banButton = UnityEngine.Object.Instantiate(BanButtonPrefab, ContentParent.transform);
						banButton.transform.localPosition = new Vector3(-0.2f, -0.15f - 0.4f * (float)num, -1f);
						banButton.Parent = this;
						banButton.NameText.Text = data.PlayerName;
						banButton.TargetClientId = clientData.Id;
						banButton.Unselect();
						allButtons.Add(banButton);
						num++;
					}
				}
			}
		}
		KickButton.transform.localPosition = new Vector3(-0.8f, -0.15f - 0.4f * (float)num - 0.1f, -1f);
		BanButton.transform.localPosition = new Vector3(0.3f, -0.15f - 0.4f * (float)num - 0.1f, -1f);
		float num2 = 0.3f + (float)(num + 1) * 0.4f;
		Background.size = new Vector2(3f, num2);
		Background.GetComponent<BoxCollider2D>().size = new Vector2(3f, num2);
		Background.transform.localPosition = new Vector3(0f, (0f - num2) / 2f + 0.15f, 0.1f);
	}

	public void Hide()
	{
		selected = -1;
		ContentParent.SetActive(false);
		for (int i = 0; i < allButtons.Count; i++)
		{
			UnityEngine.Object.Destroy(allButtons[i].gameObject);
		}
		allButtons.Clear();
	}

	public void Select(int client)
	{
		if (VoteBanSystem.Instance.HasMyVote(client))
		{
			return;
		}
		selected = client;
		for (int i = 0; i < allButtons.Count; i++)
		{
			BanButton banButton = allButtons[i];
			if (banButton.TargetClientId != client)
			{
				banButton.Unselect();
			}
		}
		KickButton.color = Color.white;
		BanButton.color = Color.white;
	}

	public void Kick(bool ban)
	{
		if (selected >= 0)
		{
			if (AmongUsClient.Instance.CanBan())
			{
				AmongUsClient.Instance.KickPlayer(selected, ban);
				Hide();
			}
			else
			{
				VoteBanSystem.Instance.CmdAddVote(selected);
			}
		}
		Select(-1);
	}
}
