using System.Collections;
using System.Runtime.CompilerServices;
using QRCoder;
using QRCoder.Unity;
using UnityEngine;

public class BoardPassGame : Minigame
{
	public SpriteRenderer renderer;

	public SpriteRenderer pass;

	public Sprite passBack;

	public SpriteRenderer ImageBg;

	public SpriteRenderer Image;

	public Sprite[] Photos;

	public PassiveButton pullButton;

	public PassiveButton flipButton;

	public SpriteRenderer Scanner;

	public Sprite ScannerAccept;

	public Sprite ScannerScanning;

	public Sprite ScannerWaiting;

	public Collider2D Sensor;

	public Collider2D BarCode;

	private float timer;

	public AudioClip slideinSound;

	public AudioClip flipSound;

	public AudioClip scanSound;

	private Coroutine blinky;

	private bool grabbed;

	public void Start()
	{
		Texture2D graphic = new UnityQRCode(new QRCodeGenerator().CreateQrCode("Yo holmes, smell you later", QRCodeGenerator.ECCLevel.M)).GetGraphic(1);
		renderer.sprite = Sprite.Create(graphic, new Rect(0f, 0f, graphic.width, graphic.height), Vector2.one / 2f);
		Image.sprite = Photos[(int)PlayerControl.LocalPlayer.PlayerId % Photos.Length];
		PlayerControl.LocalPlayer.SetPlayerMaterialColors(Image);
	}

	public void Update()
	{
		if (grabbed)
		{
			Vector3 localPosition = (Vector3)DestroyableSingleton<PassiveButtonManager>.Instance.controller.DragPosition - base.transform.position;
			localPosition.z = -1f;
			pass.transform.localPosition = localPosition;
		}
		if (flipButton.isActiveAndEnabled || pullButton.isActiveAndEnabled || MyNormTask.IsComplete)
		{
			return;
		}
		if (Sensor.IsTouching(BarCode))
		{
			if (blinky == null)
			{
				blinky = StartCoroutine(CoRunBlinky());
			}
		}
		else if (blinky != null)
		{
			StopAllCoroutines();
			Scanner.sprite = ScannerWaiting;
		}
	}

	private IEnumerator CoRunBlinky()
	{
		int i = 0;
		while (i < 3)
		{
			Scanner.sprite = ScannerAccept;
			yield return Effects.Wait(0.1f);
			Scanner.sprite = ScannerScanning;
			yield return Effects.Wait(0.2f);
			int num = i + 1;
			i = num;
		}
		if (Constants.ShouldPlaySfx())
		{
			SoundManager.Instance.PlaySound(scanSound, false);
		}
		blinky = null;
		Scanner.sprite = ScannerAccept;
		MyNormTask.NextStep();
		yield return CoStartClose();
	}

	public void PullPass()
	{
		StartCoroutine(CoPullPass());
	}

	private IEnumerator CoPullPass()
	{
		pullButton.gameObject.SetActive(false);
		if (Constants.ShouldPlaySfx())
		{
			SoundManager.Instance.PlaySound(slideinSound, false);
		}
		yield return Effects.Slide2D(pass.transform, new Vector2(-10f, 0f), new Vector2(-1.4f, 0f), 0.3f);
		flipButton.gameObject.SetActive(true);
	}

	public void Grab()
	{
		if (!flipButton.isActiveAndEnabled && !pullButton.isActiveAndEnabled)
		{
			grabbed = !grabbed;
		}
	}

	public void FlipPass()
	{
		StartCoroutine(CoFlipPass());
	}

	private IEnumerator CoFlipPass()
	{
		flipButton.gameObject.SetActive(false);
		if (Constants.ShouldPlaySfx())
		{
			SoundManager.Instance.PlaySound(flipSound, false);
		}
		yield return Effects.Lerp(0.2f, _003CCoFlipPass_003Eb__27_0);
		pass.sprite = passBack;
		renderer.gameObject.SetActive(false);
		ImageBg.gameObject.SetActive(false);
		yield return Effects.Lerp(0.2f, _003CCoFlipPass_003Eb__27_1);
	}

	[CompilerGenerated]
	private void _003CCoFlipPass_003Eb__27_0(float t)
	{
		pass.transform.localEulerAngles = new Vector3(0f, Mathf.Lerp(0f, 90f, t), 0f);
	}

	[CompilerGenerated]
	private void _003CCoFlipPass_003Eb__27_1(float t)
	{
		pass.transform.localEulerAngles = new Vector3(0f, Mathf.Lerp(90f, 180f, t), 0f);
	}
}
