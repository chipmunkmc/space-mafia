using UnityEngine;

internal class ChatBubble : PoolableBehavior
{
	public SpriteRenderer ChatFace;

	public SpriteRenderer Xmark;

	public SpriteRenderer votedMark;

	public TextRenderer NameText;

	public TextRenderer TextArea;

	public SpriteRenderer Background;

	public void SetLeft()
	{
		base.transform.localPosition = new Vector3(-3f, 0f, 0f);
		ChatFace.flipX = false;
		ChatFace.transform.localScale = new Vector3(1f, 1f, 1f);
		ChatFace.transform.localPosition = new Vector3(0f, 0.07f, 0f);
		Xmark.transform.localPosition = new Vector3(-0.15f, -0.13f, -0.0001f);
		votedMark.transform.localPosition = new Vector3(-0.15f, -0.13f, -0.0001f);
		NameText.transform.localPosition = new Vector3(0.5f, 0.34f, 0f);
		NameText.RightAligned = false;
		TextArea.transform.localPosition = new Vector3(0.5f, 0.09f, 0f);
		TextArea.RightAligned = false;
	}

	public void SetNotification()
	{
		base.transform.localPosition = new Vector3(-2.75f, 0f, 0f);
		ChatFace.flipX = false;
		ChatFace.transform.localScale = new Vector3(0.75f, 0.75f, 1f);
		ChatFace.transform.localPosition = new Vector3(0f, 0.18f, 0f);
		Xmark.transform.localPosition = new Vector3(-0.15f, -0.13f, -0.0001f);
		votedMark.transform.localPosition = new Vector3(-0.15f, -0.13f, -0.0001f);
		NameText.transform.localPosition = new Vector3(0.5f, 0.34f, 0f);
		NameText.RightAligned = false;
		TextArea.transform.localPosition = new Vector3(0.5f, 0.09f, 0f);
		TextArea.RightAligned = false;
		TextArea.Text = string.Empty;
	}

	public void SetRight()
	{
		base.transform.localPosition = new Vector3(-2.35f, 0f, 0f);
		ChatFace.flipX = true;
		ChatFace.transform.localScale = new Vector3(1f, 1f, 1f);
		ChatFace.transform.localPosition = new Vector3(4.75f, 0.07f, 0f);
		Xmark.transform.localPosition = new Vector3(0.15f, -0.13f, -0.0001f);
		votedMark.transform.localPosition = new Vector3(0.15f, -0.13f, -0.0001f);
		NameText.transform.localPosition = new Vector3(4.35f, 0.34f, 0f);
		NameText.RightAligned = true;
		TextArea.transform.localPosition = new Vector3(4.35f, 0.09f, 0f);
		TextArea.RightAligned = true;
	}

	public void SetName(string playerName, bool isDead, bool voted, Color color)
	{
		NameText.Text = playerName ?? "...";
		NameText.Color = color;
		NameText.RefreshMesh();
		if (isDead)
		{
			Xmark.enabled = true;
			Background.color = Palette.HalfWhite;
		}
		if (voted)
		{
			votedMark.enabled = true;
		}
	}

	public override void Reset()
	{
		Xmark.enabled = false;
		votedMark.enabled = false;
		Background.color = Color.white;
	}
}
