using System;
using UnityEngine;

public class DateHide : MonoBehaviour
{
	public int MonthStart;

	public int DayStart;

	public int MonthEnd;

	public int DayEnd;

	private void Awake()
	{
		try
		{
			DateTime utcNow = DateTime.UtcNow;
			DateTime dateTime = new DateTime(utcNow.Year, MonthStart, DayStart);
			DateTime dateTime2 = new DateTime(utcNow.Year, MonthEnd, DayEnd);
			if (dateTime <= utcNow && utcNow <= dateTime2)
			{
				return;
			}
		}
		catch
		{
		}
		base.gameObject.SetActive(false);
	}
}
