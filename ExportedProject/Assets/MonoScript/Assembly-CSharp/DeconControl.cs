using UnityEngine;
using UnityEngine.UI;

public class DeconControl : MonoBehaviour, IUsable
{
	public DeconSystem System;

	public float usableDistance = 1f;

	public SpriteRenderer Image;

	public AudioClip UseSound;

	public Button.ButtonClickedEvent OnUse;

	private const float CooldownDuration = 6f;

	private float cooldown;

	public float UsableDistance
	{
		get
		{
			return usableDistance;
		}
	}

	public float PercentCool
	{
		get
		{
			return cooldown / 6f;
		}
	}

	public void SetOutline(bool on, bool mainTarget)
	{
		if ((bool)Image)
		{
			Image.material.SetFloat("_Outline", on ? 1 : 0);
			Image.material.SetColor("_OutlineColor", Color.white);
			Image.material.SetColor("_AddColor", mainTarget ? Color.white : Color.clear);
		}
	}

	public void Update()
	{
		cooldown = Mathf.Max(cooldown - Time.deltaTime, 0f);
	}

	public float CanUse(GameData.PlayerInfo pc, out bool canUse, out bool couldUse)
	{
		if (System.CurState != 0)
		{
			canUse = false;
			couldUse = false;
			return 0f;
		}
		float num = float.MaxValue;
		PlayerControl @object = pc.Object;
		Vector2 truePosition = @object.GetTruePosition();
		Vector3 position = base.transform.position;
		position.y -= 0.1f;
		couldUse = @object.CanMove && !pc.IsDead && !PhysicsHelpers.AnythingBetween(truePosition, position, Constants.ShipAndObjectsMask, false);
		canUse = couldUse && cooldown == 0f;
		if (canUse)
		{
			num = Vector2.Distance(truePosition, position);
			canUse &= num <= UsableDistance;
		}
		return num;
	}

	public void Use()
	{
		bool canUse;
		bool couldUse;
		CanUse(PlayerControl.LocalPlayer.Data, out canUse, out couldUse);
		if (canUse)
		{
			cooldown = 6f;
			if (Constants.ShouldPlaySfx())
			{
				SoundManager.Instance.PlaySound(UseSound, false);
			}
			OnUse.Invoke();
		}
	}
}
