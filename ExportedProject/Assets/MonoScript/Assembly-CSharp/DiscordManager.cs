using System;
using System.Collections;
using System.Runtime.CompilerServices;
using Discord;
using InnerNet;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DiscordManager : DestroyableSingleton<DiscordManager>
{
	[Serializable]
	[CompilerGenerated]
	private sealed class _003C_003Ec
	{
		public static readonly _003C_003Ec _003C_003E9 = new _003C_003Ec();

		public static ActivityManager.UpdateActivityHandler _003C_003E9__7_0;

		public static ActivityManager.UpdateActivityHandler _003C_003E9__8_0;

		public static ActivityManager.UpdateActivityHandler _003C_003E9__9_0;

		public static ActivityManager.UpdateActivityHandler _003C_003E9__10_0;

		public static ActivityManager.ClearActivityHandler _003C_003E9__11_0;

		public static ActivityManager.UpdateActivityHandler _003C_003E9__12_0;

		public static ActivityManager.SendRequestReplyHandler _003C_003E9__16_0;

		public static ActivityManager.SendRequestReplyHandler _003C_003E9__17_0;

		internal void _003CSetInMenus_003Eb__7_0(Result r)
		{
		}

		internal void _003CSetPlayingGame_003Eb__8_0(Result r)
		{
		}

		internal void _003CSetHowToPlay_003Eb__9_0(Result r)
		{
		}

		internal void _003CSetInLobbyClient_003Eb__10_0(Result r)
		{
		}

		internal void _003CClearPresence_003Eb__11_0(Result r)
		{
		}

		internal void _003CSetInLobbyHost_003Eb__12_0(Result r)
		{
		}

		internal void _003CRequestRespondYes_003Eb__16_0(Result r)
		{
		}

		internal void _003CRequestRespondNo_003Eb__17_0(Result r)
		{
		}
	}

	private const long ClientId = 477175586805252107L;

	[NonSerialized]
	private global::Discord.Discord presence;

	private DateTime? StartTime;

	private static readonly DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

	public void Start()
	{
		if (DestroyableSingleton<DiscordManager>.Instance == this)
		{
			try
			{
				presence = new global::Discord.Discord(477175586805252107L, 1uL);
				ActivityManager activityManager = presence.GetActivityManager();
				activityManager.OnActivityJoinRequest += HandleAutoJoin;
				activityManager.OnActivityJoin += HandleJoinRequest;
				SetInMenus();
				SceneManager.sceneLoaded += _003CStart_003Eb__3_0;
			}
			catch
			{
			}
		}
	}

	private void HandleError(int errorCode, string message)
	{
		Debug.LogError(message ?? string.Format("No message: {0}", errorCode));
	}

	private void OnSceneChange(string name)
	{
		switch (name)
		{
		case "MatchMaking":
		case "MMOnline":
		case "MainMenu":
			SetInMenus();
			break;
		}
	}

	public void FixedUpdate()
	{
		if (presence == null)
		{
			return;
		}
		try
		{
			presence.RunCallbacks();
		}
		catch (ResultException)
		{
			presence.Dispose();
			presence = null;
		}
	}

	public void SetInMenus()
	{
		if (presence != null)
		{
			ClearPresence();
			StartTime = null;
			Activity activity = default(Activity);
			activity.State = "In Menus";
			activity.Assets.LargeImage = "icon";
			presence.GetActivityManager().UpdateActivity(activity, _003C_003Ec._003C_003E9__7_0 ?? (_003C_003Ec._003C_003E9__7_0 = _003C_003Ec._003C_003E9._003CSetInMenus_003Eb__7_0));
		}
	}

	public void SetPlayingGame()
	{
		if (presence != null)
		{
			if (!StartTime.HasValue)
			{
				StartTime = DateTime.UtcNow;
			}
			Activity activity = default(Activity);
			activity.State = "In Game";
			activity.Details = "Playing";
			activity.Assets.LargeImage = "icon";
			activity.Timestamps.Start = ToUnixTime(StartTime.Value);
			presence.GetActivityManager().UpdateActivity(activity, _003C_003Ec._003C_003E9__8_0 ?? (_003C_003Ec._003C_003E9__8_0 = _003C_003Ec._003C_003E9._003CSetPlayingGame_003Eb__8_0));
		}
	}

	public void SetHowToPlay()
	{
		if (presence != null)
		{
			ClearPresence();
			Activity activity = default(Activity);
			activity.State = "In Freeplay";
			activity.Assets.LargeImage = "icon";
			presence.GetActivityManager().UpdateActivity(activity, _003C_003Ec._003C_003E9__9_0 ?? (_003C_003Ec._003C_003E9__9_0 = _003C_003Ec._003C_003E9._003CSetHowToPlay_003Eb__9_0));
		}
	}

	public void SetInLobbyClient()
	{
		if (presence != null)
		{
			if (!StartTime.HasValue)
			{
				StartTime = DateTime.UtcNow;
			}
			ClearPresence();
			Activity activity = default(Activity);
			activity.State = "In Lobby";
			activity.Assets.LargeImage = "icon";
			activity.Timestamps.Start = ToUnixTime(StartTime.Value);
			presence.GetActivityManager().UpdateActivity(activity, _003C_003Ec._003C_003E9__10_0 ?? (_003C_003Ec._003C_003E9__10_0 = _003C_003Ec._003C_003E9._003CSetInLobbyClient_003Eb__10_0));
		}
	}

	private void ClearPresence()
	{
		if (presence != null)
		{
			presence.GetActivityManager().ClearActivity(_003C_003Ec._003C_003E9__11_0 ?? (_003C_003Ec._003C_003E9__11_0 = _003C_003Ec._003C_003E9._003CClearPresence_003Eb__11_0));
		}
	}

	public void SetInLobbyHost(int numPlayers, int gameId)
	{
		if (presence != null)
		{
			if (!StartTime.HasValue)
			{
				StartTime = DateTime.UtcNow;
			}
			string text = InnerNetClient.IntToGameName(gameId);
			Activity activity = default(Activity);
			activity.State = "In Lobby";
			activity.Details = "Hosting a game";
			activity.Party.Size.CurrentSize = numPlayers;
			activity.Party.Size.MaxSize = 10;
			activity.Assets.SmallImage = "icon";
			activity.Assets.LargeText = "Ask to play!";
			activity.Secrets.Join = "join" + text;
			activity.Secrets.Match = "match" + text;
			activity.Party.Id = text;
			presence.GetActivityManager().UpdateActivity(activity, _003C_003Ec._003C_003E9__12_0 ?? (_003C_003Ec._003C_003E9__12_0 = _003C_003Ec._003C_003E9._003CSetInLobbyHost_003Eb__12_0));
		}
	}

	private void HandleAutoJoin(ref User requestUser)
	{
		Debug.Log("Discord: request from " + requestUser.Username);
		if (AmongUsClient.Instance.IsGameStarted)
		{
			RequestRespondNo(requestUser.Id);
		}
		else
		{
			RequestRespondYes(requestUser.Id);
		}
	}

	private void HandleJoinRequest(string joinSecret)
	{
		if (!joinSecret.StartsWith("join"))
		{
			Debug.LogWarning("Invalid join secret: " + joinSecret);
			return;
		}
		if (!AmongUsClient.Instance)
		{
			Debug.LogWarning("Missing AmongUsClient");
			return;
		}
		if (!DestroyableSingleton<DiscordManager>.InstanceExists)
		{
			Debug.LogWarning("Missing DiscordManager");
			return;
		}
		if (AmongUsClient.Instance.mode != 0)
		{
			Debug.LogWarning("Already connected");
			return;
		}
		AmongUsClient.Instance.GameMode = GameModes.OnlineGame;
		AmongUsClient.Instance.GameId = InnerNetClient.GameNameToInt(joinSecret.Substring(4));
		AmongUsClient.Instance.SetEndpoint(DestroyableSingleton<ServerManager>.Instance.OnlineNetAddress, 22023);
		AmongUsClient.Instance.MainMenuScene = "MMOnline";
		AmongUsClient.Instance.OnlineScene = "OnlineGame";
		DestroyableSingleton<DiscordManager>.Instance.StopAllCoroutines();
		DestroyableSingleton<DiscordManager>.Instance.StartCoroutine(DestroyableSingleton<DiscordManager>.Instance.CoJoinGame());
	}

	public IEnumerator CoJoinGame()
	{
		AmongUsClient.Instance.Connect(MatchMakerModes.Client);
		yield return AmongUsClient.Instance.WaitForConnectionOrFail();
		if (AmongUsClient.Instance.ClientId < 0)
		{
			SceneManager.LoadScene("MMOnline");
		}
	}

	public void RequestRespondYes(long userId)
	{
		if (presence != null)
		{
			presence.GetActivityManager().SendRequestReply(userId, ActivityJoinRequestReply.Yes, _003C_003Ec._003C_003E9__16_0 ?? (_003C_003Ec._003C_003E9__16_0 = _003C_003Ec._003C_003E9._003CRequestRespondYes_003Eb__16_0));
		}
	}

	public void RequestRespondNo(long userId)
	{
		if (presence != null)
		{
			Debug.Log("Discord: responding no to Ask to Join request");
			presence.GetActivityManager().SendRequestReply(userId, ActivityJoinRequestReply.No, _003C_003Ec._003C_003E9__17_0 ?? (_003C_003Ec._003C_003E9__17_0 = _003C_003Ec._003C_003E9._003CRequestRespondNo_003Eb__17_0));
		}
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
		if (presence != null)
		{
			presence.Dispose();
		}
	}

	private static long ToUnixTime(DateTime time)
	{
		return (long)(time - epoch).TotalSeconds;
	}

	[CompilerGenerated]
	private void _003CStart_003Eb__3_0(Scene scene, LoadSceneMode mode)
	{
		OnSceneChange(scene.name);
	}
}
