using System;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;

public class DoorBreakerGame : Minigame
{
	[Serializable]
	[CompilerGenerated]
	private sealed class _003C_003Ec
	{
		public static readonly _003C_003Ec _003C_003E9 = new _003C_003Ec();

		public static Func<SpriteRenderer, bool> _003C_003E9__4_0;

		internal bool _003CFlipSwitch_003Eb__4_0(SpriteRenderer s)
		{
			return !s.flipX;
		}
	}

	public PlainDoor MyDoor;

	public SpriteRenderer[] Buttons;

	public AudioClip FlipSound;

	public void Start()
	{
		for (int i = 0; i < Buttons.Length; i++)
		{
			SpriteRenderer obj = Buttons[i];
			obj.color = Color.gray;
			obj.GetComponent<PassiveButton>().enabled = false;
		}
		int num = 0;
		while (num < 4)
		{
			SpriteRenderer spriteRenderer = Buttons.Random();
			if (!spriteRenderer.flipX)
			{
				spriteRenderer.color = Color.white;
				spriteRenderer.GetComponent<PassiveButton>().enabled = true;
				spriteRenderer.flipX = true;
				num++;
			}
		}
	}

	public void FlipSwitch(SpriteRenderer button)
	{
		if (Constants.ShouldPlaySfx())
		{
			SoundManager.Instance.PlaySound(FlipSound, false);
		}
		button.color = Color.gray;
		button.flipX = false;
		button.GetComponent<PassiveButton>().enabled = false;
		if (Buttons.All(_003C_003Ec._003C_003E9__4_0 ?? (_003C_003Ec._003C_003E9__4_0 = _003C_003Ec._003C_003E9._003CFlipSwitch_003Eb__4_0)))
		{
			MyDoor.SetDoorway(true);
			StartCoroutine(CoStartClose());
		}
	}
}
