using UnityEngine;

public class DoorConsole : MonoBehaviour, IUsable
{
	private PlainDoor MyDoor;

	public DoorBreakerGame MinigamePrefab;

	public SpriteRenderer Image;

	public float UsableDistance
	{
		get
		{
			return 1f;
		}
	}

	public float PercentCool
	{
		get
		{
			return 0f;
		}
	}

	public void Awake()
	{
		MyDoor = GetComponent<PlainDoor>();
	}

	public float CanUse(GameData.PlayerInfo pc, out bool canUse, out bool couldUse)
	{
		float num = Vector2.Distance(pc.Object.GetTruePosition(), base.transform.position);
		couldUse = !pc.IsDead && !MyDoor.Open;
		canUse = couldUse && num <= UsableDistance;
		return num;
	}

	public void SetOutline(bool on, bool mainTarget)
	{
		if ((bool)Image)
		{
			Image.material.SetFloat("_Outline", on ? 1 : 0);
			Image.material.SetColor("_OutlineColor", Color.white);
			Image.material.SetColor("_AddColor", mainTarget ? Color.white : Color.clear);
		}
	}

	public void Use()
	{
		bool canUse;
		bool couldUse;
		CanUse(PlayerControl.LocalPlayer.Data, out canUse, out couldUse);
		if (canUse)
		{
			PlayerControl.LocalPlayer.NetTransform.Halt();
			DoorBreakerGame doorBreakerGame = Object.Instantiate(MinigamePrefab, Camera.main.transform);
			doorBreakerGame.transform.localPosition = new Vector3(0f, 0f, -50f);
			doorBreakerGame.MyDoor = MyDoor;
			doorBreakerGame.Begin(null);
		}
	}
}
