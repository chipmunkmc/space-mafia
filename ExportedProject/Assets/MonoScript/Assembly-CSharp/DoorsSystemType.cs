using System.Collections.Generic;
using Hazel;
using UnityEngine;

public class DoorsSystemType : ISystemType, IActivatable, ISystemTimer, IDoorSystem
{
	public const byte CloseDoors = 128;

	public const byte OpenDoor = 64;

	private const byte Mask = 192;

	private Dictionary<SystemTypes, float> timers = new Dictionary<SystemTypes, float>();

	public bool IsActive
	{
		get
		{
			return false;
		}
	}

	public void SetDoors(PlainDoor[] doors)
	{
		ShipStatus.Instance.AllDoors = doors;
	}

	public bool Detoriorate(float deltaTime)
	{
		for (int i = 0; i < SystemTypeHelpers.AllTypes.Length; i++)
		{
			SystemTypes key = SystemTypeHelpers.AllTypes[i];
			float value;
			if (timers.TryGetValue(key, out value))
			{
				timers[key] = Mathf.Clamp(value - deltaTime, 0f, 30f);
			}
		}
		return false;
	}

	public void RepairDamage(PlayerControl player, byte amount)
	{
		int num = amount & 0xC0;
		if (num != 64)
		{
			int num2 = 128;
		}
	}

	public void Serialize(MessageWriter writer, bool initialState)
	{
		for (int i = 0; i < ShipStatus.Instance.AllDoors.Length; i++)
		{
			ShipStatus.Instance.AllDoors[i].Serialize(writer);
		}
	}

	public void Deserialize(MessageReader reader, bool initialState)
	{
		for (int i = 0; i < ShipStatus.Instance.AllDoors.Length; i++)
		{
			ShipStatus.Instance.AllDoors[i].Deserialize(reader);
		}
	}

	public void SetDoor(AutoOpenDoor door, bool open)
	{
		door.SetDoorway(open);
	}

	public void CloseDoorsOfType(SystemTypes room)
	{
		timers[room] = 30f;
		for (int i = 0; i < ShipStatus.Instance.AllDoors.Length; i++)
		{
			PlainDoor plainDoor = ShipStatus.Instance.AllDoors[i];
			if (plainDoor.Room == room)
			{
				plainDoor.SetDoorway(false);
			}
		}
	}

	public virtual float GetTimer(SystemTypes room)
	{
		float value;
		if (timers.TryGetValue(room, out value))
		{
			return value;
		}
		return 0f;
	}
}
