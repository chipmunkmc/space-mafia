public enum DragState
{
	NoTouch = 0,
	TouchStart = 1,
	Holding = 2,
	Dragging = 3,
	Released = 4
}
