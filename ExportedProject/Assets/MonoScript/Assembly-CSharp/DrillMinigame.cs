using System.Linq;
using System.Runtime.CompilerServices;
using PowerTools;
using UnityEngine;

public class DrillMinigame : Minigame
{
	public SpriteRenderer CaseImage;

	public SpriteRenderer StatusImage;

	public Sprite FineStatus;

	public SpriteAnim[] Buttons;

	public AnimationClip BadAnim;

	public AudioClip ButtonSound;

	private int MaxState = 4;

	private int[] states = new int[4];

	public void Start()
	{
		PlayerControl.LocalPlayer.SetPlayerMaterialColors(CaseImage);
	}

	public override void Begin(PlayerTask task)
	{
		base.Begin(task);
		do
		{
			for (int i = 0; i < states.Length; i++)
			{
				int num = (states[i] = 0);
				Buttons[i].transform.localScale = Vector3.one * Mathf.Lerp(1f, 0.5f, (float)num / (float)MaxState);
				Buttons[i].gameObject.SetActive(num != MaxState);
			}
		}
		while (states.All(_003CBegin_003Eb__9_0));
	}

	public void FixButton(SpriteAnim button)
	{
		int num = Buttons.IndexOf(button);
		if (states[num] != MaxState)
		{
			if (Constants.ShouldPlaySfx())
			{
				SoundManager.Instance.PlaySound(ButtonSound, false);
			}
			int num2 = ++states[num];
			Buttons[num].transform.localScale = Vector3.one * Mathf.Lerp(1f, 0.5f, (float)num2 / (float)MaxState);
			Buttons[num].gameObject.SetActive(num2 != MaxState);
			if (states.All(_003CFixButton_003Eb__10_0))
			{
				StatusImage.sprite = FineStatus;
				MyNormTask.NextStep();
				StartCoroutine(CoStartClose());
			}
		}
	}

	[CompilerGenerated]
	private bool _003CBegin_003Eb__9_0(int s)
	{
		return s == MaxState;
	}

	[CompilerGenerated]
	private bool _003CFixButton_003Eb__10_0(int ss)
	{
		return ss == MaxState;
	}
}
