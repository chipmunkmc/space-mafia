using System.Collections;
using System.Linq;
using System.Runtime.CompilerServices;
using PowerTools;
using UnityEngine;

public class DripBehaviour : MonoBehaviour
{
	[CompilerGenerated]
	private sealed class _003C_003Ec__DisplayClass7_0
	{
		public Vector3 pos;

		internal bool _003CRun_003Eb__0(Collider2D i)
		{
			return i.OverlapPoint(pos);
		}
	}

	public Vector2Range SpawnArea;

	public FloatRange FirstWait = new FloatRange(0f, 3f);

	public FloatRange Frequency = new FloatRange(0.75f, 3f);

	public SpriteAnim myAnim;

	public Collider2D[] IgnoreAreas;

	public bool FixDepth = true;

	public void Start()
	{
		StartCoroutine(Run());
	}

	private IEnumerator Run()
	{
		yield return Effects.Wait(FirstWait.Next());
		while (true)
		{
			_003C_003Ec__DisplayClass7_0 _003C_003Ec__DisplayClass7_ = new _003C_003Ec__DisplayClass7_0();
			_003C_003Ec__DisplayClass7_.pos = SpawnArea.Next();
			base.transform.localPosition = _003C_003Ec__DisplayClass7_.pos;
			if (FixDepth)
			{
				_003C_003Ec__DisplayClass7_.pos = base.transform.position;
				_003C_003Ec__DisplayClass7_.pos.z = _003C_003Ec__DisplayClass7_.pos.y / 1000f;
				base.transform.position = _003C_003Ec__DisplayClass7_.pos;
			}
			if (!IgnoreAreas.Any(_003C_003Ec__DisplayClass7_._003CRun_003Eb__0))
			{
				myAnim.Play();
				while (myAnim.IsPlaying())
				{
					yield return null;
				}
				yield return Effects.Wait(Frequency.Next());
			}
		}
	}
}
