using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Assets.CoreScripts;
using UnityEngine;

public class EndGameManager : DestroyableSingleton<EndGameManager>
{
	[Serializable]
	[CompilerGenerated]
	private sealed class _003C_003Ec
	{
		public static readonly _003C_003Ec _003C_003E9 = new _003C_003Ec();

		public static Func<WinningPlayerData, bool> _003C_003E9__15_1;

		public static Func<WinningPlayerData, bool> _003C_003E9__15_2;

		public static Func<WinningPlayerData, int> _003C_003E9__15_0;

		public static Func<bool> _003C_003E9__20_0;

		internal bool _003CSetEverythingUp_003Eb__15_1(WinningPlayerData h)
		{
			return h.IsYou;
		}

		internal bool _003CSetEverythingUp_003Eb__15_2(WinningPlayerData h)
		{
			return h.IsYou;
		}

		internal int _003CSetEverythingUp_003Eb__15_0(WinningPlayerData b)
		{
			if (!b.IsYou)
			{
				return 0;
			}
			return -1;
		}

		internal bool _003CCoJoinGame_003Eb__20_0()
		{
			return AmongUsClient.Instance.ClientId >= 0;
		}
	}

	public TextRenderer WinText;

	public MeshRenderer BackgroundBar;

	public MeshRenderer Foreground;

	public FloatRange ForegroundRadius;

	public SpriteRenderer FrontMost;

	public PoolablePlayer PlayerPrefab;

	public Sprite GhostSprite;

	public SpriteRenderer PlayAgainButton;

	public SpriteRenderer ExitButton;

	public AudioClip DisconnectStinger;

	public AudioClip CrewStinger;

	public AudioClip ImpostorStinger;

	public float BaseY = -0.25f;

	private float stingerTime;

	public void Start()
	{
		if (TempData.showAd && !SaveManager.BoughtNoAds)
		{
			AdPlayer.RequestInterstitial();
		}
		SetEverythingUp();
		StartCoroutine(CoBegin());
		Invoke("ShowButtons", 1.1f);
	}

	private void ShowButtons()
	{
		FrontMost.gameObject.SetActive(false);
		PlayAgainButton.gameObject.SetActive(true);
		ExitButton.gameObject.SetActive(true);
	}

	private void SetEverythingUp()
	{
		StatsManager.Instance.GamesFinished++;
		bool flag = TempData.DidHumansWin(TempData.EndReason);
		if (TempData.EndReason == GameOverReason.ImpostorDisconnect)
		{
			StatsManager.Instance.AddDrawReason(TempData.EndReason);
			WinText.Text = DestroyableSingleton<TranslationController>.Instance.GetString(StringNames.ImpostorDisconnected);
			SoundManager.Instance.PlaySound(DisconnectStinger, false);
		}
		else if (TempData.EndReason == GameOverReason.HumansDisconnect)
		{
			StatsManager.Instance.AddDrawReason(TempData.EndReason);
			WinText.Text = DestroyableSingleton<TranslationController>.Instance.GetString(StringNames.CrewmatesDisconnected);
			SoundManager.Instance.PlaySound(DisconnectStinger, false);
		}
		else
		{
			if (TempData.winners.Any(_003C_003Ec._003C_003E9__15_1 ?? (_003C_003Ec._003C_003E9__15_1 = _003C_003Ec._003C_003E9._003CSetEverythingUp_003Eb__15_1)))
			{
				StatsManager.Instance.AddWinReason(TempData.EndReason);
				WinText.Text = DestroyableSingleton<TranslationController>.Instance.GetString(StringNames.Victory);
				BackgroundBar.material.SetColor("_Color", Palette.CrewmateBlue);
				WinningPlayerData winningPlayerData = TempData.winners.FirstOrDefault(_003C_003Ec._003C_003E9__15_2 ?? (_003C_003Ec._003C_003E9__15_2 = _003C_003Ec._003C_003E9._003CSetEverythingUp_003Eb__15_2));
				if (winningPlayerData != null)
				{
					DestroyableSingleton<Telemetry>.Instance.WonGame(winningPlayerData.ColorId, winningPlayerData.HatId);
				}
			}
			else
			{
				StatsManager.Instance.AddLoseReason(TempData.EndReason);
				WinText.Text = DestroyableSingleton<TranslationController>.Instance.GetString(StringNames.Defeat);
				WinText.Color = Color.red;
			}
			if (flag)
			{
				SoundManager.Instance.PlayDynamicSound("Stinger", CrewStinger, false, GetStingerVol);
			}
			else
			{
				SoundManager.Instance.PlayDynamicSound("Stinger", ImpostorStinger, false, GetStingerVol);
			}
		}
		List<WinningPlayerData> list = TempData.winners.OrderBy(_003C_003Ec._003C_003E9__15_0 ?? (_003C_003Ec._003C_003E9__15_0 = _003C_003Ec._003C_003E9._003CSetEverythingUp_003Eb__15_0)).ToList();
		for (int i = 0; i < list.Count; i++)
		{
			WinningPlayerData winningPlayerData2 = list[i];
			int num = ((i % 2 != 0) ? 1 : (-1));
			int num2 = (i + 1) / 2;
			float num3 = 1f - (float)num2 * 0.075f;
			float num4 = 1f - (float)num2 * 0.035f;
			float num5 = ((i == 0) ? (-8) : (-1));
			PoolablePlayer poolablePlayer = UnityEngine.Object.Instantiate(PlayerPrefab, base.transform);
			poolablePlayer.transform.localPosition = new Vector3(0.8f * (float)num * (float)num2 * num4, BaseY - 0.25f + (float)num2 * 0.1f, num5 + (float)num2 * 0.01f) * 1.25f;
			Vector3 vector = new Vector3(num3, num3, num3) * 1.25f;
			poolablePlayer.transform.localScale = vector;
			if (winningPlayerData2.IsDead)
			{
				poolablePlayer.Body.sprite = GhostSprite;
				poolablePlayer.SetDeadFlipX(i % 2 != 0);
			}
			else
			{
				poolablePlayer.SetFlipX(i % 2 == 0);
			}
			if (!winningPlayerData2.IsDead)
			{
				DestroyableSingleton<HatManager>.Instance.SetSkin(poolablePlayer.SkinSlot, winningPlayerData2.SkinId);
			}
			else
			{
				poolablePlayer.HatSlot.color = new Color(1f, 1f, 1f, 0.5f);
			}
			PlayerControl.SetPlayerMaterialColors(winningPlayerData2.ColorId, poolablePlayer.Body);
			poolablePlayer.HatSlot.SetHat(winningPlayerData2.HatId);
			PlayerControl.SetPetImage(winningPlayerData2.PetId, winningPlayerData2.ColorId, poolablePlayer.PetSlot);
			if (flag)
			{
				poolablePlayer.NameText.gameObject.SetActive(false);
				continue;
			}
			poolablePlayer.NameText.Text = winningPlayerData2.Name;
			if (winningPlayerData2.IsImpostor)
			{
				poolablePlayer.NameText.Color = Palette.ImpostorRed;
			}
			poolablePlayer.NameText.transform.localScale = vector.Inv();
		}
	}

	private void GetStingerVol(AudioSource source, float dt)
	{
		stingerTime += dt * 0.75f;
		source.volume = Mathf.Clamp(1f / stingerTime, 0f, 1f);
	}

	public IEnumerator CoBegin()
	{
		Color c = WinText.Color;
		Color fade = Color.black;
		Color white = Color.white;
		Vector3 titlePos = WinText.transform.localPosition;
		float timer = 0f;
		while (timer < 3f)
		{
			timer += Time.deltaTime;
			float num = Mathf.Min(1f, timer / 3f);
			Foreground.material.SetFloat("_Rad", ForegroundRadius.ExpOutLerp(num * 2f));
			fade.a = Mathf.Lerp(1f, 0f, num * 3f);
			FrontMost.color = fade;
			c.a = Mathf.Clamp(FloatRange.ExpOutLerp(num, 0f, 1f), 0f, 1f);
			WinText.Color = c;
			titlePos.y = 2.7f - num * 0.3f;
			WinText.transform.localPosition = titlePos;
			yield return null;
		}
		FrontMost.gameObject.SetActive(false);
	}

	public void NextGame()
	{
		PlayAgainButton.gameObject.SetActive(false);
		ExitButton.gameObject.SetActive(false);
		if (TempData.showAd && !SaveManager.BoughtNoAds)
		{
			TempData.showAd = false;
			AdPlayer.ShowInterstitial(this, true);
		}
		else
		{
			StartCoroutine(CoJoinGame());
		}
	}

	public IEnumerator CoJoinGame()
	{
		AmongUsClient.Instance.JoinGame();
		yield return WaitWithTimeout(_003C_003Ec._003C_003E9__20_0 ?? (_003C_003Ec._003C_003E9__20_0 = _003C_003Ec._003C_003E9._003CCoJoinGame_003Eb__20_0));
		if (AmongUsClient.Instance.ClientId < 0)
		{
			AmongUsClient.Instance.ExitGame(AmongUsClient.Instance.LastDisconnectReason);
		}
	}

	public void Exit()
	{
		PlayAgainButton.gameObject.SetActive(false);
		ExitButton.gameObject.SetActive(false);
		if (TempData.showAd && !SaveManager.BoughtNoAds)
		{
			TempData.showAd = false;
			AdPlayer.ShowInterstitial(this, false);
		}
		else
		{
			AmongUsClient.Instance.ExitGame();
		}
	}

	public static IEnumerator WaitWithTimeout(Func<bool> success)
	{
		for (float timer = 0f; timer < 5f; timer += Time.deltaTime)
		{
			if (success())
			{
				break;
			}
			yield return null;
		}
	}
}
