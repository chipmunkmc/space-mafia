using System.Runtime.CompilerServices;
using UnityEngine;

public class EngineBehaviour : MonoBehaviour
{
	[CompilerGenerated]
	private sealed class _003C_003Ec__DisplayClass4_0
	{
		public float pitch;

		public EngineBehaviour _003C_003E4__this;

		internal void _003CPlaySteamSound_003Eb__0(AudioSource p, float d)
		{
			_003C_003E4__this.GetSoundDistance(p, d, pitch);
		}
	}

	public AudioClip ElectricSound;

	public AudioClip SteamSound;

	public float SoundDistance = 5f;

	public void PlayElectricSound()
	{
		if (Constants.ShouldPlaySfx())
		{
			SoundManager.Instance.PlayDynamicSound("EngineShock" + base.name, ElectricSound, false, GetSoundDistance);
		}
	}

	public void PlaySteamSound()
	{
		if (Constants.ShouldPlaySfx())
		{
			_003C_003Ec__DisplayClass4_0 _003C_003Ec__DisplayClass4_ = new _003C_003Ec__DisplayClass4_0();
			_003C_003Ec__DisplayClass4_._003C_003E4__this = this;
			_003C_003Ec__DisplayClass4_.pitch = FloatRange.Next(0.7f, 1.1f);
			SoundManager.Instance.PlayDynamicSound("EngineSteam" + base.name, SteamSound, false, _003C_003Ec__DisplayClass4_._003CPlaySteamSound_003Eb__0);
		}
	}

	private void GetSoundDistance(AudioSource player, float dt)
	{
		GetSoundDistance(player, dt, 1f);
	}

	private void GetSoundDistance(AudioSource player, float dt, float pitch)
	{
		float num = 1f;
		if ((bool)PlayerControl.LocalPlayer)
		{
			float num2 = Vector2.Distance(base.transform.position, PlayerControl.LocalPlayer.GetTruePosition());
			num = 1f - num2 / SoundDistance;
		}
		player.volume = num * 0.8f;
		player.pitch = pitch;
	}
}
