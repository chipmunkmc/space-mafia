using System.Collections;
using System.Runtime.CompilerServices;
using UnityEngine;

public class FillCanistersGame : Minigame
{
	private Vector3 CanisterAppearPosition = new Vector3(0f, 4.5f, 0f);

	private Vector3 CanisterStartPosition = new Vector3(-0.75f, 1.5f, 0f);

	private Vector3 CanisterDragPosition = new Vector3(0.4f, -1f, 0f);

	private Vector3 CanisterSnapPosition = new Vector3(0f, -1f, 0f);

	private Vector3 CanisterAwayPosition = new Vector3(8f, -1f, 0f);

	public float FillTime = 2.5f;

	public CanisterBehaviour Canister;

	private Controller controller = new Controller();

	public AudioClip FillLoop;

	public AudioClip DropSound;

	public AudioClip GrabSound;

	public AudioClip PlugInSound;

	public AudioClip PlugOutSound;

	public void Start()
	{
		StartCoroutine(Run());
	}

	private IEnumerator Run()
	{
		do
		{
			Canister.Gauge.Value = 0f;
			yield return Effects.Slide2D(Canister.transform, CanisterAppearPosition, CanisterStartPosition, 0.1f);
			controller.ClearTouch();
			while (true)
			{
				controller.Update();
				switch (controller.CheckDrag(Canister.Hitbox))
				{
				case DragState.TouchStart:
					if (Constants.ShouldPlaySfx())
					{
						SoundManager.Instance.PlaySound(GrabSound, false);
					}
					goto default;
				case DragState.Dragging:
				{
					float t = FloatRange.ReverseLerp(((Vector3)controller.DragPosition - base.transform.position).y, CanisterDragPosition.y, CanisterStartPosition.y);
					Canister.transform.localPosition = Vector3.Lerp(CanisterDragPosition, CanisterStartPosition, t);
					goto default;
				}
				case DragState.Released:
					if (Constants.ShouldPlaySfx())
					{
						SoundManager.Instance.PlaySound(DropSound, false);
					}
					if (FloatRange.ReverseLerp(Canister.transform.localPosition.y, CanisterDragPosition.y, CanisterStartPosition.y) < 0.05f)
					{
						if (Constants.ShouldPlaySfx())
						{
							SoundManager.Instance.PlaySound(PlugInSound, false);
						}
						break;
					}
					goto default;
				default:
					yield return null;
					continue;
				}
				break;
			}
			AudioSource fillSound = null;
			if (Constants.ShouldPlaySfx())
			{
				fillSound = SoundManager.Instance.PlaySound(FillLoop, true);
			}
			yield return Effects.Slide2D(Canister.transform, CanisterDragPosition, CanisterSnapPosition, 0.1f);
			yield return Effects.Lerp(FillTime, _003CRun_003Eb__14_0);
			if ((bool)fillSound)
			{
				fillSound.Stop();
			}
			while (controller.CheckDrag(Canister.Hitbox) != DragState.TouchStart)
			{
				controller.Update();
				yield return null;
			}
			if (Constants.ShouldPlaySfx())
			{
				SoundManager.Instance.PlaySound(PlugOutSound, false);
			}
			yield return Effects.Slide2D(Canister.transform, CanisterSnapPosition, CanisterAwayPosition, 0.3f);
			MyNormTask.NextStep();
		}
		while (!MyNormTask.IsComplete);
		StartCoroutine(CoStartClose());
	}

	[CompilerGenerated]
	private void _003CRun_003Eb__14_0(float t)
	{
		Canister.Gauge.Value = t;
	}
}
