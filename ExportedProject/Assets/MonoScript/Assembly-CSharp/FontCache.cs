using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class FontCache : MonoBehaviour
{
	[CompilerGenerated]
	private sealed class _003C_003Ec__DisplayClass7_0
	{
		public TextAsset dataSrc;

		internal bool _003CLoadFont_003Eb__0(FontExtensionData ed)
		{
			return ed.FontName.Equals(dataSrc.name, StringComparison.OrdinalIgnoreCase);
		}
	}

	public static FontCache Instance;

	private Dictionary<string, FontData> cache = new Dictionary<string, FontData>();

	public List<FontExtensionData> extraData = new List<FontExtensionData>();

	public List<TextAsset> DefaultFonts = new List<TextAsset>();

	public List<Material> DefaultFontMaterials = new List<Material>();

	public void OnEnable()
	{
		if (!Instance)
		{
			Instance = this;
			UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
		}
		else if (Instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	public void SetFont(TextRenderer self, string name)
	{
		if (self.FontData.name == name)
		{
			return;
		}
		for (int i = 0; i < DefaultFonts.Count; i++)
		{
			if (DefaultFonts[i].name == name)
			{
				MeshRenderer component = self.GetComponent<MeshRenderer>();
				Material material = component.material;
				self.FontData = DefaultFonts[i];
				component.sharedMaterial = DefaultFontMaterials[i];
				component.material.SetColor("_OutlineColor", material.GetColor("_OutlineColor"));
				component.material.SetInt("_Mask", material.GetInt("_Mask"));
				break;
			}
		}
	}

	public FontData LoadFont(TextAsset dataSrc)
	{
		_003C_003Ec__DisplayClass7_0 _003C_003Ec__DisplayClass7_ = new _003C_003Ec__DisplayClass7_0();
		_003C_003Ec__DisplayClass7_.dataSrc = dataSrc;
		if (cache == null)
		{
			cache = new Dictionary<string, FontData>();
		}
		FontData value;
		if (cache.TryGetValue(_003C_003Ec__DisplayClass7_.dataSrc.name, out value))
		{
			return value;
		}
		int num = extraData.FindIndex(_003C_003Ec__DisplayClass7_._003CLoadFont_003Eb__0);
		FontExtensionData eData = null;
		if (num >= 0)
		{
			eData = extraData[num];
		}
		value = LoadFontUncached(_003C_003Ec__DisplayClass7_.dataSrc, eData);
		cache[_003C_003Ec__DisplayClass7_.dataSrc.name] = value;
		return value;
	}

	public static FontData LoadFontUncached(TextAsset dataSrc, FontExtensionData eData = null)
	{
		return FontLoader.FromBinary(dataSrc, eData);
	}
}
