using System;

[Flags]
public enum FreeWeekendState : byte
{
	NotFree = 0,
	FreeMIRA = 1,
	Free = 2
}
