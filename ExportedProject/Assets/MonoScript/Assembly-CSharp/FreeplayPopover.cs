using UnityEngine;

public class FreeplayPopover : MonoBehaviour
{
	public GameObject Content;

	public SpriteRenderer[] MapButtons;

	public HostGameButton HostGame;

	public void Show()
	{
		int num = 0;
		for (int i = 0; i < MapButtons.Length; i++)
		{
			if (SaveManager.GetMapPurchased(i) && i < AmongUsClient.Instance.ShipPrefabs.Count)
			{
				num++;
			}
			else
			{
				MapButtons[i].gameObject.SetActive(false);
			}
		}
		int num2 = 0;
		for (int j = 0; j < MapButtons.Length; j++)
		{
			SpriteRenderer spriteRenderer = MapButtons[j];
			if (spriteRenderer.gameObject.activeSelf)
			{
				Vector3 localPosition = spriteRenderer.transform.localPosition;
				localPosition.y = -0.65f * ((float)num2 - (float)(num - 1) / 2f);
				spriteRenderer.transform.localPosition = localPosition;
				num2++;
			}
		}
		if (num == 1)
		{
			HostGame.OnClick();
		}
		else
		{
			Content.SetActive(true);
		}
	}

	public void PlayMap(int i)
	{
		AmongUsClient.Instance.TutorialMapId = i;
		HostGame.OnClick();
	}
}
