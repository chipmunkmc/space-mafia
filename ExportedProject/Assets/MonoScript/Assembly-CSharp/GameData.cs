using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Hazel;
using InnerNet;
using UnityEngine;

public class GameData : InnerNetObject, IDisconnectHandler
{
	public class TaskInfo
	{
		public uint Id;

		public bool Complete;

		public void Serialize(MessageWriter writer)
		{
			writer.WritePacked(Id);
			writer.Write(Complete);
		}

		public void Deserialize(MessageReader reader)
		{
			Id = reader.ReadPackedUInt32();
			Complete = reader.ReadBoolean();
		}
	}

	public class PlayerInfo
	{
		public readonly byte PlayerId;

		public string PlayerName = string.Empty;

		public byte ColorId;

		public uint HatId;

		public uint PetId;

		public uint SkinId;

		public bool Disconnected;

		public List<TaskInfo> Tasks;

		public bool IsImpostor;

		public bool IsDead;

		private PlayerControl _object;

		public PlayerControl Object
		{
			get
			{
				if (!_object)
				{
					_object = PlayerControl.AllPlayerControls.FirstOrDefault(_003Cget_Object_003Eb__12_0);
				}
				return _object;
			}
		}

		public PlayerInfo(byte playerId)
		{
			PlayerId = playerId;
		}

		public PlayerInfo(PlayerControl pc)
			: this(pc.PlayerId)
		{
			_object = pc;
		}

		public void Serialize(MessageWriter writer)
		{
			writer.Write(PlayerName);
			writer.Write(ColorId);
			writer.WritePacked(HatId);
			writer.WritePacked(PetId);
			writer.WritePacked(SkinId);
			byte b = 0;
			if (Disconnected)
			{
				b = (byte)(b | 1u);
			}
			if (IsImpostor)
			{
				b = (byte)(b | 2u);
			}
			if (IsDead)
			{
				b = (byte)(b | 4u);
			}
			writer.Write(b);
			if (Tasks != null)
			{
				writer.Write((byte)Tasks.Count);
				for (int i = 0; i < Tasks.Count; i++)
				{
					Tasks[i].Serialize(writer);
				}
			}
			else
			{
				writer.Write((byte)0);
			}
		}

		public void Deserialize(MessageReader reader)
		{
			PlayerName = reader.ReadString();
			ColorId = reader.ReadByte();
			HatId = reader.ReadPackedUInt32();
			PetId = reader.ReadPackedUInt32();
			SkinId = reader.ReadPackedUInt32();
			byte b = reader.ReadByte();
			Disconnected = (b & 1) != 0;
			IsImpostor = (b & 2) != 0;
			IsDead = (b & 4) != 0;
			byte b2 = reader.ReadByte();
			Tasks = new List<TaskInfo>(b2);
			for (int i = 0; i < b2; i++)
			{
				Tasks.Add(new TaskInfo());
				Tasks[i].Deserialize(reader);
			}
		}

		public TaskInfo FindTaskById(uint taskId)
		{
			for (int i = 0; i < Tasks.Count; i++)
			{
				if (Tasks[i].Id == taskId)
				{
					return Tasks[i];
				}
			}
			return null;
		}

		[CompilerGenerated]
		private bool _003Cget_Object_003Eb__12_0(PlayerControl p)
		{
			return p.PlayerId == PlayerId;
		}
	}

	private enum RpcCalls
	{
		SetTasks = 0
	}

	[CompilerGenerated]
	private sealed class _003C_003Ec__DisplayClass13_0
	{
		public sbyte i;

		internal bool _003CGetAvailableId_003Eb__0(PlayerInfo p)
		{
			return p.PlayerId == i;
		}
	}

	public static GameData Instance;

	public List<PlayerInfo> AllPlayers = new List<PlayerInfo>();

	public int TotalTasks;

	public int CompletedTasks;

	public const byte InvalidPlayerId = byte.MaxValue;

	public const byte DisconnectedPlayerId = 254;

	public int PlayerCount
	{
		get
		{
			return AllPlayers.Count;
		}
	}

	public void Awake()
	{
		if ((bool)Instance && Instance != this)
		{
			Object.Destroy(base.gameObject);
			return;
		}
		Instance = this;
		if ((bool)AmongUsClient.Instance)
		{
			AmongUsClient.Instance.DisconnectHandlers.AddUnique(this);
		}
	}

	internal void SetDirty()
	{
		SetDirtyBit(uint.MaxValue);
	}

	public PlayerInfo GetHost()
	{
		ClientData host = AmongUsClient.Instance.GetHost();
		if (host != null && (bool)host.Character)
		{
			return host.Character.Data;
		}
		return null;
	}

	public sbyte GetAvailableId()
	{
		_003C_003Ec__DisplayClass13_0 _003C_003Ec__DisplayClass13_ = new _003C_003Ec__DisplayClass13_0();
		_003C_003Ec__DisplayClass13_.i = 0;
		while (_003C_003Ec__DisplayClass13_.i < 10)
		{
			if (!AllPlayers.Any(_003C_003Ec__DisplayClass13_._003CGetAvailableId_003Eb__0))
			{
				return _003C_003Ec__DisplayClass13_.i;
			}
			_003C_003Ec__DisplayClass13_.i++;
		}
		return -1;
	}

	public PlayerInfo GetPlayerById(byte id)
	{
		if (id == byte.MaxValue)
		{
			return null;
		}
		for (int i = 0; i < AllPlayers.Count; i++)
		{
			if (AllPlayers[i].PlayerId == id)
			{
				return AllPlayers[i];
			}
		}
		return null;
	}

	public void UpdateName(byte playerId, string name)
	{
		PlayerInfo playerById = GetPlayerById(playerId);
		if (playerById != null)
		{
			playerById.PlayerName = name;
		}
		SetDirtyBit((uint)(1 << (int)playerId));
	}

	public void UpdateColor(byte playerId, byte color)
	{
		PlayerInfo playerById = GetPlayerById(playerId);
		if (playerById != null)
		{
			playerById.ColorId = color;
		}
		SetDirtyBit((uint)(1 << (int)playerId));
	}

	public void UpdateHat(byte playerId, uint hat)
	{
		PlayerInfo playerById = GetPlayerById(playerId);
		if (playerById != null)
		{
			playerById.HatId = hat;
		}
		SetDirtyBit((uint)(1 << (int)playerId));
	}

	public void UpdatePet(byte playerId, uint petId)
	{
		PlayerInfo playerById = GetPlayerById(playerId);
		if (playerById != null)
		{
			playerById.PetId = petId;
		}
		SetDirtyBit((uint)(1 << (int)playerId));
	}

	public void UpdateSkin(byte playerId, uint skin)
	{
		PlayerInfo playerById = GetPlayerById(playerId);
		if (playerById != null)
		{
			playerById.SkinId = skin;
		}
		SetDirtyBit((uint)(1 << (int)playerId));
	}

	public void AddPlayer(PlayerControl pc)
	{
		PlayerInfo item = new PlayerInfo(pc);
		AllPlayers.Add(item);
		SetDirtyBit((uint)(1 << (int)pc.PlayerId));
	}

	public bool RemovePlayer(byte playerId)
	{
		for (int i = 0; i < AllPlayers.Count; i++)
		{
			if (AllPlayers[i].PlayerId == playerId)
			{
				SetDirty();
				AllPlayers.RemoveAt(i);
				return true;
			}
		}
		return false;
	}

	public void RecomputeTaskCounts()
	{
		TotalTasks = 0;
		CompletedTasks = 0;
		for (int i = 0; i < AllPlayers.Count; i++)
		{
			PlayerInfo playerInfo = AllPlayers[i];
			if (playerInfo.Disconnected || playerInfo.Tasks == null || !playerInfo.Object || (!PlayerControl.GameOptions.GhostsDoTasks && playerInfo.IsDead) || playerInfo.IsImpostor)
			{
				continue;
			}
			for (int j = 0; j < playerInfo.Tasks.Count; j++)
			{
				TotalTasks++;
				if (playerInfo.Tasks[j].Complete)
				{
					CompletedTasks++;
				}
			}
		}
	}

	public void TutOnlyRemoveTask(byte playerId, uint taskId)
	{
		PlayerInfo playerById = GetPlayerById(playerId);
		TaskInfo item = playerById.FindTaskById(taskId);
		playerById.Tasks.Remove(item);
		RecomputeTaskCounts();
	}

	public void TutOnlyAddTask(byte playerId, uint taskId)
	{
		GetPlayerById(playerId).Tasks.Add(new TaskInfo
		{
			Id = taskId
		});
		TotalTasks++;
	}

	private void SetTasks(byte playerId, byte[] taskTypeIds)
	{
		PlayerInfo playerById = GetPlayerById(playerId);
		if (playerById == null)
		{
			Debug.Log("Could not set tasks for player id: " + playerId);
		}
		else
		{
			if (playerById.Disconnected)
			{
				return;
			}
			if (!playerById.Object)
			{
				Debug.Log("Could not set tasks for player (" + playerById.PlayerName + "): " + playerId);
				return;
			}
			playerById.Tasks = new List<TaskInfo>(taskTypeIds.Length);
			for (int i = 0; i < taskTypeIds.Length; i++)
			{
				playerById.Tasks.Add(new TaskInfo());
				playerById.Tasks[i].Id = (uint)i;
			}
			playerById.Object.SetTasks(taskTypeIds);
			SetDirtyBit((uint)(1 << (int)playerById.PlayerId));
		}
	}

	public void CompleteTask(PlayerControl pc, uint taskId)
	{
		TaskInfo taskInfo = pc.Data.FindTaskById(taskId);
		if (taskInfo != null)
		{
			if (!taskInfo.Complete)
			{
				taskInfo.Complete = true;
				CompletedTasks++;
			}
			else
			{
				Debug.LogWarning("Double complete task: " + taskId);
			}
		}
		else
		{
			Debug.LogWarning("Couldn't find task: " + taskId);
		}
	}

	public void HandleDisconnect(PlayerControl player, DisconnectReasons reason)
	{
		if (!player)
		{
			return;
		}
		PlayerInfo playerById = GetPlayerById(player.PlayerId);
		if (playerById == null)
		{
			return;
		}
		if (AmongUsClient.Instance.IsGameStarted)
		{
			if (!playerById.Disconnected)
			{
				playerById.Disconnected = true;
				TempData.LastDeathReason = DeathReason.Disconnect;
				ShowNotification(playerById.PlayerName, reason);
			}
		}
		else if (RemovePlayer(player.PlayerId))
		{
			ShowNotification(playerById.PlayerName, reason);
		}
		RecomputeTaskCounts();
	}

	private void ShowNotification(string playerName, DisconnectReasons reason)
	{
		if (string.IsNullOrEmpty(playerName))
		{
			return;
		}
		if (reason <= DisconnectReasons.Banned)
		{
			switch (reason)
			{
			case DisconnectReasons.ExitGame:
				DestroyableSingleton<HudManager>.Instance.Notifier.AddItem(DestroyableSingleton<TranslationController>.Instance.GetString(StringNames.UserLeftGame, playerName));
				return;
			case DisconnectReasons.Banned:
			{
				PlayerInfo data = AmongUsClient.Instance.GetHost().Character.Data;
				DestroyableSingleton<HudManager>.Instance.Notifier.AddItem(playerName + " was banned by " + data.PlayerName + ".");
				return;
			}
			}
		}
		else
		{
			if (reason == DisconnectReasons.Kicked)
			{
				PlayerInfo data2 = AmongUsClient.Instance.GetHost().Character.Data;
				DestroyableSingleton<HudManager>.Instance.Notifier.AddItem(playerName + " was kicked by " + data2.PlayerName + ".");
				return;
			}
			int num = 17;
		}
		DestroyableSingleton<HudManager>.Instance.Notifier.AddItem(playerName + " left the game due to error.");
	}

	public void HandleDisconnect()
	{
		if (AmongUsClient.Instance.IsGameStarted)
		{
			return;
		}
		for (int num = AllPlayers.Count - 1; num >= 0; num--)
		{
			if (!AllPlayers[num].Object)
			{
				AllPlayers.RemoveAt(num);
			}
		}
	}

	public override bool Serialize(MessageWriter writer, bool initialState)
	{
		if (initialState)
		{
			writer.WritePacked(AllPlayers.Count);
			for (int i = 0; i < AllPlayers.Count; i++)
			{
				PlayerInfo playerInfo = AllPlayers[i];
				writer.Write(playerInfo.PlayerId);
				playerInfo.Serialize(writer);
			}
		}
		else
		{
			int position = writer.Position;
			byte b = 0;
			writer.Write(b);
			for (int j = 0; j < AllPlayers.Count; j++)
			{
				PlayerInfo playerInfo2 = AllPlayers[j];
				if ((DirtyBits & (uint)(1 << (int)playerInfo2.PlayerId)) != 0)
				{
					writer.Write(playerInfo2.PlayerId);
					playerInfo2.Serialize(writer);
					b = (byte)(b + 1);
				}
			}
			writer.Position = position;
			writer.Write(b);
			writer.Position = writer.Length;
			DirtyBits = 0u;
		}
		return true;
	}

	public override void Deserialize(MessageReader reader, bool initialState)
	{
		if (initialState)
		{
			int num = reader.ReadPackedInt32();
			for (int i = 0; i < num; i++)
			{
				PlayerInfo playerInfo = new PlayerInfo(reader.ReadByte());
				playerInfo.Deserialize(reader);
				AllPlayers.Add(playerInfo);
			}
		}
		else
		{
			byte b = reader.ReadByte();
			for (int j = 0; j < b; j++)
			{
				byte b2 = reader.ReadByte();
				PlayerInfo playerById = GetPlayerById(b2);
				if (playerById != null)
				{
					playerById.Deserialize(reader);
					continue;
				}
				playerById = new PlayerInfo(b2);
				playerById.Deserialize(reader);
				AllPlayers.Add(playerById);
			}
		}
		RecomputeTaskCounts();
	}

	public void RpcSetTasks(byte playerId, byte[] taskTypeIds)
	{
		if (AmongUsClient.Instance.AmClient)
		{
			SetTasks(playerId, taskTypeIds);
		}
		MessageWriter messageWriter = AmongUsClient.Instance.StartRpc(NetId, 0);
		messageWriter.Write(playerId);
		messageWriter.WriteBytesAndSize(taskTypeIds);
		messageWriter.EndMessage();
	}

	public override void HandleRpc(byte callId, MessageReader reader)
	{
		if (callId == 0)
		{
			SetTasks(reader.ReadByte(), reader.ReadBytesAndSize());
		}
	}
}
