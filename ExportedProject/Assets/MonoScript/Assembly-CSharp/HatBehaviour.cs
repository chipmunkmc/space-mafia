using UnityEngine;

[CreateAssetMenu]
public class HatBehaviour : ScriptableObject, IBuyable
{
	public Sprite MainImage;

	public Sprite BackImage;

	public Sprite FloorImage;

	public bool InFront;

	public bool NoBounce;

	public bool NotInStore;

	public bool Free;

	public Vector2 ChipOffset;

	public int LimitedMonth;

	public int LimitedYear;

	public SkinData RelatedSkin;

	public string StoreName;

	public string ProductId;

	public int Order;

	public string ProdId
	{
		get
		{
			return ProductId;
		}
	}
}
