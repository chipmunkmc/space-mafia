using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;

public class HatManager : DestroyableSingleton<HatManager>
{
	[CompilerGenerated]
	private sealed class _003C_003Ec__DisplayClass6_0
	{
		public PetBehaviour pet;

		internal bool _003CGetIdFromPet_003Eb__0(PetBehaviour p)
		{
			return p.idleClip == pet.idleClip;
		}
	}

	[Serializable]
	[CompilerGenerated]
	private sealed class _003C_003Ec
	{
		public static readonly _003C_003Ec _003C_003E9 = new _003C_003Ec();

		public static Func<PetBehaviour, bool> _003C_003E9__7_0;

		public static Func<HatBehaviour, bool> _003C_003E9__9_0;

		public static Func<HatBehaviour, int> _003C_003E9__9_1;

		public static Func<HatBehaviour, string> _003C_003E9__9_2;

		public static Func<SkinData, bool> _003C_003E9__11_0;

		public static Func<SkinData, int> _003C_003E9__11_1;

		public static Func<SkinData, string> _003C_003E9__11_2;

		internal bool _003CGetUnlockedPets_003Eb__7_0(PetBehaviour h)
		{
			if (!h.Free)
			{
				return SaveManager.GetPurchase(h.ProductId);
			}
			return true;
		}

		internal bool _003CGetUnlockedHats_003Eb__9_0(HatBehaviour h)
		{
			if (!(h.ProdId != "map_mira") || h.LimitedMonth != 0)
			{
				return SaveManager.GetPurchase(h.ProductId);
			}
			return true;
		}

		internal int _003CGetUnlockedHats_003Eb__9_1(HatBehaviour o)
		{
			return o.Order;
		}

		internal string _003CGetUnlockedHats_003Eb__9_2(HatBehaviour o)
		{
			return o.name;
		}

		internal bool _003CGetUnlockedSkins_003Eb__11_0(SkinData s)
		{
			if (!(s.ProdId != "map_mira"))
			{
				return SaveManager.GetPurchase(s.ProdId);
			}
			return true;
		}

		internal int _003CGetUnlockedSkins_003Eb__11_1(SkinData o)
		{
			return o.Order;
		}

		internal string _003CGetUnlockedSkins_003Eb__11_2(SkinData o)
		{
			return o.name;
		}
	}

	public HatBehaviour NoneHat;

	public List<PetBehaviour> AllPets = new List<PetBehaviour>();

	public List<HatBehaviour> AllHats = new List<HatBehaviour>();

	public List<SkinData> AllSkins = new List<SkinData>();

	public List<MapBuyable> AllMaps = new List<MapBuyable>();

	internal PetBehaviour GetPetById(uint petId)
	{
		if (petId >= AllPets.Count)
		{
			return AllPets[0];
		}
		return AllPets[(int)petId];
	}

	public uint GetIdFromPet(PetBehaviour pet)
	{
		_003C_003Ec__DisplayClass6_0 _003C_003Ec__DisplayClass6_ = new _003C_003Ec__DisplayClass6_0();
		_003C_003Ec__DisplayClass6_.pet = pet;
		return (uint)AllPets.FindIndex(_003C_003Ec__DisplayClass6_._003CGetIdFromPet_003Eb__0);
	}

	public PetBehaviour[] GetUnlockedPets()
	{
		return AllPets.Where(_003C_003Ec._003C_003E9__7_0 ?? (_003C_003Ec._003C_003E9__7_0 = _003C_003Ec._003C_003E9._003CGetUnlockedPets_003Eb__7_0)).ToArray();
	}

	public HatBehaviour GetHatById(uint hatId)
	{
		if (hatId >= AllHats.Count)
		{
			return NoneHat;
		}
		return AllHats[(int)hatId];
	}

	public HatBehaviour[] GetUnlockedHats()
	{
		return AllHats.Where(_003C_003Ec._003C_003E9__9_0 ?? (_003C_003Ec._003C_003E9__9_0 = _003C_003Ec._003C_003E9._003CGetUnlockedHats_003Eb__9_0)).OrderByDescending(_003C_003Ec._003C_003E9__9_1 ?? (_003C_003Ec._003C_003E9__9_1 = _003C_003Ec._003C_003E9._003CGetUnlockedHats_003Eb__9_1)).ThenBy(_003C_003Ec._003C_003E9__9_2 ?? (_003C_003Ec._003C_003E9__9_2 = _003C_003Ec._003C_003E9._003CGetUnlockedHats_003Eb__9_2))
			.ToArray();
	}

	public uint GetIdFromHat(HatBehaviour hat)
	{
		return (uint)AllHats.IndexOf(hat);
	}

	public SkinData[] GetUnlockedSkins()
	{
		return AllSkins.Where(_003C_003Ec._003C_003E9__11_0 ?? (_003C_003Ec._003C_003E9__11_0 = _003C_003Ec._003C_003E9._003CGetUnlockedSkins_003Eb__11_0)).OrderByDescending(_003C_003Ec._003C_003E9__11_1 ?? (_003C_003Ec._003C_003E9__11_1 = _003C_003Ec._003C_003E9._003CGetUnlockedSkins_003Eb__11_1)).ThenBy(_003C_003Ec._003C_003E9__11_2 ?? (_003C_003Ec._003C_003E9__11_2 = _003C_003Ec._003C_003E9._003CGetUnlockedSkins_003Eb__11_2))
			.ToArray();
	}

	public uint GetIdFromSkin(SkinData skin)
	{
		return (uint)AllSkins.IndexOf(skin);
	}

	internal SkinData GetSkinById(uint skinId)
	{
		if (skinId >= AllSkins.Count)
		{
			return AllSkins[0];
		}
		return AllSkins[(int)skinId];
	}

	internal void SetSkin(SpriteRenderer skinRend, uint skinId)
	{
		SkinData skinById = GetSkinById(skinId);
		if ((bool)skinById)
		{
			skinRend.sprite = skinById.IdleFrame;
		}
	}
}
