using PowerTools;
using UnityEngine;

public class HatParent : MonoBehaviour
{
	public SpriteRenderer BackLayer;

	public SpriteRenderer FrontLayer;

	public SpriteRenderer Parent;

	public Color color
	{
		set
		{
			BackLayer.color = value;
			FrontLayer.color = value;
		}
	}

	public bool flipX
	{
		set
		{
			BackLayer.flipX = value;
			FrontLayer.flipX = value;
		}
	}

	public void SetHat(HatBehaviour hat)
	{
		SpriteAnimNodeSync component = GetComponent<SpriteAnimNodeSync>();
		if ((bool)component)
		{
			component.NodeId = (hat.NoBounce ? 1 : 0);
		}
		if (hat.InFront)
		{
			BackLayer.enabled = false;
			FrontLayer.enabled = true;
			FrontLayer.sprite = hat.MainImage;
		}
		else if ((bool)hat.BackImage)
		{
			BackLayer.enabled = true;
			FrontLayer.enabled = true;
			BackLayer.sprite = hat.BackImage;
			FrontLayer.sprite = hat.MainImage;
		}
		else
		{
			BackLayer.enabled = true;
			FrontLayer.enabled = false;
			BackLayer.sprite = hat.MainImage;
		}
	}

	public void SetHat(uint hatId)
	{
		if (DestroyableSingleton<HatManager>.InstanceExists)
		{
			HatBehaviour hatById = DestroyableSingleton<HatManager>.Instance.GetHatById(hatId);
			SetHat(hatById);
		}
	}

	internal void SetHatFloor(HatBehaviour hat)
	{
		BackLayer.enabled = false;
		FrontLayer.enabled = true;
		FrontLayer.sprite = hat.FloorImage;
	}

	public void LateUpdate()
	{
		if ((bool)Parent)
		{
			flipX = Parent.flipX;
		}
	}
}
