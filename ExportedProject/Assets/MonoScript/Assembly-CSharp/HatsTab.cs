using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class HatsTab : MonoBehaviour
{
	[CompilerGenerated]
	private sealed class _003C_003Ec__DisplayClass11_0
	{
		public HatBehaviour hat;

		public HatsTab _003C_003E4__this;

		internal void _003COnEnable_003Eb__0()
		{
			_003C_003E4__this.SelectHat(hat);
		}
	}

	public ColorChip ColorTabPrefab;

	public SpriteRenderer DemoImage;

	public HatParent HatImage;

	public SpriteRenderer SkinImage;

	public SpriteRenderer PetImage;

	public FloatRange XRange = new FloatRange(1.5f, 3f);

	public float YStart = 0.8f;

	public float YOffset = 0.8f;

	public int NumPerRow = 4;

	public Scroller scroller;

	private List<ColorChip> ColorChips = new List<ColorChip>();

	public void OnEnable()
	{
		PlayerControl.SetPlayerMaterialColors(PlayerControl.LocalPlayer.Data.ColorId, DemoImage);
		HatImage.SetHat(SaveManager.LastHat);
		PlayerControl.SetSkinImage(SaveManager.LastSkin, SkinImage);
		PlayerControl.SetPetImage(SaveManager.LastPet, PlayerControl.LocalPlayer.Data.ColorId, PetImage);
		HatBehaviour[] unlockedHats = DestroyableSingleton<HatManager>.Instance.GetUnlockedHats();
		for (int i = 0; i < unlockedHats.Length; i++)
		{
			_003C_003Ec__DisplayClass11_0 _003C_003Ec__DisplayClass11_ = new _003C_003Ec__DisplayClass11_0();
			_003C_003Ec__DisplayClass11_._003C_003E4__this = this;
			_003C_003Ec__DisplayClass11_.hat = unlockedHats[i];
			float x = XRange.Lerp((float)(i % NumPerRow) / ((float)NumPerRow - 1f));
			float y = YStart - (float)(i / NumPerRow) * YOffset;
			ColorChip colorChip = Object.Instantiate(ColorTabPrefab, scroller.Inner);
			colorChip.transform.localPosition = new Vector3(x, y, -1f);
			colorChip.Button.OnClick.AddListener(_003C_003Ec__DisplayClass11_._003COnEnable_003Eb__0);
			colorChip.Inner.SetHat(_003C_003Ec__DisplayClass11_.hat);
			colorChip.Inner.transform.localPosition = _003C_003Ec__DisplayClass11_.hat.ChipOffset;
			colorChip.Tag = _003C_003Ec__DisplayClass11_.hat;
			ColorChips.Add(colorChip);
		}
		scroller.YBounds.max = 0f - (YStart - (float)(unlockedHats.Length / NumPerRow) * YOffset) - 3f;
	}

	public void OnDisable()
	{
		for (int i = 0; i < ColorChips.Count; i++)
		{
			Object.Destroy(ColorChips[i].gameObject);
		}
		ColorChips.Clear();
	}

	public void Update()
	{
		PlayerControl.SetPlayerMaterialColors(PlayerControl.LocalPlayer.Data.ColorId, DemoImage);
		HatBehaviour hatById = DestroyableSingleton<HatManager>.Instance.GetHatById(SaveManager.LastHat);
		for (int i = 0; i < ColorChips.Count; i++)
		{
			ColorChip colorChip = ColorChips[i];
			colorChip.InUseForeground.SetActive(hatById == colorChip.Tag);
		}
	}

	private void SelectHat(HatBehaviour hat)
	{
		uint num = (SaveManager.LastHat = DestroyableSingleton<HatManager>.Instance.GetIdFromHat(hat));
		HatImage.SetHat(num);
		if ((bool)PlayerControl.LocalPlayer)
		{
			PlayerControl.LocalPlayer.RpcSetHat(num);
		}
	}
}
