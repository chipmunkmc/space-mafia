using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using UnityEngine;

public class HudManager : DestroyableSingleton<HudManager>
{
	[Serializable]
	[CompilerGenerated]
	private sealed class _003C_003Ec
	{
		public static readonly _003C_003Ec _003C_003E9 = new _003C_003Ec();

		public static Action<MapBehaviour> _003C_003E9__40_0;

		internal void _003COpenMap_003Eb__40_0(MapBehaviour m)
		{
			m.ShowNormalMap();
		}
	}

	public MeetingHud MeetingPrefab;

	public KillButtonManager KillButton;

	public UseButtonManager UseButton;

	public ReportButtonManager ReportButton;

	public TextRenderer GameSettings;

	public GameObject TaskStuff;

	public ChatController Chat;

	public DialogueBox Dialogue;

	public TextRenderer TaskText;

	public Transform TaskCompleteOverlay;

	private float taskDirtyTimer;

	public MeshRenderer ShadowQuad;

	public SpriteRenderer FullScreen;

	public SpriteRenderer MapButton;

	public KillOverlay KillOverlay;

	public IVirtualJoystick joystick;

	public MonoBehaviour[] Joysticks;

	public DiscussBehaviour discussEmblem;

	public ShhhBehaviour shhhEmblem;

	public IntroCutscene IntroPrefab;

	public OptionsMenuBehaviour GameMenu;

	public NotificationPopper Notifier;

	public RoomTracker roomTracker;

	public AudioClip SabotageSound;

	public AudioClip TaskCompleteSound;

	public AudioClip TaskUpdateSound;

	private StringBuilder tasksString = new StringBuilder();

	public Coroutine ReactorFlash { get; set; }

	public Coroutine OxyFlash { get; set; }

	public void Start()
	{
		SetTouchType(SaveManager.TouchConfig);
	}

	public void ShowTaskComplete()
	{
		StartCoroutine(CoTaskComplete());
	}

	private IEnumerator CoTaskComplete()
	{
		if (Constants.ShouldPlaySfx())
		{
			SoundManager.Instance.PlaySound(TaskCompleteSound, false);
		}
		TaskCompleteOverlay.gameObject.SetActive(true);
		yield return Effects.Slide2D(TaskCompleteOverlay, new Vector2(0f, -8f), Vector2.zero, 0.25f);
		for (float time = 0f; time < 0.75f; time += Time.deltaTime)
		{
			yield return null;
		}
		yield return Effects.Slide2D(TaskCompleteOverlay, Vector2.zero, new Vector2(0f, 8f), 0.25f);
		TaskCompleteOverlay.gameObject.SetActive(false);
	}

	public void SetJoystickSize(float size)
	{
		if (joystick != null && joystick is VirtualJoystick)
		{
			VirtualJoystick obj = (VirtualJoystick)joystick;
			obj.transform.localScale = new Vector3(size, size, 1f);
			AspectPosition component = obj.GetComponent<AspectPosition>();
			float num = Mathf.Lerp(0.65f, 1.1f, FloatRange.ReverseLerp(size, 0.5f, 1.5f));
			component.DistanceFromEdge = new Vector3(num, num, -10f);
			component.AdjustPosition();
		}
	}

	public void SetTouchType(int type)
	{
		if (joystick != null && !(joystick is KeyboardJoystick))
		{
			UnityEngine.Object.Destroy((joystick as MonoBehaviour).gameObject);
		}
		MonoBehaviour monoBehaviour = UnityEngine.Object.Instantiate(Joysticks[Mathf.Clamp(type + 1, 1, Joysticks.Length)]);
		monoBehaviour.transform.SetParent(base.transform, false);
		joystick = monoBehaviour.GetComponent<IVirtualJoystick>();
	}

	public void OpenMap()
	{
		ShowMap(_003C_003Ec._003C_003E9__40_0 ?? (_003C_003Ec._003C_003E9__40_0 = _003C_003Ec._003C_003E9._003COpenMap_003Eb__40_0));
	}

	public void ShowMap(Action<MapBehaviour> mapAction)
	{
		if ((bool)ShipStatus.Instance)
		{
			if (!MapBehaviour.Instance)
			{
				MapBehaviour.Instance = UnityEngine.Object.Instantiate(ShipStatus.Instance.MapPrefab, base.transform);
				MapBehaviour.Instance.gameObject.SetActive(false);
			}
			mapAction(MapBehaviour.Instance);
		}
	}

	public void SetHudActive(bool isActive)
	{
		DestroyableSingleton<HudManager>.Instance.UseButton.gameObject.SetActive(isActive);
		DestroyableSingleton<HudManager>.Instance.UseButton.Refresh();
		DestroyableSingleton<HudManager>.Instance.ReportButton.gameObject.SetActive(isActive);
		GameData.PlayerInfo data = PlayerControl.LocalPlayer.Data;
		DestroyableSingleton<HudManager>.Instance.KillButton.gameObject.SetActive(isActive && data.IsImpostor && !data.IsDead);
		DestroyableSingleton<HudManager>.Instance.TaskText.transform.parent.gameObject.SetActive(isActive);
	}

	public void FixedUpdate()
	{
		taskDirtyTimer += Time.fixedDeltaTime;
		if (!(taskDirtyTimer > 0.25f))
		{
			return;
		}
		taskDirtyTimer = 0f;
		if (!PlayerControl.LocalPlayer)
		{
			TaskText.Text = string.Empty;
			return;
		}
		GameData.PlayerInfo data = PlayerControl.LocalPlayer.Data;
		if (data == null)
		{
			return;
		}
		bool isImpostor = data.IsImpostor;
		tasksString.Clear();
		if (PlayerControl.LocalPlayer.myTasks.Count == 0)
		{
			tasksString.Append("None");
		}
		else
		{
			for (int i = 0; i < PlayerControl.LocalPlayer.myTasks.Count; i++)
			{
				PlayerTask playerTask = PlayerControl.LocalPlayer.myTasks[i];
				if ((bool)playerTask)
				{
					if (playerTask.TaskType == TaskTypes.FixComms && !isImpostor)
					{
						tasksString.Clear();
						playerTask.AppendTaskText(tasksString);
						break;
					}
					playerTask.AppendTaskText(tasksString);
				}
			}
			tasksString.TrimEnd();
		}
		TaskText.Text = tasksString.ToString();
	}

	public IEnumerator ShowEmblem(bool shhh)
	{
		if (shhh)
		{
			shhhEmblem.gameObject.SetActive(true);
			yield return shhhEmblem.PlayAnimation();
			shhhEmblem.gameObject.SetActive(false);
		}
		else
		{
			discussEmblem.gameObject.SetActive(true);
			yield return discussEmblem.PlayAnimation();
			discussEmblem.gameObject.SetActive(false);
		}
	}

	public void StartReactorFlash()
	{
		if (ReactorFlash == null)
		{
			ReactorFlash = StartCoroutine(CoReactorFlash());
		}
	}

	public void StartOxyFlash()
	{
		if (OxyFlash == null)
		{
			OxyFlash = StartCoroutine(CoReactorFlash());
		}
	}

	public void ShowPopUp(string text)
	{
		Dialogue.Show(text);
	}

	public void StopReactorFlash()
	{
		if (ReactorFlash != null)
		{
			StopCoroutine(ReactorFlash);
			FullScreen.enabled = false;
			ReactorFlash = null;
		}
	}

	public void StopOxyFlash()
	{
		if (OxyFlash != null)
		{
			StopCoroutine(OxyFlash);
			FullScreen.enabled = false;
			OxyFlash = null;
		}
	}

	public IEnumerator CoFadeFullScreen(Color source, Color target, float duration = 0.2f)
	{
		if (!FullScreen.enabled || !(FullScreen.color == target))
		{
			FullScreen.enabled = true;
			for (float t = 0f; t < duration; t += Time.deltaTime)
			{
				FullScreen.color = Color.Lerp(source, target, t / duration);
				yield return null;
			}
			FullScreen.color = target;
			if (target.a < 0.05f)
			{
				FullScreen.enabled = false;
			}
		}
	}

	private IEnumerator CoReactorFlash()
	{
		WaitForSeconds wait = new WaitForSeconds(1f);
		FullScreen.color = new Color(1f, 0f, 0f, 19f / 51f);
		while (true)
		{
			FullScreen.enabled = !FullScreen.enabled;
			SoundManager.Instance.PlaySound(SabotageSound, false);
			yield return wait;
		}
	}

	public IEnumerator CoShowIntro(List<PlayerControl> yourTeam)
	{
		DestroyableSingleton<HudManager>.Instance.FullScreen.transform.localPosition = new Vector3(0f, 0f, -250f);
		yield return DestroyableSingleton<HudManager>.Instance.ShowEmblem(true);
		IntroCutscene introCutscene = UnityEngine.Object.Instantiate(IntroPrefab, base.transform);
		yield return introCutscene.CoBegin(yourTeam, PlayerControl.LocalPlayer.Data.IsImpostor);
		PlayerControl.LocalPlayer.SetKillTimer(10f);
		((SabotageSystemType)ShipStatus.Instance.Systems[SystemTypes.Sabotage]).ForceSabTime(10f);
		yield return CoFadeFullScreen(Color.black, Color.clear);
		DestroyableSingleton<HudManager>.Instance.FullScreen.transform.localPosition = new Vector3(0f, 0f, -500f);
	}

	public void OpenMeetingRoom(PlayerControl reporter)
	{
		if (!MeetingHud.Instance)
		{
			Debug.Log("Opening meeting room: " + reporter);
			ShipStatus.Instance.RepairSystem(SystemTypes.Reactor, PlayerControl.LocalPlayer, 16);
			ShipStatus.Instance.RepairSystem(SystemTypes.LifeSupp, PlayerControl.LocalPlayer, 16);
			MeetingHud.Instance = UnityEngine.Object.Instantiate(MeetingPrefab);
			MeetingHud.Instance.ServerStart(reporter.PlayerId);
			AmongUsClient.Instance.Spawn(MeetingHud.Instance);
		}
	}
}
