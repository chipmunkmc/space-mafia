public interface ISystemTimer
{
	float GetTimer(SystemTypes system);
}
