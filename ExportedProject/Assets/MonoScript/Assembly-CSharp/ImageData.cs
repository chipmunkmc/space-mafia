using System;
using UnityEngine;

[Serializable]
public struct ImageData
{
	public ImageNames Name;

	public Sprite Sprite;
}
