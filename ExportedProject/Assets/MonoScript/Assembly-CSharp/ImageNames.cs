public enum ImageNames
{
	LocalButton = 0,
	OnlineButton = 1,
	HowToPlayButton = 2,
	FreeplayButton = 3,
	HostHeader = 4,
	PublicHeader = 5,
	PrivateHeader = 6
}
