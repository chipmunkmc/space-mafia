using UnityEngine;

public class InfectedOverlay : MonoBehaviour
{
	public MapRoom[] rooms;

	private IActivatable doors;

	private SabotageSystemType SabSystem;

	public bool CanUseDoors
	{
		get
		{
			return !SabSystem.AnyActive;
		}
	}

	public bool CanUseSpecial
	{
		get
		{
			if (SabSystem.Timer <= 0f)
			{
				IActivatable activatable = doors;
				if (activatable == null || !activatable.IsActive)
				{
					return !SabSystem.AnyActive;
				}
			}
			return false;
		}
	}

	public void Start()
	{
		for (int i = 0; i < rooms.Length; i++)
		{
			rooms[i].Parent = this;
		}
		SabSystem = (SabotageSystemType)ShipStatus.Instance.Systems[SystemTypes.Sabotage];
		ISystemType value;
		if (ShipStatus.Instance.Systems.TryGetValue(SystemTypes.Doors, out value))
		{
			doors = (IActivatable)value;
		}
	}

	private void FixedUpdate()
	{
		if (doors != null)
		{
			float specialActive = (doors.IsActive ? 1f : SabSystem.PercentCool);
			for (int i = 0; i < rooms.Length; i++)
			{
				rooms[i].SetSpecialActive(specialActive);
				rooms[i].OOBUpdate();
			}
		}
	}
}
