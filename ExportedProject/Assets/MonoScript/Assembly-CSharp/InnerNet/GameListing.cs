using System;
using Hazel;

namespace InnerNet
{
	[Serializable]
	public struct GameListing
	{
		public int GameId;

		public byte PlayerCount;

		public string HostName;

		public int Age;

		public GameOptionsData Options;

		public GameListing(MessageReader reader)
		{
			GameId = reader.ReadInt32();
			HostName = reader.ReadString();
			PlayerCount = reader.ReadByte();
			Age = reader.ReadPackedInt32();
			Options = GameOptionsData.Deserialize(reader);
		}
	}
}
