using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading;
using Hazel;
using Hazel.Udp;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace InnerNet
{
	public abstract class InnerNetClient : MonoBehaviour
	{
		public enum GameStates
		{
			NotJoined = 0,
			Joined = 1,
			Started = 2,
			Ended = 3
		}

		[Serializable]
		[CompilerGenerated]
		private sealed class _003C_003Ec
		{
			public static readonly _003C_003Ec _003C_003E9 = new _003C_003Ec();

			public static Func<byte, string> _003C_003E9__45_0;

			public static Func<byte, string> _003C_003E9__46_0;

			public static Func<char, bool> _003C_003E9__91_0;

			internal string _003CConnection_DataReceivedRaw_003Eb__45_0(byte b)
			{
				return b.ToString();
			}

			internal string _003CConnection_DataSentRaw_003Eb__46_0(byte b)
			{
				return b.ToString();
			}

			internal bool _003CIntToGameName_003Eb__91_0(char c)
			{
				if (c >= 'A')
				{
					return c > 'z';
				}
				return true;
			}
		}

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass55_0
		{
			public InnerNetClient _003C_003E4__this;

			public DisconnectReasons reason;

			public string stringReason;

			internal void _003CEnqueueDisconnect_003Eb__0()
			{
				_003C_003E4__this.HandleDisconnect(reason, stringReason);
			}
		}

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass70_0
		{
			public GameOverReason reason;

			public bool showAd;

			public InnerNetClient _003C_003E4__this;

			internal void _003CHandleMessage_003Eb__2()
			{
				_003C_003E4__this.OnGameEnd(reason, showAd);
			}
		}

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass70_1
		{
			public ClientData myClient;

			public InnerNetClient _003C_003E4__this;

			internal void _003CHandleMessage_003Eb__4()
			{
				_003C_003E4__this.OnGameJoined(IntToGameName(_003C_003E4__this.GameId), myClient);
			}
		}

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass70_2
		{
			public ClientData client;

			public InnerNetClient _003C_003E4__this;

			internal void _003CHandleMessage_003Eb__5()
			{
				_003C_003E4__this.OnPlayerJoined(client);
			}
		}

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass70_3
		{
			public MessageReader subReader;

			public InnerNetClient _003C_003E4__this;

			internal void _003CHandleMessage_003Eb__7()
			{
				_003C_003E4__this.HandleGameData(subReader);
			}
		}

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass70_4
		{
			public MessageReader subReader;

			public InnerNetClient _003C_003E4__this;

			internal void _003CHandleMessage_003Eb__8()
			{
				_003C_003E4__this.HandleGameData(subReader);
			}
		}

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass70_5
		{
			public int totalGames;

			public List<GameListing> output;

			public InnerNetClient _003C_003E4__this;

			internal void _003CHandleMessage_003Eb__9()
			{
				_003C_003E4__this.OnGetGameList(totalGames, output);
			}
		}

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass72_0
		{
			public int clientId;

			internal bool _003CGetOrCreateClient_003Eb__0(ClientData c)
			{
				return c.Id == clientId;
			}
		}

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass73_0
		{
			public InnerNetClient _003C_003E4__this;

			public ClientData client;

			public DisconnectReasons reason;

			internal void _003CRemovePlayer_003Eb__0()
			{
				_003C_003E4__this.OnPlayerLeft(client, reason);
			}
		}

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass119_0
		{
			public InnerNetClient _003C_003E4__this;

			public ClientData client;

			public string sceneName;

			internal void _003CCoSendSceneChange_003Eb__0()
			{
				_003C_003E4__this.OnPlayerChangedScene(client, sceneName);
			}
		}

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass127_0
		{
			public ClientData client;

			public string targetScene;

			public InnerNetClient _003C_003E4__this;

			internal void _003CHandleGameDataInner_003Eb__0()
			{
				_003C_003E4__this.OnPlayerChangedScene(client, targetScene);
			}
		}

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass128_0
		{
			public InnerNetClient _003C_003E4__this;

			public MessageReader copy;

			public int cnt;

			internal void _003CDeferMessage_003Eb__0()
			{
				_003C_003E4__this.HandleGameData(copy, cnt);
			}
		}

		private static readonly DisconnectReasons[] disconnectReasons = new DisconnectReasons[10]
		{
			DisconnectReasons.Error,
			DisconnectReasons.GameFull,
			DisconnectReasons.GameStarted,
			DisconnectReasons.GameNotFound,
			DisconnectReasons.IncorrectVersion,
			DisconnectReasons.Banned,
			DisconnectReasons.Kicked,
			DisconnectReasons.Hacking,
			DisconnectReasons.ServerFull,
			DisconnectReasons.Custom
		};

		public const int NoClientId = -1;

		private string networkAddress = "127.0.0.1";

		private int networkPort;

		private UdpClientConnection connection;

		public MatchMakerModes mode;

		public int GameId = 32;

		public int HostId;

		public int ClientId = -1;

		public List<ClientData> allClients = new List<ClientData>();

		public DisconnectReasons LastDisconnectReason;

		public string LastCustomDisconnect;

		private readonly List<Action> PreSpawnDispatcher = new List<Action>();

		private readonly List<Action> Dispatcher = new List<Action>();

		public GameStates GameState;

		private List<Action> TempQueue = new List<Action>();

		private volatile bool appPaused;

		public const int CurrentClient = -3;

		public const int InvalidClient = -2;

		internal const byte DataFlag = 1;

		internal const byte RpcFlag = 2;

		internal const byte SpawnFlag = 4;

		internal const byte DespawnFlag = 5;

		internal const byte SceneChangeFlag = 6;

		internal const byte ReadyFlag = 7;

		internal const byte ChangeSettingsFlag = 8;

		public float MinSendInterval = 0.1f;

		private uint NetIdCnt = 1u;

		private float timer;

		public InnerNetObject[] SpawnableObjects;

		private bool InOnlineScene;

		private HashSet<uint> DestroyedObjects = new HashSet<uint>();

		public List<InnerNetObject> allObjects = new List<InnerNetObject>();

		private Dictionary<uint, InnerNetObject> allObjectsFast = new Dictionary<uint, InnerNetObject>();

		private MessageWriter[] Streams;

		private bool AmConnected
		{
			get
			{
				return connection != null;
			}
		}

		public int Ping
		{
			get
			{
				if (connection == null)
				{
					return 0;
				}
				return (int)connection.AveragePingMs;
			}
		}

		public int BytesSent
		{
			get
			{
				if (connection == null)
				{
					return 0;
				}
				return (int)connection.Statistics.TotalBytesSent;
			}
		}

		public int BytesGot
		{
			get
			{
				if (connection == null)
				{
					return 0;
				}
				return (int)connection.Statistics.TotalBytesReceived;
			}
		}

		public int Resends
		{
			get
			{
				if (connection == null)
				{
					return 0;
				}
				return connection.Statistics.MessagesResent;
			}
		}

		public bool AmHost
		{
			get
			{
				return HostId == ClientId;
			}
		}

		public bool AmClient
		{
			get
			{
				return ClientId > 0;
			}
		}

		public bool IsGamePublic { get; private set; }

		public bool IsGameStarted
		{
			get
			{
				return GameState == GameStates.Started;
			}
		}

		public bool IsGameOver
		{
			get
			{
				return GameState == GameStates.Ended;
			}
		}

		public void SetEndpoint(string addr, ushort port)
		{
			networkAddress = addr;
			networkPort = port;
		}

		public virtual void Start()
		{
			SceneManager.activeSceneChanged += _003CStart_003Eb__39_0;
			ClientId = -1;
			GameId = 32;
		}

		private void SendOrDisconnect(MessageWriter msg)
		{
			try
			{
				connection.Send(msg);
			}
			catch (Exception ex)
			{
				EnqueueDisconnect(DisconnectReasons.Error, "Failed to send message: " + ex.Message);
			}
		}

		public ClientData GetHost()
		{
			lock (allClients)
			{
				for (int i = 0; i < allClients.Count; i++)
				{
					ClientData clientData = allClients[i];
					if (clientData.Id == HostId)
					{
						return clientData;
					}
				}
			}
			return null;
		}

		public int GetClientIdFromCharacter(InnerNetObject character)
		{
			if (!character)
			{
				return -1;
			}
			lock (allClients)
			{
				for (int i = 0; i < allClients.Count; i++)
				{
					ClientData clientData = allClients[i];
					if (clientData.Character == character)
					{
						return clientData.Id;
					}
				}
			}
			return -1;
		}

		public virtual void OnDestroy()
		{
			if (AmConnected)
			{
				DisconnectInternal(DisconnectReasons.Destroy);
			}
		}

		public IEnumerator CoConnect()
		{
			if (AmConnected)
			{
				yield break;
			}
			while (true)
			{
				DestroyableSingleton<DisconnectPopup>.Instance.Close();
				LastDisconnectReason = DisconnectReasons.ExitGame;
				NetIdCnt = 1u;
				DestroyedObjects.Clear();
				if (Streams == null)
				{
					Streams = new MessageWriter[2];
					for (int i = 0; i < Streams.Length; i++)
					{
						Streams[i] = MessageWriter.Get((SendOption)i);
					}
				}
				for (int j = 0; j < Streams.Length; j++)
				{
					MessageWriter obj = Streams[j];
					obj.Clear((SendOption)j);
					obj.StartMessage(5);
					obj.Write(GameId);
				}
				IPEndPoint remoteEndPoint = new IPEndPoint(IPAddress.Parse(networkAddress), networkPort);
				connection = new UdpClientConnection(remoteEndPoint);
				connection.KeepAliveInterval = 1000;
				connection.DisconnectTimeout = 7500;
				connection.ResendPingMultiplier = 1.2f;
				connection.DataReceived += OnMessageReceived;
				connection.Disconnected += OnDisconnect;
				connection.ConnectAsync(GetConnectionData(), 4000);
				yield return WaitWithTimeout(_003CCoConnect_003Eb__44_0, null);
				if ((connection == null || connection.State != ConnectionState.Connected) && LastDisconnectReason != DisconnectReasons.IncorrectVersion)
				{
					Debug.Log("Failed to connected to: " + networkAddress);
					if (DestroyableSingleton<ServerManager>.Instance.TrackServerFailure(networkAddress))
					{
						Debug.Log("Attempting another server: " + networkAddress);
						DisconnectInternal(DisconnectReasons.NewConnection);
						continue;
					}
					break;
				}
				break;
			}
		}

		private void Connection_DataReceivedRaw(byte[] data)
		{
			Debug.Log("Client Got: " + string.Join(" ", data.Select(_003C_003Ec._003C_003E9__45_0 ?? (_003C_003Ec._003C_003E9__45_0 = _003C_003Ec._003C_003E9._003CConnection_DataReceivedRaw_003Eb__45_0))));
		}

		private void Connection_DataSentRaw(byte[] data, int length)
		{
			Debug.Log("Client Sent: " + string.Join(" ", data.Select(_003C_003Ec._003C_003E9__46_0 ?? (_003C_003Ec._003C_003E9__46_0 = _003C_003Ec._003C_003E9._003CConnection_DataSentRaw_003Eb__46_0)).ToArray(), 0, length));
		}

		public void Connect(MatchMakerModes mode)
		{
			StartCoroutine(CoConnect(mode));
		}

		private IEnumerator CoConnect(MatchMakerModes mode)
		{
			if (this.mode != 0)
			{
				DisconnectInternal(DisconnectReasons.NewConnection);
			}
			this.mode = mode;
			yield return CoConnect();
			if (!AmConnected)
			{
				yield break;
			}
			switch (this.mode)
			{
			case MatchMakerModes.Client:
			{
				JoinGame();
				yield return WaitWithTimeout(_003CCoConnect_003Eb__48_0, "Failed to join game. Try again later.");
				bool amConnected = AmConnected;
				break;
			}
			case MatchMakerModes.HostAndClient:
				GameId = 0;
				PlayerControl.GameOptions = SaveManager.GameHostOptions;
				HostGame(PlayerControl.GameOptions);
				yield return WaitWithTimeout(_003CCoConnect_003Eb__48_1, "Failed to create a game. Try again later.");
				if (AmConnected)
				{
					JoinGame();
					yield return WaitWithTimeout(_003CCoConnect_003Eb__48_2, "Failed to join game after creating it. Try again later.");
					bool amConnected2 = AmConnected;
				}
				break;
			}
		}

		public IEnumerator WaitForConnectionOrFail()
		{
			while (AmConnected)
			{
				switch (mode)
				{
				default:
					yield break;
				case MatchMakerModes.Client:
					if (ClientId >= 0)
					{
						yield break;
					}
					break;
				case MatchMakerModes.HostAndClient:
					if (GameId != 0 && ClientId >= 0)
					{
						yield break;
					}
					break;
				}
				yield return null;
			}
		}

		private IEnumerator WaitWithTimeout(Func<bool> success, string errorMessage)
		{
			bool failed = true;
			for (float timer = 0f; timer < 15f; timer += Time.deltaTime)
			{
				if (success())
				{
					failed = false;
					break;
				}
				if (!AmConnected)
				{
					yield break;
				}
				yield return null;
			}
			if (failed && errorMessage != null)
			{
				LastCustomDisconnect = errorMessage;
				EnqueueDisconnect(DisconnectReasons.Custom, "Couldn't connect");
			}
		}

		public void Update()
		{
			if (Input.GetKeyDown(KeyCode.Return) && (Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt)))
			{
				ResolutionManager.ToggleFullscreen();
			}
			TempQueue.Clear();
			lock (Dispatcher)
			{
				TempQueue.AddAll(Dispatcher);
				Dispatcher.Clear();
			}
			for (int i = 0; i < TempQueue.Count; i++)
			{
				Action action = TempQueue[i];
				try
				{
					action();
				}
				catch (Exception exception)
				{
					Debug.LogException(exception);
				}
			}
			if (!InOnlineScene)
			{
				return;
			}
			TempQueue.Clear();
			lock (PreSpawnDispatcher)
			{
				TempQueue.AddAll(PreSpawnDispatcher);
				PreSpawnDispatcher.Clear();
			}
			for (int j = 0; j < TempQueue.Count; j++)
			{
				Action action2 = TempQueue[j];
				try
				{
					action2();
				}
				catch (Exception exception2)
				{
					Debug.LogException(exception2);
				}
			}
		}

		private void OnDisconnect(object sender, DisconnectedEventArgs e)
		{
			MessageReader message = e.Message;
			if (message != null && message.Position < message.Length)
			{
				if (message.ReadByte() != 1)
				{
					LastCustomDisconnect = "Forcibly disconnected from the server.";
					EnqueueDisconnect(DisconnectReasons.Custom);
					return;
				}
				MessageReader messageReader = message.ReadMessage();
				DisconnectReasons disconnectReasons = (DisconnectReasons)messageReader.ReadByte();
				if (disconnectReasons == DisconnectReasons.Custom)
				{
					LastCustomDisconnect = messageReader.ReadString();
				}
				EnqueueDisconnect(disconnectReasons);
			}
			else if (e.Reason == null)
			{
				LastCustomDisconnect = "You disconnected from the server.\r\n\r\nNull";
				EnqueueDisconnect(DisconnectReasons.Custom, e.Reason);
			}
			else if (!e.Reason.Contains("The remote sent a"))
			{
				LastCustomDisconnect = "You disconnected from the server.\r\n\r\n" + e.Reason;
				EnqueueDisconnect(DisconnectReasons.Custom, e.Reason);
			}
			else
			{
				EnqueueDisconnect(DisconnectReasons.Error, e.Reason);
			}
		}

		public void HandleDisconnect(DisconnectReasons reason, string stringReason = null)
		{
			StopAllCoroutines();
			DisconnectInternal(reason, stringReason);
			OnDisconnected();
		}

		protected void EnqueueDisconnect(DisconnectReasons reason, string stringReason = null)
		{
			_003C_003Ec__DisplayClass55_0 _003C_003Ec__DisplayClass55_ = new _003C_003Ec__DisplayClass55_0();
			_003C_003Ec__DisplayClass55_._003C_003E4__this = this;
			_003C_003Ec__DisplayClass55_.reason = reason;
			_003C_003Ec__DisplayClass55_.stringReason = stringReason;
			UdpClientConnection connection2 = connection;
			lock (Dispatcher)
			{
				Dispatcher.Add(_003C_003Ec__DisplayClass55_._003CEnqueueDisconnect_003Eb__0);
			}
		}

		protected void DisconnectInternal(DisconnectReasons reason, string stringReason = null)
		{
			if (reason != DisconnectReasons.NewConnection && reason != DisconnectReasons.FocusLostBackground)
			{
				LastDisconnectReason = reason;
				if (reason != 0 && DestroyableSingleton<DisconnectPopup>.InstanceExists)
				{
					DestroyableSingleton<DisconnectPopup>.Instance.Show();
				}
			}
			if (mode == MatchMakerModes.HostAndClient)
			{
				GameId = 0;
			}
			if (mode == MatchMakerModes.Client || mode == MatchMakerModes.HostAndClient)
			{
				ClientId = -1;
			}
			mode = MatchMakerModes.None;
			GameState = GameStates.NotJoined;
			UdpClientConnection udpClientConnection = connection;
			connection = null;
			if (udpClientConnection != null)
			{
				try
				{
					udpClientConnection.Dispose();
				}
				catch (Exception exception)
				{
					Debug.LogException(exception);
				}
			}
			if (DestroyableSingleton<InnerNetServer>.InstanceExists)
			{
				DestroyableSingleton<InnerNetServer>.Instance.StopServer();
			}
			lock (Dispatcher)
			{
				Dispatcher.Clear();
			}
			lock (PreSpawnDispatcher)
			{
				PreSpawnDispatcher.Clear();
			}
			if (reason != DisconnectReasons.Error)
			{
				TempQueue.Clear();
			}
			allObjects.Clear();
			allClients.Clear();
			allObjectsFast.Clear();
		}

		public void HostGame(IBytesSerializable settings)
		{
			IsGamePublic = false;
			MessageWriter messageWriter = MessageWriter.Get(SendOption.Reliable);
			messageWriter.StartMessage(0);
			messageWriter.WriteBytesAndSize(settings.ToBytes());
			messageWriter.EndMessage();
			SendOrDisconnect(messageWriter);
			messageWriter.Recycle();
			Debug.Log("Client requesting new game.");
		}

		public void JoinGame()
		{
			ClientId = -1;
			if (!AmConnected)
			{
				LastCustomDisconnect = "Disconnected before joining game.";
				HandleDisconnect(DisconnectReasons.Custom);
				return;
			}
			Debug.Log("Client joining game: " + IntToGameName(GameId));
			MessageWriter messageWriter = MessageWriter.Get(SendOption.Reliable);
			messageWriter.StartMessage(1);
			messageWriter.Write(GameId);
			messageWriter.WritePacked(SaveManager.GetMapPurchaseField());
			messageWriter.EndMessage();
			SendOrDisconnect(messageWriter);
			messageWriter.Recycle();
		}

		public bool CanBan()
		{
			if (AmHost)
			{
				return !IsGameStarted;
			}
			return false;
		}

		public bool CanKick()
		{
			if (!IsGameStarted)
			{
				return AmHost;
			}
			return true;
		}

		public void KickPlayer(int clientId, bool ban)
		{
			if (AmHost)
			{
				MessageWriter messageWriter = MessageWriter.Get(SendOption.Reliable);
				messageWriter.StartMessage(11);
				messageWriter.Write(GameId);
				messageWriter.WritePacked(clientId);
				messageWriter.Write(ban);
				messageWriter.EndMessage();
				SendOrDisconnect(messageWriter);
				messageWriter.Recycle();
			}
		}

		public MessageWriter StartEndGame()
		{
			MessageWriter messageWriter = MessageWriter.Get(SendOption.Reliable);
			messageWriter.StartMessage(8);
			messageWriter.Write(GameId);
			return messageWriter;
		}

		public void FinishEndGame(MessageWriter msg)
		{
			msg.EndMessage();
			SendOrDisconnect(msg);
			msg.Recycle();
		}

		protected void SendLateRejection(int targetId, DisconnectReasons reason)
		{
			MessageWriter messageWriter = MessageWriter.Get(SendOption.Reliable);
			messageWriter.StartMessage(4);
			messageWriter.Write(GameId);
			messageWriter.WritePacked(targetId);
			messageWriter.Write((byte)reason);
			messageWriter.EndMessage();
			SendOrDisconnect(messageWriter);
			messageWriter.Recycle();
		}

		protected void SendClientReady()
		{
			if (AmHost)
			{
				ClientData clientData = FindClientById(ClientId);
				if (clientData == null)
				{
					HandleDisconnect(DisconnectReasons.Error, "Couldn't find self as host");
				}
				else
				{
					clientData.IsReady = true;
				}
				return;
			}
			MessageWriter messageWriter = MessageWriter.Get(SendOption.Reliable);
			messageWriter.StartMessage(5);
			messageWriter.Write(GameId);
			messageWriter.StartMessage(7);
			messageWriter.WritePacked(ClientId);
			messageWriter.EndMessage();
			messageWriter.EndMessage();
			SendOrDisconnect(messageWriter);
			messageWriter.Recycle();
		}

		protected void SendStartGame()
		{
			MessageWriter messageWriter = MessageWriter.Get(SendOption.Reliable);
			messageWriter.StartMessage(2);
			messageWriter.Write(GameId);
			messageWriter.EndMessage();
			SendOrDisconnect(messageWriter);
			messageWriter.Recycle();
		}

		public void RequestGameList(bool includePrivate, IBytesSerializable settings)
		{
			MessageWriter messageWriter = MessageWriter.Get(SendOption.Reliable);
			messageWriter.StartMessage(9);
			messageWriter.Write(includePrivate);
			messageWriter.WriteBytesAndSize(settings.ToBytes());
			messageWriter.EndMessage();
			SendOrDisconnect(messageWriter);
			messageWriter.Recycle();
		}

		public void ChangeGamePublic(bool isPublic)
		{
			if (AmHost)
			{
				MessageWriter messageWriter = MessageWriter.Get(SendOption.Reliable);
				messageWriter.StartMessage(10);
				messageWriter.Write(GameId);
				messageWriter.Write((byte)1);
				messageWriter.Write(isPublic);
				messageWriter.EndMessage();
				SendOrDisconnect(messageWriter);
				messageWriter.Recycle();
				IsGamePublic = isPublic;
			}
		}

		private void OnMessageReceived(DataReceivedEventArgs e)
		{
			MessageReader message = e.Message;
			try
			{
				while (message.Position < message.Length)
				{
					HandleMessage(message.ReadMessage(), e.SendOption);
				}
			}
			finally
			{
				message.Recycle();
			}
		}

		private void HandleMessage(MessageReader reader, SendOption sendOption)
		{
			switch (reader.Tag)
			{
			case 14:
			{
				reader.ReadByte();
				int num12 = reader.ReadPackedInt32();
				ServerInfo[] array = new ServerInfo[num12];
				for (int j = 0; j < num12; j++)
				{
					MessageReader parts = reader.ReadMessage();
					array[j] = ServerInfo.Deserialize(parts);
				}
				DestroyableSingleton<ServerManager>.Instance.SetServers(array);
				break;
			}
			case 0:
				GameId = reader.ReadInt32();
				Debug.Log("Client hosting game: " + IntToGameName(GameId));
				lock (Dispatcher)
				{
					Dispatcher.Add(_003CHandleMessage_003Eb__70_0);
					break;
				}
			case 4:
			{
				int num3 = reader.ReadInt32();
				if (GameId != num3)
				{
					break;
				}
				int playerIdThatLeft = reader.ReadInt32();
				int hostId = reader.ReadInt32();
				DisconnectReasons reason = (DisconnectReasons)reader.ReadByte();
				if (!AmHost)
				{
					HostId = hostId;
					if (AmHost)
					{
						lock (Dispatcher)
						{
							Dispatcher.Add(_003CHandleMessage_003Eb__70_1);
						}
					}
				}
				RemovePlayer(playerIdThatLeft, reason);
				break;
			}
			case 8:
			{
				int num11 = reader.ReadInt32();
				if (GameId == num11 && GameState != GameStates.Ended)
				{
					_003C_003Ec__DisplayClass70_0 _003C_003Ec__DisplayClass70_6 = new _003C_003Ec__DisplayClass70_0();
					_003C_003Ec__DisplayClass70_6._003C_003E4__this = this;
					GameState = GameStates.Ended;
					lock (allClients)
					{
						allClients.Clear();
					}
					_003C_003Ec__DisplayClass70_6.reason = (GameOverReason)reader.ReadByte();
					_003C_003Ec__DisplayClass70_6.showAd = reader.ReadBoolean();
					lock (Dispatcher)
					{
						Dispatcher.Add(_003C_003Ec__DisplayClass70_6._003CHandleMessage_003Eb__2);
						break;
					}
				}
				break;
			}
			case 12:
			{
				int num2 = reader.ReadInt32();
				if (GameId == num2)
				{
					ClientId = reader.ReadInt32();
					lock (Dispatcher)
					{
						Dispatcher.Add(_003CHandleMessage_003Eb__70_3);
						break;
					}
				}
				break;
			}
			case 7:
			{
				int num6 = reader.ReadInt32();
				if (GameId == num6 && GameState != GameStates.Joined)
				{
					_003C_003Ec__DisplayClass70_1 _003C_003Ec__DisplayClass70_4 = new _003C_003Ec__DisplayClass70_1();
					_003C_003Ec__DisplayClass70_4._003C_003E4__this = this;
					GameState = GameStates.Joined;
					ClientId = reader.ReadInt32();
					_003C_003Ec__DisplayClass70_4.myClient = GetOrCreateClient(ClientId);
					HostId = reader.ReadInt32();
					int num7 = reader.ReadPackedInt32();
					for (int i = 0; i < num7; i++)
					{
						GetOrCreateClient(reader.ReadPackedInt32());
					}
					lock (Dispatcher)
					{
						Dispatcher.Add(_003C_003Ec__DisplayClass70_4._003CHandleMessage_003Eb__4);
						break;
					}
				}
				break;
			}
			case 1:
			{
				int num8 = reader.ReadInt32();
				DisconnectReasons disconnectReasons = (DisconnectReasons)num8;
				if (InnerNetClient.disconnectReasons.Contains(disconnectReasons))
				{
					if (disconnectReasons == DisconnectReasons.Custom)
					{
						LastCustomDisconnect = reader.ReadString();
					}
					GameId = -1;
					EnqueueDisconnect(disconnectReasons);
				}
				else if (GameId == num8)
				{
					_003C_003Ec__DisplayClass70_2 _003C_003Ec__DisplayClass70_5 = new _003C_003Ec__DisplayClass70_2();
					_003C_003Ec__DisplayClass70_5._003C_003E4__this = this;
					int num9 = reader.ReadInt32();
					bool amHost = AmHost;
					HostId = reader.ReadInt32();
					_003C_003Ec__DisplayClass70_5.client = GetOrCreateClient(num9);
					Debug.Log(string.Format("Player {0} joined", num9));
					lock (Dispatcher)
					{
						Dispatcher.Add(_003C_003Ec__DisplayClass70_5._003CHandleMessage_003Eb__5);
					}
					if (AmHost && !amHost)
					{
						lock (Dispatcher)
						{
							Dispatcher.Add(_003CHandleMessage_003Eb__70_6);
							break;
						}
					}
				}
				else
				{
					EnqueueDisconnect(DisconnectReasons.IncorrectGame);
				}
				break;
			}
			case 5:
			case 6:
			{
				int num4 = reader.ReadInt32();
				if (GameId != num4)
				{
					break;
				}
				if (reader.Tag == 6)
				{
					int num5 = reader.ReadPackedInt32();
					if (ClientId != num5)
					{
						Debug.LogError(string.Format("Got data meant for {0}", num5));
						break;
					}
				}
				if (InOnlineScene)
				{
					_003C_003Ec__DisplayClass70_3 _003C_003Ec__DisplayClass70_2 = new _003C_003Ec__DisplayClass70_3();
					_003C_003Ec__DisplayClass70_2._003C_003E4__this = this;
					_003C_003Ec__DisplayClass70_2.subReader = MessageReader.Get(reader);
					lock (Dispatcher)
					{
						Dispatcher.Add(_003C_003Ec__DisplayClass70_2._003CHandleMessage_003Eb__7);
						break;
					}
				}
				if (sendOption != 0)
				{
					_003C_003Ec__DisplayClass70_4 _003C_003Ec__DisplayClass70_3 = new _003C_003Ec__DisplayClass70_4();
					_003C_003Ec__DisplayClass70_3._003C_003E4__this = this;
					_003C_003Ec__DisplayClass70_3.subReader = MessageReader.Get(reader);
					lock (PreSpawnDispatcher)
					{
						PreSpawnDispatcher.Add(_003C_003Ec__DisplayClass70_3._003CHandleMessage_003Eb__8);
						break;
					}
				}
				break;
			}
			case 9:
			{
				_003C_003Ec__DisplayClass70_5 _003C_003Ec__DisplayClass70_ = new _003C_003Ec__DisplayClass70_5();
				_003C_003Ec__DisplayClass70_._003C_003E4__this = this;
				_003C_003Ec__DisplayClass70_.totalGames = reader.ReadPackedInt32();
				_003C_003Ec__DisplayClass70_.output = new List<GameListing>();
				while (reader.Position < reader.Length)
				{
					GameListing item = new GameListing(reader);
					if (item.Options == null)
					{
						EnqueueDisconnect(DisconnectReasons.IncorrectVersion);
						return;
					}
					_003C_003Ec__DisplayClass70_.output.Add(item);
				}
				lock (Dispatcher)
				{
					Dispatcher.Add(_003C_003Ec__DisplayClass70_._003CHandleMessage_003Eb__9);
					break;
				}
			}
			case 10:
			{
				int num10 = reader.ReadInt32();
				if (GameId == num10)
				{
					byte b = reader.ReadByte();
					if (b == 1)
					{
						IsGamePublic = reader.ReadBoolean();
						Debug.Log("Alter Public = " + IsGamePublic);
					}
					else
					{
						Debug.Log("Alter unknown");
					}
				}
				break;
			}
			case 3:
			{
				DisconnectReasons reason2 = DisconnectReasons.ServerRequest;
				if (reader.Position < reader.Length)
				{
					reason2 = (DisconnectReasons)reader.ReadByte();
				}
				EnqueueDisconnect(reason2);
				break;
			}
			case 2:
				GameState = GameStates.Started;
				lock (Dispatcher)
				{
					Dispatcher.Add(_003CHandleMessage_003Eb__70_10);
					break;
				}
			case 11:
			{
				int num = reader.ReadInt32();
				if (GameId == num && reader.ReadPackedInt32() == ClientId)
				{
					bool flag = reader.ReadBoolean();
					EnqueueDisconnect(flag ? DisconnectReasons.Banned : DisconnectReasons.Kicked);
				}
				break;
			}
			case 13:
			{
				uint address = reader.ReadUInt32();
				ushort port = reader.ReadUInt16();
				AmongUsClient.Instance.SetEndpoint(AddressToString(address), port);
				Debug.Log(string.Format("Redirected to: {0}:{1}", networkAddress, networkPort));
				lock (Dispatcher)
				{
					Dispatcher.Add(_003CHandleMessage_003Eb__70_11);
					break;
				}
			}
			default:
				Debug.Log(string.Format("Bad tag {0} at {1}+{2}={3}:  ", reader.Tag, reader.Offset, reader.Position, reader.Length) + string.Join(" ", reader.Buffer.Skip(reader.Offset).Take(reader.Length)));
				break;
			}
		}

		private static string AddressToString(uint address)
		{
			return string.Format("{0}.{1}.{2}.{3}", (byte)address, (byte)(address >> 8), (byte)(address >> 16), (byte)(address >> 24));
		}

		private ClientData GetOrCreateClient(int clientId)
		{
			_003C_003Ec__DisplayClass72_0 _003C_003Ec__DisplayClass72_ = new _003C_003Ec__DisplayClass72_0();
			_003C_003Ec__DisplayClass72_.clientId = clientId;
			lock (allClients)
			{
				ClientData clientData = allClients.FirstOrDefault(_003C_003Ec__DisplayClass72_._003CGetOrCreateClient_003Eb__0);
				if (clientData == null)
				{
					clientData = new ClientData(_003C_003Ec__DisplayClass72_.clientId);
					allClients.Add(clientData);
					return clientData;
				}
				return clientData;
			}
		}

		private void RemovePlayer(int playerIdThatLeft, DisconnectReasons reason)
		{
			_003C_003Ec__DisplayClass73_0 _003C_003Ec__DisplayClass73_ = new _003C_003Ec__DisplayClass73_0();
			_003C_003Ec__DisplayClass73_._003C_003E4__this = this;
			_003C_003Ec__DisplayClass73_.reason = reason;
			_003C_003Ec__DisplayClass73_.client = null;
			lock (allClients)
			{
				for (int i = 0; i < allClients.Count; i++)
				{
					ClientData clientData = allClients[i];
					if (clientData.Id == playerIdThatLeft)
					{
						_003C_003Ec__DisplayClass73_.client = clientData;
						allClients.RemoveAt(i);
						break;
					}
				}
			}
			if (_003C_003Ec__DisplayClass73_.client != null)
			{
				lock (Dispatcher)
				{
					Dispatcher.Add(_003C_003Ec__DisplayClass73_._003CRemovePlayer_003Eb__0);
				}
			}
		}

		protected virtual void OnApplicationPause(bool pause)
		{
			appPaused = pause;
			if (!pause)
			{
				Debug.Log("Resumed Game");
				if (AmHost)
				{
					RemoveUnownedObjects();
				}
			}
			else if (GameState != GameStates.Ended && AmConnected)
			{
				Debug.Log("Lost focus during game");
				ThreadPool.QueueUserWorkItem(WaitToDisconnect);
			}
		}

		private void WaitToDisconnect(object state)
		{
			for (int i = 0; i < 10; i++)
			{
				if (!appPaused)
				{
					break;
				}
				Thread.Sleep(1000);
			}
			if (appPaused && GameState != GameStates.Ended && AmConnected)
			{
				DisconnectInternal(DisconnectReasons.FocusLostBackground);
				EnqueueDisconnect(DisconnectReasons.FocusLost);
			}
		}

		protected void SendInitialData(int clientId)
		{
			MessageWriter messageWriter = MessageWriter.Get(SendOption.Reliable);
			messageWriter.StartMessage(6);
			messageWriter.Write(GameId);
			messageWriter.WritePacked(clientId);
			lock (allObjects)
			{
				HashSet<GameObject> hashSet = new HashSet<GameObject>();
				for (int i = 0; i < allObjects.Count; i++)
				{
					InnerNetObject innerNetObject = allObjects[i];
					if ((bool)innerNetObject && hashSet.Add(innerNetObject.gameObject))
					{
						WriteSpawnMessage(innerNetObject, innerNetObject.OwnerId, innerNetObject.SpawnFlags, messageWriter);
					}
				}
			}
			messageWriter.EndMessage();
			SendOrDisconnect(messageWriter);
			messageWriter.Recycle();
		}

		protected abstract void OnGameCreated(string gameIdString);

		protected abstract void OnGameJoined(string gameIdString, ClientData client);

		protected abstract void OnWaitForHost(string gameIdString);

		protected abstract void OnStartGame();

		protected abstract void OnGameEnd(GameOverReason reason, bool showAd);

		protected abstract void OnBecomeHost();

		protected abstract void OnPlayerJoined(ClientData client);

		protected abstract void OnPlayerChangedScene(ClientData client, string targetScene);

		protected abstract void OnPlayerLeft(ClientData client, DisconnectReasons reason);

		protected abstract void OnDisconnected();

		protected abstract void OnGetGameList(int totalGames, List<GameListing> availableGames);

		protected abstract byte[] GetConnectionData();

		protected ClientData FindClientById(int id)
		{
			if (id < 0)
			{
				return null;
			}
			lock (allClients)
			{
				for (int i = 0; i < allClients.Count; i++)
				{
					ClientData clientData = allClients[i];
					if (clientData.Id == id)
					{
						return clientData;
					}
				}
				return null;
			}
		}

		public static string IntToGameName(int gameId)
		{
			char[] array = new char[4]
			{
				(char)((uint)gameId & 0xFFu),
				(char)((uint)(gameId >> 8) & 0xFFu),
				(char)((uint)(gameId >> 16) & 0xFFu),
				(char)((uint)(gameId >> 24) & 0xFFu)
			};
			if (array.Any(_003C_003Ec._003C_003E9__91_0 ?? (_003C_003Ec._003C_003E9__91_0 = _003C_003Ec._003C_003E9._003CIntToGameName_003Eb__91_0)))
			{
				return null;
			}
			return new string(array);
		}

		public static int GameNameToInt(string gameId)
		{
			if (gameId.Length != 4)
			{
				return -1;
			}
			gameId = gameId.ToUpperInvariant();
			return (int)(gameId[0] | ((uint)gameId[1] << 8) | ((uint)gameId[2] << 16) | ((uint)gameId[3] << 24));
		}

		private void FixedUpdate()
		{
			if (mode == MatchMakerModes.None || Streams == null)
			{
				timer = 0f;
				return;
			}
			timer += Time.fixedDeltaTime;
			if (timer < MinSendInterval)
			{
				return;
			}
			timer = 0f;
			lock (allObjects)
			{
				for (int i = 0; i < allObjects.Count; i++)
				{
					InnerNetObject innerNetObject = allObjects[i];
					if ((bool)innerNetObject && innerNetObject.DirtyBits != 0 && (innerNetObject.AmOwner || (innerNetObject.OwnerId == -2 && AmHost)))
					{
						MessageWriter messageWriter = Streams[(uint)innerNetObject.sendMode];
						messageWriter.StartMessage(1);
						messageWriter.WritePacked(innerNetObject.NetId);
						if (innerNetObject.Serialize(messageWriter, false))
						{
							messageWriter.EndMessage();
						}
						else
						{
							messageWriter.CancelMessage();
						}
					}
				}
			}
			for (int j = 0; j < Streams.Length; j++)
			{
				MessageWriter messageWriter2 = Streams[j];
				if (messageWriter2.HasBytes(7))
				{
					messageWriter2.EndMessage();
					SendOrDisconnect(messageWriter2);
					messageWriter2.Clear((SendOption)j);
					messageWriter2.StartMessage(5);
					messageWriter2.Write(GameId);
				}
			}
		}

		public T FindObjectByNetId<T>(uint netId) where T : InnerNetObject
		{
			InnerNetObject value;
			if (allObjectsFast.TryGetValue(netId, out value))
			{
				return (T)value;
			}
			return null;
		}

		public void SendRpcImmediately(uint targetNetId, byte callId, SendOption option)
		{
			MessageWriter messageWriter = MessageWriter.Get(option);
			messageWriter.StartMessage(5);
			messageWriter.Write(GameId);
			messageWriter.StartMessage(2);
			messageWriter.WritePacked(targetNetId);
			messageWriter.Write(callId);
			messageWriter.EndMessage();
			messageWriter.EndMessage();
			connection.Send(messageWriter);
			messageWriter.Recycle();
		}

		public MessageWriter StartRpcImmediately(uint targetNetId, byte callId, SendOption option, int targetClientId = -1)
		{
			MessageWriter messageWriter = MessageWriter.Get(option);
			if (targetClientId < 0)
			{
				messageWriter.StartMessage(5);
				messageWriter.Write(GameId);
			}
			else
			{
				messageWriter.StartMessage(6);
				messageWriter.Write(GameId);
				messageWriter.WritePacked(targetClientId);
			}
			messageWriter.StartMessage(2);
			messageWriter.WritePacked(targetNetId);
			messageWriter.Write(callId);
			return messageWriter;
		}

		public void FinishRpcImmediately(MessageWriter msg)
		{
			msg.EndMessage();
			msg.EndMessage();
			SendOrDisconnect(msg);
			msg.Recycle();
		}

		public void SendRpc(uint targetNetId, byte callId, SendOption option = SendOption.Reliable)
		{
			StartRpc(targetNetId, callId, option).EndMessage();
		}

		public MessageWriter StartRpc(uint targetNetId, byte callId, SendOption option = SendOption.Reliable)
		{
			MessageWriter obj = Streams[(uint)option];
			obj.StartMessage(2);
			obj.WritePacked(targetNetId);
			obj.Write(callId);
			return obj;
		}

		private void SendSceneChange(string sceneName)
		{
			InOnlineScene = string.Equals(sceneName, "OnlineGame");
			if (AmConnected)
			{
				Debug.Log("Changed To " + sceneName);
				StartCoroutine(CoSendSceneChange(sceneName));
			}
		}

		private IEnumerator CoSendSceneChange(string sceneName)
		{
			_003C_003Ec__DisplayClass119_0 _003C_003Ec__DisplayClass119_ = new _003C_003Ec__DisplayClass119_0();
			_003C_003Ec__DisplayClass119_._003C_003E4__this = this;
			_003C_003Ec__DisplayClass119_.sceneName = sceneName;
			lock (allObjects)
			{
				for (int num = allObjects.Count - 1; num > -1; num--)
				{
					if (!allObjects[num])
					{
						allObjects.RemoveAt(num);
					}
				}
			}
			while (AmConnected && ClientId < 0)
			{
				yield return null;
			}
			if (!AmConnected)
			{
				yield break;
			}
			if (!AmHost && connection.State == ConnectionState.Connected)
			{
				MessageWriter messageWriter = MessageWriter.Get(SendOption.Reliable);
				messageWriter.StartMessage(5);
				messageWriter.Write(GameId);
				messageWriter.StartMessage(6);
				messageWriter.WritePacked(ClientId);
				messageWriter.Write(_003C_003Ec__DisplayClass119_.sceneName);
				messageWriter.EndMessage();
				messageWriter.EndMessage();
				SendOrDisconnect(messageWriter);
				messageWriter.Recycle();
			}
			_003C_003Ec__DisplayClass119_.client = FindClientById(ClientId);
			if (_003C_003Ec__DisplayClass119_.client != null)
			{
				Debug.Log(string.Format("Self changed scene: {0} {1}", ClientId, _003C_003Ec__DisplayClass119_.sceneName));
				lock (Dispatcher)
				{
					Dispatcher.Add(_003C_003Ec__DisplayClass119_._003CCoSendSceneChange_003Eb__0);
					yield break;
				}
			}
			Debug.Log(string.Format("Couldn't find self in clients: {0}: ", ClientId) + _003C_003Ec__DisplayClass119_.sceneName);
		}

		public void Spawn(InnerNetObject netObjParent, int ownerId = -2, SpawnFlags flags = SpawnFlags.None)
		{
			if (!AmHost)
			{
				if (AmClient)
				{
					Debug.LogError("Tried to spawn while not host:" + netObjParent);
				}
			}
			else
			{
				ownerId = ((ownerId == -3) ? ClientId : ownerId);
				MessageWriter msg = Streams[1];
				WriteSpawnMessage(netObjParent, ownerId, flags, msg);
			}
		}

		private void WriteSpawnMessage(InnerNetObject netObjParent, int ownerId, SpawnFlags flags, MessageWriter msg)
		{
			msg.StartMessage(4);
			msg.WritePacked(netObjParent.SpawnId);
			msg.WritePacked(ownerId);
			msg.Write((byte)flags);
			InnerNetObject[] componentsInChildren = netObjParent.GetComponentsInChildren<InnerNetObject>();
			msg.WritePacked(componentsInChildren.Length);
			foreach (InnerNetObject innerNetObject in componentsInChildren)
			{
				innerNetObject.OwnerId = ownerId;
				innerNetObject.SpawnFlags = flags;
				if (innerNetObject.NetId == 0)
				{
					innerNetObject.NetId = NetIdCnt++;
					allObjects.Add(innerNetObject);
					allObjectsFast.Add(innerNetObject.NetId, innerNetObject);
				}
				msg.WritePacked(innerNetObject.NetId);
				msg.StartMessage(1);
				innerNetObject.Serialize(msg, true);
				msg.EndMessage();
			}
			msg.EndMessage();
		}

		public void Despawn(InnerNetObject objToDespawn)
		{
			if (objToDespawn.NetId < 1)
			{
				Debug.LogError("Tried to net destroy: " + objToDespawn);
				return;
			}
			MessageWriter obj = Streams[1];
			obj.StartMessage(5);
			obj.WritePacked(objToDespawn.NetId);
			obj.EndMessage();
			RemoveNetObject(objToDespawn);
		}

		private bool AddNetObject(InnerNetObject obj)
		{
			uint num = obj.NetId + 1;
			if (num > NetIdCnt)
			{
				NetIdCnt = num;
			}
			if (!allObjectsFast.ContainsKey(obj.NetId))
			{
				allObjects.Add(obj);
				allObjectsFast.Add(obj.NetId, obj);
				return true;
			}
			return false;
		}

		public void RemoveNetObject(InnerNetObject obj)
		{
			int num = allObjects.IndexOf(obj);
			if (num > -1)
			{
				allObjects.RemoveAt(num);
			}
			allObjectsFast.Remove(obj.NetId);
			obj.NetId = uint.MaxValue;
		}

		public void RemoveUnownedObjects()
		{
			HashSet<int> hashSet = new HashSet<int>();
			hashSet.Add(-2);
			lock (allClients)
			{
				for (int i = 0; i < allClients.Count; i++)
				{
					ClientData clientData = allClients[i];
					if ((bool)clientData.Character)
					{
						hashSet.Add(clientData.Id);
					}
				}
			}
			lock (allObjects)
			{
				for (int num = allObjects.Count - 1; num > -1; num--)
				{
					InnerNetObject innerNetObject = allObjects[num];
					if (!innerNetObject)
					{
						allObjects.RemoveAt(num);
					}
					else if (!hashSet.Contains(innerNetObject.OwnerId))
					{
						innerNetObject.OwnerId = ClientId;
						UnityEngine.Object.Destroy(innerNetObject.gameObject);
					}
				}
			}
		}

		private void HandleGameData(MessageReader parentReader, int cnt = 0)
		{
			try
			{
				while (parentReader.Position < parentReader.Length)
				{
					HandleGameDataInner(parentReader.ReadMessage(), cnt);
				}
			}
			finally
			{
				parentReader.Recycle();
			}
		}

		private void HandleGameDataInner(MessageReader reader, int cnt)
		{
			switch (reader.Tag)
			{
			case 1:
			{
				uint num3 = reader.ReadPackedUInt32();
				InnerNetObject value;
				if (allObjectsFast.TryGetValue(num3, out value))
				{
					value.Deserialize(reader, false);
				}
				else if (!DestroyedObjects.Contains(num3))
				{
					DeferMessage(cnt, reader, "Stored data for " + num3);
				}
				break;
			}
			case 2:
			{
				uint num7 = reader.ReadPackedUInt32();
				InnerNetObject value2;
				if (allObjectsFast.TryGetValue(num7, out value2))
				{
					value2.HandleRpc(reader.ReadByte(), reader);
				}
				else if (num7 != uint.MaxValue && DestroyedObjects.Contains(num7))
				{
					DeferMessage(cnt, reader, "Stored RPC for " + num7);
				}
				break;
			}
			case 4:
			{
				uint num4 = reader.ReadPackedUInt32();
				if (num4 < SpawnableObjects.Length)
				{
					int num5 = reader.ReadPackedInt32();
					ClientData clientData2 = FindClientById(num5);
					if (num5 > 0 && clientData2 == null)
					{
						DeferMessage(cnt, reader, "Delay spawn for unowned " + num4);
						break;
					}
					InnerNetObject innerNetObject2 = UnityEngine.Object.Instantiate(SpawnableObjects[num4]);
					innerNetObject2.SpawnFlags = (SpawnFlags)reader.ReadByte();
					if ((innerNetObject2.SpawnFlags & SpawnFlags.IsClientCharacter) != 0)
					{
						if (!clientData2.Character)
						{
							clientData2.InScene = true;
							clientData2.Character = innerNetObject2 as PlayerControl;
						}
						else if ((bool)innerNetObject2)
						{
							Debug.LogWarning(string.Format("Double spawn character: {0} already has {1}", clientData2.Id, clientData2.Character.NetId));
							UnityEngine.Object.Destroy(innerNetObject2.gameObject);
							break;
						}
					}
					int num6 = reader.ReadPackedInt32();
					InnerNetObject[] componentsInChildren = innerNetObject2.GetComponentsInChildren<InnerNetObject>();
					if (num6 != componentsInChildren.Length)
					{
						Debug.LogError("Children didn't match for spawnable " + num4);
						UnityEngine.Object.Destroy(innerNetObject2.gameObject);
						break;
					}
					for (int i = 0; i < num6; i++)
					{
						InnerNetObject innerNetObject3 = componentsInChildren[i];
						innerNetObject3.NetId = reader.ReadPackedUInt32();
						innerNetObject3.OwnerId = num5;
						if (DestroyedObjects.Contains(innerNetObject3.NetId))
						{
							innerNetObject2.NetId = uint.MaxValue;
							UnityEngine.Object.Destroy(innerNetObject2.gameObject);
							break;
						}
						if (!AddNetObject(innerNetObject3))
						{
							innerNetObject2.NetId = uint.MaxValue;
							UnityEngine.Object.Destroy(innerNetObject2.gameObject);
							break;
						}
						MessageReader messageReader = reader.ReadMessage();
						if (messageReader.Length > 0)
						{
							innerNetObject3.Deserialize(messageReader, true);
						}
					}
				}
				else
				{
					Debug.LogError("Couldn't find spawnable prefab: " + num4);
				}
				break;
			}
			case 5:
			{
				uint num = reader.ReadPackedUInt32();
				DestroyedObjects.Add(num);
				InnerNetObject innerNetObject = FindObjectByNetId<InnerNetObject>(num);
				if ((bool)innerNetObject && !innerNetObject.AmOwner)
				{
					RemoveNetObject(innerNetObject);
					UnityEngine.Object.Destroy(innerNetObject.gameObject);
				}
				break;
			}
			case 6:
			{
				_003C_003Ec__DisplayClass127_0 _003C_003Ec__DisplayClass127_ = new _003C_003Ec__DisplayClass127_0();
				_003C_003Ec__DisplayClass127_._003C_003E4__this = this;
				int num2 = reader.ReadPackedInt32();
				_003C_003Ec__DisplayClass127_.client = FindClientById(num2);
				_003C_003Ec__DisplayClass127_.targetScene = reader.ReadString();
				if (_003C_003Ec__DisplayClass127_.client != null && !string.IsNullOrWhiteSpace(_003C_003Ec__DisplayClass127_.targetScene))
				{
					Debug.Log(string.Format("Client {0} changed scene to {1}", _003C_003Ec__DisplayClass127_.client.Id, _003C_003Ec__DisplayClass127_.targetScene));
					lock (Dispatcher)
					{
						Dispatcher.Add(_003C_003Ec__DisplayClass127_._003CHandleGameDataInner_003Eb__0);
						break;
					}
				}
				Debug.Log(string.Format("Couldn't find client {0} to change scene to {1}", num2, _003C_003Ec__DisplayClass127_.targetScene));
				break;
			}
			case 7:
			{
				ClientData clientData = FindClientById(reader.ReadPackedInt32());
				if (clientData != null)
				{
					Debug.Log(string.Format("Client {0} ready", clientData.Id));
					clientData.IsReady = true;
				}
				break;
			}
			default:
				Debug.Log(string.Format("Bad tag {0} at {1}+{2}={3}:  ", reader.Tag, reader.Offset, reader.Position, reader.Length) + string.Join(" ", reader.Buffer));
				break;
			}
		}

		private void DeferMessage(int cnt, MessageReader reader, string logMsg)
		{
			_003C_003Ec__DisplayClass128_0 _003C_003Ec__DisplayClass128_ = new _003C_003Ec__DisplayClass128_0();
			_003C_003Ec__DisplayClass128_._003C_003E4__this = this;
			_003C_003Ec__DisplayClass128_.cnt = cnt;
			if (_003C_003Ec__DisplayClass128_.cnt > 10)
			{
				Debug.Log("Giving up on: " + logMsg);
				return;
			}
			_003C_003Ec__DisplayClass128_.cnt++;
			Debug.Log(logMsg);
			_003C_003Ec__DisplayClass128_.copy = MessageReader.CopyMessageIntoParent(reader);
			lock (PreSpawnDispatcher)
			{
				PreSpawnDispatcher.Add(_003C_003Ec__DisplayClass128_._003CDeferMessage_003Eb__0);
			}
		}

		[CompilerGenerated]
		private void _003CStart_003Eb__39_0(Scene oldScene, Scene scene)
		{
			SendSceneChange(scene.name);
		}

		[CompilerGenerated]
		private bool _003CCoConnect_003Eb__44_0()
		{
			if (connection != null)
			{
				return connection.State == ConnectionState.Connected;
			}
			return true;
		}

		[CompilerGenerated]
		private bool _003CCoConnect_003Eb__48_0()
		{
			return ClientId >= 0;
		}

		[CompilerGenerated]
		private bool _003CCoConnect_003Eb__48_1()
		{
			return GameId != 0;
		}

		[CompilerGenerated]
		private bool _003CCoConnect_003Eb__48_2()
		{
			return ClientId >= 0;
		}

		[CompilerGenerated]
		private void _003CHandleMessage_003Eb__70_0()
		{
			OnGameCreated(IntToGameName(GameId));
		}

		[CompilerGenerated]
		private void _003CHandleMessage_003Eb__70_1()
		{
			OnBecomeHost();
		}

		[CompilerGenerated]
		private void _003CHandleMessage_003Eb__70_3()
		{
			OnWaitForHost(IntToGameName(GameId));
		}

		[CompilerGenerated]
		private void _003CHandleMessage_003Eb__70_6()
		{
			OnBecomeHost();
		}

		[CompilerGenerated]
		private void _003CHandleMessage_003Eb__70_10()
		{
			OnStartGame();
		}

		[CompilerGenerated]
		private void _003CHandleMessage_003Eb__70_11()
		{
			StopAllCoroutines();
			Connect(mode);
		}
	}
}
