using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading;
using Hazel;
using Hazel.Udp;
using UnityEngine;

namespace InnerNet
{
	public class InnerNetServer : DestroyableSingleton<InnerNetServer>
	{
		protected class Player
		{
			private static int IdCount = 1;

			public int Id;

			public Connection Connection;

			public LimboStates LimboState;

			public Player(Connection connection)
			{
				Id = Interlocked.Increment(ref IdCount);
				Connection = connection;
			}
		}

		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass15_0
		{
			public InnerNetServer _003C_003E4__this;

			public Player client;

			internal void _003COnServerConnect_003Eb__0(DataReceivedEventArgs e)
			{
				_003C_003E4__this.OnDataReceived(client, e);
			}

			internal void _003COnServerConnect_003Eb__1(object o, DisconnectedEventArgs e)
			{
				_003C_003E4__this.ClientDisconnect(client);
			}
		}

		[Serializable]
		[CompilerGenerated]
		private sealed class _003C_003Ec
		{
			public static readonly _003C_003Ec _003C_003E9 = new _003C_003Ec();

			public static Func<byte, string> _003C_003E9__17_0;

			internal string _003CConnection_DataSentRaw_003Eb__17_0(byte b)
			{
				return b.ToString();
			}
		}

		public const int MaxPlayers = 10;

		public bool Running;

		public const int LocalGameId = 32;

		private const int InvalidHost = -1;

		private int HostId = -1;

		public HashSet<string> ipBans = new HashSet<string>();

		public int Port = 22023;

		[SerializeField]
		private GameStates GameState;

		private NetworkConnectionListener listener;

		private List<Player> Clients = new List<Player>();

		public override void OnDestroy()
		{
			StopServer();
			base.OnDestroy();
		}

		public void StartAsServer()
		{
			if (listener != null)
			{
				StopServer();
			}
			GameState = GameStates.NotStarted;
			listener = new UdpConnectionListener(new IPEndPoint(IPAddress.Any, Port));
			listener.NewConnection += OnServerConnect;
			listener.Start();
			Running = true;
		}

		public void StopServer()
		{
			HostId = -1;
			Running = false;
			GameState = GameStates.Destroyed;
			if (listener != null)
			{
				listener.Close();
				listener.Dispose();
				listener = null;
			}
			lock (Clients)
			{
				Clients.Clear();
			}
		}

		public static bool IsCompatibleVersion(int version)
		{
			return Constants.CompatVersions.Contains(version);
		}

		private void OnServerConnect(NewConnectionEventArgs evt)
		{
			_003C_003Ec__DisplayClass15_0 _003C_003Ec__DisplayClass15_ = new _003C_003Ec__DisplayClass15_0();
			_003C_003Ec__DisplayClass15_._003C_003E4__this = this;
			MessageReader handshakeData = evt.HandshakeData;
			try
			{
				if (evt.HandshakeData.Length < 5)
				{
					SendIncorrectVersion(evt.Connection);
					return;
				}
				if (!IsCompatibleVersion(handshakeData.ReadInt32()))
				{
					SendIncorrectVersion(evt.Connection);
					return;
				}
			}
			finally
			{
				handshakeData.Recycle();
			}
			_003C_003Ec__DisplayClass15_.client = new Player(evt.Connection);
			Debug.Log(string.Format("Client {0} added: {1}", _003C_003Ec__DisplayClass15_.client.Id, evt.Connection.EndPoint));
			UdpConnection obj = (UdpConnection)evt.Connection;
			obj.KeepAliveInterval = 1500;
			obj.DisconnectTimeout = 6000;
			obj.ResendPingMultiplier = 1.5f;
			obj.DataReceived += _003C_003Ec__DisplayClass15_._003COnServerConnect_003Eb__0;
			obj.Disconnected += _003C_003Ec__DisplayClass15_._003COnServerConnect_003Eb__1;
		}

		private static void SendIncorrectVersion(Connection connection)
		{
			MessageWriter messageWriter = MessageWriter.Get(SendOption.Reliable);
			messageWriter.StartMessage(1);
			messageWriter.Write(5);
			messageWriter.EndMessage();
			connection.Send(messageWriter);
			messageWriter.Recycle();
		}

		private void Connection_DataSentRaw(byte[] data, int length)
		{
			Debug.Log("Server Sent: " + string.Join(" ", data.Select(_003C_003Ec._003C_003E9__17_0 ?? (_003C_003Ec._003C_003E9__17_0 = _003C_003Ec._003C_003E9._003CConnection_DataSentRaw_003Eb__17_0)).ToArray(), 0, length));
		}

		private void OnDataReceived(Player client, DataReceivedEventArgs evt)
		{
			MessageReader message = evt.Message;
			if (message.Length <= 0)
			{
				Debug.Log("Server got 0 bytes");
				message.Recycle();
				return;
			}
			try
			{
				while (message.Position < message.Length)
				{
					HandleMessage(client, message.ReadMessage(), evt.SendOption);
				}
			}
			catch (Exception arg)
			{
				Debug.Log(string.Format("{0}\r\n{1}", string.Join(" ", message.Buffer), arg));
			}
			finally
			{
				message.Recycle();
			}
		}

		private void HandleMessage(Player client, MessageReader reader, SendOption sendOption)
		{
			switch (reader.Tag)
			{
			case 1:
			{
				Debug.Log("Server got join game");
				if (reader.ReadInt32() == 32)
				{
					JoinGame(client);
					break;
				}
				MessageWriter messageWriter = MessageWriter.Get(SendOption.Reliable);
				messageWriter.StartMessage(1);
				messageWriter.Write((byte)3);
				messageWriter.EndMessage();
				client.Connection.Send(messageWriter);
				messageWriter.Recycle();
				break;
			}
			case 0:
			{
				Debug.Log("Server got host game");
				MessageWriter messageWriter3 = MessageWriter.Get(SendOption.Reliable);
				messageWriter3.StartMessage(0);
				messageWriter3.Write(32);
				messageWriter3.EndMessage();
				client.Connection.Send(messageWriter3);
				messageWriter3.Recycle();
				break;
			}
			case 3:
				if (reader.ReadInt32() == 32)
				{
					ClientDisconnect(client);
				}
				break;
			case 2:
				if (reader.ReadInt32() == 32)
				{
					StartGame(reader, client);
				}
				break;
			case 8:
				if (reader.ReadInt32() == 32)
				{
					EndGame(reader, client);
				}
				break;
			case 6:
				if (Clients.Contains(client))
				{
					if (reader.ReadInt32() == 32)
					{
						int targetId = reader.ReadPackedInt32();
						MessageWriter messageWriter4 = MessageWriter.Get(sendOption);
						messageWriter4.CopyFrom(reader);
						SendTo(messageWriter4, targetId);
						messageWriter4.Recycle();
					}
				}
				else if (GameState == GameStates.Started)
				{
					Debug.Log("GameDataTo: Server didn't have client");
					client.Connection.Dispose();
				}
				break;
			case 5:
				if (Clients.Contains(client))
				{
					if (reader.ReadInt32() == 32)
					{
						MessageWriter messageWriter2 = MessageWriter.Get(sendOption);
						messageWriter2.CopyFrom(reader);
						Broadcast(messageWriter2, client);
						messageWriter2.Recycle();
					}
				}
				else if (GameState == GameStates.Started)
				{
					client.Connection.Dispose();
				}
				break;
			case 11:
				if (reader.ReadInt32() == 32)
				{
					KickPlayer(reader.ReadPackedInt32(), reader.ReadBoolean());
				}
				break;
			case 4:
			case 7:
			case 9:
			case 10:
				break;
			}
		}

		private void KickPlayer(int targetId, bool ban)
		{
			lock (Clients)
			{
				Player player = null;
				for (int i = 0; i < Clients.Count; i++)
				{
					if (Clients[i].Id == targetId)
					{
						player = Clients[i];
						break;
					}
				}
				if (player == null)
				{
					return;
				}
				if (ban)
				{
					lock (ipBans)
					{
						IPEndPoint endPoint = player.Connection.EndPoint;
						ipBans.Add(endPoint.Address.ToString());
					}
				}
				MessageWriter messageWriter = MessageWriter.Get(SendOption.Reliable);
				messageWriter.StartMessage(11);
				messageWriter.Write(32);
				messageWriter.WritePacked(targetId);
				messageWriter.Write(ban);
				messageWriter.EndMessage();
				Broadcast(messageWriter, null);
				messageWriter.Recycle();
			}
		}

		protected void JoinGame(Player client)
		{
			lock (ipBans)
			{
				IPEndPoint endPoint = client.Connection.EndPoint;
				if (ipBans.Contains(endPoint.Address.ToString()))
				{
					MessageWriter messageWriter = MessageWriter.Get(SendOption.Reliable);
					messageWriter.StartMessage(1);
					messageWriter.Write(6);
					messageWriter.EndMessage();
					client.Connection.Send(messageWriter);
					messageWriter.Recycle();
					return;
				}
			}
			lock (Clients)
			{
				switch (GameState)
				{
				case GameStates.NotStarted:
					HandleNewGameJoin(client);
					return;
				case GameStates.Ended:
					HandleRejoin(client);
					return;
				}
				MessageWriter messageWriter2 = MessageWriter.Get(SendOption.Reliable);
				messageWriter2.StartMessage(1);
				messageWriter2.Write(2);
				messageWriter2.EndMessage();
				client.Connection.Send(messageWriter2);
				messageWriter2.Recycle();
			}
		}

		private void HandleRejoin(Player client)
		{
			if (client.Id == HostId)
			{
				GameState = GameStates.NotStarted;
				HandleNewGameJoin(client);
				MessageWriter messageWriter = MessageWriter.Get(SendOption.Reliable);
				for (int i = 0; i < Clients.Count; i++)
				{
					Player player = Clients[i];
					if (player != client)
					{
						try
						{
							WriteJoinedMessage(player, messageWriter, true);
							player.Connection.Send(messageWriter);
						}
						catch
						{
						}
					}
				}
				messageWriter.Recycle();
				return;
			}
			if (Clients.Count >= 9)
			{
				MessageWriter messageWriter2 = MessageWriter.Get(SendOption.Reliable);
				messageWriter2.StartMessage(1);
				messageWriter2.Write(1);
				messageWriter2.EndMessage();
				client.Connection.Send(messageWriter2);
				messageWriter2.Recycle();
				return;
			}
			Clients.Add(client);
			client.LimboState = LimboStates.WaitingForHost;
			MessageWriter messageWriter3 = MessageWriter.Get(SendOption.Reliable);
			try
			{
				messageWriter3.StartMessage(12);
				messageWriter3.Write(32);
				messageWriter3.Write(client.Id);
				messageWriter3.EndMessage();
				client.Connection.Send(messageWriter3);
				BroadcastJoinMessage(client, messageWriter3);
			}
			catch
			{
			}
			finally
			{
				messageWriter3.Recycle();
			}
		}

		private void HandleNewGameJoin(Player client)
		{
			if (Clients.Count >= 10)
			{
				MessageWriter messageWriter = MessageWriter.Get(SendOption.Reliable);
				try
				{
					messageWriter.StartMessage(1);
					messageWriter.Write(1);
					messageWriter.EndMessage();
					client.Connection.Send(messageWriter);
					return;
				}
				catch
				{
					return;
				}
				finally
				{
					messageWriter.Recycle();
				}
			}
			Clients.Add(client);
			client.LimboState = LimboStates.PreSpawn;
			if (HostId == -1)
			{
				HostId = Clients[0].Id;
			}
			if (HostId == client.Id)
			{
				client.LimboState = LimboStates.NotLimbo;
			}
			MessageWriter messageWriter2 = MessageWriter.Get(SendOption.Reliable);
			try
			{
				WriteJoinedMessage(client, messageWriter2, true);
				client.Connection.Send(messageWriter2);
				BroadcastJoinMessage(client, messageWriter2);
			}
			catch
			{
			}
			finally
			{
				messageWriter2.Recycle();
			}
		}

		private void EndGame(MessageReader message, Player source)
		{
			if (source.Id == HostId)
			{
				GameState = GameStates.Ended;
				MessageWriter messageWriter = MessageWriter.Get(SendOption.Reliable);
				messageWriter.CopyFrom(message);
				Broadcast(messageWriter, null);
				messageWriter.Recycle();
				lock (Clients)
				{
					Clients.Clear();
					return;
				}
			}
			Debug.LogWarning("Reset request rejected from: " + source.Id);
		}

		private void StartGame(MessageReader message, Player source)
		{
			GameState = GameStates.Started;
			MessageWriter messageWriter = MessageWriter.Get(SendOption.Reliable);
			messageWriter.CopyFrom(message);
			Broadcast(messageWriter, null);
			messageWriter.Recycle();
		}

		private void ClientDisconnect(Player client)
		{
			Debug.Log("Server DC client " + client.Id);
			lock (Clients)
			{
				Clients.Remove(client);
				client.Connection.Dispose();
				if (Clients.Count > 0)
				{
					HostId = Clients[0].Id;
				}
			}
			MessageWriter messageWriter = MessageWriter.Get(SendOption.Reliable);
			messageWriter.StartMessage(4);
			messageWriter.Write(32);
			messageWriter.Write(client.Id);
			messageWriter.Write(HostId);
			messageWriter.Write((byte)0);
			messageWriter.EndMessage();
			Broadcast(messageWriter, null);
			messageWriter.Recycle();
		}

		protected void SendTo(MessageWriter msg, int targetId)
		{
			lock (Clients)
			{
				for (int i = 0; i < Clients.Count; i++)
				{
					Player player = Clients[i];
					if (player.Id == targetId)
					{
						try
						{
							player.Connection.Send(msg);
							break;
						}
						catch (Exception exception)
						{
							Debug.LogException(exception);
							break;
						}
					}
				}
			}
		}

		protected void Broadcast(MessageWriter msg, Player source)
		{
			lock (Clients)
			{
				for (int i = 0; i < Clients.Count; i++)
				{
					Player player = Clients[i];
					if (player != source)
					{
						try
						{
							player.Connection.Send(msg);
						}
						catch
						{
						}
					}
				}
			}
		}

		private void BroadcastJoinMessage(Player client, MessageWriter msg)
		{
			msg.Clear(SendOption.Reliable);
			msg.StartMessage(1);
			msg.Write(32);
			msg.Write(client.Id);
			msg.Write(HostId);
			msg.EndMessage();
			Broadcast(msg, client);
		}

		private void WriteJoinedMessage(Player client, MessageWriter msg, bool clear)
		{
			if (clear)
			{
				msg.Clear(SendOption.Reliable);
			}
			msg.StartMessage(7);
			msg.Write(32);
			msg.Write(client.Id);
			msg.Write(HostId);
			msg.WritePacked(Clients.Count - 1);
			for (int i = 0; i < Clients.Count; i++)
			{
				Player player = Clients[i];
				if (player != client)
				{
					msg.WritePacked(player.Id);
				}
			}
			msg.EndMessage();
		}
	}
}
