namespace InnerNet
{
	public enum MatchMakerModes
	{
		None = 0,
		Client = 1,
		HostAndClient = 2
	}
}
