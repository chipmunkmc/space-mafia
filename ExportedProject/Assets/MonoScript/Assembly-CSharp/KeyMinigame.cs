using UnityEngine;

public class KeyMinigame : Minigame
{
	public KeySlotBehaviour[] Slots;

	private int keyState;

	public SpriteRenderer KeyImage;

	public Sprite normalImage;

	public Sprite insertImage;

	public BoxCollider2D key;

	private int targetSlotId;

	private Controller controller = new Controller();

	public AudioClip KeyGrab;

	public AudioClip KeyInsert;

	public AudioClip KeyOpen;

	public AudioClip KeyTurn;

	public void Start()
	{
		PlayerControl localPlayer = PlayerControl.LocalPlayer;
		targetSlotId = (((object)localPlayer != null) ? localPlayer.PlayerId : 0);
		Slots[targetSlotId].SetHighlight();
	}

	public void Update()
	{
		if (keyState == 2)
		{
			return;
		}
		KeySlotBehaviour keySlotBehaviour = Slots[targetSlotId];
		controller.Update();
		switch (controller.CheckDrag(key))
		{
		case DragState.TouchStart:
			if (keyState == 0)
			{
				if (Constants.ShouldPlaySfx())
				{
					SoundManager.Instance.PlaySound(KeyGrab, false);
				}
			}
			else if (Constants.ShouldPlaySfx())
			{
				SoundManager.Instance.PlaySound(KeyTurn, false);
			}
			break;
		case DragState.Dragging:
			if (keyState == 0)
			{
				Vector2 vector = controller.DragPosition - (Vector2)base.transform.position;
				key.transform.localPosition = vector;
				if (key.IsTouching(keySlotBehaviour.Hitbox))
				{
					KeyImage.sprite = insertImage;
				}
				else
				{
					KeyImage.sprite = normalImage;
				}
			}
			else
			{
				Vector2 vector2 = key.transform.position;
				float value = Vector2.SignedAngle(controller.DragStartPosition - vector2, controller.DragPosition - vector2);
				key.transform.localEulerAngles = new Vector3(0f, 0f, Mathf.Clamp(value, -90f, 90f));
			}
			break;
		case DragState.Released:
		{
			if (keyState == 0)
			{
				if (key.IsTouching(keySlotBehaviour.Hitbox))
				{
					if (Constants.ShouldPlaySfx())
					{
						SoundManager.Instance.PlaySound(KeyInsert, false);
					}
					keyState = 1;
					key.size = new Vector2(2f, 2f);
					Vector3 position = keySlotBehaviour.transform.position;
					position.z -= 1f;
					key.transform.position = position;
					KeyImage.sprite = insertImage;
					keySlotBehaviour.SetInserted();
				}
				break;
			}
			float num = key.transform.localEulerAngles.z;
			if (num > 180f)
			{
				num -= 360f;
			}
			num %= 360f;
			if (Mathf.Abs(num) > 80f)
			{
				if (Constants.ShouldPlaySfx())
				{
					SoundManager.Instance.PlaySound(KeyOpen, false);
				}
				keySlotBehaviour.SetFinished();
				key.transform.localEulerAngles = new Vector3(0f, 0f, 90f);
				keyState = 2;
				MyNormTask.NextStep();
				StartCoroutine(CoStartClose());
			}
			else
			{
				key.transform.localEulerAngles = new Vector3(0f, 0f, 0f);
			}
			break;
		}
		case DragState.Holding:
			break;
		}
	}
}
