public enum KillAnimType
{
	Stab = 0,
	Tongue = 1,
	Shoot = 2,
	Neck = 3,
	None = 4
}
