using System.Collections;
using PowerTools;
using UnityEngine;

public class KillAnimation : MonoBehaviour
{
	public AnimationClip BlurAnim;

	public DeadBody bodyPrefab;

	public Vector3 BodyOffset;

	public IEnumerator CoPerformKill(PlayerControl source, PlayerControl target)
	{
		bool isParticipant = PlayerControl.LocalPlayer == source || PlayerControl.LocalPlayer == target;
		PlayerPhysics sourcePhys = source.MyPhysics;
		SetMovement(source, false);
		SetMovement(target, false);
		if (isParticipant)
		{
			Camera.main.GetComponent<FollowerCamera>().Locked = true;
		}
		target.Die(DeathReason.Kill);
		SpriteAnim sourceAnim = source.GetComponent<SpriteAnim>();
		yield return new WaitForAnimationFinish(sourceAnim, BlurAnim);
		source.NetTransform.SnapTo(target.transform.position);
		sourceAnim.Play(sourcePhys.IdleAnim);
		SetMovement(source, true);
		DeadBody deadBody = Object.Instantiate(bodyPrefab);
		Vector3 position = target.transform.position + BodyOffset;
		position.z = position.y / 1000f;
		deadBody.transform.position = position;
		deadBody.ParentId = target.PlayerId;
		target.SetPlayerMaterialColors(deadBody.GetComponent<Renderer>());
		SetMovement(target, true);
		if (isParticipant)
		{
			Camera.main.GetComponent<FollowerCamera>().Locked = false;
		}
	}

	public static void SetMovement(PlayerControl source, bool canMove)
	{
		source.moveable = canMove;
		source.MyPhysics.ResetAnim(false);
		source.NetTransform.enabled = canMove;
		source.MyPhysics.enabled = canMove;
		source.NetTransform.Halt();
	}
}
