using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class KillOverlay : MonoBehaviour
{
	[CompilerGenerated]
	private sealed class _003C_003Ec__DisplayClass12_0
	{
		public KillOverlay _003C_003E4__this;

		public PlayerControl killer;

		public GameData.PlayerInfo victim;

		internal IEnumerator _003CShowOne_003Eb__0()
		{
			return _003C_003E4__this.CoShowOne(_003C_003E4__this.KillAnims.Random(), killer, victim);
		}
	}

	[CompilerGenerated]
	private sealed class _003C_003Ec__DisplayClass13_0
	{
		public KillOverlay _003C_003E4__this;

		public OverlayKillAnimation killAnimPrefab;

		public PlayerControl killer;

		public GameData.PlayerInfo victim;

		internal IEnumerator _003CShowOne_003Eb__0()
		{
			return _003C_003E4__this.CoShowOne(killAnimPrefab, killer, victim);
		}
	}

	public SpriteRenderer background;

	public GameObject flameParent;

	public OverlayKillAnimation[] KillAnims;

	public float FadeTime = 0.6f;

	public OverlayKillAnimation EmergencyOverlay;

	public OverlayKillAnimation ReportOverlay;

	private Queue<Func<IEnumerator>> queue = new Queue<Func<IEnumerator>>();

	private Coroutine showAll;

	private Coroutine showOne;

	public bool IsOpen
	{
		get
		{
			if (showAll == null)
			{
				return queue.Count > 0;
			}
			return true;
		}
	}

	public IEnumerator WaitForFinish()
	{
		while (showAll != null || queue.Count > 0)
		{
			yield return null;
		}
	}

	public void ShowOne(PlayerControl killer, GameData.PlayerInfo victim)
	{
		_003C_003Ec__DisplayClass12_0 _003C_003Ec__DisplayClass12_ = new _003C_003Ec__DisplayClass12_0();
		_003C_003Ec__DisplayClass12_._003C_003E4__this = this;
		_003C_003Ec__DisplayClass12_.killer = killer;
		_003C_003Ec__DisplayClass12_.victim = victim;
		queue.Enqueue(_003C_003Ec__DisplayClass12_._003CShowOne_003Eb__0);
		if (showAll == null)
		{
			showAll = StartCoroutine(ShowAll());
		}
	}

	public void ShowOne(OverlayKillAnimation killAnimPrefab, PlayerControl killer, GameData.PlayerInfo victim)
	{
		_003C_003Ec__DisplayClass13_0 _003C_003Ec__DisplayClass13_ = new _003C_003Ec__DisplayClass13_0();
		_003C_003Ec__DisplayClass13_._003C_003E4__this = this;
		_003C_003Ec__DisplayClass13_.killAnimPrefab = killAnimPrefab;
		_003C_003Ec__DisplayClass13_.killer = killer;
		_003C_003Ec__DisplayClass13_.victim = victim;
		queue.Enqueue(_003C_003Ec__DisplayClass13_._003CShowOne_003Eb__0);
		if (showAll == null)
		{
			showAll = StartCoroutine(ShowAll());
		}
	}

	private IEnumerator ShowAll()
	{
		while (queue.Count > 0 || showOne != null)
		{
			if (showOne == null)
			{
				showOne = StartCoroutine(queue.Dequeue()());
			}
			yield return null;
		}
		showAll = null;
	}

	private IEnumerator CoShowOne(OverlayKillAnimation killAnimPrefab, PlayerControl killer, GameData.PlayerInfo victim)
	{
		OverlayKillAnimation overlayKillAnimation = UnityEngine.Object.Instantiate(killAnimPrefab, base.transform);
		overlayKillAnimation.Begin(killer, victim);
		overlayKillAnimation.gameObject.SetActive(false);
		yield return CoShowOne(overlayKillAnimation);
	}

	private IEnumerator CoShowOne(OverlayKillAnimation anim)
	{
		if (Constants.ShouldPlaySfx())
		{
			SoundManager.Instance.PlaySound(anim.Stinger, false).volume = anim.StingerVolume;
		}
		WaitForSeconds wait = new WaitForSeconds(1f / 12f);
		background.enabled = true;
		yield return wait;
		background.enabled = false;
		flameParent.SetActive(true);
		flameParent.transform.localScale = new Vector3(1f, 0.3f, 1f);
		flameParent.transform.localEulerAngles = new Vector3(0f, 0f, 25f);
		yield return wait;
		flameParent.transform.localScale = new Vector3(1f, 0.5f, 1f);
		flameParent.transform.localEulerAngles = new Vector3(0f, 0f, -15f);
		yield return wait;
		flameParent.transform.localScale = new Vector3(1f, 1f, 1f);
		flameParent.transform.localEulerAngles = new Vector3(0f, 0f, 0f);
		anim.gameObject.SetActive(true);
		yield return anim.WaitForFinish();
		UnityEngine.Object.Destroy(anim.gameObject);
		yield return new WaitForLerp(1f / 6f, _003CCoShowOne_003Eb__16_0);
		flameParent.SetActive(false);
		showOne = null;
	}

	[CompilerGenerated]
	private void _003CCoShowOne_003Eb__16_0(float t)
	{
		flameParent.transform.localScale = new Vector3(1f, 1f - t, 1f);
	}
}
