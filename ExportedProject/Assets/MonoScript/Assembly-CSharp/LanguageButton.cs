using UnityEngine;

public class LanguageButton : MonoBehaviour
{
	public TextRenderer Title;

	public PassiveButton Button;

	[HideInInspector]
	public TextAsset Language;
}
