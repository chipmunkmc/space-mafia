using System.Runtime.CompilerServices;
using UnityEngine;

public class LanguageSetter : MonoBehaviour
{
	[CompilerGenerated]
	private sealed class _003C_003Ec__DisplayClass5_0
	{
		public LanguageButton button;

		public LanguageSetter _003C_003E4__this;

		internal void _003CStart_003Eb__0()
		{
			_003C_003E4__this.SetLanguage(button);
		}
	}

	public LanguageButton ButtonPrefab;

	public Scroller ButtonParent;

	public float ButtonStart = 0.5f;

	public float ButtonHeight = 0.5f;

	private LanguageButton[] AllButtons;

	public void Start()
	{
		TextAsset[] languages = DestroyableSingleton<TranslationController>.Instance.Languages;
		Vector3 localPosition = new Vector3(0f, ButtonStart, -1f);
		AllButtons = new LanguageButton[languages.Length];
		for (int i = 0; i < languages.Length; i++)
		{
			_003C_003Ec__DisplayClass5_0 _003C_003Ec__DisplayClass5_ = new _003C_003Ec__DisplayClass5_0();
			_003C_003Ec__DisplayClass5_._003C_003E4__this = this;
			_003C_003Ec__DisplayClass5_.button = Object.Instantiate(ButtonPrefab, ButtonParent.Inner);
			AllButtons[i] = _003C_003Ec__DisplayClass5_.button;
			_003C_003Ec__DisplayClass5_.button.Language = languages[i];
			_003C_003Ec__DisplayClass5_.button.Title.Text = languages[i].name;
			if (i == SaveManager.LastLanguage)
			{
				_003C_003Ec__DisplayClass5_.button.Title.Color = Color.green;
			}
			_003C_003Ec__DisplayClass5_.button.Button.OnClick.AddListener(_003C_003Ec__DisplayClass5_._003CStart_003Eb__0);
			_003C_003Ec__DisplayClass5_.button.transform.localPosition = localPosition;
			localPosition.y -= ButtonHeight;
		}
		ButtonParent.YBounds.max = Mathf.Max(0f, 0f - localPosition.y - ButtonStart * 2f);
	}

	public void SetLanguage(LanguageButton selected)
	{
		for (int i = 0; i < AllButtons.Length; i++)
		{
			AllButtons[i].Title.Color = Color.white;
		}
		selected.Title.Color = Color.green;
		DestroyableSingleton<TranslationController>.Instance.SetLanguage(selected.Language);
	}
}
