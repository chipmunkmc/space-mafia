using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class LanguageUnit
{
	public bool IsEnglish;

	private Dictionary<StringNames, string> AllStrings = new Dictionary<StringNames, string>();

	private Dictionary<ImageNames, Sprite> AllImages = new Dictionary<ImageNames, Sprite>();

	private StringBuilder builder = new StringBuilder(512);

	public LanguageUnit(TextAsset data, ImageData[] images)
	{
		for (int i = 0; i < images.Length; i++)
		{
			ImageData imageData = images[i];
			AllImages.Add(imageData.Name, imageData.Sprite);
		}
		using (StringReader stringReader = new StringReader(data.text))
		{
			for (string text = stringReader.ReadLine(); text != null; text = stringReader.ReadLine())
			{
				if (text.Length != 0)
				{
					int num = text.IndexOf(',');
					if (num < 0)
					{
						Debug.LogWarning("Couldn't parse: " + text);
					}
					else
					{
						string text2 = text.Substring(0, num);
						StringNames result;
						if (!Enum.TryParse<StringNames>(text2, out result))
						{
							Debug.LogWarning("Couldn't parse: " + text2);
						}
						else
						{
							string value = UnescapeCodes(text, num + 1);
							AllStrings.Add(result, value);
						}
					}
				}
			}
		}
	}

	public string UnescapeCodes(string src, int startAt)
	{
		builder.Clear();
		for (int i = startAt; i < src.Length; i++)
		{
			char c = src[i];
			if (c == '\\')
			{
				switch (src[++i])
				{
				case 'n':
					builder.Append('\n');
					break;
				case 't':
					builder.Append('\t');
					break;
				}
			}
			else
			{
				builder.Append(c);
			}
		}
		return builder.ToString();
	}

	public string GetString(StringNames stringId, params object[] parts)
	{
		string value;
		if (AllStrings.TryGetValue(stringId, out value))
		{
			if (parts.Length != 0)
			{
				return string.Format(value, parts);
			}
			return value;
		}
		return "STRMISS";
	}

	public Sprite GetImage(ImageNames id)
	{
		Sprite value;
		AllImages.TryGetValue(id, out value);
		return value;
	}
}
