using UnityEngine;

public class LogEntryBubble : PoolableBehavior
{
	public SpriteRenderer HeadImage;

	public SpriteRenderer Background;

	public TextRenderer Text;
}
