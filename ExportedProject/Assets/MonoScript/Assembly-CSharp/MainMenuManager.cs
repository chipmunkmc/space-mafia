using System;
using System.Collections;
using UnityEngine;

public class MainMenuManager : MonoBehaviour
{
	public AdDataCollectScreen AdsPolicy;

	public AnnouncementPopUp Announcement;

	public void Start()
	{
		StartCoroutine(Announcement.Init());
		StartCoroutine(RunStartUp());
		QualitySettings.vSyncCount = (SaveManager.VSync ? 1 : 0);
	}

	private IEnumerator RunStartUp()
	{
		yield return Announcement.Show();
		DateTime utcNow = DateTime.UtcNow;
		for (int i = 0; i < DestroyableSingleton<HatManager>.Instance.AllHats.Count; i++)
		{
			HatBehaviour hatBehaviour = DestroyableSingleton<HatManager>.Instance.AllHats[i];
			if ((hatBehaviour.LimitedMonth == utcNow.Month || hatBehaviour.LimitedMonth == 0) && (hatBehaviour.LimitedYear == utcNow.Year || hatBehaviour.LimitedYear == 0) && !hatBehaviour.NotInStore && !SaveManager.GetPurchase(hatBehaviour.ProductId))
			{
				SaveManager.SetPurchased(hatBehaviour.ProductId);
			}
		}
	}

	private void Update()
	{
		if (Input.GetKeyUp(KeyCode.Escape))
		{
			Application.Quit();
		}
	}
}
