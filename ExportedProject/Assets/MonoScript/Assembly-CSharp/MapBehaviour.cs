using UnityEngine;

public class MapBehaviour : MonoBehaviour
{
	public static MapBehaviour Instance;

	public AlphaPulse ColorControl;

	public SpriteRenderer HerePoint;

	public MapCountOverlay countOverlay;

	public InfectedOverlay infectedOverlay;

	public MapTaskOverlay taskOverlay;

	public bool IsOpen
	{
		get
		{
			return base.isActiveAndEnabled;
		}
	}

	public bool IsOpenStopped
	{
		get
		{
			if (IsOpen)
			{
				return countOverlay.isActiveAndEnabled;
			}
			return false;
		}
	}

	private void Awake()
	{
		Instance = this;
	}

	private void GenericShow()
	{
		base.transform.localScale = Vector3.one;
		base.transform.localPosition = new Vector3(0f, 0f, -25f);
		base.gameObject.SetActive(true);
	}

	public void ShowInfectedMap()
	{
		if (IsOpen)
		{
			Close();
		}
		else if (PlayerControl.LocalPlayer.CanMove)
		{
			PlayerControl.LocalPlayer.SetPlayerMaterialColors(HerePoint);
			GenericShow();
			infectedOverlay.gameObject.SetActive(true);
			ColorControl.SetColor(Palette.ImpostorRed);
			taskOverlay.Hide();
			DestroyableSingleton<HudManager>.Instance.SetHudActive(false);
		}
	}

	public void ShowNormalMap()
	{
		if (IsOpen)
		{
			Close();
		}
		else if (PlayerControl.LocalPlayer.CanMove)
		{
			PlayerControl.LocalPlayer.SetPlayerMaterialColors(HerePoint);
			GenericShow();
			taskOverlay.Show();
			ColorControl.SetColor(new Color(0.05f, 0.2f, 1f, 1f));
			DestroyableSingleton<HudManager>.Instance.SetHudActive(false);
		}
	}

	public void ShowCountOverlay()
	{
		GenericShow();
		countOverlay.gameObject.SetActive(true);
		taskOverlay.Hide();
		HerePoint.enabled = false;
		DestroyableSingleton<HudManager>.Instance.SetHudActive(false);
	}

	public void FixedUpdate()
	{
		if ((bool)ShipStatus.Instance)
		{
			Vector3 position = PlayerControl.LocalPlayer.transform.position;
			position /= ShipStatus.Instance.MapScale;
			position.z = -1f;
			HerePoint.transform.localPosition = position;
		}
	}

	public void Close()
	{
		base.gameObject.SetActive(false);
		countOverlay.gameObject.SetActive(false);
		infectedOverlay.gameObject.SetActive(false);
		taskOverlay.Hide();
		HerePoint.enabled = true;
		DestroyableSingleton<HudManager>.Instance.SetHudActive(true);
	}
}
