using System;
using System.Collections;
using System.Runtime.CompilerServices;
using InnerNet;
using PowerTools;
using UnityEngine;

public class MatchMakerGameButton : PoolableBehavior, IConnectButton
{
	[Serializable]
	[CompilerGenerated]
	private sealed class _003C_003Ec
	{
		public static readonly _003C_003Ec _003C_003E9 = new _003C_003Ec();

		public static Func<bool> _003C_003E9__9_0;

		internal bool _003CConnectForFindGame_003Eb__9_0()
		{
			if (AmongUsClient.Instance.ClientId < 0)
			{
				return AmongUsClient.Instance.LastDisconnectReason != DisconnectReasons.ExitGame;
			}
			return true;
		}
	}

	public TextRenderer NameText;

	public TextRenderer PlayerCountText;

	public TextRenderer ImpostorCountText;

	public SpriteRenderer MapIcon;

	public Sprite[] MapIcons;

	public SpriteAnim connectIcon;

	public AnimationClip connectClip;

	public GameListing myListing;

	public void OnClick()
	{
		if (DestroyableSingleton<MatchMaker>.Instance.Connecting(this))
		{
			AmongUsClient.Instance.GameMode = GameModes.OnlineGame;
			AmongUsClient.Instance.OnlineScene = "OnlineGame";
			AmongUsClient.Instance.GameId = myListing.GameId;
			AmongUsClient.Instance.JoinGame();
			StartCoroutine(ConnectForFindGame());
		}
	}

	private IEnumerator ConnectForFindGame()
	{
		yield return EndGameManager.WaitWithTimeout(_003C_003Ec._003C_003E9__9_0 ?? (_003C_003Ec._003C_003E9__9_0 = _003C_003Ec._003C_003E9._003CConnectForFindGame_003Eb__9_0));
		DestroyableSingleton<MatchMaker>.Instance.NotConnecting();
	}

	public void StartIcon()
	{
		connectIcon.Play(connectClip);
	}

	public void StopIcon()
	{
		connectIcon.Stop();
		connectIcon.GetComponent<SpriteRenderer>().sprite = null;
	}

	public void SetGame(GameListing gameListing)
	{
		myListing = gameListing;
		NameText.Text = myListing.HostName;
		ImpostorCountText.Text = myListing.Options.NumImpostors.ToString();
		PlayerCountText.Text = string.Format("{0}/{1}", myListing.PlayerCount, myListing.Options.MaxPlayers);
		MapIcon.sprite = MapIcons[Mathf.Clamp(myListing.Options.MapId, 0, MapIcons.Length - 1)];
	}
}
