using System;
using System.Collections;
using System.Linq;
using System.Runtime.CompilerServices;
using Hazel;
using InnerNet;
using UnityEngine;

public class MeetingHud : InnerNetObject, IDisconnectHandler
{
	public enum VoteStates
	{
		Discussion = 0,
		NotVoted = 1,
		Voted = 2,
		Results = 3,
		Proceeding = 4
	}

	private enum RpcCalls
	{
		Close = 0,
		VotingComplete = 1,
		CastVote = 2,
		ClearVote = 3
	}

	[CompilerGenerated]
	private sealed class _003C_003Ec__DisplayClass43_0
	{
		public PlayerControl pc;

		internal bool _003CHandleDisconnect_003Eb__0(PlayerVoteArea pv)
		{
			return pv.TargetPlayerId == pc.PlayerId;
		}
	}

	[CompilerGenerated]
	private sealed class _003C_003Ec__DisplayClass46_0
	{
		public byte srcPlayerId;

		internal bool _003CCastVote_003Eb__0(PlayerVoteArea pv)
		{
			return pv.TargetPlayerId == srcPlayerId;
		}
	}

	[CompilerGenerated]
	private sealed class _003C_003Ec__DisplayClass48_0
	{
		public int maxIdx;

		internal bool _003CCheckForEndVoting_003Eb__2(GameData.PlayerInfo v)
		{
			return v.PlayerId == maxIdx;
		}
	}

	[Serializable]
	[CompilerGenerated]
	private sealed class _003C_003Ec
	{
		public static readonly _003C_003Ec _003C_003E9 = new _003C_003Ec();

		public static Func<PlayerVoteArea, bool> _003C_003E9__48_0;

		public static Func<byte, int> _003C_003E9__48_1;

		public static Func<PlayerVoteArea, byte> _003C_003E9__48_3;

		public static Func<PlayerVoteArea, int> _003C_003E9__57_0;

		public static Func<PlayerVoteArea, sbyte> _003C_003E9__57_1;

		public static Func<PlayerVoteArea, bool> _003C_003E9__60_0;

		internal bool _003CCheckForEndVoting_003Eb__48_0(PlayerVoteArea ps)
		{
			if (!ps.isDead)
			{
				return ps.didVote;
			}
			return true;
		}

		internal int _003CCheckForEndVoting_003Eb__48_1(byte p)
		{
			return p;
		}

		internal byte _003CCheckForEndVoting_003Eb__48_3(PlayerVoteArea ps)
		{
			return ps.GetState();
		}

		internal int _003CSortButtons_003Eb__57_0(PlayerVoteArea p)
		{
			if (!p.isDead)
			{
				return 0;
			}
			return 50;
		}

		internal sbyte _003CSortButtons_003Eb__57_1(PlayerVoteArea p)
		{
			return p.TargetPlayerId;
		}

		internal bool _003CGetVotesRemaining_003Eb__60_0(PlayerVoteArea ps)
		{
			if (!ps.isDead)
			{
				return !ps.didVote;
			}
			return false;
		}
	}

	[CompilerGenerated]
	private sealed class _003C_003Ec__DisplayClass59_0
	{
		public byte playerId;

		internal bool _003CDidVote_003Eb__0(PlayerVoteArea p)
		{
			return p.TargetPlayerId == playerId;
		}
	}

	private const float ResultsTime = 5f;

	private const float Depth = -100f;

	public static MeetingHud Instance;

	public Transform ButtonParent;

	public TextRenderer TitleText;

	public Vector3 VoteOrigin = new Vector3(-3.6f, 1.75f);

	public Vector3 VoteButtonOffsets = new Vector2(3.6f, -0.91f);

	private Vector3 CounterOrigin = new Vector2(0.5f, -0.13f);

	private Vector3 CounterOffsets = new Vector2(0.3f, 0f);

	public PlayerVoteArea SkipVoteButton;

	[HideInInspector]
	private PlayerVoteArea[] playerStates;

	public PlayerVoteArea PlayerButtonPrefab;

	public SpriteRenderer PlayerVotePrefab;

	public Sprite CrackedGlass;

	public SpriteRenderer Glass;

	public PassiveButton ProceedButton;

	public AudioClip VoteSound;

	public AudioClip VoteLockinSound;

	public AudioClip VoteEndingSound;

	private VoteStates state;

	public SpriteRenderer SkippedVoting;

	public SpriteRenderer HostIcon;

	public Sprite KillBackground;

	private GameData.PlayerInfo exiledPlayer;

	private bool wasTie;

	public TextRenderer TimerText;

	public float discussionTimer;

	private byte reporterId;

	private bool amDead;

	private float resultsStartedAt;

	private int lastSecond = 10;

	private void Awake()
	{
		if (!Instance)
		{
			Instance = this;
		}
		else if (Instance != this)
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	private void Start()
	{
		DestroyableSingleton<HudManager>.Instance.Chat.gameObject.SetActive(true);
		DestroyableSingleton<HudManager>.Instance.Chat.SetPosition(this);
		DestroyableSingleton<HudManager>.Instance.StopOxyFlash();
		DestroyableSingleton<HudManager>.Instance.StopReactorFlash();
		SkipVoteButton.TargetPlayerId = -1;
		SkipVoteButton.Parent = this;
		Camera.main.GetComponent<FollowerCamera>().Locked = true;
		if (PlayerControl.LocalPlayer.Data.IsDead)
		{
			SetForegroundForDead();
		}
		AmongUsClient.Instance.DisconnectHandlers.AddUnique(this);
	}

	private void SetForegroundForDead()
	{
		amDead = true;
		SkipVoteButton.gameObject.SetActive(false);
		Glass.sprite = CrackedGlass;
		Glass.color = Color.white;
	}

	public void Update()
	{
		discussionTimer += Time.deltaTime;
		UpdateButtons();
		switch (state)
		{
		case VoteStates.Discussion:
			if (discussionTimer < (float)PlayerControl.GameOptions.DiscussionTime)
			{
				float f = (float)PlayerControl.GameOptions.DiscussionTime - discussionTimer;
				TimerText.Text = DestroyableSingleton<TranslationController>.Instance.GetString(StringNames.MeetingVotingBegins, Mathf.CeilToInt(f));
				for (int i = 0; i < playerStates.Length; i++)
				{
					playerStates[i].SetDisabled();
				}
				SkipVoteButton.SetDisabled();
			}
			else
			{
				state = VoteStates.NotVoted;
				bool active = PlayerControl.GameOptions.VotingTime > 0;
				TimerText.gameObject.SetActive(active);
				for (int j = 0; j < playerStates.Length; j++)
				{
					playerStates[j].SetEnabled();
				}
				SkipVoteButton.SetEnabled();
			}
			break;
		case VoteStates.NotVoted:
		case VoteStates.Voted:
			if (PlayerControl.GameOptions.VotingTime > 0)
			{
				float num3 = discussionTimer - (float)PlayerControl.GameOptions.DiscussionTime;
				float f2 = Mathf.Max(0f, (float)PlayerControl.GameOptions.VotingTime - num3);
				TimerText.Text = DestroyableSingleton<TranslationController>.Instance.GetString(StringNames.MeetingVotingEnds, Mathf.CeilToInt(f2));
				if (state == VoteStates.NotVoted && Mathf.CeilToInt(f2) <= lastSecond)
				{
					lastSecond--;
					StartCoroutine(Effects.PulseColor(TimerText, Color.red, Color.white, 0.25f));
					SoundManager.Instance.PlaySound(VoteEndingSound, false).pitch = Mathf.Lerp(1.5f, 0.8f, (float)lastSecond / 10f);
				}
				if (AmongUsClient.Instance.AmHost && num3 >= (float)PlayerControl.GameOptions.VotingTime)
				{
					ForceSkipAll();
				}
			}
			break;
		case VoteStates.Results:
			if (AmongUsClient.Instance.GameMode == GameModes.OnlineGame)
			{
				float num = discussionTimer - resultsStartedAt;
				float num2 = Mathf.Max(0f, 5f - num);
				TimerText.Text = DestroyableSingleton<TranslationController>.Instance.GetString(StringNames.MeetingProceeds, Mathf.CeilToInt(num2));
				if (AmongUsClient.Instance.AmHost && num2 <= 0f)
				{
					HandleProceed();
				}
			}
			break;
		}
	}

	public IEnumerator CoIntro(PlayerControl reporter, GameData.PlayerInfo targetPlayer)
	{
		if (DestroyableSingleton<HudManager>.InstanceExists)
		{
			DestroyableSingleton<HudManager>.Instance.Chat.ForceClosed();
			base.transform.SetParent(DestroyableSingleton<HudManager>.Instance.transform);
			base.transform.localPosition = new Vector3(0f, -10f, -100f);
			DestroyableSingleton<HudManager>.Instance.SetHudActive(false);
		}
		OverlayKillAnimation killAnimPrefab = ((targetPlayer == null) ? DestroyableSingleton<HudManager>.Instance.KillOverlay.EmergencyOverlay : DestroyableSingleton<HudManager>.Instance.KillOverlay.ReportOverlay);
		DestroyableSingleton<HudManager>.Instance.KillOverlay.ShowOne(killAnimPrefab, reporter, targetPlayer);
		yield return DestroyableSingleton<HudManager>.Instance.KillOverlay.WaitForFinish();
		Vector3 temp = new Vector3(0f, 0f, -50f);
		for (float timer = 0f; timer < 0.25f; timer += Time.deltaTime)
		{
			float t = timer / 0.25f;
			temp.y = Mathf.SmoothStep(-10f, 0f, t);
			base.transform.localPosition = temp;
			yield return null;
		}
		temp.y = 0f;
		base.transform.localPosition = temp;
		TitleText.Text = DestroyableSingleton<TranslationController>.Instance.GetString(StringNames.MeetingWhoIsTitle);
		if (!PlayerControl.LocalPlayer.Data.IsDead)
		{
			yield return DestroyableSingleton<HudManager>.Instance.ShowEmblem(false);
		}
		for (int i = 0; i < playerStates.Length; i++)
		{
			StartCoroutine(playerStates[i].CoAnimateOverlay());
		}
	}

	private IEnumerator CoStartCutscene()
	{
		yield return DestroyableSingleton<HudManager>.Instance.CoFadeFullScreen(Color.clear, Color.black, 1f);
		ExileController exileController = UnityEngine.Object.Instantiate(ShipStatus.Instance.ExileCutscenePrefab);
		exileController.transform.SetParent(DestroyableSingleton<HudManager>.Instance.transform, false);
		exileController.transform.localPosition = new Vector3(0f, 0f, -60f);
		exileController.Begin(exiledPlayer, wasTie);
		DespawnOnDestroy = false;
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public void ServerStart(byte reporter)
	{
		reporterId = reporter;
		PopulateButtons(reporter);
	}

	public void Close()
	{
		GameData.PlayerInfo data = PlayerControl.LocalPlayer.Data;
		DestroyableSingleton<HudManager>.Instance.Chat.SetPosition(null);
		DestroyableSingleton<HudManager>.Instance.Chat.SetVisible(data.IsDead);
		StartCoroutine(CoStartCutscene());
	}

	private void VotingComplete(byte[] states, GameData.PlayerInfo exiled, bool tie)
	{
		if (state != VoteStates.Results)
		{
			state = VoteStates.Results;
			resultsStartedAt = discussionTimer;
			exiledPlayer = exiled;
			wasTie = tie;
			SkipVoteButton.gameObject.SetActive(false);
			SkippedVoting.gameObject.SetActive(true);
			AmongUsClient.Instance.DisconnectHandlers.Remove(this);
			PopulateResults(states);
			SetupProceedButton();
		}
	}

	public bool Select(int suspectStateIdx)
	{
		if (discussionTimer < (float)PlayerControl.GameOptions.DiscussionTime)
		{
			return false;
		}
		if (PlayerControl.LocalPlayer.Data.IsDead)
		{
			return false;
		}
		SoundManager.Instance.PlaySound(VoteSound, false).volume = 0.8f;
		for (int i = 0; i < playerStates.Length; i++)
		{
			PlayerVoteArea playerVoteArea = playerStates[i];
			if (suspectStateIdx != playerVoteArea.TargetPlayerId)
			{
				playerVoteArea.ClearButtons();
			}
		}
		if (suspectStateIdx != -1)
		{
			SkipVoteButton.ClearButtons();
		}
		return true;
	}

	public void Confirm(sbyte suspectStateIdx)
	{
		if (!PlayerControl.LocalPlayer.Data.IsDead)
		{
			for (int i = 0; i < playerStates.Length; i++)
			{
				PlayerVoteArea obj = playerStates[i];
				obj.ClearButtons();
				obj.voteComplete = true;
			}
			SkipVoteButton.ClearButtons();
			SkipVoteButton.voteComplete = true;
			SkipVoteButton.gameObject.SetActive(false);
			VoteStates voteStates = state;
			if (voteStates != VoteStates.NotVoted)
			{
				int num = 3;
				return;
			}
			state = VoteStates.Voted;
			CmdCastVote(PlayerControl.LocalPlayer.PlayerId, suspectStateIdx);
		}
	}

	public void HandleDisconnect(PlayerControl pc, DisconnectReasons reason)
	{
		_003C_003Ec__DisplayClass43_0 _003C_003Ec__DisplayClass43_ = new _003C_003Ec__DisplayClass43_0();
		_003C_003Ec__DisplayClass43_.pc = pc;
		if (!AmongUsClient.Instance.AmHost)
		{
			return;
		}
		int num = playerStates.IndexOf(_003C_003Ec__DisplayClass43_._003CHandleDisconnect_003Eb__0);
		PlayerVoteArea obj = playerStates[num];
		obj.isDead = true;
		obj.Overlay.gameObject.SetActive(true);
		for (int i = 0; i < playerStates.Length; i++)
		{
			PlayerVoteArea playerVoteArea = playerStates[i];
			if (playerVoteArea.isDead || !playerVoteArea.didVote || playerVoteArea.votedFor != _003C_003Ec__DisplayClass43_.pc.PlayerId)
			{
				continue;
			}
			playerVoteArea.UnsetVote();
			SetDirtyBit((uint)(1 << i));
			GameData.PlayerInfo playerById = GameData.Instance.GetPlayerById((byte)playerVoteArea.TargetPlayerId);
			if (playerById != null)
			{
				int clientIdFromCharacter = AmongUsClient.Instance.GetClientIdFromCharacter(playerById.Object);
				if (clientIdFromCharacter != -1)
				{
					RpcClearVote(clientIdFromCharacter);
				}
			}
		}
		SetDirtyBit((uint)(1 << num));
		CheckForEndVoting();
		if (state == VoteStates.Results)
		{
			SetupProceedButton();
		}
	}

	public void HandleDisconnect()
	{
	}

	private void ForceSkipAll()
	{
		for (int i = 0; i < playerStates.Length; i++)
		{
			PlayerVoteArea playerVoteArea = playerStates[i];
			if (!playerVoteArea.didVote)
			{
				playerVoteArea.didVote = true;
				playerVoteArea.votedFor = -2;
				SetDirtyBit((uint)(1 << i));
			}
		}
		CheckForEndVoting();
	}

	public void CastVote(byte srcPlayerId, sbyte suspectPlayerId)
	{
		_003C_003Ec__DisplayClass46_0 _003C_003Ec__DisplayClass46_ = new _003C_003Ec__DisplayClass46_0();
		_003C_003Ec__DisplayClass46_.srcPlayerId = srcPlayerId;
		int num = playerStates.IndexOf(_003C_003Ec__DisplayClass46_._003CCastVote_003Eb__0);
		PlayerVoteArea playerVoteArea = playerStates[num];
		if (!playerVoteArea.isDead && !playerVoteArea.didVote)
		{
			if (PlayerControl.LocalPlayer.PlayerId == _003C_003Ec__DisplayClass46_.srcPlayerId || AmongUsClient.Instance.GameMode != 0)
			{
				SoundManager.Instance.PlaySound(VoteLockinSound, false);
			}
			playerVoteArea.SetVote(suspectPlayerId);
			SetDirtyBit((uint)(1 << num));
			CheckForEndVoting();
			PlayerControl.LocalPlayer.RpcSendChatNote(_003C_003Ec__DisplayClass46_.srcPlayerId, ChatNoteTypes.DidVote);
		}
	}

	public void ClearVote()
	{
		for (int i = 0; i < playerStates.Length; i++)
		{
			playerStates[i].voteComplete = false;
		}
		SkipVoteButton.voteComplete = false;
		SkipVoteButton.gameObject.SetActive(true);
		state = VoteStates.NotVoted;
	}

	private void CheckForEndVoting()
	{
		if (playerStates.All(_003C_003Ec._003C_003E9__48_0 ?? (_003C_003Ec._003C_003E9__48_0 = _003C_003Ec._003C_003E9._003CCheckForEndVoting_003Eb__48_0)))
		{
			_003C_003Ec__DisplayClass48_0 _003C_003Ec__DisplayClass48_ = new _003C_003Ec__DisplayClass48_0();
			byte[] self = CalculateVotes();
			bool tie;
			_003C_003Ec__DisplayClass48_.maxIdx = self.IndexOfMax(_003C_003Ec._003C_003E9__48_1 ?? (_003C_003Ec._003C_003E9__48_1 = _003C_003Ec._003C_003E9._003CCheckForEndVoting_003Eb__48_1), out tie) - 1;
			GameData.PlayerInfo exiled = GameData.Instance.AllPlayers.FirstOrDefault(_003C_003Ec__DisplayClass48_._003CCheckForEndVoting_003Eb__2);
			byte[] states = playerStates.Select(_003C_003Ec._003C_003E9__48_3 ?? (_003C_003Ec._003C_003E9__48_3 = _003C_003Ec._003C_003E9._003CCheckForEndVoting_003Eb__48_3)).ToArray();
			RpcVotingComplete(states, exiled, tie);
		}
	}

	private byte[] CalculateVotes()
	{
		byte[] array = new byte[11];
		for (int i = 0; i < playerStates.Length; i++)
		{
			PlayerVoteArea playerVoteArea = playerStates[i];
			if (playerVoteArea.didVote)
			{
				int num = playerVoteArea.votedFor + 1;
				if (num >= 0 && num < array.Length)
				{
					array[num]++;
				}
			}
		}
		return array;
	}

	public override bool Serialize(MessageWriter writer, bool initialState)
	{
		if (playerStates == null)
		{
			return false;
		}
		if (initialState)
		{
			for (int i = 0; i < playerStates.Length; i++)
			{
				playerStates[i].Serialize(writer);
			}
		}
		else
		{
			writer.WritePacked(DirtyBits);
			for (int j = 0; j < playerStates.Length; j++)
			{
				if ((DirtyBits & (uint)(1 << j)) != 0)
				{
					playerStates[j].Serialize(writer);
				}
			}
		}
		DirtyBits = 0u;
		return true;
	}

	public override void Deserialize(MessageReader reader, bool initialState)
	{
		if (initialState)
		{
			Instance = this;
			PopulateButtons(0);
			for (int i = 0; i < playerStates.Length; i++)
			{
				PlayerVoteArea playerVoteArea = playerStates[i];
				playerVoteArea.Deserialize(reader);
				if (playerVoteArea.didReport)
				{
					reporterId = (byte)playerVoteArea.TargetPlayerId;
				}
			}
			return;
		}
		uint num = reader.ReadPackedUInt32();
		for (int j = 0; j < playerStates.Length; j++)
		{
			if ((num & (uint)(1 << j)) != 0)
			{
				playerStates[j].Deserialize(reader);
			}
		}
	}

	public void HandleProceed()
	{
		if (AmongUsClient.Instance.AmHost)
		{
			if (state == VoteStates.Results)
			{
				state = VoteStates.Proceeding;
				RpcClose();
			}
		}
		else
		{
			StartCoroutine(Effects.SwayX(HostIcon.transform));
		}
	}

	private void SetupProceedButton()
	{
		if (AmongUsClient.Instance.GameMode != GameModes.OnlineGame)
		{
			TimerText.gameObject.SetActive(false);
			ProceedButton.gameObject.SetActive(true);
			HostIcon.gameObject.SetActive(true);
			GameData.PlayerInfo host = GameData.Instance.GetHost();
			if (host != null)
			{
				PlayerControl.SetPlayerMaterialColors(host.ColorId, HostIcon);
			}
			else
			{
				HostIcon.enabled = false;
			}
		}
	}

	private void PopulateResults(byte[] states)
	{
		TitleText.Text = DestroyableSingleton<TranslationController>.Instance.GetString(StringNames.MeetingVotingResults);
		int num = 0;
		for (int i = 0; i < playerStates.Length; i++)
		{
			PlayerVoteArea playerVoteArea = playerStates[i];
			playerVoteArea.ClearForResults();
			int num2 = 0;
			for (int j = 0; j < states.Length; j++)
			{
				byte self = states[j];
				if (!self.HasAnyBit((byte)128))
				{
					if (j > playerStates.Length)
					{
						break;
					}
					GameData.PlayerInfo playerById = GameData.Instance.GetPlayerById((byte)playerStates[j].TargetPlayerId);
					int votedFor = PlayerVoteArea.GetVotedFor(self);
					if (votedFor == playerVoteArea.TargetPlayerId)
					{
						SpriteRenderer spriteRenderer = UnityEngine.Object.Instantiate(PlayerVotePrefab);
						PlayerControl.SetPlayerMaterialColors(playerById.ColorId, spriteRenderer);
						spriteRenderer.transform.SetParent(playerVoteArea.transform);
						spriteRenderer.transform.localPosition = CounterOrigin + new Vector3(CounterOffsets.x * (float)num2, 0f, 0f);
						spriteRenderer.transform.localScale = Vector3.zero;
						StartCoroutine(Effects.Bloop((float)num2 * 0.5f, spriteRenderer.transform));
						num2++;
					}
					else if (i == 0 && votedFor == -1)
					{
						SpriteRenderer spriteRenderer2 = UnityEngine.Object.Instantiate(PlayerVotePrefab);
						PlayerControl.SetPlayerMaterialColors(playerById.ColorId, spriteRenderer2);
						spriteRenderer2.transform.SetParent(SkippedVoting.transform);
						spriteRenderer2.transform.localPosition = CounterOrigin + new Vector3(CounterOffsets.x * (float)num, 0f, 0f);
						spriteRenderer2.transform.localScale = Vector3.zero;
						StartCoroutine(Effects.Bloop((float)num * 0.5f, spriteRenderer2.transform));
						num++;
					}
				}
			}
		}
	}

	private void UpdateButtons()
	{
		if (PlayerControl.LocalPlayer.Data.IsDead && !amDead)
		{
			SetForegroundForDead();
		}
		if (!AmongUsClient.Instance.AmHost)
		{
			return;
		}
		for (int i = 0; i < playerStates.Length; i++)
		{
			PlayerVoteArea playerVoteArea = playerStates[i];
			GameData.PlayerInfo playerById = GameData.Instance.GetPlayerById((byte)playerVoteArea.TargetPlayerId);
			if (playerById == null)
			{
				playerVoteArea.SetDisabled();
				continue;
			}
			bool flag = playerById.Disconnected || playerById.IsDead;
			if (flag != playerVoteArea.isDead)
			{
				playerVoteArea.SetDead(playerById.PlayerId == PlayerControl.LocalPlayer.PlayerId, reporterId == playerById.PlayerId, flag);
				SetDirtyBit((uint)(1 << i));
			}
		}
	}

	private void PopulateButtons(byte reporter)
	{
		playerStates = new PlayerVoteArea[GameData.Instance.PlayerCount];
		for (int i = 0; i < playerStates.Length; i++)
		{
			GameData.PlayerInfo playerInfo = GameData.Instance.AllPlayers[i];
			PlayerVoteArea obj = (playerStates[i] = CreateButton(playerInfo));
			obj.Parent = this;
			obj.TargetPlayerId = (sbyte)playerInfo.PlayerId;
			obj.SetDead(playerInfo.PlayerId == PlayerControl.LocalPlayer.PlayerId, reporter == playerInfo.PlayerId, playerInfo.Disconnected || playerInfo.IsDead);
		}
		SortButtons();
	}

	private void SortButtons()
	{
		PlayerVoteArea[] array = playerStates.OrderBy(_003C_003Ec._003C_003E9__57_0 ?? (_003C_003Ec._003C_003E9__57_0 = _003C_003Ec._003C_003E9._003CSortButtons_003Eb__57_0)).ThenBy(_003C_003Ec._003C_003E9__57_1 ?? (_003C_003Ec._003C_003E9__57_1 = _003C_003Ec._003C_003E9._003CSortButtons_003Eb__57_1)).ToArray();
		for (int i = 0; i < array.Length; i++)
		{
			int num = i % 2;
			int num2 = i / 2;
			array[i].transform.localPosition = VoteOrigin + new Vector3(VoteButtonOffsets.x * (float)num, VoteButtonOffsets.y * (float)num2, -1f);
		}
	}

	private PlayerVoteArea CreateButton(GameData.PlayerInfo playerInfo)
	{
		PlayerVoteArea playerVoteArea = UnityEngine.Object.Instantiate(PlayerButtonPrefab, ButtonParent.transform);
		PlayerControl.SetPlayerMaterialColors(playerInfo.ColorId, playerVoteArea.PlayerIcon);
		playerVoteArea.NameText.Text = playerInfo.PlayerName;
		bool flag = PlayerControl.LocalPlayer.Data.IsImpostor && playerInfo.IsImpostor;
		playerVoteArea.NameText.Color = (flag ? Palette.ImpostorRed : Color.white);
		playerVoteArea.transform.localScale = Vector3.one;
		return playerVoteArea;
	}

	public bool DidVote(byte playerId)
	{
		_003C_003Ec__DisplayClass59_0 _003C_003Ec__DisplayClass59_ = new _003C_003Ec__DisplayClass59_0();
		_003C_003Ec__DisplayClass59_.playerId = playerId;
		return playerStates.First(_003C_003Ec__DisplayClass59_._003CDidVote_003Eb__0).didVote;
	}

	public int GetVotesRemaining()
	{
		try
		{
			return playerStates.Count(_003C_003Ec._003C_003E9__60_0 ?? (_003C_003Ec._003C_003E9__60_0 = _003C_003Ec._003C_003E9._003CGetVotesRemaining_003Eb__60_0));
		}
		catch
		{
			return 0;
		}
	}

	public void RpcClose()
	{
		if (AmongUsClient.Instance.AmClient)
		{
			Close();
		}
		AmongUsClient.Instance.SendRpc(NetId, 0);
	}

	public void CmdCastVote(byte playerId, sbyte suspectIdx)
	{
		if (AmongUsClient.Instance.AmHost)
		{
			CastVote(playerId, suspectIdx);
			return;
		}
		MessageWriter messageWriter = AmongUsClient.Instance.StartRpcImmediately(NetId, 2, SendOption.Reliable, AmongUsClient.Instance.HostId);
		messageWriter.Write(playerId);
		messageWriter.Write(suspectIdx);
		AmongUsClient.Instance.FinishRpcImmediately(messageWriter);
	}

	private void RpcVotingComplete(byte[] states, GameData.PlayerInfo exiled, bool tie)
	{
		if (AmongUsClient.Instance.AmClient)
		{
			VotingComplete(states, exiled, tie);
		}
		MessageWriter messageWriter = AmongUsClient.Instance.StartRpc(NetId, 1);
		messageWriter.WriteBytesAndSize(states);
		messageWriter.Write((exiled != null) ? exiled.PlayerId : byte.MaxValue);
		messageWriter.Write(tie);
		messageWriter.EndMessage();
	}

	private void RpcClearVote(int clientId)
	{
		if (AmongUsClient.Instance.ClientId == clientId)
		{
			ClearVote();
			return;
		}
		MessageWriter msg = AmongUsClient.Instance.StartRpcImmediately(NetId, 3, SendOption.Reliable, clientId);
		AmongUsClient.Instance.FinishRpcImmediately(msg);
	}

	public override void HandleRpc(byte callId, MessageReader reader)
	{
		switch (callId)
		{
		case 0:
			Close();
			break;
		case 2:
		{
			byte srcPlayerId = reader.ReadByte();
			sbyte suspectPlayerId = reader.ReadSByte();
			CastVote(srcPlayerId, suspectPlayerId);
			break;
		}
		case 1:
		{
			byte[] states = reader.ReadBytesAndSize();
			GameData.PlayerInfo playerById = GameData.Instance.GetPlayerById(reader.ReadByte());
			bool tie = reader.ReadBoolean();
			VotingComplete(states, playerById, tie);
			break;
		}
		case 3:
			ClearVote();
			break;
		}
	}
}
