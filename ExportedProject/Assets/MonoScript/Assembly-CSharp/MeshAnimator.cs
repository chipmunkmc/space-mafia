using UnityEngine;

public class MeshAnimator : MonoBehaviour
{
	private MeshFilter filter;

	public Mesh[] Frames;

	public float frameRate;

	private float timer;

	private int frameId;

	private void Start()
	{
		filter = GetComponent<MeshFilter>();
	}

	private void Update()
	{
		timer += Time.deltaTime;
		if (timer > 1f / frameRate)
		{
			timer = 0f;
			frameId++;
			if (frameId >= Frames.Length)
			{
				frameId = 0;
			}
			filter.mesh = Frames[frameId];
		}
	}
}
