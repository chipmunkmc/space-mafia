using System;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;

public class MonitorOxyMinigame : Minigame
{
	[Serializable]
	[CompilerGenerated]
	private sealed class _003C_003Ec
	{
		public static readonly _003C_003Ec _003C_003E9 = new _003C_003Ec();

		public static Func<BoxCollider2D, bool> _003C_003E9__9_0;

		internal bool _003CUpdate_003Eb__9_0(BoxCollider2D s)
		{
			return !s.enabled;
		}
	}

	public SpriteRenderer[] Targets;

	public BoxCollider2D[] Sliders;

	public VerticalSpriteGauge[] Fills;

	public FloatRange YRange;

	public FloatRange[] RandomRanges;

	private Controller controller = new Controller();

	public AudioClip[] DragSounds;

	private AudioSource ActiveSound;

	public override void Begin(PlayerTask task)
	{
		base.Begin(task);
		for (int i = 0; i < Sliders.Length; i++)
		{
			BoxCollider2D obj = Sliders[i];
			Vector3 localPosition = obj.transform.localPosition;
			localPosition.y = RandomRanges[i].Next();
			obj.transform.localPosition = localPosition;
			float value = YRange.ReverseLerp(localPosition.y);
			Fills[i].Value = value;
		}
	}

	public void Update()
	{
		controller.Update();
		for (int i = 0; i < Sliders.Length; i++)
		{
			BoxCollider2D boxCollider2D = Sliders[i];
			if (!boxCollider2D.enabled)
			{
				continue;
			}
			switch (controller.CheckDrag(boxCollider2D))
			{
			case DragState.TouchStart:
				if (Constants.ShouldPlaySfx())
				{
					ActiveSound = SoundManager.Instance.PlaySound(DragSounds[i], true, 0.7f);
				}
				break;
			case DragState.Dragging:
			{
				Vector2 vector = controller.DragPosition - (Vector2)boxCollider2D.transform.parent.position;
				Vector3 localPosition = boxCollider2D.transform.localPosition;
				localPosition.y = YRange.Clamp(vector.y);
				boxCollider2D.transform.localPosition = localPosition;
				float num = YRange.ReverseLerp(localPosition.y);
				Fills[i].Value = num;
				if ((bool)ActiveSound)
				{
					ActiveSound.pitch = Mathf.Lerp(0.8f, 1.2f, num);
				}
				break;
			}
			case DragState.Released:
			{
				if ((bool)ActiveSound)
				{
					ActiveSound.Stop();
				}
				SpriteRenderer spriteRenderer = Targets[i];
				if (Mathf.Abs(boxCollider2D.transform.localPosition.y - spriteRenderer.transform.localPosition.y) < 0.1f)
				{
					boxCollider2D.enabled = false;
					spriteRenderer.color = Color.green;
					if (Sliders.All(_003C_003Ec._003C_003E9__9_0 ?? (_003C_003Ec._003C_003E9__9_0 = _003C_003Ec._003C_003E9._003CUpdate_003Eb__9_0)))
					{
						MyNormTask.NextStep();
						StartCoroutine(CoStartClose());
					}
				}
				break;
			}
			}
		}
	}

	public override void Close()
	{
		if ((bool)ActiveSound)
		{
			ActiveSound.Stop();
		}
		base.Close();
	}
}
