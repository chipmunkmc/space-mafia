public class MultistageMinigame : Minigame
{
	public Minigame[] Stages;

	private Minigame stage;

	public override void Begin(PlayerTask task)
	{
		NormalPlayerTask normalPlayerTask = task as NormalPlayerTask;
		if (normalPlayerTask.TaskType == TaskTypes.FuelEngines)
		{
			stage = Stages[normalPlayerTask.Data[1]];
		}
		else
		{
			stage = Stages[normalPlayerTask.taskStep];
		}
		stage.gameObject.SetActive(true);
		stage.Begin(task);
		Minigame.Instance = this;
	}

	public override void Close()
	{
		Minigame.Instance = null;
		stage.Close();
		base.Close();
	}
}
