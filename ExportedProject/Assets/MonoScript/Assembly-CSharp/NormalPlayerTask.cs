using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using UnityEngine;

public class NormalPlayerTask : PlayerTask
{
	public enum TimerState
	{
		NotStarted = 0,
		Started = 1,
		Finished = 2
	}

	[Serializable]
	[CompilerGenerated]
	private sealed class _003C_003Ec
	{
		public static readonly _003C_003Ec _003C_003E9 = new _003C_003Ec();

		public static Func<Console, bool> _003C_003E9__13_0;

		public static Func<Console, bool> _003C_003E9__15_0;

		public static Func<Console, bool> _003C_003E9__15_1;

		internal bool _003CInitialize_003Eb__13_0(Console t)
		{
			return t.TaskTypes.Contains(TaskTypes.FixWiring);
		}

		internal bool _003CUpdateArrow_003Eb__15_0(Console c)
		{
			if (c.TaskTypes.Contains(TaskTypes.AlignEngineOutput))
			{
				return c.ConsoleId == 1;
			}
			return false;
		}

		internal bool _003CUpdateArrow_003Eb__15_1(Console console)
		{
			if (console.TaskTypes.Contains(TaskTypes.AlignEngineOutput))
			{
				return console.ConsoleId == 0;
			}
			return false;
		}
	}

	public int taskStep;

	public int MaxStep;

	public bool ShowTaskStep = true;

	public bool ShowTaskTimer;

	public TimerState TimerStarted;

	public float TaskTimer;

	public byte[] Data;

	public ArrowBehaviour Arrow;

	public override int TaskStep
	{
		get
		{
			return taskStep;
		}
	}

	public override bool IsComplete
	{
		get
		{
			return taskStep >= MaxStep;
		}
	}

	public override void Initialize()
	{
		if ((bool)Arrow && !base.Owner.AmOwner)
		{
			Arrow.gameObject.SetActive(false);
		}
		HasLocation = true;
		LocationDirty = true;
		switch (TaskType)
		{
		case TaskTypes.OpenWaterways:
			Data = new byte[3];
			break;
		case TaskTypes.ReplaceWaterJug:
			Data = new byte[1];
			break;
		case TaskTypes.WaterPlants:
			Data = new byte[4];
			break;
		case TaskTypes.EnterIdCode:
			Data = BitConverter.GetBytes(IntRange.Next(1, 99999));
			break;
		case TaskTypes.ChartCourse:
			Data = new byte[4];
			break;
		case TaskTypes.InspectSample:
			Data = new byte[2];
			break;
		case TaskTypes.FuelEngines:
			Data = new byte[2];
			break;
		case TaskTypes.StartReactor:
			Data = new byte[6];
			break;
		case TaskTypes.PrimeShields:
		{
			Data = new byte[1];
			int num = 0;
			for (int j = 0; j < 7; j++)
			{
				byte b = (byte)(1 << j);
				if (BoolRange.Next(0.7f))
				{
					Data[0] |= b;
					num++;
				}
			}
			Data[0] &= 118;
			break;
		}
		case TaskTypes.AlignEngineOutput:
			Data = new byte[2];
			Data[0] = AlignGame.ToByte((float)IntRange.RandomSign() * FloatRange.Next(1f, 3f));
			Data[1] = (byte)(IntRange.RandomSign() * IntRange.Next(25, 255));
			break;
		case TaskTypes.FixWiring:
		{
			Data = new byte[MaxStep];
			List<Console> list = ShipStatus.Instance.AllConsoles.Where(_003C_003Ec._003C_003E9__13_0 ?? (_003C_003Ec._003C_003E9__13_0 = _003C_003Ec._003C_003E9._003CInitialize_003Eb__13_0)).ToList();
			List<Console> list2 = new List<Console>(list);
			for (int i = 0; i < Data.Length; i++)
			{
				int index = list2.RandomIdx();
				Data[i] = (byte)list2[index].ConsoleId;
				list2.RemoveAt(index);
			}
			Array.Sort(Data);
			Console console = list.First(_003CInitialize_003Eb__13_1);
			StartAt = console.Room;
			break;
		}
		}
	}

	public void NextStep()
	{
		taskStep++;
		UpdateArrow();
		if (taskStep >= MaxStep)
		{
			taskStep = MaxStep;
			if (!PlayerControl.LocalPlayer)
			{
				return;
			}
			if (DestroyableSingleton<HudManager>.InstanceExists)
			{
				DestroyableSingleton<HudManager>.Instance.ShowTaskComplete();
				StatsManager.Instance.TasksCompleted++;
				if (PlayerTask.AllTasksCompleted(PlayerControl.LocalPlayer))
				{
					StatsManager.Instance.CompletedAllTasks++;
				}
			}
			PlayerControl.LocalPlayer.RpcCompleteTask(base.Id);
		}
		else if (ShowTaskStep && Constants.ShouldPlaySfx())
		{
			SoundManager.Instance.PlaySound(DestroyableSingleton<HudManager>.Instance.TaskUpdateSound, false);
		}
	}

	public void UpdateArrow()
	{
		if (!Arrow)
		{
			return;
		}
		if (!base.Owner.AmOwner)
		{
			Arrow.gameObject.SetActive(false);
		}
		else if (!IsComplete)
		{
			Arrow.gameObject.SetActive(true);
			if (TaskType == TaskTypes.FixWiring)
			{
				Console console = FindSpecialConsole(_003CUpdateArrow_003Eb__15_2);
				Arrow.target = console.transform.position;
				StartAt = console.Room;
			}
			else if (TaskType == TaskTypes.AlignEngineOutput)
			{
				if (AlignGame.IsSuccess(Data[0]))
				{
					Arrow.target = FindSpecialConsole(_003C_003Ec._003C_003E9__15_0 ?? (_003C_003Ec._003C_003E9__15_0 = _003C_003Ec._003C_003E9._003CUpdateArrow_003Eb__15_0)).transform.position;
					StartAt = SystemTypes.UpperEngine;
				}
				else
				{
					Arrow.target = FindSpecialConsole(_003C_003Ec._003C_003E9__15_1 ?? (_003C_003Ec._003C_003E9__15_1 = _003C_003Ec._003C_003E9._003CUpdateArrow_003Eb__15_1)).transform.position;
					StartAt = SystemTypes.LowerEngine;
				}
			}
			else
			{
				Console console2 = FindObjectPos();
				Arrow.target = console2.transform.position;
				StartAt = console2.Room;
			}
			LocationDirty = true;
		}
		else
		{
			Arrow.gameObject.SetActive(false);
		}
	}

	private void FixedUpdate()
	{
		if (TimerStarted == TimerState.Started)
		{
			TaskTimer -= Time.fixedDeltaTime;
			if (TaskTimer <= 0f)
			{
				TaskTimer = 0f;
				TimerStarted = TimerState.Finished;
			}
		}
	}

	public override bool ValidConsole(Console console)
	{
		if (TaskType == TaskTypes.FixWiring)
		{
			if (console.TaskTypes.Contains(TaskType))
			{
				return console.ConsoleId == Data[taskStep];
			}
			return false;
		}
		if (TaskType == TaskTypes.AlignEngineOutput)
		{
			if (console.TaskTypes.Contains(TaskType))
			{
				return !AlignGame.IsSuccess(Data[console.ConsoleId]);
			}
			return false;
		}
		if (TaskType == TaskTypes.FuelEngines)
		{
			return console.ValidTasks.Any(_003CValidConsole_003Eb__17_0);
		}
		if (TaskType == TaskTypes.RecordTemperature)
		{
			if (console.Room == StartAt)
			{
				return console.TaskTypes.Any(_003CValidConsole_003Eb__17_1);
			}
			return false;
		}
		if (!console.TaskTypes.Any(_003CValidConsole_003Eb__17_2))
		{
			return console.ValidTasks.Any(_003CValidConsole_003Eb__17_3);
		}
		return true;
	}

	public override void Complete()
	{
		taskStep = MaxStep;
	}

	public override void AppendTaskText(StringBuilder sb)
	{
		bool num = ShouldYellowText();
		if (num)
		{
			if (IsComplete)
			{
				sb.Append("[00DD00FF]");
			}
			else
			{
				sb.Append("[FFFF00FF]");
			}
		}
		sb.Append(DestroyableSingleton<TranslationController>.Instance.GetString(StartAt));
		sb.Append(": ");
		sb.Append(DestroyableSingleton<TranslationController>.Instance.GetString(TaskType));
		if (ShowTaskTimer && TimerStarted == TimerState.Started)
		{
			sb.Append(" (");
			sb.Append((int)TaskTimer);
			sb.Append("s)");
		}
		else if (ShowTaskStep)
		{
			sb.Append(" (");
			sb.Append(taskStep);
			sb.Append("/");
			sb.Append(MaxStep);
			sb.Append(")");
		}
		if (num)
		{
			sb.Append("[]");
		}
		sb.AppendLine();
	}

	private bool ShouldYellowText()
	{
		if (TaskType == TaskTypes.FuelEngines && Data[1] > 0)
		{
			return true;
		}
		if (taskStep <= 0)
		{
			return TimerStarted != TimerState.NotStarted;
		}
		return true;
	}

	[CompilerGenerated]
	private bool _003CInitialize_003Eb__13_1(Console v)
	{
		return v.ConsoleId == Data[0];
	}

	[CompilerGenerated]
	private bool _003CUpdateArrow_003Eb__15_2(Console c)
	{
		if (c.TaskTypes.Contains(TaskTypes.FixWiring))
		{
			return c.ConsoleId == Data[taskStep];
		}
		return false;
	}

	[CompilerGenerated]
	private bool _003CValidConsole_003Eb__17_0(TaskSet set)
	{
		if (TaskType == set.taskType)
		{
			return set.taskStep.Contains(Data[1]);
		}
		return false;
	}

	[CompilerGenerated]
	private bool _003CValidConsole_003Eb__17_1(TaskTypes tt)
	{
		return tt == TaskType;
	}

	[CompilerGenerated]
	private bool _003CValidConsole_003Eb__17_2(TaskTypes tt)
	{
		return tt == TaskType;
	}

	[CompilerGenerated]
	private bool _003CValidConsole_003Eb__17_3(TaskSet set)
	{
		if (TaskType == set.taskType)
		{
			return set.taskStep.Contains(taskStep);
		}
		return false;
	}
}
