using UnityEngine;

public class NumberOption : OptionBehaviour
{
	public TextRenderer TitleText;

	public TextRenderer ValueText;

	public float Value = 1f;

	private float oldValue = float.MaxValue;

	public float Increment;

	public FloatRange ValidRange = new FloatRange(0f, 2f);

	public string FormatString = "{0:0.0}x";

	public bool ZeroIsInfinity;

	public void OnEnable()
	{
		TitleText.Text = DestroyableSingleton<TranslationController>.Instance.GetString(Title);
		ValueText.Text = string.Format(FormatString, Value);
		GameOptionsData gameOptions = PlayerControl.GameOptions;
		switch (Title)
		{
		case StringNames.GamePlayerSpeed:
			Value = gameOptions.PlayerSpeedMod;
			break;
		case StringNames.GameCrewLight:
			Value = gameOptions.CrewLightMod;
			break;
		case StringNames.GameImpostorLight:
			Value = gameOptions.ImpostorLightMod;
			break;
		case StringNames.GameKillCooldown:
			Value = gameOptions.KillCooldown;
			break;
		case StringNames.GameCommonTasks:
			Value = gameOptions.NumCommonTasks;
			break;
		case StringNames.GameLongTasks:
			Value = gameOptions.NumLongTasks;
			break;
		case StringNames.GameShortTasks:
			Value = gameOptions.NumShortTasks;
			break;
		case StringNames.GameNumImpostors:
			Value = gameOptions.NumImpostors;
			break;
		case StringNames.GameNumMeetings:
			Value = gameOptions.NumEmergencyMeetings;
			break;
		case StringNames.GameEmergencyCooldown:
			Value = gameOptions.EmergencyCooldown;
			break;
		case StringNames.GameDiscussTime:
			Value = gameOptions.DiscussionTime;
			break;
		case StringNames.GameVotingTime:
			Value = gameOptions.VotingTime;
			break;
		default:
			Debug.Log("Ono, unrecognized setting: " + Title);
			break;
		}
	}

	private void FixedUpdate()
	{
		if (oldValue != Value)
		{
			oldValue = Value;
			if (ZeroIsInfinity && Mathf.Abs(Value) < 0.0001f)
			{
				ValueText.Text = string.Format(FormatString, "∞");
			}
			else
			{
				ValueText.Text = string.Format(FormatString, Value);
			}
		}
	}

	public void Increase()
	{
		Value = ValidRange.Clamp(Value + Increment);
		OnValueChanged(this);
	}

	public void Decrease()
	{
		Value = ValidRange.Clamp(Value - Increment);
		OnValueChanged(this);
	}

	public override float GetFloat()
	{
		return Value;
	}

	public override int GetInt()
	{
		return (int)Value;
	}
}
