using System;

[Serializable]
public class OffsetAdjustment
{
	public char Char;

	public int OffsetX;

	public int OffsetY;
}
