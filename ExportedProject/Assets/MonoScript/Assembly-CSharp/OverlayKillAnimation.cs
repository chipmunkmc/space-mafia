using System.Collections;
using System.Runtime.CompilerServices;
using PowerTools;
using UnityEngine;

public class OverlayKillAnimation : MonoBehaviour
{
	[CompilerGenerated]
	private sealed class _003C_003Ec__DisplayClass7_0
	{
		public GameData.PlayerInfo kInfo;

		internal void _003CBegin_003Eb__0(SpriteRenderer b)
		{
			PlayerControl.SetPlayerMaterialColors(kInfo.ColorId, b);
		}
	}

	public KillAnimType KillType;

	public PoolablePlayer killerParts;

	public PoolablePlayer victimParts;

	private uint victimHat;

	public AudioClip Stinger;

	public AudioClip Sfx;

	public float StingerVolume = 0.6f;

	public void Begin(PlayerControl killer, GameData.PlayerInfo vInfo)
	{
		if ((bool)killerParts)
		{
			_003C_003Ec__DisplayClass7_0 _003C_003Ec__DisplayClass7_ = new _003C_003Ec__DisplayClass7_0();
			_003C_003Ec__DisplayClass7_.kInfo = killer.Data;
			PlayerControl.SetPlayerMaterialColors(_003C_003Ec__DisplayClass7_.kInfo.ColorId, killerParts.Body);
			killerParts.Hands.ForEach(_003C_003Ec__DisplayClass7_._003CBegin_003Eb__0);
			if ((bool)killerParts.HatSlot)
			{
				killerParts.HatSlot.SetHat(_003C_003Ec__DisplayClass7_.kInfo.HatId);
			}
			switch (KillType)
			{
			case KillAnimType.Tongue:
			{
				SkinData skinById2 = DestroyableSingleton<HatManager>.Instance.GetSkinById(_003C_003Ec__DisplayClass7_.kInfo.SkinId);
				killerParts.SkinSlot.GetComponent<SpriteAnim>().Play(skinById2.KillTongueImpostor);
				break;
			}
			case KillAnimType.Shoot:
			{
				SkinData skinById = DestroyableSingleton<HatManager>.Instance.GetSkinById(_003C_003Ec__DisplayClass7_.kInfo.SkinId);
				killerParts.SkinSlot.GetComponent<SpriteAnim>().Play(skinById.KillShootImpostor);
				break;
			}
			case KillAnimType.Stab:
			case KillAnimType.Neck:
				PlayerControl.SetSkinImage(_003C_003Ec__DisplayClass7_.kInfo.SkinId, killerParts.SkinSlot);
				break;
			}
			if ((bool)killerParts.PetSlot)
			{
				PetBehaviour petById = DestroyableSingleton<HatManager>.Instance.GetPetById(_003C_003Ec__DisplayClass7_.kInfo.PetId);
				if ((bool)petById && (bool)petById.scaredClip)
				{
					killerParts.PetSlot.GetComponent<SpriteAnim>().Play(petById.idleClip);
					killerParts.PetSlot.sharedMaterial = petById.rend.sharedMaterial;
					PlayerControl.SetPlayerMaterialColors(_003C_003Ec__DisplayClass7_.kInfo.ColorId, killerParts.PetSlot);
				}
				else
				{
					killerParts.PetSlot.enabled = false;
				}
			}
		}
		if (vInfo == null || !victimParts)
		{
			return;
		}
		victimHat = vInfo.HatId;
		PlayerControl.SetPlayerMaterialColors(vInfo.ColorId, victimParts.Body);
		if ((bool)victimParts.HatSlot)
		{
			victimParts.HatSlot.SetHat(vInfo.HatId);
		}
		SkinData skinById3 = DestroyableSingleton<HatManager>.Instance.GetSkinById(vInfo.SkinId);
		switch (KillType)
		{
		case KillAnimType.Tongue:
			victimParts.SkinSlot.GetComponent<SpriteAnim>().Play(skinById3.KillTongueVictim);
			break;
		case KillAnimType.Stab:
			victimParts.SkinSlot.GetComponent<SpriteAnim>().Play(skinById3.KillStabVictim);
			break;
		case KillAnimType.Neck:
			victimParts.SkinSlot.GetComponent<SpriteAnim>().Play(skinById3.KillNeckVictim);
			break;
		case KillAnimType.Shoot:
			victimParts.SkinSlot.GetComponent<SpriteAnim>().Play(skinById3.KillShootVictim);
			break;
		}
		if ((bool)victimParts.PetSlot)
		{
			PetBehaviour petById2 = DestroyableSingleton<HatManager>.Instance.GetPetById(vInfo.PetId);
			if ((bool)petById2 && (bool)petById2.scaredClip)
			{
				victimParts.PetSlot.GetComponent<SpriteAnim>().Play(petById2.scaredClip);
				victimParts.PetSlot.sharedMaterial = petById2.rend.sharedMaterial;
				PlayerControl.SetPlayerMaterialColors(vInfo.ColorId, victimParts.PetSlot);
			}
			else
			{
				victimParts.PetSlot.enabled = false;
			}
		}
	}

	public void SetHatFloor()
	{
		HatBehaviour hatById = DestroyableSingleton<HatManager>.Instance.GetHatById(victimHat);
		if ((bool)hatById)
		{
			victimParts.HatSlot.SetHatFloor(hatById);
		}
	}

	public void PlayKillSound()
	{
		if (Constants.ShouldPlaySfx())
		{
			SoundManager.Instance.PlaySound(Sfx, false).volume = 0.8f;
		}
	}

	public IEnumerator WaitForFinish()
	{
		SpriteAnim[] anims = GetComponentsInChildren<SpriteAnim>();
		if (anims.Length == 0)
		{
			yield return new WaitForSeconds(1f);
			yield break;
		}
		while (true)
		{
			bool flag = false;
			for (int i = 0; i < anims.Length; i++)
			{
				if (anims[i].IsPlaying())
				{
					flag = true;
					break;
				}
			}
			if (flag)
			{
				yield return null;
				continue;
			}
			break;
		}
	}
}
