using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PassiveButton : PassiveUiElement
{
	public Button.ButtonClickedEvent OnClick = new Button.ButtonClickedEvent();

	public AudioClip ClickSound;

	public bool OnUp = true;

	public bool OnDown;

	public bool OnRepeat;

	public float RepeatDuration = 0.3f;

	public UnityEvent OnMouseOver;

	public UnityEvent OnMouseOut;

	private float repeatTimer;

	public override bool HandleUp
	{
		get
		{
			return OnUp;
		}
	}

	public override bool HandleDown
	{
		get
		{
			return OnDown;
		}
	}

	public override bool HandleRepeat
	{
		get
		{
			return OnRepeat;
		}
	}

	public override void ReceiveClickDown()
	{
		if ((bool)ClickSound)
		{
			SoundManager.Instance.PlaySound(ClickSound, false);
		}
		OnClick.Invoke();
	}

	public override void ReceiveRepeatDown()
	{
		repeatTimer += Time.deltaTime;
		if (!(repeatTimer < RepeatDuration))
		{
			repeatTimer = 0f;
			if ((bool)ClickSound)
			{
				SoundManager.Instance.PlaySound(ClickSound, false);
			}
			OnClick.Invoke();
		}
	}

	public override void ReceiveClickUp()
	{
		ReceiveClickDown();
	}

	public override void ReceiveMouseOut()
	{
		OnMouseOut.Invoke();
	}

	public override void ReceiveMouseOver()
	{
		OnMouseOver.Invoke();
	}
}
