using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;

public class PassiveButtonManager : DestroyableSingleton<PassiveButtonManager>
{
	private enum ButtonStates
	{
		Up = 0,
		Down = 1,
		Drag = 2
	}

	private class DepthComparer : IComparer<MonoBehaviour>
	{
		public static readonly DepthComparer Instance = new DepthComparer();

		public int Compare(MonoBehaviour x, MonoBehaviour y)
		{
			if (x == null)
			{
				return 1;
			}
			if (y == null)
			{
				return -1;
			}
			return x.transform.position.z.CompareTo(y.transform.position.z);
		}
	}

	[CompilerGenerated]
	private sealed class _003C_003Ec__DisplayClass11_0
	{
		public Collider2D col;

		internal bool _003CUpdate_003Eb__0(PassiveUiElement b2)
		{
			if (b2.HandleDrag)
			{
				return b2.transform.position.z > col.transform.position.z;
			}
			return false;
		}
	}

	[CompilerGenerated]
	private sealed class _003C_003Ec__DisplayClass13_0
	{
		public Controller.TouchState pt;

		internal bool _003CHandleMouseOut_003Eb__0(Collider2D c)
		{
			return c.OverlapPoint(pt.Position);
		}
	}

	public List<PassiveUiElement> Buttons = new List<PassiveUiElement>();

	private List<IFocusHolder> FocusHolders = new List<IFocusHolder>();

	private PassiveUiElement currentOver;

	public Controller controller = new Controller();

	private ButtonStates currentState;

	private Collider2D[] results = new Collider2D[40];

	public void RegisterOne(PassiveUiElement button)
	{
		Buttons.Add(button);
	}

	public void RemoveOne(PassiveUiElement passiveButton)
	{
		Buttons.Remove(passiveButton);
	}

	public void RegisterOne(IFocusHolder focusHolder)
	{
		FocusHolders.Add(focusHolder);
	}

	public void RemoveOne(IFocusHolder focusHolder)
	{
		FocusHolders.Remove(focusHolder);
	}

	public void Update()
	{
		controller.Update();
		for (int i = 1; i < Buttons.Count; i++)
		{
			if (DepthComparer.Instance.Compare(Buttons[i - 1], Buttons[i]) > 0)
			{
				Buttons.Sort(DepthComparer.Instance);
				break;
			}
		}
		HandleMouseOut();
		for (int j = 0; j < Buttons.Count; j++)
		{
			PassiveUiElement passiveUiElement = Buttons[j];
			if (!passiveUiElement)
			{
				Buttons.RemoveAt(j);
				j--;
			}
			else
			{
				if (!passiveUiElement.isActiveAndEnabled)
				{
					continue;
				}
				for (int k = 0; k < passiveUiElement.Colliders.Length; k++)
				{
					_003C_003Ec__DisplayClass11_0 _003C_003Ec__DisplayClass11_ = new _003C_003Ec__DisplayClass11_0();
					_003C_003Ec__DisplayClass11_.col = passiveUiElement.Colliders[k];
					if (!_003C_003Ec__DisplayClass11_.col || !_003C_003Ec__DisplayClass11_.col.isActiveAndEnabled)
					{
						continue;
					}
					HandleMouseOver(passiveUiElement, _003C_003Ec__DisplayClass11_.col);
					switch (controller.CheckDrag(_003C_003Ec__DisplayClass11_.col))
					{
					case DragState.TouchStart:
						if (passiveUiElement.HandleDown)
						{
							passiveUiElement.ReceiveClickDown();
						}
						break;
					case DragState.Holding:
						if (passiveUiElement.HandleRepeat)
						{
							passiveUiElement.ReceiveRepeatDown();
						}
						break;
					case DragState.Dragging:
						if (passiveUiElement.HandleDrag)
						{
							Vector2 dragDelta = controller.DragPosition - controller.DragStartPosition;
							passiveUiElement.ReceiveClickDrag(dragDelta);
							controller.ResetDragPosition();
						}
						else if (passiveUiElement.HandleRepeat)
						{
							passiveUiElement.ReceiveRepeatDown();
						}
						else if (Buttons.Any(_003C_003Ec__DisplayClass11_._003CUpdate_003Eb__0))
						{
							controller.ClearTouch();
						}
						break;
					case DragState.Released:
						if (passiveUiElement.HandleUp)
						{
							passiveUiElement.ReceiveClickUp();
						}
						break;
					}
				}
			}
		}
		if (controller.AnyTouchDown)
		{
			Vector2 touch = GetTouch(true);
			HandleFocus(touch);
		}
	}

	private void HandleFocus(Vector2 pt)
	{
		bool flag = false;
		for (int i = 0; i < FocusHolders.Count; i++)
		{
			IFocusHolder focusHolder = FocusHolders[i];
			if (!(focusHolder as MonoBehaviour))
			{
				FocusHolders.RemoveAt(i);
				i--;
			}
			else
			{
				if (!focusHolder.CheckCollision(pt))
				{
					continue;
				}
				flag = true;
				focusHolder.GiveFocus();
				for (int j = 0; j < FocusHolders.Count; j++)
				{
					if (j != i)
					{
						FocusHolders[j].LoseFocus();
					}
				}
				break;
			}
		}
		if (!flag)
		{
			for (int k = 0; k < FocusHolders.Count; k++)
			{
				FocusHolders[k].LoseFocus();
			}
		}
	}

	private void HandleMouseOut()
	{
		_003C_003Ec__DisplayClass13_0 _003C_003Ec__DisplayClass13_ = new _003C_003Ec__DisplayClass13_0();
		_003C_003Ec__DisplayClass13_.pt = controller.GetTouch(0);
		if ((bool)currentOver && !currentOver.Colliders.Any(_003C_003Ec__DisplayClass13_._003CHandleMouseOut_003Eb__0))
		{
			currentOver.ReceiveMouseOut();
			currentOver = null;
		}
	}

	private void HandleMouseOver(PassiveUiElement button, Collider2D col)
	{
		if (button.HandleOverOut && !(button == currentOver) && (!currentOver || !(button.transform.position.z > currentOver.transform.position.z)) && col.OverlapPoint(controller.GetTouch(0).Position))
		{
			if ((bool)currentOver)
			{
				currentOver.ReceiveMouseOut();
			}
			currentOver = button;
			currentOver.ReceiveMouseOver();
		}
	}

	private Vector2 GetTouch(bool getDownTouch)
	{
		if (getDownTouch)
		{
			if (controller.Touches[0].TouchStart)
			{
				return controller.Touches[0].Position;
			}
			return controller.Touches[1].Position;
		}
		if (controller.Touches[0].TouchEnd)
		{
			return controller.Touches[0].Position;
		}
		return controller.Touches[1].Position;
	}
}
