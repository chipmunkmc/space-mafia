using System.Collections;
using System.Runtime.CompilerServices;
using UnityEngine;

public class PbExileController : ExileController
{
	[CompilerGenerated]
	private sealed class _003C_003Ec__DisplayClass6_0
	{
		public PbExileController _003C_003E4__this;

		public Vector2 bottom;

		public Vector2 top;

		internal void _003CPlayerSpin_003Eb__0(float p)
		{
			_003C_003E4__this.Player.transform.localPosition = Vector2.LerpUnclamped(bottom, top, p);
		}
	}

	public SpriteRenderer HandSlot;

	public Sprite BadHand;

	public Sprite GoodHand;

	public void OnEnable()
	{
		StartCoroutine(Animate());
	}

	protected override IEnumerator Animate()
	{
		yield return DestroyableSingleton<HudManager>.Instance.CoFadeFullScreen(Color.black, Color.clear);
		yield return Effects.All(PlayerSpin(), HandleText());
		ImpostorText.gameObject.SetActive(true);
		yield return Effects.Bloop(0f, ImpostorText.transform);
		yield return new WaitForSeconds(0.5f);
		yield return DestroyableSingleton<HudManager>.Instance.CoFadeFullScreen(Color.clear, Color.black);
		WrapUp();
	}

	private IEnumerator HandleText()
	{
		yield return Effects.Wait(Duration * 0.5f);
		float newDur = Duration * 0.5f;
		for (float t = 0f; t <= newDur; t += Time.deltaTime)
		{
			int num = (int)(t / newDur * (float)completeString.Length);
			if (num > Text.Text.Length)
			{
				Text.Text = completeString.Substring(0, num);
				Text.gameObject.SetActive(true);
				if (completeString[num - 1] != ' ')
				{
					SoundManager.Instance.PlaySoundImmediate(TextSound, false, 0.8f);
				}
			}
			yield return null;
		}
		Text.Text = completeString;
	}

	private IEnumerator PlayerSpin()
	{
		_003C_003Ec__DisplayClass6_0 _003C_003Ec__DisplayClass6_ = new _003C_003Ec__DisplayClass6_0();
		_003C_003Ec__DisplayClass6_._003C_003E4__this = this;
		float num = Camera.main.orthographicSize + 1f;
		_003C_003Ec__DisplayClass6_.top = Vector2.up * num;
		_003C_003Ec__DisplayClass6_.bottom = Vector2.down * 2.81f;
		float d1 = Duration / 1.8f;
		for (float t2 = 0f; t2 <= d1; t2 += Time.deltaTime)
		{
			float time = t2 / d1;
			float t3 = LerpCurve.Evaluate(time);
			Player.transform.localPosition = Vector2.Lerp(_003C_003Ec__DisplayClass6_.top, _003C_003Ec__DisplayClass6_.bottom, t3);
			float num2 = (t2 + 0.75f) * 25f / Mathf.Exp(t2 * 0.75f + 1f);
			Player.transform.Rotate(new Vector3(0f, 0f, num2 * Time.deltaTime * 5f));
			yield return null;
		}
		if (exiled != null)
		{
			HandSlot.sprite = (exiled.IsImpostor ? GoodHand : BadHand);
			PlayerControl.SetPlayerMaterialColors(exiled.ColorId, HandSlot);
		}
		Player.transform.eulerAngles = new Vector3(0f, 0f, -10f);
		float duration = Duration / 4f;
		_003C_003Ec__DisplayClass6_.top.y = -1.68f;
		yield return Effects.Overlerp(duration, _003C_003Ec__DisplayClass6_._003CPlayerSpin_003Eb__0);
		float d2 = Duration / 2f;
		for (float t2 = 0f; t2 <= d2; t2 += Time.deltaTime)
		{
			float t4 = t2 / d2;
			Vector2 vector = Vector2.Lerp(_003C_003Ec__DisplayClass6_.top, _003C_003Ec__DisplayClass6_.bottom, t4);
			vector += Random.insideUnitCircle * 0.025f;
			Player.transform.localPosition = vector;
			Player.transform.eulerAngles = new Vector3(0f, 0f, Mathf.Lerp(-10f, 17f, t4));
			yield return null;
		}
	}
}
