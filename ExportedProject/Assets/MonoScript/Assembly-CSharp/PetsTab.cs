using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class PetsTab : MonoBehaviour
{
	[CompilerGenerated]
	private sealed class _003C_003Ec__DisplayClass11_0
	{
		public ColorChip chip;

		public PetBehaviour pet;

		public PetsTab _003C_003E4__this;

		internal void _003COnEnable_003Eb__0()
		{
			_003C_003E4__this.SelectPet(chip, pet);
		}
	}

	public ColorChip ColorTabPrefab;

	public SpriteRenderer DemoImage;

	public HatParent HatImage;

	public SpriteRenderer SkinImage;

	public SpriteRenderer PetImage;

	public FloatRange XRange = new FloatRange(1.5f, 3f);

	public float YStart = 0.8f;

	public float YOffset = 0.8f;

	public int NumPerRow = 4;

	public Scroller scroller;

	private List<ColorChip> ColorChips = new List<ColorChip>();

	public void OnEnable()
	{
		PlayerControl.SetPlayerMaterialColors(PlayerControl.LocalPlayer.Data.ColorId, DemoImage);
		HatImage.SetHat(SaveManager.LastHat);
		PlayerControl.SetSkinImage(SaveManager.LastSkin, SkinImage);
		PlayerControl.SetPetImage(SaveManager.LastPet, PlayerControl.LocalPlayer.Data.ColorId, PetImage);
		PetBehaviour[] unlockedPets = DestroyableSingleton<HatManager>.Instance.GetUnlockedPets();
		for (int i = 0; i < unlockedPets.Length; i++)
		{
			_003C_003Ec__DisplayClass11_0 _003C_003Ec__DisplayClass11_ = new _003C_003Ec__DisplayClass11_0();
			_003C_003Ec__DisplayClass11_._003C_003E4__this = this;
			_003C_003Ec__DisplayClass11_.pet = unlockedPets[i];
			float x = XRange.Lerp((float)(i % NumPerRow) / ((float)NumPerRow - 1f));
			float y = YStart - (float)(i / NumPerRow) * YOffset;
			_003C_003Ec__DisplayClass11_.chip = Object.Instantiate(ColorTabPrefab, scroller.Inner);
			_003C_003Ec__DisplayClass11_.chip.transform.localPosition = new Vector3(x, y, -1f);
			_003C_003Ec__DisplayClass11_.chip.InUseForeground.SetActive(DestroyableSingleton<HatManager>.Instance.GetIdFromPet(_003C_003Ec__DisplayClass11_.pet) == SaveManager.LastPet);
			_003C_003Ec__DisplayClass11_.chip.Button.OnClick.AddListener(_003C_003Ec__DisplayClass11_._003COnEnable_003Eb__0);
			PlayerControl.SetPetImage(_003C_003Ec__DisplayClass11_.pet, PlayerControl.LocalPlayer.Data.ColorId, _003C_003Ec__DisplayClass11_.chip.Inner.FrontLayer);
			ColorChips.Add(_003C_003Ec__DisplayClass11_.chip);
		}
		scroller.YBounds.max = 0f - (YStart - (float)(unlockedPets.Length / NumPerRow) * YOffset) - 3f;
	}

	public void OnDisable()
	{
		for (int i = 0; i < ColorChips.Count; i++)
		{
			Object.Destroy(ColorChips[i].gameObject);
		}
		ColorChips.Clear();
	}

	private void SelectPet(ColorChip sender, PetBehaviour pet)
	{
		uint petId = (SaveManager.LastPet = DestroyableSingleton<HatManager>.Instance.GetIdFromPet(pet));
		PlayerControl.SetPetImage(pet, PlayerControl.LocalPlayer.Data.ColorId, PetImage);
		if ((bool)PlayerControl.LocalPlayer)
		{
			PlayerControl.LocalPlayer.RpcSetPet(petId);
		}
		for (int i = 0; i < ColorChips.Count; i++)
		{
			ColorChip colorChip = ColorChips[i];
			colorChip.InUseForeground.SetActive(colorChip == sender);
		}
	}
}
