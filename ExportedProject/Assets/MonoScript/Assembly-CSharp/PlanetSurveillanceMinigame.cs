using System.Collections;
using UnityEngine;

public class PlanetSurveillanceMinigame : Minigame
{
	public Camera Camera;

	public GameObject Viewables;

	public MeshRenderer ViewPort;

	public TextRenderer LocationName;

	public TextRenderer SabText;

	private RenderTexture texture;

	public MeshRenderer FillQuad;

	public Material DefaultMaterial;

	public Material StaticMaterial;

	private bool isStatic;

	private SurvCamera[] survCameras;

	public Transform DotParent;

	private SpriteRenderer[] Dots;

	public Sprite DotEnabled;

	public Sprite DotDisabled;

	private int currentCamera;

	public AudioClip ChangeSound;

	public override void Begin(PlayerTask task)
	{
		base.Begin(task);
		RenderTexture renderTexture = (texture = RenderTexture.GetTemporary(330, 230, 16, RenderTextureFormat.ARGB32));
		Camera.targetTexture = renderTexture;
		ViewPort.material.SetTexture("_MainTex", renderTexture);
		survCameras = ShipStatus.Instance.AllCameras;
		Dots = new SpriteRenderer[survCameras.Length];
		for (int i = 0; i < Dots.Length; i++)
		{
			GameObject obj = new GameObject("Dot" + i, typeof(SpriteRenderer));
			obj.layer = base.gameObject.layer;
			obj.transform.SetParent(DotParent);
			SpriteRenderer component = obj.GetComponent<SpriteRenderer>();
			component.sprite = DotDisabled;
			Dots[i] = component;
		}
		DotAligner.Align(DotParent, 0.25f, true);
		NextCamera(0);
		if (!PlayerControl.LocalPlayer.Data.IsDead)
		{
			ShipStatus.Instance.RpcRepairSystem(SystemTypes.Security, 1);
		}
	}

	public void Update()
	{
		if (isStatic && !PlayerTask.PlayerHasTaskOfType<IHudOverrideTask>(PlayerControl.LocalPlayer))
		{
			isStatic = false;
			ViewPort.sharedMaterial = DefaultMaterial;
			ViewPort.material.SetTexture("_MainTex", texture);
			SabText.gameObject.SetActive(false);
		}
		else if (!isStatic && PlayerTask.PlayerHasTaskOfType<HudOverrideTask>(PlayerControl.LocalPlayer))
		{
			isStatic = true;
			ViewPort.sharedMaterial = StaticMaterial;
			SabText.gameObject.SetActive(true);
		}
	}

	public void NextCamera(int direction)
	{
		if (direction != 0 && Constants.ShouldPlaySfx())
		{
			SoundManager.Instance.PlaySound(ChangeSound, false);
		}
		Dots[currentCamera].sprite = DotDisabled;
		currentCamera = (currentCamera + direction).Wrap(survCameras.Length);
		Dots[currentCamera].sprite = DotEnabled;
		SurvCamera survCamera = survCameras[currentCamera];
		Camera.transform.position = survCamera.transform.position + survCameras[currentCamera].Offset;
		LocationName.Text = survCamera.CamName;
		StartCoroutine(PulseStatic());
	}

	private IEnumerator PulseStatic()
	{
		ViewPort.sharedMaterial = StaticMaterial;
		ViewPort.material.SetTexture("_MainTex", null);
		yield return Effects.Wait(0.2f);
		ViewPort.sharedMaterial = DefaultMaterial;
		ViewPort.material.SetTexture("_MainTex", texture);
	}

	protected override IEnumerator CoAnimateOpen()
	{
		Viewables.SetActive(false);
		FillQuad.material.SetFloat("_Center", -5f);
		FillQuad.material.SetColor("_Color2", Color.clear);
		for (float timer3 = 0f; timer3 < 0.25f; timer3 += Time.deltaTime)
		{
			FillQuad.material.SetColor("_Color2", Color.Lerp(Color.clear, Color.black, timer3 / 0.25f));
			yield return null;
		}
		FillQuad.material.SetColor("_Color2", Color.black);
		Viewables.SetActive(true);
		for (float timer3 = 0f; timer3 < 0.1f; timer3 += Time.deltaTime)
		{
			FillQuad.material.SetFloat("_Center", Mathf.Lerp(-5f, 0f, timer3 / 0.1f));
			yield return null;
		}
		for (float timer3 = 0f; timer3 < 0.15f; timer3 += Time.deltaTime)
		{
			FillQuad.material.SetFloat("_Center", Mathf.Lerp(-3f, 0.4f, timer3 / 0.15f));
			yield return null;
		}
		FillQuad.material.SetFloat("_Center", 0.4f);
	}

	private IEnumerator CoAnimateClose()
	{
		for (float timer2 = 0f; timer2 < 0.1f; timer2 += Time.deltaTime)
		{
			FillQuad.material.SetFloat("_Center", Mathf.Lerp(0.4f, -5f, timer2 / 0.1f));
			yield return null;
		}
		Viewables.SetActive(false);
		for (float timer2 = 0f; timer2 < 0.3f; timer2 += Time.deltaTime)
		{
			FillQuad.material.SetColor("_Color2", Color.Lerp(Color.black, Color.clear, timer2 / 0.3f));
			yield return null;
		}
		FillQuad.material.SetColor("_Color2", Color.clear);
	}

	protected override IEnumerator CoDestroySelf()
	{
		yield return CoAnimateClose();
		Object.Destroy(base.gameObject);
	}

	public override void Close()
	{
		ShipStatus.Instance.RpcRepairSystem(SystemTypes.Security, 2);
		base.Close();
	}

	public void OnDestroy()
	{
		texture.Release();
	}
}
