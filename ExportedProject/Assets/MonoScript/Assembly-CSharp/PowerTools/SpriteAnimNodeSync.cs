using UnityEngine;

namespace PowerTools
{
	public class SpriteAnimNodeSync : MonoBehaviour
	{
		public int NodeId;

		public SpriteAnimNodes Parent;

		public SpriteRenderer ParentRenderer;

		public SpriteRenderer Renderer;

		public void LateUpdate()
		{
			bool flag = false;
			if ((bool)ParentRenderer)
			{
				flag = ParentRenderer.flipX;
				if ((bool)Renderer)
				{
					Renderer.flipX = ParentRenderer.flipX;
				}
				else
				{
					flag = flag;
				}
			}
			else if ((bool)Renderer)
			{
				flag = Renderer.flipX;
			}
			Vector3 localPosition = base.transform.localPosition;
			Vector3 localPosition2 = Parent.GetLocalPosition(NodeId);
			localPosition.x = localPosition2.x;
			localPosition.y = localPosition2.y;
			base.transform.localPosition = localPosition;
			float angle = Parent.GetAngle(NodeId);
			if (flag)
			{
				base.transform.eulerAngles = new Vector3(0f, 0f, 0f - angle);
			}
			else
			{
				base.transform.eulerAngles = new Vector3(0f, 0f, angle);
			}
		}
	}
}
