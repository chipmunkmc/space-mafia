using System;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;

public class ProgressTracker : MonoBehaviour
{
	[Serializable]
	[CompilerGenerated]
	private sealed class _003C_003Ec
	{
		public static readonly _003C_003Ec _003C_003E9 = new _003C_003Ec();

		public static Func<GameData.PlayerInfo, bool> _003C_003E9__3_0;

		internal bool _003CFixedUpdate_003Eb__3_0(GameData.PlayerInfo p)
		{
			return p.Disconnected;
		}
	}

	public MeshRenderer TileParent;

	private float curValue;

	public void Start()
	{
		TileParent.material.SetFloat("_Buckets", 1f);
		TileParent.material.SetFloat("_FullBuckets", 0f);
	}

	public void FixedUpdate()
	{
		if (PlayerTask.PlayerHasTaskOfType<IHudOverrideTask>(PlayerControl.LocalPlayer))
		{
			TileParent.enabled = false;
			return;
		}
		if (!TileParent.enabled)
		{
			TileParent.enabled = true;
		}
		GameData instance = GameData.Instance;
		if ((bool)instance && instance.TotalTasks > 0)
		{
			int num = (DestroyableSingleton<TutorialManager>.InstanceExists ? 1 : (instance.AllPlayers.Count - PlayerControl.GameOptions.NumImpostors));
			num -= instance.AllPlayers.Count(_003C_003Ec._003C_003E9__3_0 ?? (_003C_003Ec._003C_003E9__3_0 = _003C_003Ec._003C_003E9._003CFixedUpdate_003Eb__3_0));
			float b = (float)instance.CompletedTasks / (float)instance.TotalTasks * (float)num;
			curValue = Mathf.Lerp(curValue, b, Time.fixedDeltaTime * 2f);
			TileParent.material.SetFloat("_Buckets", num);
			TileParent.material.SetFloat("_FullBuckets", curValue);
		}
	}
}
