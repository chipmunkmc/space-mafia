using System.Runtime.CompilerServices;
using UnityEngine;

public class RaycastAmbientSoundPlayer : MonoBehaviour
{
	public AudioClip AmbientSound;

	public float AmbientVolume = 1f;

	public float AmbientMaxDist = 8f;

	public float HitModifier = 0.25f;

	private RaycastHit2D[] volumeBuffer = new RaycastHit2D[5];

	public void Start()
	{
		if (Constants.ShouldPlaySfx() && (bool)AmbientSound)
		{
			SoundManager.Instance.PlayDynamicSound("Amb " + base.name, AmbientSound, true, _003CStart_003Eb__4_0);
		}
	}

	private void GetAmbientSoundVolume(AudioSource player, float dt)
	{
		if (!PlayerControl.LocalPlayer)
		{
			player.volume = 0f;
			return;
		}
		Vector2 vector = base.transform.position;
		Vector2 truePosition = PlayerControl.LocalPlayer.GetTruePosition();
		float num = Vector2.Distance(vector, truePosition);
		if (num > AmbientMaxDist)
		{
			player.volume = 0f;
			return;
		}
		Vector2 direction = truePosition - vector;
		int num2 = Physics2D.RaycastNonAlloc(vector, direction, volumeBuffer, num, Constants.ShipOnlyMask);
		float num3 = 1f - num / AmbientMaxDist - (float)num2 * HitModifier;
		player.volume = Mathf.Lerp(player.volume, num3 * AmbientVolume, dt);
	}

	[CompilerGenerated]
	private void _003CStart_003Eb__4_0(AudioSource player, float dt)
	{
		GetAmbientSoundVolume(player, dt);
	}
}
