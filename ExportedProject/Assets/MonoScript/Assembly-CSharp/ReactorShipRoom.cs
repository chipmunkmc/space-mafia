using PowerTools;
using UnityEngine;

public class ReactorShipRoom : SkeldShipRoom
{
	public Sprite normalManifolds;

	public Sprite meltdownManifolds;

	public SpriteRenderer Manifolds;

	public AnimationClip normalReactor;

	public AnimationClip meltdownReactor;

	public SpriteAnim Reactor;

	public AnimationClip normalHighFloor;

	public AnimationClip meltdownHighFloor;

	public SpriteAnim HighFloor;

	public AnimationClip normalMidFloor;

	public AnimationClip meltdownMidFloor;

	public SpriteAnim MidFloor1;

	public SpriteAnim MidFloor2;

	public AnimationClip normalLowFloor;

	public AnimationClip meltdownLowFloor;

	public SpriteAnim LowFloor;

	public AnimationClip[] normalPipes;

	public AnimationClip[] meltdownPipes;

	public SpriteAnim[] Pipes;

	public SupressorBehaviour[] Supressors;

	public void StartMeltdown()
	{
		if ((bool)Manifolds)
		{
			Manifolds.sprite = meltdownManifolds;
		}
		if ((bool)Reactor)
		{
			Reactor.Play(meltdownReactor);
		}
		if ((bool)HighFloor)
		{
			HighFloor.Play(meltdownHighFloor);
		}
		if ((bool)MidFloor1)
		{
			MidFloor1.Play(meltdownMidFloor);
		}
		if ((bool)MidFloor2)
		{
			MidFloor2.Play(meltdownMidFloor);
		}
		if ((bool)LowFloor)
		{
			LowFloor.Play(meltdownLowFloor);
		}
		for (int i = 0; i < Pipes.Length; i++)
		{
			Pipes[i].Play(meltdownPipes[i]);
		}
		for (int j = 0; j < Supressors.Length; j++)
		{
			Supressors[j].Deactivate();
		}
	}

	public void StopMeltdown()
	{
		if ((bool)Manifolds)
		{
			Manifolds.sprite = normalManifolds;
		}
		if ((bool)Reactor)
		{
			Reactor.Play(normalReactor);
		}
		if ((bool)HighFloor)
		{
			HighFloor.Play(normalHighFloor);
		}
		if ((bool)MidFloor1)
		{
			MidFloor1.Play(normalMidFloor);
		}
		if ((bool)MidFloor2)
		{
			MidFloor2.Play(normalMidFloor);
		}
		if ((bool)LowFloor)
		{
			LowFloor.Play(normalLowFloor);
		}
		for (int i = 0; i < Pipes.Length; i++)
		{
			Pipes[i].Play(normalPipes[i]);
		}
		for (int j = 0; j < Supressors.Length; j++)
		{
			Supressors[j].Activate();
		}
	}
}
