using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Hazel;

public class ReactorSystemType : ISystemType, IActivatable
{
	[CompilerGenerated]
	private sealed class _003C_003Ec__DisplayClass16_0
	{
		public int consoleId;

		internal bool _003CGetConsoleComplete_003Eb__0(Tuple<byte, byte> kvp)
		{
			return kvp.Item2 == consoleId;
		}
	}

	private const float SyncRate = 2f;

	private float timer;

	public const byte StartCountdown = 128;

	public const byte AddUserOp = 64;

	public const byte RemoveUserOp = 32;

	public const byte ClearCountdown = 16;

	public const float CountdownStopped = 10000f;

	public readonly float ReactorDuration = 30f;

	public const byte ConsoleIdMask = 3;

	public const byte RequiredUserCount = 2;

	public float Countdown = 10000f;

	private HashSet<Tuple<byte, byte>> UserConsolePairs = new HashSet<Tuple<byte, byte>>();

	private SystemTypes system;

	public int UserCount
	{
		get
		{
			int num = 0;
			int num2 = 0;
			foreach (Tuple<byte, byte> userConsolePair in UserConsolePairs)
			{
				int num3 = 1 << (int)userConsolePair.Item2;
				if ((num3 & num2) == 0)
				{
					num++;
					num2 |= num3;
				}
			}
			return num;
		}
	}

	public bool IsActive
	{
		get
		{
			return Countdown < 10000f;
		}
	}

	public ReactorSystemType(float duration, SystemTypes system)
	{
		ReactorDuration = duration;
		this.system = system;
	}

	public bool GetConsoleComplete(int consoleId)
	{
		_003C_003Ec__DisplayClass16_0 _003C_003Ec__DisplayClass16_ = new _003C_003Ec__DisplayClass16_0();
		_003C_003Ec__DisplayClass16_.consoleId = consoleId;
		return UserConsolePairs.Any(_003C_003Ec__DisplayClass16_._003CGetConsoleComplete_003Eb__0);
	}

	public void RepairDamage(PlayerControl player, byte opCode)
	{
		int num = opCode & 3;
		if (opCode == 128 && !IsActive)
		{
			Countdown = ReactorDuration;
			UserConsolePairs.Clear();
		}
		else if (opCode == 16)
		{
			Countdown = 10000f;
		}
		else if (opCode.HasAnyBit((byte)64))
		{
			UserConsolePairs.Add(new Tuple<byte, byte>(player.PlayerId, (byte)num));
			if (UserCount >= 2)
			{
				Countdown = 10000f;
			}
		}
		else if (opCode.HasAnyBit((byte)32))
		{
			UserConsolePairs.Remove(new Tuple<byte, byte>(player.PlayerId, (byte)num));
		}
	}

	public bool Detoriorate(float deltaTime)
	{
		if (IsActive)
		{
			if (!PlayerTask.PlayerHasTaskOfType<ReactorTask>(PlayerControl.LocalPlayer))
			{
				PlayerControl.LocalPlayer.AddSystemTask(system);
			}
			Countdown -= deltaTime;
			timer += deltaTime;
			if (timer > 2f)
			{
				timer = 0f;
				return true;
			}
		}
		else if (DestroyableSingleton<HudManager>.Instance.ReactorFlash != null)
		{
			DestroyableSingleton<HudManager>.Instance.StopReactorFlash();
			ReactorShipRoom reactorShipRoom = ShipStatus.Instance.AllRooms.FirstOrDefault(_003CDetoriorate_003Eb__20_0) as ReactorShipRoom;
			if ((bool)reactorShipRoom)
			{
				reactorShipRoom.StopMeltdown();
			}
		}
		return false;
	}

	public void Serialize(MessageWriter writer, bool initialState)
	{
		writer.Write(Countdown);
		writer.WritePacked(UserConsolePairs.Count);
		foreach (Tuple<byte, byte> userConsolePair in UserConsolePairs)
		{
			writer.Write(userConsolePair.Item1);
			writer.Write(userConsolePair.Item2);
		}
	}

	public void Deserialize(MessageReader reader, bool initialState)
	{
		Countdown = reader.ReadSingle();
		UserConsolePairs.Clear();
		int num = reader.ReadPackedInt32();
		for (int i = 0; i < num; i++)
		{
			UserConsolePairs.Add(new Tuple<byte, byte>(reader.ReadByte(), reader.ReadByte()));
		}
	}

	[CompilerGenerated]
	private bool _003CDetoriorate_003Eb__20_0(PlainShipRoom r)
	{
		return r.RoomId == system;
	}
}
