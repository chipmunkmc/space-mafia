using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using UnityEngine;

public class ReactorTask : SabotageTask
{
	private bool isComplete;

	private ReactorSystemType reactor;

	private bool even;

	public override int TaskStep
	{
		get
		{
			return reactor.UserCount;
		}
	}

	public override bool IsComplete
	{
		get
		{
			return isComplete;
		}
	}

	public override void Initialize()
	{
		SetupArrows();
	}

	public void Awake()
	{
		reactor = (ReactorSystemType)ShipStatus.Instance.Systems[StartAt];
		DestroyableSingleton<HudManager>.Instance.StartReactorFlash();
		ReactorShipRoom reactorShipRoom = ShipStatus.Instance.AllRooms.FirstOrDefault(_003CAwake_003Eb__8_0) as ReactorShipRoom;
		if ((bool)reactorShipRoom)
		{
			reactorShipRoom.StartMeltdown();
		}
	}

	private void FixedUpdate()
	{
		if (!isComplete && !reactor.IsActive)
		{
			Complete();
		}
	}

	public override bool ValidConsole(Console console)
	{
		return console.TaskTypes.Contains(TaskType);
	}

	public override void OnRemove()
	{
	}

	public override void Complete()
	{
		isComplete = true;
		PlayerControl.LocalPlayer.RemoveTask(this);
		if (didContribute)
		{
			StatsManager.Instance.SabsFixed++;
		}
	}

	public override void AppendTaskText(StringBuilder sb)
	{
		even = !even;
		Color color = (even ? Color.yellow : Color.red);
		sb.Append(color.ToTextColor());
		sb.Append(DestroyableSingleton<TranslationController>.Instance.GetString(TaskType));
		sb.Append(" ");
		sb.Append((int)reactor.Countdown);
		sb.AppendLine(string.Format(" ({0}/{1})[]", reactor.UserCount, (byte)2));
		for (int i = 0; i < Arrows.Length; i++)
		{
			Arrows[i].image.color = color;
		}
	}

	[CompilerGenerated]
	private bool _003CAwake_003Eb__8_0(PlainShipRoom r)
	{
		return r.RoomId == StartAt;
	}
}
