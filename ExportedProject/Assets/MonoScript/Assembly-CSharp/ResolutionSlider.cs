using System;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;

public class ResolutionSlider : MonoBehaviour
{
	[Serializable]
	[CompilerGenerated]
	private sealed class _003C_003Ec
	{
		public static readonly _003C_003Ec _003C_003E9 = new _003C_003Ec();

		public static Func<Resolution, bool> _003C_003E9__8_0;

		internal bool _003COnEnable_003Eb__8_0(Resolution r)
		{
			return r.height > 480;
		}
	}

	private int targetIdx;

	private Resolution targetResolution;

	private bool targetFullscreen;

	private Resolution[] allResolutions;

	public SlideBar slider;

	public ToggleButtonBehaviour Fullscreen;

	public ToggleButtonBehaviour VSync;

	public TextRenderer Display;

	public void OnEnable()
	{
		allResolutions = Screen.resolutions.Where(_003C_003Ec._003C_003E9__8_0 ?? (_003C_003Ec._003C_003E9__8_0 = _003C_003Ec._003C_003E9._003COnEnable_003Eb__8_0)).ToArray();
		targetResolution = Screen.currentResolution;
		targetFullscreen = Screen.fullScreen;
		targetIdx = allResolutions.IndexOf(_003COnEnable_003Eb__8_1);
		slider.Value = (float)targetIdx / ((float)allResolutions.Length - 1f);
		Display.Text = string.Format("{0}x{1}", targetResolution.width, targetResolution.height);
		Fullscreen.UpdateText(targetFullscreen);
		VSync.UpdateText(SaveManager.VSync);
	}

	public void ToggleVSync()
	{
		SaveManager.VSync = !SaveManager.VSync;
		if (SaveManager.VSync)
		{
			QualitySettings.vSyncCount = 1;
		}
		else
		{
			QualitySettings.vSyncCount = 0;
		}
		VSync.UpdateText(SaveManager.VSync);
	}

	public void ToggleFullscreen()
	{
		targetFullscreen = !targetFullscreen;
		Fullscreen.UpdateText(targetFullscreen);
	}

	public void OnResChange(SlideBar slider)
	{
		int num = Mathf.RoundToInt((float)(allResolutions.Length - 1) * slider.Value);
		if (num != targetIdx)
		{
			targetIdx = num;
			targetResolution = allResolutions[num];
			Display.Text = string.Format("{0}x{1}", targetResolution.width, targetResolution.height);
		}
		slider.Value = (float)targetIdx / ((float)allResolutions.Length - 1f);
	}

	public void SaveChange()
	{
		ResolutionManager.SetResolution(targetResolution.width, targetResolution.height, targetFullscreen);
	}

	[CompilerGenerated]
	private bool _003COnEnable_003Eb__8_1(Resolution e)
	{
		if (e.width == targetResolution.width)
		{
			return e.height == targetResolution.height;
		}
		return false;
	}
}
