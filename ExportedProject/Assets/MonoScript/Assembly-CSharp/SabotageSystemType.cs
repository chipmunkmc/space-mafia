using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Hazel;

public class SabotageSystemType : ISystemType
{
	public class DummySab : IActivatable
	{
		public float timer;

		public bool IsActive
		{
			get
			{
				return timer > 0f;
			}
		}
	}

	[Serializable]
	[CompilerGenerated]
	private sealed class _003C_003Ec
	{
		public static readonly _003C_003Ec _003C_003E9 = new _003C_003Ec();

		public static Func<IActivatable, bool> _003C_003E9__9_0;

		public static Predicate<IActivatable> _003C_003E9__13_0;

		internal bool _003Cget_AnyActive_003Eb__9_0(IActivatable s)
		{
			return s.IsActive;
		}

		internal bool _003C_002Ector_003Eb__13_0(IActivatable d)
		{
			return d is IDoorSystem;
		}
	}

	public const float SpecialSabDelay = 30f;

	private List<IActivatable> specials;

	private bool dirty;

	private DummySab dummy = new DummySab();

	public float Timer { get; set; }

	public float PercentCool
	{
		get
		{
			return Timer / 30f;
		}
	}

	public bool AnyActive
	{
		get
		{
			return specials.Any(_003C_003Ec._003C_003E9__9_0 ?? (_003C_003Ec._003C_003E9__9_0 = _003C_003Ec._003C_003E9._003Cget_AnyActive_003Eb__9_0));
		}
	}

	public SabotageSystemType(IActivatable[] specials)
	{
		this.specials = new List<IActivatable>(specials);
		this.specials.RemoveAll(_003C_003Ec._003C_003E9__13_0 ?? (_003C_003Ec._003C_003E9__13_0 = _003C_003Ec._003C_003E9._003C_002Ector_003Eb__13_0));
		this.specials.Add(dummy);
	}

	public bool Detoriorate(float deltaTime)
	{
		dummy.timer -= deltaTime;
		if (Timer > 0f && !AnyActive)
		{
			Timer -= deltaTime;
			if (Timer <= 0f)
			{
				return true;
			}
		}
		return dirty;
	}

	public void ForceSabTime(float t)
	{
		dummy.timer = t;
	}

	public void RepairDamage(PlayerControl player, byte amount)
	{
		dirty = true;
		if (Timer > 0f || (bool)MeetingHud.Instance)
		{
			return;
		}
		if (AmongUsClient.Instance.AmHost)
		{
			switch (amount)
			{
			case 21:
				ShipStatus.Instance.RepairSystem(SystemTypes.Laboratory, player, 128);
				break;
			case 3:
				ShipStatus.Instance.RepairSystem(SystemTypes.Reactor, player, 128);
				break;
			case 8:
				ShipStatus.Instance.RepairSystem(SystemTypes.LifeSupp, player, 128);
				break;
			case 14:
				ShipStatus.Instance.RepairSystem(SystemTypes.Comms, player, 128);
				break;
			case 7:
			{
				byte b = 4;
				for (int i = 0; i < 5; i++)
				{
					if (BoolRange.Next())
					{
						b = (byte)(b | (byte)(1 << i));
					}
				}
				ShipStatus.Instance.RpcRepairSystem(SystemTypes.Electrical, (byte)(b | 0x80));
				break;
			}
			}
		}
		Timer = 30f;
	}

	public void Serialize(MessageWriter writer, bool initialState)
	{
		writer.Write(Timer);
		if (!initialState)
		{
			dirty = false;
		}
	}

	public void Deserialize(MessageReader reader, bool initialState)
	{
		Timer = reader.ReadSingle();
	}
}
