using System.Linq;
using UnityEngine;

public class Scroller : PassiveUiElement
{
	public Transform Inner;

	public bool allowX;

	public FloatRange XBounds = new FloatRange(-10f, 10f);

	public bool allowY;

	public FloatRange YBounds = new FloatRange(-10f, 10f);

	public FloatRange ScrollerYRange;

	public SpriteRenderer ScrollerY;

	private Vector2 velocity;

	private bool active;

	public override bool HandleUp
	{
		get
		{
			return true;
		}
	}

	public override bool HandleDown
	{
		get
		{
			return true;
		}
	}

	public override bool HandleDrag
	{
		get
		{
			return true;
		}
	}

	public override bool HandleOverOut
	{
		get
		{
			return false;
		}
	}

	public bool AtTop
	{
		get
		{
			return Inner.localPosition.y <= YBounds.min + 0.25f;
		}
	}

	public bool AtBottom
	{
		get
		{
			return Inner.localPosition.y >= YBounds.max - 0.25f;
		}
	}

	public Collider2D Hitbox
	{
		get
		{
			return Colliders[0];
		}
	}

	public void FixedUpdate()
	{
		if ((bool)Inner)
		{
			Vector2 mouseScrollDelta = Input.mouseScrollDelta;
			if (mouseScrollDelta.y != 0f)
			{
				mouseScrollDelta.y = 0f - mouseScrollDelta.y;
				ScrollRelative(mouseScrollDelta);
			}
		}
	}

	public void Update()
	{
		if (!active && velocity.sqrMagnitude > 0.01f)
		{
			velocity = Vector2.ClampMagnitude(velocity, velocity.magnitude - 10f * Time.deltaTime);
			ScrollRelative(velocity * Time.deltaTime);
		}
	}

	public void ScrollDown()
	{
		Collider2D collider2D = Colliders.First();
		float num = collider2D.bounds.max.y - collider2D.bounds.min.y;
		ScrollRelative(new Vector2(0f, num * 0.75f));
	}

	public void ScrollUp()
	{
		Collider2D collider2D = Colliders.First();
		float num = collider2D.bounds.max.y - collider2D.bounds.min.y;
		ScrollRelative(new Vector2(0f, num * -0.75f));
	}

	public void ScrollPercentY(float p)
	{
		Vector3 localPosition = Inner.transform.localPosition;
		Mathf.Max(YBounds.min, YBounds.max);
		localPosition.y = YBounds.Lerp(p);
		Inner.transform.localPosition = localPosition;
		UpdateScrollBars(localPosition);
	}

	public override void ReceiveClickDown()
	{
		active = true;
	}

	public override void ReceiveClickUp()
	{
		active = false;
	}

	public override void ReceiveClickDrag(Vector2 dragDelta)
	{
		velocity = dragDelta / Time.deltaTime * 0.9f;
		ScrollRelative(dragDelta);
	}

	public void ScrollRelative(Vector2 dragDelta)
	{
		if (!(dragDelta.magnitude < 0.05f))
		{
			if (!allowX)
			{
				dragDelta.x = 0f;
			}
			if (!allowY)
			{
				dragDelta.y = 0f;
			}
			Vector3 vector = Inner.transform.localPosition + (Vector3)dragDelta;
			vector.x = XBounds.Clamp(vector.x);
			int childCount = Inner.transform.childCount;
			float max = Mathf.Max(YBounds.min, YBounds.max);
			vector.y = Mathf.Clamp(vector.y, YBounds.min, max);
			Inner.transform.localPosition = vector;
			UpdateScrollBars(vector);
		}
	}

	private void UpdateScrollBars(Vector3 pos)
	{
		if ((bool)ScrollerY)
		{
			if (YBounds.min == YBounds.max)
			{
				ScrollerY.enabled = false;
				return;
			}
			ScrollerY.enabled = true;
			float num = YBounds.ReverseLerp(pos.y);
			Vector3 localPosition = ScrollerY.transform.localPosition;
			localPosition.y = ScrollerYRange.Lerp(1f - num);
			ScrollerY.transform.localPosition = localPosition;
		}
	}
}
