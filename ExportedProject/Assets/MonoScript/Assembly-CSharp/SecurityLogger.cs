using System.Collections;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;

public class SecurityLogger : MonoBehaviour
{
	[CompilerGenerated]
	private sealed class _003C_003Ec__DisplayClass9_0
	{
		public int i;

		internal bool _003CFixedUpdate_003Eb__0(PlayerControl p)
		{
			return p.Collider == hits[i];
		}
	}

	private static Collider2D[] hits = new Collider2D[10];

	public SecurityLogBehaviour LogParent;

	public SecurityLogBehaviour.SecurityLogLocations MyLocation;

	public float Cooldown = 5f;

	public SpriteRenderer Image;

	public BoxCollider2D Sensor;

	private float[] Timers = new float[10];

	private ContactFilter2D filter;

	private void Awake()
	{
		filter = default(ContactFilter2D);
		filter.useLayerMask = true;
		filter.layerMask = Constants.PlayersOnlyMask;
	}

	public void FixedUpdate()
	{
		for (int i = 0; i < Timers.Length; i++)
		{
			Timers[i] -= Time.deltaTime;
		}
		int num = Sensor.OverlapCollider(filter, hits);
		_003C_003Ec__DisplayClass9_0 _003C_003Ec__DisplayClass9_ = new _003C_003Ec__DisplayClass9_0();
		_003C_003Ec__DisplayClass9_.i = 0;
		while (_003C_003Ec__DisplayClass9_.i < num)
		{
			PlayerControl playerControl = PlayerControl.AllPlayerControls.FirstOrDefault(_003C_003Ec__DisplayClass9_._003CFixedUpdate_003Eb__0);
			if ((bool)playerControl && playerControl.Data != null && !playerControl.Data.IsDead && Timers[playerControl.PlayerId] < 0f)
			{
				Timers[playerControl.PlayerId] = Cooldown;
				LogParent.LogPlayer(playerControl, MyLocation);
				StopAllCoroutines();
				StartCoroutine(BlinkSensor());
			}
			_003C_003Ec__DisplayClass9_.i++;
		}
	}

	private IEnumerator BlinkSensor()
	{
		yield return Effects.Wait(0.1f);
		Image.color = LogParent.BarColors[(byte)MyLocation];
		yield return Effects.Wait(0.1f);
		Image.color = new Color(1f, 1f, 1f, 0.5f);
	}
}
