using UnityEngine;

public class SensorDoor : MonoBehaviour
{
	public SpriteRenderer LeftSide;

	public SpriteRenderer RightSide;

	public float ActivationDistance = 2f;

	public bool Opening;

	public float OpenDuration;

	private float openTimer;

	public void OnEnable()
	{
		InvokeRepeating("CheckDoor", 0.1f, 0.1f);
		LeftSide.SetCooldownNormalizedUvs();
		RightSide.SetCooldownNormalizedUvs();
	}

	[ContextMenu("Set Right Uvs")]
	public void SetUvs()
	{
		RightSide.SetCooldownNormalizedUvs();
	}

	private void Update()
	{
		if (Opening && openTimer < OpenDuration)
		{
			openTimer += Time.deltaTime;
			float value = Mathf.SmoothStep(0f, 1f, openTimer / OpenDuration);
			LeftSide.material.SetFloat("_Percent", value);
			RightSide.material.SetFloat("_Percent", value);
		}
		else if (!Opening && openTimer > 0f)
		{
			openTimer -= Time.deltaTime;
			float value2 = Mathf.SmoothStep(0f, 1f, openTimer / OpenDuration);
			LeftSide.material.SetFloat("_Percent", value2);
			RightSide.material.SetFloat("_Percent", value2);
		}
	}

	private void CheckDoor()
	{
		Opening = PhysicsHelpers.CircleContains(base.transform.position, ActivationDistance, Constants.PlayersOnlyMask);
	}
}
