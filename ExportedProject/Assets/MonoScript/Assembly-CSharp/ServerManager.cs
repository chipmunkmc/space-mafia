using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;

public class ServerManager : DestroyableSingleton<ServerManager>
{
	private enum UpdateState
	{
		Connecting = 0,
		Failed = 1,
		Success = 2
	}

	[Serializable]
	[CompilerGenerated]
	private sealed class _003C_003Ec
	{
		public static readonly _003C_003Ec _003C_003E9 = new _003C_003Ec();

		public static Func<ServerInfo, bool> _003C_003E9__12_0;

		public static Func<ServerInfo, int> _003C_003E9__12_1;

		public static Func<ServerInfo, int> _003C_003E9__12_2;

		public static Func<ServerInfo, int> _003C_003E9__17_1;

		internal bool _003CReselectServer_003Eb__12_0(ServerInfo s)
		{
			return s.Players == 0;
		}

		internal int _003CReselectServer_003Eb__12_1(ServerInfo s)
		{
			return s.ConnectionFailures;
		}

		internal int _003CReselectServer_003Eb__12_2(ServerInfo s)
		{
			return s.Players;
		}

		internal int _003CTrackServerFailure_003Eb__17_1(ServerInfo s)
		{
			return s.Players;
		}
	}

	[CompilerGenerated]
	private sealed class _003C_003Ec__DisplayClass14_0
	{
		public ServerInfo existingServer;

		internal bool _003CSetServers_003Eb__0(ServerInfo s)
		{
			return s.Ip.Equals(existingServer.Ip);
		}
	}

	[CompilerGenerated]
	private sealed class _003C_003Ec__DisplayClass17_0
	{
		public string networkAddress;

		public ServerInfo srv;

		internal bool _003CTrackServerFailure_003Eb__0(ServerInfo s)
		{
			return s.Ip == networkAddress;
		}

		internal bool _003CTrackServerFailure_003Eb__2(ServerInfo s)
		{
			return s.ConnectionFailures < srv.ConnectionFailures;
		}
	}

	public static readonly ServerInfo[] DefaultOnlineServers = new ServerInfo[2]
	{
		new ServerInfo("Master-2", "104.237.135.186", 22023),
		new ServerInfo("Master-1", "50.116.1.42", 22023)
	};

	public ServerInfo[] AvailableServers = DefaultOnlineServers;

	private string serverInfoFile;

	private UpdateState state;

	public ServerInfo CurrentServer { get; private set; }

	public string OnlineNetAddress
	{
		get
		{
			return CurrentServer.Ip;
		}
	}

	public override void Awake()
	{
		base.Awake();
		if (!(DestroyableSingleton<ServerManager>.Instance != this))
		{
			serverInfoFile = Path.Combine(Application.persistentDataPath, "serverInfo.dat");
			LoadServers();
			state = UpdateState.Success;
		}
	}

	[ContextMenu("Reselect Server")]
	internal void ReselectServer()
	{
		if (AvailableServers.Length == 0)
		{
			AvailableServers = DefaultOnlineServers;
		}
		if (AvailableServers.All(_003C_003Ec._003C_003E9__12_0 ?? (_003C_003Ec._003C_003E9__12_0 = _003C_003Ec._003C_003E9._003CReselectServer_003Eb__12_0)))
		{
			AvailableServers.Shuffle();
		}
		CurrentServer = AvailableServers.OrderBy(_003C_003Ec._003C_003E9__12_1 ?? (_003C_003Ec._003C_003E9__12_1 = _003C_003Ec._003C_003E9._003CReselectServer_003Eb__12_1)).ThenBy(_003C_003Ec._003C_003E9__12_2 ?? (_003C_003Ec._003C_003E9__12_2 = _003C_003Ec._003C_003E9._003CReselectServer_003Eb__12_2)).First();
		Debug.Log("Selected server: " + CurrentServer.Name);
	}

	public IEnumerator WaitForServers()
	{
		while (state == UpdateState.Connecting)
		{
			yield return null;
		}
	}

	internal void SetServers(ServerInfo[] servers)
	{
		if (servers.Length == 0)
		{
			return;
		}
		for (int i = 0; i < AvailableServers.Length; i++)
		{
			_003C_003Ec__DisplayClass14_0 _003C_003Ec__DisplayClass14_ = new _003C_003Ec__DisplayClass14_0();
			_003C_003Ec__DisplayClass14_.existingServer = AvailableServers[i];
			ServerInfo serverInfo = servers.FirstOrDefault(_003C_003Ec__DisplayClass14_._003CSetServers_003Eb__0);
			if (serverInfo != null)
			{
				_003C_003Ec__DisplayClass14_.existingServer.Players = serverInfo.Players;
			}
		}
		AvailableServers = AvailableServers.Intersect(servers).Union(servers).ToArray();
		ReselectServer();
		SaveServers(servers, AvailableServers.IndexOf(CurrentServer));
	}

	private void SaveServers(ServerInfo[] servers, int last)
	{
		try
		{
			using (FileStream output = new FileStream(serverInfoFile, FileMode.Create, FileAccess.Write))
			{
				using (BinaryWriter binaryWriter = new BinaryWriter(output))
				{
					binaryWriter.Write(last);
					binaryWriter.Write(servers.Length);
					for (int i = 0; i < servers.Length; i++)
					{
						servers[i].Serialize(binaryWriter);
					}
				}
			}
		}
		catch
		{
		}
	}

	private void LoadServers()
	{
		if (File.Exists(serverInfoFile))
		{
			try
			{
				using (BinaryReader binaryReader = new BinaryReader(File.OpenRead(serverInfoFile)))
				{
					int num = binaryReader.ReadInt32();
					ServerInfo[] array = new ServerInfo[binaryReader.ReadInt32()];
					for (int i = 0; i < array.Length; i++)
					{
						array[i] = ServerInfo.Deserialize(binaryReader);
					}
					AvailableServers = array;
					CurrentServer = array[num];
					Debug.Log("Loaded server: " + CurrentServer.Name);
					return;
				}
			}
			catch (Exception arg)
			{
				Debug.Log(string.Format("Couldn't load servers: {0}", arg));
				ReselectServer();
				return;
			}
		}
		ReselectServer();
	}

	internal bool TrackServerFailure(string networkAddress)
	{
		_003C_003Ec__DisplayClass17_0 _003C_003Ec__DisplayClass17_ = new _003C_003Ec__DisplayClass17_0();
		_003C_003Ec__DisplayClass17_.networkAddress = networkAddress;
		_003C_003Ec__DisplayClass17_.srv = AvailableServers.FirstOrDefault(_003C_003Ec__DisplayClass17_._003CTrackServerFailure_003Eb__0);
		if (_003C_003Ec__DisplayClass17_.srv != null)
		{
			_003C_003Ec__DisplayClass17_.srv.ConnectionFailures++;
			ServerInfo serverInfo = AvailableServers.OrderBy(_003C_003Ec._003C_003E9__17_1 ?? (_003C_003Ec._003C_003E9__17_1 = _003C_003Ec._003C_003E9._003CTrackServerFailure_003Eb__17_1)).FirstOrDefault(_003C_003Ec__DisplayClass17_._003CTrackServerFailure_003Eb__2);
			if (serverInfo != null)
			{
				CurrentServer = serverInfo;
				AmongUsClient.Instance.SetEndpoint(serverInfo.Ip, serverInfo.Port);
				return true;
			}
		}
		return false;
	}
}
