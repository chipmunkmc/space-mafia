using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Assets.CoreScripts;
using Hazel;
using InnerNet;
using PowerTools;
using UnityEngine;

public class ShipStatus : InnerNetObject
{
	public enum MapType
	{
		Ship = 0,
		Hq = 1,
		Pb = 2
	}

	public class SystemTypeComparer : IEqualityComparer<SystemTypes>
	{
		public static readonly SystemTypeComparer Instance = new SystemTypeComparer();

		public bool Equals(SystemTypes x, SystemTypes y)
		{
			return x == y;
		}

		public int GetHashCode(SystemTypes obj)
		{
			return (int)obj;
		}
	}

	private enum RpcCalls
	{
		CloseDoorsOfType = 0,
		RepairSystem = 1
	}

	[Serializable]
	[CompilerGenerated]
	private sealed class _003C_003Ec
	{
		public static readonly _003C_003Ec _003C_003E9 = new _003C_003Ec();

		public static Func<ISystemType, bool> _003C_003E9__51_0;

		public static Func<IStepWatcher, int> _003C_003E9__52_0;

		public static Func<PlainShipRoom, bool> _003C_003E9__52_1;

		public static Func<PlainShipRoom, SystemTypes> _003C_003E9__52_2;

		public static Func<GameData.PlayerInfo, bool> _003C_003E9__62_0;

		public static Func<GameData.PlayerInfo, bool> _003C_003E9__62_1;

		public static Func<PlayerTask, bool> _003C_003E9__70_0;

		public static Action<DeadBody> _003C_003E9__74_0;

		public static Func<PlayerTask, bool> _003C_003E9__75_0;

		internal bool _003COnEnable_003Eb__51_0(ISystemType i)
		{
			return i is IActivatable;
		}

		internal int _003CAwake_003Eb__52_0(IStepWatcher s)
		{
			return s.Priority;
		}

		internal bool _003CAwake_003Eb__52_1(PlainShipRoom p)
		{
			return p.RoomId != SystemTypes.Hallway;
		}

		internal SystemTypes _003CAwake_003Eb__52_2(PlainShipRoom d)
		{
			return d.RoomId;
		}

		internal bool _003CSelectInfected_003Eb__62_0(GameData.PlayerInfo pcd)
		{
			return !pcd.Disconnected;
		}

		internal bool _003CSelectInfected_003Eb__62_1(GameData.PlayerInfo pc)
		{
			return !pc.IsDead;
		}

		internal bool _003CCheckEndCriteria_003Eb__70_0(PlayerTask t)
		{
			return t.IsComplete;
		}

		internal void _003CReviveEveryone_003Eb__74_0(DeadBody b)
		{
			UnityEngine.Object.Destroy(b.gameObject);
		}

		internal bool _003CCheckTaskCompletion_003Eb__75_0(PlayerTask t)
		{
			return t.IsComplete;
		}
	}

	[CompilerGenerated]
	private sealed class _003C_003Ec__DisplayClass58_0
	{
		public byte idx;

		internal bool _003CGetTaskById_003Eb__0(NormalPlayerTask t)
		{
			return t.Index == idx;
		}

		internal bool _003CGetTaskById_003Eb__1(NormalPlayerTask t)
		{
			return t.Index == idx;
		}

		internal bool _003CGetTaskById_003Eb__2(NormalPlayerTask t)
		{
			return t.Index == idx;
		}
	}

	[CompilerGenerated]
	private sealed class _003C_003Ec__DisplayClass65_0
	{
		public HashSet<TaskTypes> usedTaskTypes;

		public Func<NormalPlayerTask, bool> _003C_003E9__0;

		internal bool _003CAddTasksFromList_003Eb__0(NormalPlayerTask t)
		{
			return usedTaskTypes.Contains(t.TaskType);
		}
	}

	public static ShipStatus Instance;

	public Color CameraColor = Color.black;

	public float MaxLightRadius = 5f;

	public float MinLightRadius = 1f;

	public float MapScale = 4.4f;

	public MapBehaviour MapPrefab;

	public ExileController ExileCutscenePrefab;

	public Vector2 InitialSpawnCenter;

	public Vector2 MeetingSpawnCenter;

	public float SpawnRadius = 1.55f;

	public AudioClip shipHum;

	public NormalPlayerTask[] CommonTasks;

	public NormalPlayerTask[] LongTasks;

	public NormalPlayerTask[] NormalTasks;

	public PlayerTask[] SpecialTasks;

	public Transform[] DummyLocations;

	public SurvCamera[] AllCameras;

	public PlainDoor[] AllDoors;

	public Console[] AllConsoles;

	public Dictionary<SystemTypes, ISystemType> Systems;

	public AnimationClip[] WeaponFires;

	public SpriteAnim WeaponsImage;

	public AnimationClip HatchActive;

	public SpriteAnim Hatch;

	public ParticleSystem HatchParticles;

	public AnimationClip ShieldsActive;

	public SpriteAnim[] ShieldsImages;

	public SpriteRenderer ShieldBorder;

	public Sprite ShieldBorderOn;

	public MedScannerBehaviour MedScanner;

	private int WeaponFireIdx;

	public float Timer;

	public float EmergencyCooldown;

	public MapType Type;

	public IStepWatcher[] AllStepWatchers { get; private set; }

	public PlainShipRoom[] AllRooms { get; private set; }

	public Dictionary<SystemTypes, PlainShipRoom> FastRooms { get; private set; }

	public Vent[] AllVents { get; private set; }

	private void OnEnable()
	{
		if (Systems == null)
		{
			Systems = new Dictionary<SystemTypes, ISystemType>(SystemTypeComparer.Instance)
			{
				{
					SystemTypes.Electrical,
					new SwitchSystem()
				},
				{
					SystemTypes.MedBay,
					new MedScanSystem()
				}
			};
			switch (Type)
			{
			case MapType.Pb:
				DestroyableSingleton<HudManager>.Instance.ShadowQuad.material.SetInt("_Mask", 7);
				Systems.Add(SystemTypes.Doors, new DoorsSystemType());
				Systems.Add(SystemTypes.Comms, new HudOverrideSystemType());
				Systems.Add(SystemTypes.Security, new SecurityCameraSystemType());
				Systems.Add(SystemTypes.Laboratory, new ReactorSystemType(30f, SystemTypes.Laboratory));
				break;
			case MapType.Hq:
				Systems.Add(SystemTypes.Comms, new HqHudSystemType());
				Systems.Add(SystemTypes.Reactor, new ReactorSystemType(45f, SystemTypes.Reactor));
				Systems.Add(SystemTypes.LifeSupp, new LifeSuppSystemType(45f));
				break;
			case MapType.Ship:
				Systems.Add(SystemTypes.Doors, new AutoDoorsSystemType());
				Systems.Add(SystemTypes.Comms, new HudOverrideSystemType());
				Systems.Add(SystemTypes.Security, new SecurityCameraSystemType());
				Systems.Add(SystemTypes.Reactor, new ReactorSystemType(30f, SystemTypes.Reactor));
				Systems.Add(SystemTypes.LifeSupp, new LifeSuppSystemType(30f));
				break;
			}
			Systems.Add(SystemTypes.Sabotage, new SabotageSystemType(Systems.Values.Where(_003C_003Ec._003C_003E9__51_0 ?? (_003C_003Ec._003C_003E9__51_0 = _003C_003Ec._003C_003E9._003COnEnable_003Eb__51_0)).Cast<IActivatable>().ToArray()));
		}
	}

	private void Awake()
	{
		AllStepWatchers = GetComponentsInChildren<IStepWatcher>().OrderByDescending(_003C_003Ec._003C_003E9__52_0 ?? (_003C_003Ec._003C_003E9__52_0 = _003C_003Ec._003C_003E9._003CAwake_003Eb__52_0)).ToArray();
		AllRooms = GetComponentsInChildren<PlainShipRoom>();
		FastRooms = AllRooms.Where(_003C_003Ec._003C_003E9__52_1 ?? (_003C_003Ec._003C_003E9__52_1 = _003C_003Ec._003C_003E9._003CAwake_003Eb__52_1)).ToDictionary(_003C_003Ec._003C_003E9__52_2 ?? (_003C_003Ec._003C_003E9__52_2 = _003C_003Ec._003C_003E9._003CAwake_003Eb__52_2));
		AllCameras = GetComponentsInChildren<SurvCamera>();
		AllConsoles = GetComponentsInChildren<Console>();
		AllVents = GetComponentsInChildren<Vent>();
		AssignTaskIndexes();
		Instance = this;
	}

	public void Start()
	{
		Camera.main.backgroundColor = CameraColor;
		if (DestroyableSingleton<HudManager>.InstanceExists)
		{
			DestroyableSingleton<HudManager>.Instance.Chat.ForceClosed();
			DestroyableSingleton<HudManager>.Instance.Chat.SetVisible(false);
			DestroyableSingleton<HudManager>.Instance.GameSettings.gameObject.SetActive(false);
		}
		DeconSystem[] componentsInChildren = GetComponentsInChildren<DeconSystem>();
		foreach (DeconSystem deconSystem in componentsInChildren)
		{
			Systems.Add(deconSystem.TargetSystem, deconSystem);
		}
		SoundManager.Instance.StopAllSound();
		if ((bool)shipHum)
		{
			AudioSource audioSource = SoundManager.Instance.PlaySound(shipHum, true);
			if ((bool)audioSource)
			{
				audioSource.pitch = 0.8f;
			}
		}
	}

	public override void OnDestroy()
	{
		if ((bool)SoundManager.Instance)
		{
			SoundManager.Instance.StopAllSound();
		}
		base.OnDestroy();
	}

	public Vector2 GetSpawnLocation(int playerId, int numPlayer, bool initialSpawn)
	{
		Vector2 up = Vector2.up;
		up = up.Rotate((float)(playerId - 1) * (360f / (float)numPlayer));
		up *= SpawnRadius;
		return (initialSpawn ? InitialSpawnCenter : MeetingSpawnCenter) + up + new Vector2(0f, 0.3636f);
	}

	public void StartShields()
	{
		for (int i = 0; i < ShieldsImages.Length; i++)
		{
			ShieldsImages[i].Play(ShieldsActive);
		}
		ShieldBorder.sprite = ShieldBorderOn;
	}

	public void FireWeapon()
	{
		if ((bool)WeaponsImage && !WeaponsImage.IsPlaying())
		{
			WeaponsImage.Play(WeaponFires[WeaponFireIdx]);
			WeaponFireIdx = (WeaponFireIdx + 1) % WeaponFires.Length;
		}
	}

	public NormalPlayerTask GetTaskById(byte idx)
	{
		_003C_003Ec__DisplayClass58_0 _003C_003Ec__DisplayClass58_ = new _003C_003Ec__DisplayClass58_0();
		_003C_003Ec__DisplayClass58_.idx = idx;
		return CommonTasks.FirstOrDefault(_003C_003Ec__DisplayClass58_._003CGetTaskById_003Eb__0) ?? LongTasks.FirstOrDefault(_003C_003Ec__DisplayClass58_._003CGetTaskById_003Eb__1) ?? NormalTasks.FirstOrDefault(_003C_003Ec__DisplayClass58_._003CGetTaskById_003Eb__2);
	}

	public void OpenHatch()
	{
		if ((bool)Hatch && !Hatch.IsPlaying())
		{
			Hatch.Play(HatchActive);
			HatchParticles.Play();
		}
	}

	public void CloseDoorsOfType(SystemTypes room)
	{
		((IDoorSystem)Systems[SystemTypes.Doors]).CloseDoorsOfType(room);
		SetDirtyBit(65536u);
	}

	public void RepairSystem(SystemTypes systemType, PlayerControl player, byte amount)
	{
		ISystemType value;
		if (Systems.TryGetValue(systemType, out value))
		{
			value.RepairDamage(player, amount);
			SetDirtyBit((uint)(1 << (int)systemType));
		}
	}

	internal void SelectInfected()
	{
		List<GameData.PlayerInfo> list = GameData.Instance.AllPlayers.Where(_003C_003Ec._003C_003E9__62_0 ?? (_003C_003Ec._003C_003E9__62_0 = _003C_003Ec._003C_003E9._003CSelectInfected_003Eb__62_0)).Where(_003C_003Ec._003C_003E9__62_1 ?? (_003C_003Ec._003C_003E9__62_1 = _003C_003Ec._003C_003E9._003CSelectInfected_003Eb__62_1)).ToList();
		int adjustedNumImpostors = PlayerControl.GameOptions.GetAdjustedNumImpostors(GameData.Instance.PlayerCount);
		list.RemoveDupes();
		GameData.PlayerInfo[] array = new GameData.PlayerInfo[Mathf.Min(list.Count, adjustedNumImpostors)];
		for (int i = 0; i < array.Length; i++)
		{
			int index = HashRandom.FastNext(list.Count);
			array[i] = list[index];
			list.RemoveAt(index);
		}
		foreach (GameData.PlayerInfo playerInfo in array)
		{
			DestroyableSingleton<Telemetry>.Instance.SelectInfected(playerInfo.ColorId, playerInfo.HatId);
		}
		PlayerControl.LocalPlayer.RpcSetInfected(array);
	}

	private void AssignTaskIndexes()
	{
		int num = 0;
		for (int i = 0; i < CommonTasks.Length; i++)
		{
			CommonTasks[i].Index = num++;
		}
		for (int j = 0; j < LongTasks.Length; j++)
		{
			LongTasks[j].Index = num++;
		}
		for (int k = 0; k < NormalTasks.Length; k++)
		{
			NormalTasks[k].Index = num++;
		}
	}

	public void Begin()
	{
		AssignTaskIndexes();
		GameOptionsData gameOptions = PlayerControl.GameOptions;
		List<GameData.PlayerInfo> allPlayers = GameData.Instance.AllPlayers;
		HashSet<TaskTypes> hashSet = new HashSet<TaskTypes>();
		List<byte> list = new List<byte>(10);
		List<NormalPlayerTask> list2 = CommonTasks.ToList();
		list2.Shuffle();
		int start = 0;
		AddTasksFromList(ref start, gameOptions.NumCommonTasks, list, hashSet, list2);
		for (int i = 0; i < gameOptions.NumCommonTasks; i++)
		{
			if (list2.Count == 0)
			{
				Debug.LogWarning("Not enough common tasks");
				break;
			}
			int index = list2.RandomIdx();
			list.Add((byte)list2[index].Index);
			list2.RemoveAt(index);
		}
		List<NormalPlayerTask> list3 = LongTasks.ToList();
		list3.Shuffle();
		List<NormalPlayerTask> list4 = NormalTasks.ToList();
		list4.Shuffle();
		int start2 = 0;
		int start3 = 0;
		int count = gameOptions.NumShortTasks;
		if (gameOptions.NumCommonTasks + gameOptions.NumLongTasks + gameOptions.NumShortTasks == 0)
		{
			count = 1;
		}
		for (byte b = 0; b < allPlayers.Count; b = (byte)(b + 1))
		{
			hashSet.Clear();
			list.RemoveRange(gameOptions.NumCommonTasks, list.Count - gameOptions.NumCommonTasks);
			AddTasksFromList(ref start2, gameOptions.NumLongTasks, list, hashSet, list3);
			AddTasksFromList(ref start3, count, list, hashSet, list4);
			GameData.PlayerInfo playerInfo = allPlayers[b];
			if ((bool)playerInfo.Object && !playerInfo.Object.GetComponent<DummyBehaviour>().enabled)
			{
				byte[] taskTypeIds = list.ToArray();
				GameData.Instance.RpcSetTasks(playerInfo.PlayerId, taskTypeIds);
			}
		}
		base.enabled = true;
	}

	private void AddTasksFromList(ref int start, int count, List<byte> tasks, HashSet<TaskTypes> usedTaskTypes, List<NormalPlayerTask> unusedTasks)
	{
		_003C_003Ec__DisplayClass65_0 _003C_003Ec__DisplayClass65_ = new _003C_003Ec__DisplayClass65_0();
		_003C_003Ec__DisplayClass65_.usedTaskTypes = usedTaskTypes;
		int num = 0;
		for (int i = 0; i < count; i++)
		{
			if (num++ == 1000)
			{
				break;
			}
			if (start >= unusedTasks.Count)
			{
				start = 0;
				unusedTasks.Shuffle();
				if (unusedTasks.All(_003C_003Ec__DisplayClass65_._003C_003E9__0 ?? (_003C_003Ec__DisplayClass65_._003C_003E9__0 = _003C_003Ec__DisplayClass65_._003CAddTasksFromList_003Eb__0)))
				{
					Debug.Log("Not enough task types");
					_003C_003Ec__DisplayClass65_.usedTaskTypes.Clear();
				}
			}
			NormalPlayerTask normalPlayerTask = unusedTasks[start++];
			if (!_003C_003Ec__DisplayClass65_.usedTaskTypes.Add(normalPlayerTask.TaskType))
			{
				i--;
			}
			else
			{
				tasks.Add((byte)normalPlayerTask.Index);
			}
		}
	}

	public void FixedUpdate()
	{
		if (!AmongUsClient.Instance)
		{
			return;
		}
		Timer += Time.fixedDeltaTime;
		EmergencyCooldown -= Time.fixedDeltaTime;
		if ((bool)GameData.Instance)
		{
			GameData.Instance.RecomputeTaskCounts();
		}
		if (AmongUsClient.Instance.AmHost)
		{
			CheckEndCriteria();
		}
		if (!AmongUsClient.Instance.AmClient)
		{
			return;
		}
		for (int i = 0; i < SystemTypeHelpers.AllTypes.Length; i++)
		{
			SystemTypes systemTypes = SystemTypeHelpers.AllTypes[i];
			ISystemType value;
			if (Systems.TryGetValue(systemTypes, out value) && value.Detoriorate(Time.fixedDeltaTime))
			{
				SetDirtyBit((uint)(1 << (int)systemTypes));
			}
		}
	}

	public float CalculateLightRadius(GameData.PlayerInfo player)
	{
		if (player == null || player.IsDead)
		{
			return MaxLightRadius;
		}
		SwitchSystem switchSystem = (SwitchSystem)Systems[SystemTypes.Electrical];
		if (player.IsImpostor)
		{
			return MaxLightRadius * PlayerControl.GameOptions.ImpostorLightMod;
		}
		float t = (float)(int)switchSystem.Value / 255f;
		return Mathf.Lerp(MinLightRadius, MaxLightRadius, t) * PlayerControl.GameOptions.CrewLightMod;
	}

	public override bool Serialize(MessageWriter writer, bool initialState)
	{
		if (initialState)
		{
			for (short num = 0; num < SystemTypeHelpers.AllTypes.Length; num = (short)(num + 1))
			{
				SystemTypes key = SystemTypeHelpers.AllTypes[num];
				ISystemType value;
				if (Systems.TryGetValue(key, out value))
				{
					value.Serialize(writer, true);
				}
			}
			return true;
		}
		if (DirtyBits != 0)
		{
			writer.WritePacked(DirtyBits);
			for (short num2 = 0; num2 < SystemTypeHelpers.AllTypes.Length; num2 = (short)(num2 + 1))
			{
				SystemTypes systemTypes = SystemTypeHelpers.AllTypes[num2];
				ISystemType value2;
				if ((DirtyBits & (1 << (int)systemTypes)) != 0L && Systems.TryGetValue(systemTypes, out value2))
				{
					value2.Serialize(writer, false);
				}
			}
			DirtyBits = 0u;
			return true;
		}
		return false;
	}

	public override void Deserialize(MessageReader reader, bool initialState)
	{
		if (initialState)
		{
			for (short num = 0; num < SystemTypeHelpers.AllTypes.Length; num = (short)(num + 1))
			{
				SystemTypes key = (SystemTypes)num;
				ISystemType value;
				if (Systems.TryGetValue(key, out value))
				{
					value.Deserialize(reader, true);
				}
			}
			return;
		}
		uint num2 = reader.ReadPackedUInt32();
		for (short num3 = 0; num3 < SystemTypeHelpers.AllTypes.Length; num3 = (short)(num3 + 1))
		{
			SystemTypes systemTypes = SystemTypeHelpers.AllTypes[num3];
			ISystemType value2;
			if ((num2 & (1 << (int)systemTypes)) != 0L && Systems.TryGetValue(systemTypes, out value2))
			{
				value2.Deserialize(reader, false);
			}
		}
	}

	private void CheckEndCriteria()
	{
		if (!GameData.Instance)
		{
			return;
		}
		ISystemType value;
		if (Systems.TryGetValue(SystemTypes.LifeSupp, out value))
		{
			LifeSuppSystemType lifeSuppSystemType = (LifeSuppSystemType)value;
			if (lifeSuppSystemType.Countdown < 0f)
			{
				EndGameForSabotage();
				lifeSuppSystemType.Countdown = 10000f;
			}
		}
		ISystemType value2;
		if (Systems.TryGetValue(SystemTypes.Reactor, out value2) || Systems.TryGetValue(SystemTypes.Laboratory, out value2))
		{
			ReactorSystemType reactorSystemType = (ReactorSystemType)value2;
			if (reactorSystemType.Countdown < 0f)
			{
				EndGameForSabotage();
				reactorSystemType.Countdown = 10000f;
			}
		}
		int num = 0;
		int num2 = 0;
		int num3 = 0;
		for (int i = 0; i < GameData.Instance.PlayerCount; i++)
		{
			GameData.PlayerInfo playerInfo = GameData.Instance.AllPlayers[i];
			if (playerInfo.Disconnected)
			{
				continue;
			}
			if (playerInfo.IsImpostor)
			{
				num3++;
			}
			if (!playerInfo.IsDead)
			{
				if (playerInfo.IsImpostor)
				{
					num2++;
				}
				else
				{
					num++;
				}
			}
		}
		if (num2 <= 0 && (!DestroyableSingleton<TutorialManager>.InstanceExists || num3 > 0))
		{
			if (!DestroyableSingleton<TutorialManager>.InstanceExists)
			{
				base.enabled = false;
				RpcEndGame((TempData.LastDeathReason == DeathReason.Disconnect) ? GameOverReason.ImpostorDisconnect : GameOverReason.HumansByVote, !SaveManager.BoughtNoAds);
			}
			else
			{
				DestroyableSingleton<HudManager>.Instance.ShowPopUp("Normally The Crew would have just won because The Impostor is dead. For free play, we revive everyone instead.");
				ReviveEveryone();
			}
		}
		else if (num <= num2)
		{
			if (!DestroyableSingleton<TutorialManager>.InstanceExists)
			{
				base.enabled = false;
				GameOverReason endReason;
				switch (TempData.LastDeathReason)
				{
				default:
					endReason = GameOverReason.HumansDisconnect;
					break;
				case DeathReason.Kill:
					endReason = GameOverReason.ImpostorByKill;
					break;
				case DeathReason.Exile:
					endReason = GameOverReason.ImpostorByVote;
					break;
				}
				RpcEndGame(endReason, !SaveManager.BoughtNoAds);
			}
			else
			{
				DestroyableSingleton<HudManager>.Instance.ShowPopUp("Normally The Impostor would have just won because The Crew can no longer win. For free play, we revive everyone instead.");
				ReviveEveryone();
			}
		}
		else if (!DestroyableSingleton<TutorialManager>.InstanceExists)
		{
			if (GameData.Instance.TotalTasks <= GameData.Instance.CompletedTasks)
			{
				base.enabled = false;
				RpcEndGame(GameOverReason.HumansByTask, !SaveManager.BoughtNoAds);
			}
		}
		else if (PlayerControl.LocalPlayer.myTasks.All(_003C_003Ec._003C_003E9__70_0 ?? (_003C_003Ec._003C_003E9__70_0 = _003C_003Ec._003C_003E9._003CCheckEndCriteria_003Eb__70_0)))
		{
			DestroyableSingleton<HudManager>.Instance.ShowPopUp("Normally The Crew would have just won because the task bar is full. For free play, we issue new tasks instead.");
			Begin();
		}
	}

	private void EndGameForSabotage()
	{
		if (!DestroyableSingleton<TutorialManager>.InstanceExists)
		{
			base.enabled = false;
			RpcEndGame(GameOverReason.ImpostorBySabotage, !SaveManager.BoughtNoAds);
		}
		else
		{
			DestroyableSingleton<HudManager>.Instance.ShowPopUp("Normally The Impostor would have just won because of the critical sabotage. Instead we just shut it off.");
		}
	}

	public bool IsGameOverDueToDeath()
	{
		int num = 0;
		int num2 = 0;
		int num3 = 0;
		for (int i = 0; i < GameData.Instance.PlayerCount; i++)
		{
			GameData.PlayerInfo playerInfo = GameData.Instance.AllPlayers[i];
			if (playerInfo.Disconnected)
			{
				continue;
			}
			if (playerInfo.IsImpostor)
			{
				num3++;
			}
			if (!playerInfo.IsDead)
			{
				if (playerInfo.IsImpostor)
				{
					num2++;
				}
				else
				{
					num++;
				}
			}
		}
		if (num2 <= 0 && (!DestroyableSingleton<TutorialManager>.InstanceExists || num3 > 0))
		{
			return true;
		}
		if (num <= num2)
		{
			return true;
		}
		return false;
	}

	private static void RpcEndGame(GameOverReason endReason, bool showAd)
	{
		MessageWriter messageWriter = AmongUsClient.Instance.StartEndGame();
		messageWriter.Write((byte)endReason);
		messageWriter.Write(showAd);
		AmongUsClient.Instance.FinishEndGame(messageWriter);
	}

	private static void ReviveEveryone()
	{
		for (int i = 0; i < GameData.Instance.PlayerCount; i++)
		{
			GameData.Instance.AllPlayers[i].Object.Revive();
		}
		UnityEngine.Object.FindObjectsOfType<DeadBody>().ForEach(_003C_003Ec._003C_003E9__74_0 ?? (_003C_003Ec._003C_003E9__74_0 = _003C_003Ec._003C_003E9._003CReviveEveryone_003Eb__74_0));
	}

	public bool CheckTaskCompletion()
	{
		if (DestroyableSingleton<TutorialManager>.InstanceExists)
		{
			if (PlayerControl.LocalPlayer.myTasks.All(_003C_003Ec._003C_003E9__75_0 ?? (_003C_003Ec._003C_003E9__75_0 = _003C_003Ec._003C_003E9._003CCheckTaskCompletion_003Eb__75_0)))
			{
				DestroyableSingleton<HudManager>.Instance.ShowPopUp("Normally The Crew would have just won because the task bar is full. For free play, we issue new tasks instead.");
				Begin();
			}
			return false;
		}
		GameData.Instance.RecomputeTaskCounts();
		if (GameData.Instance.TotalTasks <= GameData.Instance.CompletedTasks)
		{
			base.enabled = false;
			RpcEndGame(GameOverReason.HumansByTask, !SaveManager.BoughtNoAds);
			return true;
		}
		return false;
	}

	public void RpcCloseDoorsOfType(SystemTypes type)
	{
		if (AmongUsClient.Instance.AmHost)
		{
			CloseDoorsOfType(type);
			return;
		}
		MessageWriter messageWriter = AmongUsClient.Instance.StartRpcImmediately(NetId, 0, SendOption.Reliable, AmongUsClient.Instance.HostId);
		messageWriter.Write((byte)type);
		AmongUsClient.Instance.FinishRpcImmediately(messageWriter);
	}

	public void RpcRepairSystem(SystemTypes systemType, int amount)
	{
		if (AmongUsClient.Instance.AmHost)
		{
			RepairSystem(systemType, PlayerControl.LocalPlayer, (byte)amount);
			return;
		}
		MessageWriter messageWriter = AmongUsClient.Instance.StartRpcImmediately(NetId, 1, SendOption.Reliable, AmongUsClient.Instance.HostId);
		messageWriter.Write((byte)systemType);
		messageWriter.WriteNetObject(PlayerControl.LocalPlayer);
		messageWriter.Write((byte)amount);
		AmongUsClient.Instance.FinishRpcImmediately(messageWriter);
	}

	public override void HandleRpc(byte callId, MessageReader reader)
	{
		switch (callId)
		{
		case 0:
			CloseDoorsOfType((SystemTypes)reader.ReadByte());
			break;
		case 1:
			RepairSystem((SystemTypes)reader.ReadByte(), reader.ReadNetObject<PlayerControl>(), reader.ReadByte());
			break;
		}
	}
}
