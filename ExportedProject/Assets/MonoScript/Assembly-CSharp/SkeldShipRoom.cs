using System.Runtime.CompilerServices;
using UnityEngine;

public class SkeldShipRoom : PlainShipRoom, IStepWatcher
{
	public AudioClip AmbientSound;

	public float AmbientVolume = 0.7f;

	public float AmbientMaxDist = 8f;

	public Vector2 AmbientOffset;

	[CompilerGenerated]
	private readonly int _003CPriority_003Ek__BackingField;

	public SoundGroup FootStepSounds;

	private RaycastHit2D[] volumeBuffer = new RaycastHit2D[5];

	public int Priority
	{
		[CompilerGenerated]
		get
		{
			return _003CPriority_003Ek__BackingField;
		}
	}

	public void Start()
	{
		if (Constants.ShouldPlaySfx() && (bool)AmbientSound)
		{
			SoundManager.Instance.PlayDynamicSound("Amb " + RoomId, AmbientSound, true, _003CStart_003Eb__8_0);
		}
	}

	public SoundGroup MakeFootstep(PlayerControl player)
	{
		if (!DestroyableSingleton<HudManager>.InstanceExists)
		{
			return null;
		}
		RoomTracker roomTracker = DestroyableSingleton<HudManager>.Instance.roomTracker;
		if ((bool)roomTracker && roomTracker.LastRoom == this)
		{
			return FootStepSounds;
		}
		return null;
	}

	private void GetAmbientSoundVolume(AudioSource player, float dt)
	{
		if (!PlayerControl.LocalPlayer)
		{
			player.volume = 0f;
			return;
		}
		Vector2 vector = (Vector2)base.transform.position + AmbientOffset;
		Vector2 truePosition = PlayerControl.LocalPlayer.GetTruePosition();
		float num = Vector2.Distance(vector, truePosition);
		if (num > AmbientMaxDist)
		{
			player.volume = 0f;
			return;
		}
		Vector2 direction = truePosition - vector;
		int num2 = Physics2D.RaycastNonAlloc(vector, direction, volumeBuffer, num, Constants.ShipOnlyMask);
		float num3 = 1f - num / AmbientMaxDist - (float)num2 * 0.25f;
		player.volume = Mathf.Lerp(player.volume, num3 * AmbientVolume, dt);
	}

	[CompilerGenerated]
	private void _003CStart_003Eb__8_0(AudioSource player, float dt)
	{
		GetAmbientSoundVolume(player, dt);
	}
}
