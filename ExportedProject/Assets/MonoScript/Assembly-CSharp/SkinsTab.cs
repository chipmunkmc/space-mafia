using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class SkinsTab : MonoBehaviour
{
	[CompilerGenerated]
	private sealed class _003C_003Ec__DisplayClass11_0
	{
		public SkinData skin;

		public SkinsTab _003C_003E4__this;

		internal void _003COnEnable_003Eb__0()
		{
			_003C_003E4__this.SelectHat(skin);
		}
	}

	public ColorChip ColorTabPrefab;

	public SpriteRenderer DemoImage;

	public HatParent HatImage;

	public SpriteRenderer SkinImage;

	public SpriteRenderer PetImage;

	public FloatRange XRange = new FloatRange(1.5f, 3f);

	public float YStart = 0.8f;

	public float YOffset = 0.8f;

	public int NumPerRow = 4;

	public Scroller scroller;

	private List<ColorChip> ColorChips = new List<ColorChip>();

	public void OnEnable()
	{
		PlayerControl.SetPlayerMaterialColors(PlayerControl.LocalPlayer.Data.ColorId, DemoImage);
		HatImage.SetHat(SaveManager.LastHat);
		PlayerControl.SetSkinImage(SaveManager.LastSkin, SkinImage);
		PlayerControl.SetPetImage(SaveManager.LastPet, PlayerControl.LocalPlayer.Data.ColorId, PetImage);
		SkinData[] unlockedSkins = DestroyableSingleton<HatManager>.Instance.GetUnlockedSkins();
		for (int i = 0; i < unlockedSkins.Length; i++)
		{
			_003C_003Ec__DisplayClass11_0 _003C_003Ec__DisplayClass11_ = new _003C_003Ec__DisplayClass11_0();
			_003C_003Ec__DisplayClass11_._003C_003E4__this = this;
			_003C_003Ec__DisplayClass11_.skin = unlockedSkins[i];
			float x = XRange.Lerp((float)(i % NumPerRow) / ((float)NumPerRow - 1f));
			float y = YStart - (float)(i / NumPerRow) * YOffset;
			ColorChip colorChip = Object.Instantiate(ColorTabPrefab, scroller.Inner);
			colorChip.transform.localPosition = new Vector3(x, y, -1f);
			colorChip.Button.OnClick.AddListener(_003C_003Ec__DisplayClass11_._003COnEnable_003Eb__0);
			colorChip.Inner.FrontLayer.sprite = _003C_003Ec__DisplayClass11_.skin.IdleFrame;
			ColorChips.Add(colorChip);
		}
		scroller.YBounds.max = 0f - (YStart - (float)(unlockedSkins.Length / NumPerRow) * YOffset) - 3f;
	}

	public void OnDisable()
	{
		for (int i = 0; i < ColorChips.Count; i++)
		{
			Object.Destroy(ColorChips[i].gameObject);
		}
		ColorChips.Clear();
	}

	public void Update()
	{
		PlayerControl.SetPlayerMaterialColors(PlayerControl.LocalPlayer.Data.ColorId, DemoImage);
		SkinData skinById = DestroyableSingleton<HatManager>.Instance.GetSkinById(SaveManager.LastSkin);
		for (int i = 0; i < ColorChips.Count; i++)
		{
			ColorChip colorChip = ColorChips[i];
			colorChip.InUseForeground.SetActive(skinById.IdleFrame == colorChip.Inner.FrontLayer.sprite);
		}
	}

	private void SelectHat(SkinData skin)
	{
		uint skinId = (SaveManager.LastSkin = DestroyableSingleton<HatManager>.Instance.GetIdFromSkin(skin));
		PlayerControl.SetSkinImage(SaveManager.LastSkin, SkinImage);
		if ((bool)PlayerControl.LocalPlayer)
		{
			PlayerControl.LocalPlayer.RpcSetSkin(skinId);
		}
	}
}
