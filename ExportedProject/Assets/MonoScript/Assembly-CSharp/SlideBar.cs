using UnityEngine;
using UnityEngine.Events;

public class SlideBar : PassiveUiElement
{
	public TextRenderer Title;

	public SpriteRenderer Bar;

	public SpriteRenderer Dot;

	public FloatRange Range;

	public bool Vertical;

	public float Value;

	public UnityEvent OnValueChange;

	public override bool HandleDrag
	{
		get
		{
			return true;
		}
	}

	public void OnEnable()
	{
		if ((bool)Title)
		{
			Title.Color = Color.white;
		}
		if ((bool)Bar)
		{
			Bar.color = Color.white;
		}
		Dot.color = Color.white;
		UpdateValue();
	}

	public void OnDisable()
	{
		if ((bool)Title)
		{
			Title.Color = Color.gray;
		}
		if ((bool)Bar)
		{
			Bar.color = Color.gray;
		}
		Dot.color = Color.gray;
	}

	public override void ReceiveClickDrag(Vector2 dragDelta)
	{
		Vector3 localPosition = Dot.transform.localPosition;
		Vector2 vector = DestroyableSingleton<PassiveButtonManager>.Instance.controller.DragPosition - (Vector2)Bar.transform.position;
		if (Vertical)
		{
			localPosition.y = Range.Clamp(vector.y);
			Value = Range.ReverseLerp(localPosition.y);
		}
		else
		{
			localPosition.x = Range.Clamp(vector.x);
			Value = Range.ReverseLerp(localPosition.x);
		}
		UpdateValue();
		OnValueChange.Invoke();
	}

	public void UpdateValue()
	{
		Vector3 localPosition = Dot.transform.localPosition;
		if (Vertical)
		{
			localPosition.y = Range.Lerp(Value);
		}
		else
		{
			localPosition.x = Range.Lerp(Value);
		}
		Dot.transform.localPosition = localPosition;
	}
}
