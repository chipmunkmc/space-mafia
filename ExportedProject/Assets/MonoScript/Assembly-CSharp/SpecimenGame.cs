using UnityEngine;

public class SpecimenGame : Minigame
{
	public Collider2D[] Specimens;

	public Transform[] Slots;

	private Controller cont = new Controller();

	public void Update()
	{
		cont.Update();
		for (int i = 0; i < Specimens.Length; i++)
		{
			Collider2D collider2D = Specimens[i];
			switch (cont.CheckDrag(collider2D))
			{
			case DragState.Dragging:
			{
				Vector3 localPosition = (Vector3)cont.DragPosition - base.transform.position;
				localPosition.z = 0f;
				collider2D.transform.localPosition = localPosition;
				if (Vector2.Distance(collider2D.transform.position, Slots[i].position) < 0.25f)
				{
					collider2D.transform.position = Slots[i].position;
				}
				break;
			}
			case DragState.Released:
				if (Vector2.Distance(collider2D.transform.position, Slots[i].position) < 0.25f)
				{
					collider2D.transform.position = Slots[i].position;
				}
				CheckForWin();
				break;
			}
		}
	}

	private void CheckForWin()
	{
		bool flag = true;
		for (int i = 0; i < Specimens.Length; i++)
		{
			if (Vector2.Distance(Specimens[i].transform.position, Slots[i].position) > 0.25f)
			{
				flag = false;
				break;
			}
		}
		if (flag)
		{
			MyNormTask.NextStep();
			StartCoroutine(CoStartClose());
		}
	}
}
