using System;
using System.IO;
using UnityEngine;

public class StatsManager
{
	public static StatsManager Instance = new StatsManager();

	private const byte StatsVersion = 3;

	private bool loadedStats;

	private uint bodiesReported;

	private uint emergenciesCalls;

	private uint tasksCompleted;

	private uint completedAllTasks;

	private uint sabsFixed;

	private uint impostorKills;

	private uint timesMurdered;

	private uint timesEjected;

	private uint crewmateStreak;

	private uint timesImpostor;

	private uint timesCrewmate;

	private uint gamesStarted;

	private uint gamesFinished;

	private float banPoints;

	private long lastGameStarted;

	private const int PointsUntilBanStarts = 2;

	private const int MinutesPerBanPoint = 5;

	private uint[] WinReasons = new uint[7];

	private uint[] DrawReasons = new uint[7];

	private uint[] LoseReasons = new uint[7];

	public uint BodiesReported
	{
		get
		{
			LoadStats();
			return bodiesReported;
		}
		set
		{
			LoadStats();
			bodiesReported = value;
			SaveStats();
		}
	}

	public uint EmergenciesCalled
	{
		get
		{
			LoadStats();
			return emergenciesCalls;
		}
		set
		{
			LoadStats();
			emergenciesCalls = value;
			SaveStats();
		}
	}

	public uint TasksCompleted
	{
		get
		{
			LoadStats();
			return tasksCompleted;
		}
		set
		{
			LoadStats();
			tasksCompleted = value;
			SaveStats();
		}
	}

	public uint CompletedAllTasks
	{
		get
		{
			LoadStats();
			return completedAllTasks;
		}
		set
		{
			LoadStats();
			completedAllTasks = value;
			SaveStats();
		}
	}

	public uint SabsFixed
	{
		get
		{
			LoadStats();
			return sabsFixed;
		}
		set
		{
			LoadStats();
			sabsFixed = value;
			SaveStats();
		}
	}

	public uint ImpostorKills
	{
		get
		{
			LoadStats();
			return impostorKills;
		}
		set
		{
			LoadStats();
			impostorKills = value;
			SaveStats();
		}
	}

	public uint TimesMurdered
	{
		get
		{
			LoadStats();
			return timesMurdered;
		}
		set
		{
			LoadStats();
			timesMurdered = value;
			SaveStats();
		}
	}

	public uint TimesEjected
	{
		get
		{
			LoadStats();
			return timesEjected;
		}
		set
		{
			LoadStats();
			timesEjected = value;
			SaveStats();
		}
	}

	public uint CrewmateStreak
	{
		get
		{
			LoadStats();
			return crewmateStreak;
		}
		set
		{
			LoadStats();
			crewmateStreak = value;
			SaveStats();
		}
	}

	public uint TimesImpostor
	{
		get
		{
			LoadStats();
			return timesImpostor;
		}
		set
		{
			LoadStats();
			timesImpostor = value;
			SaveStats();
		}
	}

	public uint TimesCrewmate
	{
		get
		{
			LoadStats();
			return timesCrewmate;
		}
		set
		{
			LoadStats();
			timesCrewmate = value;
			SaveStats();
		}
	}

	public uint GamesStarted
	{
		get
		{
			LoadStats();
			return gamesStarted;
		}
		set
		{
			LoadStats();
			gamesStarted = value;
			SaveStats();
		}
	}

	public uint GamesFinished
	{
		get
		{
			LoadStats();
			return gamesFinished;
		}
		set
		{
			LoadStats();
			gamesFinished = value;
			SaveStats();
		}
	}

	public float BanPoints
	{
		get
		{
			LoadStats();
			return banPoints;
		}
		set
		{
			LoadStats();
			banPoints = Mathf.Max(0f, value);
			SaveStats();
		}
	}

	public DateTime LastGameStarted
	{
		get
		{
			LoadStats();
			return new DateTime(lastGameStarted);
		}
		set
		{
			LoadStats();
			lastGameStarted = value.Ticks;
			SaveStats();
		}
	}

	public float BanMinutes
	{
		get
		{
			return Mathf.Max(BanPoints - 2f, 0f) * 5f;
		}
	}

	public bool AmBanned
	{
		get
		{
			return BanMinutesLeft > 0;
		}
	}

	public int BanMinutesLeft
	{
		get
		{
			TimeSpan timeSpan = DateTime.UtcNow - LastGameStarted;
			if (timeSpan.TotalDays > 1.0)
			{
				return 0;
			}
			return Mathf.CeilToInt(BanMinutes - (float)timeSpan.TotalMinutes);
		}
	}

	public void AddDrawReason(GameOverReason reason)
	{
		LoadStats();
		DrawReasons[(int)reason]++;
		SaveStats();
	}

	public void AddWinReason(GameOverReason reason)
	{
		LoadStats();
		WinReasons[(int)reason]++;
		SaveStats();
	}

	public uint GetWinReason(GameOverReason reason)
	{
		LoadStats();
		return WinReasons[(int)reason];
	}

	public void AddLoseReason(GameOverReason reason)
	{
		LoadStats();
		LoseReasons[(int)reason]++;
		SaveStats();
	}

	public uint GetLoseReason(GameOverReason reason)
	{
		LoadStats();
		return LoseReasons[(int)reason];
	}

	protected virtual void LoadStats()
	{
		if (loadedStats)
		{
			return;
		}
		loadedStats = true;
		string path = Path.Combine(Application.persistentDataPath, "playerStats2");
		if (!File.Exists(path))
		{
			return;
		}
		try
		{
			using (BinaryReader binaryReader = new BinaryReader(File.OpenRead(path)))
			{
				byte b = binaryReader.ReadByte();
				bodiesReported = binaryReader.ReadUInt32();
				emergenciesCalls = binaryReader.ReadUInt32();
				tasksCompleted = binaryReader.ReadUInt32();
				completedAllTasks = binaryReader.ReadUInt32();
				sabsFixed = binaryReader.ReadUInt32();
				impostorKills = binaryReader.ReadUInt32();
				timesMurdered = binaryReader.ReadUInt32();
				timesEjected = binaryReader.ReadUInt32();
				crewmateStreak = binaryReader.ReadUInt32();
				timesImpostor = binaryReader.ReadUInt32();
				timesCrewmate = binaryReader.ReadUInt32();
				gamesStarted = binaryReader.ReadUInt32();
				gamesFinished = binaryReader.ReadUInt32();
				for (int i = 0; i < WinReasons.Length; i++)
				{
					WinReasons[i] = binaryReader.ReadUInt32();
				}
				for (int j = 0; j < LoseReasons.Length; j++)
				{
					LoseReasons[j] = binaryReader.ReadUInt32();
				}
				if (b > 1)
				{
					for (int k = 0; k < DrawReasons.Length; k++)
					{
						DrawReasons[k] = binaryReader.ReadUInt32();
					}
				}
				if (b > 2)
				{
					banPoints = binaryReader.ReadSingle();
					lastGameStarted = binaryReader.ReadInt64();
				}
			}
		}
		catch
		{
			Debug.LogError("Deleting corrupted stats file");
			File.Delete(path);
		}
	}

	protected virtual void SaveStats()
	{
		try
		{
			using (BinaryWriter binaryWriter = new BinaryWriter(File.OpenWrite(Path.Combine(Application.persistentDataPath, "playerStats2"))))
			{
				binaryWriter.Write((byte)3);
				binaryWriter.Write(bodiesReported);
				binaryWriter.Write(emergenciesCalls);
				binaryWriter.Write(tasksCompleted);
				binaryWriter.Write(completedAllTasks);
				binaryWriter.Write(sabsFixed);
				binaryWriter.Write(impostorKills);
				binaryWriter.Write(timesMurdered);
				binaryWriter.Write(timesEjected);
				binaryWriter.Write(crewmateStreak);
				binaryWriter.Write(timesImpostor);
				binaryWriter.Write(timesCrewmate);
				binaryWriter.Write(gamesStarted);
				binaryWriter.Write(gamesFinished);
				for (int i = 0; i < WinReasons.Length; i++)
				{
					binaryWriter.Write(WinReasons[i]);
				}
				for (int j = 0; j < LoseReasons.Length; j++)
				{
					binaryWriter.Write(LoseReasons[j]);
				}
				for (int k = 0; k < DrawReasons.Length; k++)
				{
					binaryWriter.Write(DrawReasons[k]);
				}
				binaryWriter.Write(banPoints);
				binaryWriter.Write(lastGameStarted);
			}
		}
		catch (Exception ex)
		{
			Debug.Log("Failed to write out stats: " + ex.Message);
		}
	}
}
