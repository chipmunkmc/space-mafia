using System.Text;
using UnityEngine;

public class StatsPopup : MonoBehaviour
{
	public TextRenderer StatsText;

	private void OnEnable()
	{
		StringBuilder stringBuilder = new StringBuilder(1024);
		AppendStat(stringBuilder, StringNames.StatsBodiesReported, StatsManager.Instance.BodiesReported);
		AppendStat(stringBuilder, StringNames.StatsEmergenciesCalled, StatsManager.Instance.EmergenciesCalled);
		AppendStat(stringBuilder, StringNames.StatsTasksCompleted, StatsManager.Instance.TasksCompleted);
		AppendStat(stringBuilder, StringNames.StatsAllTasksCompleted, StatsManager.Instance.CompletedAllTasks);
		AppendStat(stringBuilder, StringNames.StatsSabotagesFixed, StatsManager.Instance.SabsFixed);
		AppendStat(stringBuilder, StringNames.StatsImpostorKills, StatsManager.Instance.ImpostorKills);
		AppendStat(stringBuilder, StringNames.StatsTimesMurdered, StatsManager.Instance.TimesMurdered);
		AppendStat(stringBuilder, StringNames.StatsTimesEjected, StatsManager.Instance.TimesEjected);
		AppendStat(stringBuilder, StringNames.StatsCrewmateStreak, StatsManager.Instance.CrewmateStreak);
		AppendStat(stringBuilder, StringNames.StatsGamesImpostor, StatsManager.Instance.TimesImpostor);
		AppendStat(stringBuilder, StringNames.StatsGamesCrewmate, StatsManager.Instance.TimesCrewmate);
		AppendStat(stringBuilder, StringNames.StatsGamesStarted, StatsManager.Instance.GamesStarted);
		AppendStat(stringBuilder, StringNames.StatsGamesFinished, StatsManager.Instance.GamesFinished);
		AppendStat(stringBuilder, StringNames.StatsImpostorVoteWins, StatsManager.Instance.GetWinReason(GameOverReason.ImpostorByVote));
		AppendStat(stringBuilder, StringNames.StatsImpostorKillsWins, StatsManager.Instance.GetWinReason(GameOverReason.ImpostorByKill));
		AppendStat(stringBuilder, StringNames.StatsImpostorSabotageWins, StatsManager.Instance.GetWinReason(GameOverReason.ImpostorBySabotage));
		AppendStat(stringBuilder, StringNames.StatsCrewmateVoteWins, StatsManager.Instance.GetWinReason(GameOverReason.HumansByVote));
		AppendStat(stringBuilder, StringNames.StatsCrewmateTaskWins, StatsManager.Instance.GetWinReason(GameOverReason.HumansByTask));
		StatsText.Text = stringBuilder.ToString();
	}

	private static void AppendStat(StringBuilder str, StringNames statName, object stat)
	{
		str.Append(DestroyableSingleton<TranslationController>.Instance.GetString(statName));
		str.Append("\t");
		str.Append(stat);
		str.AppendLine();
	}
}
