using System;
using System.Collections;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;

public class SurveillanceMinigame : Minigame
{
	[Serializable]
	[CompilerGenerated]
	private sealed class _003C_003Ec
	{
		public static readonly _003C_003Ec _003C_003E9 = new _003C_003Ec();

		public static Func<PlainShipRoom, bool> _003C_003E9__10_0;

		internal bool _003CBegin_003Eb__10_0(PlainShipRoom i)
		{
			return i.survCamera;
		}
	}

	public Camera CameraPrefab;

	public GameObject Viewables;

	public MeshRenderer[] ViewPorts;

	public TextRenderer[] SabText;

	private PlainShipRoom[] FilteredRooms;

	private RenderTexture[] textures;

	public MeshRenderer FillQuad;

	public Material DefaultMaterial;

	public Material StaticMaterial;

	private bool isStatic;

	public override void Begin(PlayerTask task)
	{
		base.Begin(task);
		FilteredRooms = ShipStatus.Instance.AllRooms.Where(_003C_003Ec._003C_003E9__10_0 ?? (_003C_003Ec._003C_003E9__10_0 = _003C_003Ec._003C_003E9._003CBegin_003Eb__10_0)).ToArray();
		textures = new RenderTexture[FilteredRooms.Length];
		for (int i = 0; i < FilteredRooms.Length; i++)
		{
			PlainShipRoom plainShipRoom = FilteredRooms[i];
			Camera camera = UnityEngine.Object.Instantiate(CameraPrefab);
			camera.transform.SetParent(base.transform);
			camera.transform.position = plainShipRoom.transform.position + plainShipRoom.survCamera.Offset;
			camera.orthographicSize = plainShipRoom.survCamera.CamSize;
			RenderTexture temporary = RenderTexture.GetTemporary((int)(256f * plainShipRoom.survCamera.CamAspect), 256, 16, RenderTextureFormat.ARGB32);
			textures[i] = temporary;
			camera.targetTexture = temporary;
			ViewPorts[i].material.SetTexture("_MainTex", temporary);
		}
		if (!PlayerControl.LocalPlayer.Data.IsDead)
		{
			ShipStatus.Instance.RpcRepairSystem(SystemTypes.Security, 1);
		}
	}

	public void Update()
	{
		if (isStatic && !PlayerTask.PlayerHasTaskOfType<IHudOverrideTask>(PlayerControl.LocalPlayer))
		{
			isStatic = false;
			for (int i = 0; i < ViewPorts.Length; i++)
			{
				ViewPorts[i].sharedMaterial = DefaultMaterial;
				ViewPorts[i].material.SetTexture("_MainTex", textures[i]);
				SabText[i].gameObject.SetActive(false);
			}
		}
		else if (!isStatic && PlayerTask.PlayerHasTaskOfType<HudOverrideTask>(PlayerControl.LocalPlayer))
		{
			isStatic = true;
			for (int j = 0; j < ViewPorts.Length; j++)
			{
				ViewPorts[j].sharedMaterial = StaticMaterial;
				SabText[j].gameObject.SetActive(true);
			}
		}
	}

	protected override IEnumerator CoAnimateOpen()
	{
		Viewables.SetActive(false);
		FillQuad.material.SetFloat("_Center", -5f);
		FillQuad.material.SetColor("_Color2", Color.clear);
		for (float timer3 = 0f; timer3 < 0.25f; timer3 += Time.deltaTime)
		{
			FillQuad.material.SetColor("_Color2", Color.Lerp(Color.clear, Color.black, timer3 / 0.25f));
			yield return null;
		}
		FillQuad.material.SetColor("_Color2", Color.black);
		Viewables.SetActive(true);
		for (float timer3 = 0f; timer3 < 0.1f; timer3 += Time.deltaTime)
		{
			FillQuad.material.SetFloat("_Center", Mathf.Lerp(-5f, 0f, timer3 / 0.1f));
			yield return null;
		}
		for (float timer3 = 0f; timer3 < 0.15f; timer3 += Time.deltaTime)
		{
			FillQuad.material.SetFloat("_Center", Mathf.Lerp(-3f, 0.4f, timer3 / 0.15f));
			yield return null;
		}
		FillQuad.material.SetFloat("_Center", 0.4f);
	}

	private IEnumerator CoAnimateClose()
	{
		for (float timer2 = 0f; timer2 < 0.1f; timer2 += Time.deltaTime)
		{
			FillQuad.material.SetFloat("_Center", Mathf.Lerp(0.4f, -5f, timer2 / 0.1f));
			yield return null;
		}
		Viewables.SetActive(false);
		for (float timer2 = 0f; timer2 < 0.3f; timer2 += Time.deltaTime)
		{
			FillQuad.material.SetColor("_Color2", Color.Lerp(Color.black, Color.clear, timer2 / 0.3f));
			yield return null;
		}
		FillQuad.material.SetColor("_Color2", Color.clear);
	}

	protected override IEnumerator CoDestroySelf()
	{
		yield return CoAnimateClose();
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public override void Close()
	{
		ShipStatus.Instance.RpcRepairSystem(SystemTypes.Security, 2);
		base.Close();
	}

	public void OnDestroy()
	{
		for (int i = 0; i < textures.Length; i++)
		{
			textures[i].Release();
		}
	}
}
