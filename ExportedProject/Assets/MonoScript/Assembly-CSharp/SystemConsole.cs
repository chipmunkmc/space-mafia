using UnityEngine;

public class SystemConsole : MonoBehaviour, IUsable
{
	public float usableDistance = 1f;

	public bool FreeplayOnly;

	public bool onlyFromBelow;

	public SpriteRenderer Image;

	public Minigame MinigamePrefab;

	public float UsableDistance
	{
		get
		{
			return usableDistance;
		}
	}

	public float PercentCool
	{
		get
		{
			return 0f;
		}
	}

	public void Start()
	{
		if (FreeplayOnly && !DestroyableSingleton<TutorialManager>.InstanceExists)
		{
			Object.Destroy(base.gameObject);
		}
	}

	public void SetOutline(bool on, bool mainTarget)
	{
		if ((bool)Image)
		{
			Image.material.SetFloat("_Outline", on ? 1 : 0);
			Image.material.SetColor("_OutlineColor", Color.white);
			Image.material.SetColor("_AddColor", mainTarget ? Color.white : Color.clear);
		}
	}

	public float CanUse(GameData.PlayerInfo pc, out bool canUse, out bool couldUse)
	{
		float num = float.MaxValue;
		PlayerControl @object = pc.Object;
		Vector2 truePosition = @object.GetTruePosition();
		couldUse = @object.CanMove && (!pc.IsDead || !(MinigamePrefab is EmergencyMinigame));
		canUse = couldUse && (!onlyFromBelow || truePosition.y < base.transform.position.y);
		if (canUse)
		{
			num = Vector2.Distance(truePosition, base.transform.position);
			canUse &= num <= UsableDistance;
		}
		return num;
	}

	public void Use()
	{
		bool canUse;
		bool couldUse;
		CanUse(PlayerControl.LocalPlayer.Data, out canUse, out couldUse);
		if (canUse)
		{
			PlayerControl.LocalPlayer.NetTransform.Halt();
			Minigame minigame = Object.Instantiate(MinigamePrefab);
			minigame.transform.SetParent(Camera.main.transform, false);
			minigame.transform.localPosition = new Vector3(0f, 0f, -50f);
			minigame.Begin(null);
		}
	}
}
