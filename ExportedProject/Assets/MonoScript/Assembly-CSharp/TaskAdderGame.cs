using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using UnityEngine;

public class TaskAdderGame : Minigame
{
	[Serializable]
	[CompilerGenerated]
	private sealed class _003C_003Ec
	{
		public static readonly _003C_003Ec _003C_003E9 = new _003C_003Ec();

		public static Func<TaskFolder, string> _003C_003E9__11_0;

		public static Func<PlayerTask, TaskTypes> _003C_003E9__15_0;

		internal string _003CBegin_003Eb__11_0(TaskFolder f)
		{
			return f.FolderName;
		}

		internal TaskTypes _003CShowFolder_003Eb__15_0(PlayerTask t)
		{
			return t.TaskType;
		}
	}

	public TextRenderer PathText;

	public TaskFolder RootFolderPrefab;

	public TaskAddButton TaskPrefab;

	public Transform TaskParent;

	public List<TaskFolder> Heirarchy = new List<TaskFolder>();

	public List<Transform> ActiveItems = new List<Transform>();

	public TaskAddButton InfectedButton;

	public float folderWidth;

	public float fileWidth;

	public float lineWidth;

	private TaskFolder Root;

	public override void Begin(PlayerTask t)
	{
		base.Begin(t);
		Root = UnityEngine.Object.Instantiate(RootFolderPrefab, base.transform);
		Root.gameObject.SetActive(false);
		Dictionary<SystemTypes, TaskFolder> folders = new Dictionary<SystemTypes, TaskFolder>();
		PopulateRoot(Root, folders, ShipStatus.Instance.CommonTasks);
		PopulateRoot(Root, folders, ShipStatus.Instance.LongTasks);
		PopulateRoot(Root, folders, ShipStatus.Instance.NormalTasks);
		Root.SubFolders = Root.SubFolders.OrderBy(_003C_003Ec._003C_003E9__11_0 ?? (_003C_003Ec._003C_003E9__11_0 = _003C_003Ec._003C_003E9._003CBegin_003Eb__11_0)).ToList();
		ShowFolder(Root);
	}

	private void PopulateRoot(TaskFolder rootFolder, Dictionary<SystemTypes, TaskFolder> folders, NormalPlayerTask[] taskList)
	{
		foreach (NormalPlayerTask normalPlayerTask in taskList)
		{
			SystemTypes systemTypes = normalPlayerTask.StartAt;
			if (normalPlayerTask is DivertPowerTask)
			{
				systemTypes = ((DivertPowerTask)normalPlayerTask).TargetSystem;
			}
			if (systemTypes == SystemTypes.LowerEngine)
			{
				systemTypes = SystemTypes.UpperEngine;
			}
			TaskFolder value;
			if (!folders.TryGetValue(systemTypes, out value))
			{
				TaskFolder taskFolder2 = (folders[systemTypes] = UnityEngine.Object.Instantiate(RootFolderPrefab, base.transform));
				value = taskFolder2;
				value.gameObject.SetActive(false);
				if (systemTypes == SystemTypes.UpperEngine)
				{
					value.FolderName = DestroyableSingleton<TranslationController>.Instance.GetString(StringNames.Engines);
				}
				else
				{
					value.FolderName = DestroyableSingleton<TranslationController>.Instance.GetString(systemTypes);
				}
				rootFolder.SubFolders.Add(value);
			}
			value.Children.Add(normalPlayerTask);
		}
	}

	public void GoToRoot()
	{
		Heirarchy.Clear();
		ShowFolder(Root);
	}

	public void GoUpOne()
	{
		if (Heirarchy.Count > 1)
		{
			TaskFolder taskFolder = Heirarchy[Heirarchy.Count - 2];
			Heirarchy.RemoveAt(Heirarchy.Count - 1);
			Heirarchy.RemoveAt(Heirarchy.Count - 1);
			ShowFolder(taskFolder);
		}
	}

	public void ShowFolder(TaskFolder taskFolder)
	{
		StringBuilder stringBuilder = new StringBuilder(64);
		Heirarchy.Add(taskFolder);
		for (int i = 0; i < Heirarchy.Count; i++)
		{
			stringBuilder.Append(Heirarchy[i].FolderName);
			stringBuilder.Append("\\");
		}
		PathText.Text = stringBuilder.ToString();
		for (int j = 0; j < ActiveItems.Count; j++)
		{
			UnityEngine.Object.Destroy(ActiveItems[j].gameObject);
		}
		ActiveItems.Clear();
		float xCursor = 0f;
		float yCursor = 0f;
		float maxHeight = 0f;
		for (int k = 0; k < taskFolder.SubFolders.Count; k++)
		{
			TaskFolder taskFolder2 = UnityEngine.Object.Instantiate(taskFolder.SubFolders[k], TaskParent);
			taskFolder2.gameObject.SetActive(true);
			taskFolder2.Parent = this;
			taskFolder2.transform.localPosition = new Vector3(xCursor, yCursor, 0f);
			taskFolder2.transform.localScale = Vector3.one;
			taskFolder2.Text.RefreshMesh();
			maxHeight = Mathf.Max(maxHeight, taskFolder2.Text.Height + 1.1f);
			xCursor += folderWidth;
			if (xCursor > lineWidth)
			{
				xCursor = 0f;
				yCursor -= maxHeight;
				maxHeight = 0f;
			}
			ActiveItems.Add(taskFolder2.transform);
		}
		List<PlayerTask> list = taskFolder.Children.OrderBy(_003C_003Ec._003C_003E9__15_0 ?? (_003C_003Ec._003C_003E9__15_0 = _003C_003Ec._003C_003E9._003CShowFolder_003Eb__15_0)).ToList();
		for (int l = 0; l < list.Count; l++)
		{
			TaskAddButton taskAddButton = UnityEngine.Object.Instantiate(TaskPrefab);
			taskAddButton.MyTask = list[l];
			if (taskAddButton.MyTask.TaskType == TaskTypes.DivertPower)
			{
				SystemTypes targetSystem = ((DivertPowerTask)taskAddButton.MyTask).TargetSystem;
				taskAddButton.Text.Text = DestroyableSingleton<TranslationController>.Instance.GetString(StringNames.DivertPowerTo, DestroyableSingleton<TranslationController>.Instance.GetString(targetSystem));
			}
			else if (taskAddButton.MyTask.TaskType == TaskTypes.ActivateWeatherNodes)
			{
				int nodeId = ((WeatherNodeTask)taskAddButton.MyTask).NodeId;
				taskAddButton.Text.Text = DestroyableSingleton<TranslationController>.Instance.GetString(StringNames.FixWeatherNode) + " " + WeatherSwitchGame.ControlNames[nodeId];
			}
			else
			{
				taskAddButton.Text.Text = DestroyableSingleton<TranslationController>.Instance.GetString(taskAddButton.MyTask.TaskType);
			}
			AddFileAsChild(taskFolder, taskAddButton, ref xCursor, ref yCursor, ref maxHeight);
		}
		if (Heirarchy.Count == 1)
		{
			TaskAddButton taskAddButton2 = UnityEngine.Object.Instantiate(InfectedButton);
			taskAddButton2.Text.Text = "Be_Impostor.exe";
			AddFileAsChild(Root, taskAddButton2, ref xCursor, ref yCursor, ref maxHeight);
		}
	}

	private void AddFileAsChild(TaskFolder taskFolder, TaskAddButton item, ref float xCursor, ref float yCursor, ref float maxHeight)
	{
		item.transform.SetParent(TaskParent);
		item.transform.localPosition = new Vector3(xCursor, yCursor, 0f);
		item.transform.localScale = Vector3.one;
		item.Text.RefreshMesh();
		maxHeight = Mathf.Max(maxHeight, item.Text.Height + 1.1f);
		xCursor += fileWidth;
		if (xCursor > lineWidth)
		{
			xCursor = 0f;
			yCursor -= maxHeight;
			maxHeight = 0f;
		}
		ActiveItems.Add(item.transform);
	}
}
