using System.Collections;
using UnityEngine;

public class TelescopeGame : Minigame
{
	private bool grabbed;

	public Transform Background;

	public SpriteRenderer ItemDisplay;

	public BoxCollider2D[] Items;

	private BoxCollider2D TargetItem;

	public BoxCollider2D Reticle;

	public SpriteRenderer ReticleImage;

	private Coroutine blinky;

	public void Start()
	{
		TargetItem = Items.Random();
		ItemDisplay.sprite = TargetItem.GetComponent<SpriteRenderer>().sprite;
	}

	public void Update()
	{
		NormalPlayerTask myNormTask = MyNormTask;
		if ((object)myNormTask != null && myNormTask.IsComplete)
		{
			return;
		}
		if (grabbed)
		{
			Controller controller = DestroyableSingleton<PassiveButtonManager>.Instance.controller;
			Vector2 vector = controller.DragPosition - controller.DragStartPosition;
			Vector3 localPosition = Background.transform.localPosition;
			localPosition.x = Mathf.Clamp(localPosition.x + vector.x, -6f, 6f);
			localPosition.y = Mathf.Clamp(localPosition.y + vector.y, -7f, 7f);
			Background.transform.localPosition = localPosition;
			controller.ResetDragPosition();
		}
		if (Reticle.IsTouching(TargetItem))
		{
			if (blinky == null)
			{
				blinky = StartCoroutine(CoBlinky());
			}
		}
		else if (blinky != null)
		{
			StopAllCoroutines();
			blinky = null;
			ReticleImage.color = Color.white;
		}
	}

	private IEnumerator CoBlinky()
	{
		int i = 0;
		while (i < 3)
		{
			ReticleImage.color = Color.green;
			yield return Effects.Wait(0.1f);
			ReticleImage.color = Color.white;
			yield return Effects.Wait(0.2f);
			int num = i + 1;
			i = num;
		}
		blinky = null;
		ReticleImage.color = Color.green;
		MyNormTask.NextStep();
		yield return CoStartClose();
	}

	public void Grab()
	{
		grabbed = !grabbed;
	}
}
