using UnityEngine;

public class TempMinigame : Minigame
{
	public TextRenderer LogText;

	public TextRenderer ReadingText;

	public IntRange LogRange;

	public IntRange ReadingRange;

	private int logValue;

	private int readingValue;

	public AudioClip ButtonSound;

	public override void Begin(PlayerTask task)
	{
		base.Begin(task);
		logValue = LogRange.Next();
		readingValue = ReadingRange.Next();
		ReadingText.Text = readingValue.ToString();
		ChangeNumber(0);
	}

	public void ChangeNumber(int dir)
	{
		if (logValue != readingValue)
		{
			if (dir != 0 && Constants.ShouldPlaySfx())
			{
				SoundManager.Instance.PlaySound(ButtonSound, false);
			}
			logValue += dir;
			LogText.Text = logValue.ToString();
			if (logValue == readingValue)
			{
				MyNormTask.NextStep();
				StartCoroutine(CoStartClose());
			}
		}
	}
}
