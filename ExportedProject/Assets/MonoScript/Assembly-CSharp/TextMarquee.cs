using System.Collections;
using UnityEngine;

public class TextMarquee : MonoBehaviour
{
	public TextRenderer Target;

	private string targetText;

	public float ScrollSpeed = 1f;

	public float PauseTime = 1f;

	public float AreaWidth = 3f;

	public bool IgnoreTextChanges;

	public void Start()
	{
		StartCoroutine(Run());
	}

	private IEnumerator Run()
	{
		yield return null;
		Target.render.material.SetInt("_Mask", 4);
		int i = 0;
		while (i < 1000)
		{
			Vector4 temp = default(Vector4);
			targetText = Target.Text;
			Target.render.material.SetVector("_Offset", temp);
			for (float timer3 = 0f; timer3 < PauseTime; timer3 += Time.deltaTime)
			{
				if (!IgnoreTextChanges && targetText != Target.Text)
				{
					break;
				}
				yield return null;
			}
			for (float timer3 = 0f; timer3 < 100f; timer3 += Time.deltaTime)
			{
				if (!IgnoreTextChanges && targetText != Target.Text)
				{
					break;
				}
				temp.x -= ScrollSpeed * Time.deltaTime;
				Target.render.material.SetVector("_Offset", temp);
				if (Target.Width + temp.x < AreaWidth)
				{
					break;
				}
				yield return null;
			}
			for (float timer3 = 0f; timer3 < PauseTime; timer3 += Time.deltaTime)
			{
				if (!IgnoreTextChanges && targetText != Target.Text)
				{
					break;
				}
				yield return null;
			}
			yield return null;
			int num = i + 1;
			i = num;
		}
	}
}
