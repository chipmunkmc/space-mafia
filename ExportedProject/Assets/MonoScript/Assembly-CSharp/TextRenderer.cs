using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
public class TextRenderer : MonoBehaviour
{
	[Serializable]
	[CompilerGenerated]
	private sealed class _003C_003Ec
	{
		public static readonly _003C_003Ec _003C_003E9 = new _003C_003Ec();

		public static Func<char, bool> _003C_003E9__31_0;

		internal bool _003CRefreshMesh_003Eb__31_0(char c)
		{
			return c > '✐';
		}
	}

	public TextAsset FontData;

	public float scale = 1f;

	public float TabWidth = 0.5f;

	public bool Centered;

	public bool RightAligned;

	public TextLink textLinkPrefab;

	[HideInInspector]
	private Mesh mesh;

	[HideInInspector]
	public MeshRenderer render;

	[Multiline]
	public string Text;

	private string lastText;

	public Color Color = Color.white;

	private Color lastColor = Color.white;

	public Color OutlineColor = Color.black;

	private Color lastOutlineColor = Color.white;

	public float maxWidth = -1f;

	public bool scaleToFit;

	public bool paragraphSpacing;

	private Vector2 cursorLocation;

	public float Width { get; private set; }

	public float Height { get; private set; }

	public Vector3 CursorPos
	{
		get
		{
			return new Vector3(cursorLocation.x / 100f * scale, cursorLocation.y / 100f * scale, -0.001f);
		}
	}

	public void Start()
	{
		render = GetComponent<MeshRenderer>();
		MeshFilter component = GetComponent<MeshFilter>();
		if (!component.mesh)
		{
			mesh = new Mesh();
			mesh.name = "Text" + base.name;
			component.mesh = mesh;
			render.material.SetColor("_OutlineColor", OutlineColor);
		}
		else
		{
			mesh = component.mesh;
		}
	}

	[ContextMenu("Generate Mesh")]
	public void GenerateMesh()
	{
		render = GetComponent<MeshRenderer>();
		MeshFilter component = GetComponent<MeshFilter>();
		if (!component.sharedMesh)
		{
			mesh = new Mesh();
			mesh.name = "Text" + base.name;
			component.mesh = mesh;
		}
		else
		{
			mesh = component.sharedMesh;
		}
		lastText = null;
		lastOutlineColor = OutlineColor;
		Update();
	}

	private void Update()
	{
		if (lastOutlineColor != OutlineColor)
		{
			lastOutlineColor = OutlineColor;
			render.material.SetColor("_OutlineColor", OutlineColor);
		}
		if (lastText != Text || lastColor != Color)
		{
			RefreshMesh();
		}
	}

	public void RefreshMesh()
	{
		if (render == null)
		{
			Start();
		}
		if (Text != null && Text.Any(_003C_003Ec._003C_003E9__31_0 ?? (_003C_003Ec._003C_003E9__31_0 = _003C_003Ec._003C_003E9._003CRefreshMesh_003Eb__31_0)))
		{
			FontCache.Instance.SetFont(this, "Korean");
		}
		FontData fontData = FontCache.Instance.LoadFont(FontData);
		lastText = Text;
		lastColor = Color;
		float num = scale;
		float num2 = 0f;
		if (scaleToFit)
		{
			num = Mathf.Min(scale, maxWidth / GetMaxWidth(fontData, lastText));
			num2 = fontData.LineHeight * (num - scale);
		}
		else if (maxWidth > 0f)
		{
			lastText = (Text = WrapText(fontData, lastText, maxWidth));
		}
		List<Vector3> list = new List<Vector3>(lastText.Length * 4);
		List<Vector2> list2 = new List<Vector2>(lastText.Length * 4);
		List<Vector4> list3 = new List<Vector4>(lastText.Length * 4);
		List<Color> list4 = new List<Color>(lastText.Length * 4);
		int[] array = new int[lastText.Length * 6];
		Width = 0f;
		cursorLocation.x = (cursorLocation.y = 0f);
		int num3 = -1;
		Vector2 from = default(Vector2);
		string text = null;
		int lineStart = 0;
		int num4 = 0;
		Color item = Color;
		int? num5 = null;
		for (int i = 0; i < lastText.Length; i++)
		{
			int num6 = lastText[i];
			if (num6 == 91)
			{
				if (num3 == 91)
				{
					continue;
				}
				if (lastText[i + 1] != '[')
				{
					num5 = 0;
					num3 = num6;
					continue;
				}
			}
			else if (num5.HasValue)
			{
				switch (num6)
				{
				case 93:
					if (num3 != 91)
					{
						item = new Color32((byte)((num5 >> 24) & 0xFF).Value, (byte)((num5 >> 16) & 0xFF).Value, (byte)((num5 >> 8) & 0xFF).Value, (byte)(num5 & 0xFF).Value);
						item.a *= Color.a;
					}
					else
					{
						item = Color;
					}
					num3 = -1;
					num5 = null;
					if (text != null)
					{
						TextLink textLink = UnityEngine.Object.Instantiate(textLinkPrefab, base.transform);
						textLink.transform.localScale = Vector3.one;
						Vector3 vector = list.Last();
						textLink.Set(from, vector, text);
						text = null;
					}
					break;
				case 104:
				{
					int num7 = lastText.IndexOf(']', i);
					text = lastText.Substring(i, num7 - i);
					from = list[list.Count - 2];
					item = new Color(0.5f, 0.5f, 1f);
					num3 = -1;
					num5 = null;
					i = num7;
					break;
				}
				default:
					num5 = (num5 << 4) | CharToInt(num6);
					break;
				}
				num3 = num6;
				continue;
			}
			switch (num6)
			{
			case 10:
			{
				if (Centered)
				{
					CenterVerts(list, cursorLocation.x, lineStart, num);
				}
				else if (RightAligned)
				{
					RightAlignVerts(list, cursorLocation.x, lineStart, num);
				}
				bool num9 = cursorLocation.x == 0f;
				cursorLocation.x = 0f;
				if (num9)
				{
					cursorLocation.y -= fontData.LineHeight / 2f;
				}
				else
				{
					cursorLocation.y -= fontData.LineHeight;
				}
				lineStart = list.Count;
				continue;
			}
			case 9:
			{
				float num8 = cursorLocation.x / 100f;
				num8 = Mathf.Ceil(num8 / TabWidth + 0.0001f) * TabWidth;
				cursorLocation.x = num8 * 100f;
				continue;
			}
			case 13:
				continue;
			}
			int value;
			if (!fontData.charMap.TryGetValue(num6, out value))
			{
				Debug.Log("Missing char :" + num6);
				num6 = -1;
				value = fontData.charMap[-1];
			}
			Vector4 vector2 = fontData.bounds[value];
			Vector2 textureSize = fontData.TextureSize;
			Vector3 vector3 = fontData.offsets[value];
			float kerning = fontData.GetKerning(num3, num6);
			float num10 = cursorLocation.x + vector3.x + kerning;
			float num11 = cursorLocation.y - vector3.y + num2;
			list.Add(new Vector3(num10, num11 - vector2.w) / 100f * num);
			list.Add(new Vector3(num10, num11) / 100f * num);
			list.Add(new Vector3(num10 + vector2.z, num11) / 100f * num);
			list.Add(new Vector3(num10 + vector2.z, num11 - vector2.w) / 100f * num);
			list4.Add(item);
			list4.Add(item);
			list4.Add(item);
			list4.Add(item);
			list2.Add(new Vector2(vector2.x / textureSize.x, 1f - (vector2.y + vector2.w) / textureSize.y));
			list2.Add(new Vector2(vector2.x / textureSize.x, 1f - vector2.y / textureSize.y));
			list2.Add(new Vector2((vector2.x + vector2.z) / textureSize.x, 1f - vector2.y / textureSize.y));
			list2.Add(new Vector2((vector2.x + vector2.z) / textureSize.x, 1f - (vector2.y + vector2.w) / textureSize.y));
			Vector4 item2 = fontData.Channels[value];
			list3.Add(item2);
			list3.Add(item2);
			list3.Add(item2);
			list3.Add(item2);
			array[num4 * 6] = num4 * 4;
			array[num4 * 6 + 1] = num4 * 4 + 1;
			array[num4 * 6 + 2] = num4 * 4 + 2;
			array[num4 * 6 + 3] = num4 * 4;
			array[num4 * 6 + 4] = num4 * 4 + 2;
			array[num4 * 6 + 5] = num4 * 4 + 3;
			cursorLocation.x += vector3.z + kerning;
			float num12 = cursorLocation.x / 100f * num;
			if (Width < num12)
			{
				Width = num12;
			}
			num3 = num6;
			num4++;
		}
		if (Centered)
		{
			CenterVerts(list, cursorLocation.x, lineStart, num);
			cursorLocation.x /= 2f;
			Width /= 2f;
		}
		else if (RightAligned)
		{
			RightAlignVerts(list, cursorLocation.x, lineStart, num);
		}
		Height = (0f - (cursorLocation.y - fontData.LineHeight)) / 100f * num;
		mesh.Clear();
		if (list.Count > 0)
		{
			mesh.SetVertices(list);
			mesh.SetColors(list4);
			mesh.SetUVs(0, list2);
			mesh.SetUVs(1, list3);
			mesh.SetIndices(array, MeshTopology.Triangles, 0);
		}
	}

	private float GetMaxWidth(FontData data, string lastText)
	{
		float num = 0f;
		float num2 = 0f;
		int last = -1;
		bool flag = false;
		int num3 = 0;
		for (int i = 0; i < lastText.Length && num3++ <= 1000; i++)
		{
			int num4 = lastText[i];
			switch (num4)
			{
			case 91:
				flag = true;
				break;
			case 93:
				flag = false;
				continue;
			}
			if (flag)
			{
				continue;
			}
			switch (num4)
			{
			case 10:
				last = -1;
				num2 = 0f;
				continue;
			case 9:
				num2 = Mathf.Ceil(num2 / 100f / 0.5f) * 0.5f * 100f;
				continue;
			case 13:
				continue;
			}
			int value;
			if (!data.charMap.TryGetValue(num4, out value))
			{
				Debug.Log("Missing char :" + num4);
				num4 = -1;
				value = data.charMap[-1];
			}
			int num5 = 32;
			num2 += data.offsets[value].z + data.GetKerning(last, num4);
			if (num2 > num)
			{
				num = num2;
			}
			last = num4;
		}
		return num / 100f;
	}

	private void RightAlignVerts(List<Vector3> verts, float baseX, int lineStart, float scale)
	{
		for (int i = lineStart; i < verts.Count; i++)
		{
			Vector3 value = verts[i];
			value.x -= baseX / 100f * scale;
			verts[i] = value;
		}
	}

	private void CenterVerts(List<Vector3> verts, float baseX, int lineStart, float scale)
	{
		for (int i = lineStart; i < verts.Count; i++)
		{
			Vector3 value = verts[i];
			value.x -= baseX / 200f * scale;
			verts[i] = value;
		}
	}

	private int CharToInt(int c)
	{
		if (c < 65)
		{
			return c - 48;
		}
		if (c < 97)
		{
			return 10 + (c - 65);
		}
		return 10 + (c - 97);
	}

	public static string WrapText(FontData data, string displayTxt, float maxWidth)
	{
		float num = 0f;
		int num2 = -1;
		int last = -1;
		bool flag = false;
		int num3 = 0;
		for (int i = 0; i < displayTxt.Length && num3++ <= 1000; i++)
		{
			int num4 = displayTxt[i];
			switch (num4)
			{
			case 91:
				flag = true;
				break;
			case 93:
				flag = false;
				continue;
			}
			if (flag)
			{
				continue;
			}
			switch (num4)
			{
			case 10:
				num2 = -1;
				last = -1;
				num = 0f;
				continue;
			case 9:
				num = Mathf.Ceil(num / 100f / 0.5f) * 0.5f * 100f;
				continue;
			case 13:
				continue;
			}
			int value;
			if (!data.charMap.TryGetValue(num4, out value))
			{
				Debug.Log("Missing char :" + num4);
				num4 = -1;
				value = data.charMap[-1];
			}
			if (num4 == 32)
			{
				num2 = i;
			}
			num += data.offsets[value].z + data.GetKerning(last, num4);
			if (num > maxWidth * 100f)
			{
				if (num2 != -1)
				{
					displayTxt = displayTxt.Substring(0, num2) + "\n" + displayTxt.Substring(num2 + 1);
					i = num2;
				}
				else
				{
					displayTxt = displayTxt.Substring(0, i) + "\n" + displayTxt.Substring(i);
				}
				num2 = -1;
				last = -1;
				num = 0f;
			}
			last = num4;
		}
		return displayTxt;
	}
}
