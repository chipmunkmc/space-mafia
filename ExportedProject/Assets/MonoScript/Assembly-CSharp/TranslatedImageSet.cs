using UnityEngine;

[CreateAssetMenu]
public class TranslatedImageSet : ScriptableObject
{
	public ImageData[] Images;
}
