using System.Collections;
using System.Runtime.CompilerServices;
using System.Text;
using PowerTools;
using UnityEngine;

public class UploadDataGame : Minigame
{
	[CompilerGenerated]
	private sealed class _003C_003Ec__DisplayClass22_0
	{
		public Color gray;

		public MeshRenderer rend2;

		public MeshRenderer rend1;

		internal void _003CPulseText_003Eb__0(float t)
		{
			Color value = Color.Lerp(Color.black, gray, t);
			rend2.material.SetColor("_OutlineColor", value);
			rend1.material.SetColor("_OutlineColor", value);
		}

		internal void _003CPulseText_003Eb__1(float t)
		{
			Color value = Color.Lerp(gray, Color.black, t);
			rend2.material.SetColor("_OutlineColor", value);
			rend1.material.SetColor("_OutlineColor", value);
		}
	}

	[CompilerGenerated]
	private sealed class _003C_003Ec__DisplayClass25_0
	{
		public Vector3 pos;

		public UploadDataGame _003C_003E4__this;

		internal void _003CDoRun_003Eb__0(float t)
		{
			pos.x = Mathf.Lerp(-1.25f, 0.5625f, t);
			_003C_003E4__this.Runner.transform.localPosition = pos;
		}

		internal void _003CDoRun_003Eb__1(float t)
		{
			pos.x = Mathf.Lerp(0.5625f, 1.25f, t);
			_003C_003E4__this.Runner.transform.localPosition = pos;
		}
	}

	public SpriteAnim LeftFolder;

	public SpriteAnim RightFolder;

	public AnimationClip FolderOpen;

	public AnimationClip FolderClose;

	public SpriteRenderer Runner;

	public HorizontalGauge Gauge;

	public TextRenderer PercentText;

	public TextRenderer EstimatedText;

	public TextRenderer SourceText;

	public TextRenderer TargetText;

	public SpriteRenderer Button;

	public Sprite DownloadImage;

	public GameObject Status;

	public GameObject Tower;

	private int count;

	private float timer;

	public const float RandomChunks = 5f;

	public const float ConstantTime = 3f;

	private bool running = true;

	public override void Begin(PlayerTask task)
	{
		PlayerControl.LocalPlayer.SetPlayerMaterialColors(Runner);
		base.Begin(task);
		if (MyNormTask.taskStep == 0)
		{
			Button.sprite = DownloadImage;
			Tower.SetActive(false);
			SourceText.Text = MyTask.StartAt.ToString();
			TargetText.Text = "My Tablet";
		}
		else
		{
			SourceText.Text = "My Tablet";
			TargetText.Text = "Headquarters";
		}
	}

	public void Click()
	{
		StartCoroutine(Transition());
	}

	private IEnumerator Transition()
	{
		Button.gameObject.SetActive(false);
		Status.SetActive(true);
		float target = Gauge.transform.localScale.x;
		for (float t = 0f; t < 0.15f; t += Time.deltaTime)
		{
			Gauge.transform.localScale = new Vector3(t / 0.15f * target, 1f, 1f);
			yield return null;
		}
		StartCoroutine(PulseText());
		StartCoroutine(DoRun());
		StartCoroutine(DoText());
		StartCoroutine(DoPercent());
	}

	private IEnumerator PulseText()
	{
		_003C_003Ec__DisplayClass22_0 _003C_003Ec__DisplayClass22_ = new _003C_003Ec__DisplayClass22_0();
		_003C_003Ec__DisplayClass22_.rend2 = PercentText.GetComponent<MeshRenderer>();
		_003C_003Ec__DisplayClass22_.rend1 = EstimatedText.GetComponent<MeshRenderer>();
		_003C_003Ec__DisplayClass22_.gray = new Color(0.3f, 0.3f, 0.3f, 1f);
		while (running)
		{
			yield return new WaitForLerp(0.4f, _003C_003Ec__DisplayClass22_._003CPulseText_003Eb__0);
			yield return new WaitForLerp(0.4f, _003C_003Ec__DisplayClass22_._003CPulseText_003Eb__1);
		}
		_003C_003Ec__DisplayClass22_.rend2.material.SetColor("_OutlineColor", Color.black);
		_003C_003Ec__DisplayClass22_.rend1.material.SetColor("_OutlineColor", Color.black);
	}

	private IEnumerator DoPercent()
	{
		while (running)
		{
			float num = (float)count / 5f * 0.7f + timer / 3f * 0.3f;
			if (num >= 1f)
			{
				running = false;
			}
			num = Mathf.Clamp(num, 0f, 1f);
			Gauge.Value = num;
			PercentText.Text = Mathf.RoundToInt(num * 100f) + "%";
			yield return null;
		}
	}

	private IEnumerator DoText()
	{
		StringBuilder txt = new StringBuilder("Estimated Time: ");
		int baselen = txt.Length;
		int max = 604800;
		count = 0;
		while ((float)count < 5f)
		{
			txt.Length = baselen;
			int num = IntRange.Next(max / 6, max);
			int num2 = num / 86400;
			if (num2 > 0)
			{
				txt.Append(num2 + "d ");
			}
			int num3 = num / 3600 % 24;
			if (num3 > 0)
			{
				txt.Append(num3 + "hr ");
			}
			int num4 = num / 60 % 60;
			if (num4 > 0)
			{
				txt.Append(num4 + "m ");
			}
			int num5 = num % 60;
			if (num5 > 0)
			{
				txt.Append(num5 + "s");
			}
			EstimatedText.Text = txt.ToString();
			max /= 4;
			yield return new WaitForSeconds(FloatRange.Next(0.6f, 1.2f));
			count++;
		}
		for (timer = 0f; timer < 3f; timer += Time.deltaTime)
		{
			txt.Length = baselen;
			int num6 = Mathf.RoundToInt(3f - timer);
			txt.Append(num6 + "s");
			EstimatedText.Text = txt.ToString();
			yield return null;
		}
	}

	private IEnumerator DoRun()
	{
		while (running)
		{
			_003C_003Ec__DisplayClass25_0 _003C_003Ec__DisplayClass25_ = new _003C_003Ec__DisplayClass25_0();
			_003C_003Ec__DisplayClass25_._003C_003E4__this = this;
			LeftFolder.Play(FolderOpen);
			_003C_003Ec__DisplayClass25_.pos = Runner.transform.localPosition;
			yield return new WaitForLerp(1.125f, _003C_003Ec__DisplayClass25_._003CDoRun_003Eb__0);
			LeftFolder.Play(FolderClose);
			RightFolder.Play(FolderOpen);
			yield return new WaitForLerp(1.375f, _003C_003Ec__DisplayClass25_._003CDoRun_003Eb__1);
			yield return new WaitForAnimationFinish(RightFolder, FolderClose);
		}
		EstimatedText.Text = DestroyableSingleton<TranslationController>.Instance.GetString(StringNames.WeatherComplete);
		MyNormTask.NextStep();
		StartCoroutine(CoStartClose());
	}
}
