using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

public class UploadDataTask : NormalPlayerTask
{
	public override bool ValidConsole(Console console)
	{
		if (console.Room != StartAt || !console.ValidTasks.Any(_003CValidConsole_003Eb__0_0))
		{
			if (taskStep == 1)
			{
				return console.TaskTypes.Contains(TaskType);
			}
			return false;
		}
		return true;
	}

	public override void AppendTaskText(StringBuilder sb)
	{
		if (taskStep > 0)
		{
			if (IsComplete)
			{
				sb.Append("[00DD00FF]");
			}
			else
			{
				sb.Append("[FFFF00FF]");
			}
		}
		sb.Append(DestroyableSingleton<TranslationController>.Instance.GetString((taskStep == 0) ? StartAt : SystemTypes.Admin));
		sb.Append(": ");
		sb.Append(DestroyableSingleton<TranslationController>.Instance.GetString((taskStep == 0) ? StringNames.DownloadData : StringNames.UploadData));
		sb.Append(" (");
		sb.Append(taskStep);
		sb.Append("/");
		sb.Append(MaxStep);
		sb.AppendLine(") []");
	}

	[CompilerGenerated]
	private bool _003CValidConsole_003Eb__0_0(TaskSet set)
	{
		if (TaskType == set.taskType)
		{
			return set.taskStep.Contains(taskStep);
		}
		return false;
	}
}
