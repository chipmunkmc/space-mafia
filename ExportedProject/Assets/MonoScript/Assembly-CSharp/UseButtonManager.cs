using System;
using System.Runtime.CompilerServices;
using UnityEngine;

public class UseButtonManager : MonoBehaviour
{
	[Serializable]
	[CompilerGenerated]
	private sealed class _003C_003Ec
	{
		public static readonly _003C_003Ec _003C_003E9 = new _003C_003Ec();

		public static Action<MapBehaviour> _003C_003E9__11_0;

		internal void _003CDoClick_003Eb__11_0(MapBehaviour m)
		{
			m.ShowInfectedMap();
		}
	}

	private static readonly Color DisabledColor = new Color(1f, 1f, 1f, 0.3f);

	private static readonly Color EnabledColor = new Color(1f, 1f, 1f, 1f);

	public SpriteRenderer UseButton;

	public Sprite UseImage;

	public Sprite SabotageImage;

	public Sprite VentImage;

	public Sprite AdminMapImage;

	public Sprite SecurityImage;

	public Sprite OptionsImage;

	private IUsable currentTarget;

	public void SetTarget(IUsable target)
	{
		currentTarget = target;
		if (target != null)
		{
			if (target is Vent)
			{
				UseButton.sprite = VentImage;
			}
			else if (target is MapConsole)
			{
				UseButton.sprite = AdminMapImage;
			}
			else if (target is OptionsConsole)
			{
				UseButton.sprite = OptionsImage;
			}
			else if (target is SystemConsole)
			{
				SystemConsole systemConsole = (SystemConsole)target;
				if (systemConsole.name.StartsWith("Surv"))
				{
					UseButton.sprite = SecurityImage;
				}
				else if (systemConsole.name.StartsWith("TaskAdd"))
				{
					UseButton.sprite = OptionsImage;
				}
				else
				{
					UseButton.sprite = UseImage;
				}
			}
			else
			{
				UseButton.sprite = UseImage;
			}
			UseButton.SetCooldownNormalizedUvs();
			UseButton.material.SetFloat("_Percent", target.PercentCool);
			UseButton.color = EnabledColor;
		}
		else if (PlayerControl.LocalPlayer.Data.IsImpostor && PlayerControl.LocalPlayer.CanMove)
		{
			UseButton.sprite = SabotageImage;
			UseButton.SetCooldownNormalizedUvs();
			UseButton.color = EnabledColor;
		}
		else
		{
			UseButton.sprite = UseImage;
			UseButton.color = DisabledColor;
		}
	}

	public void DoClick()
	{
		if (base.isActiveAndEnabled && (bool)PlayerControl.LocalPlayer)
		{
			GameData.PlayerInfo data = PlayerControl.LocalPlayer.Data;
			if (currentTarget != null)
			{
				PlayerControl.LocalPlayer.UseClosest();
			}
			else if (data != null && data.IsImpostor)
			{
				DestroyableSingleton<HudManager>.Instance.ShowMap(_003C_003Ec._003C_003E9__11_0 ?? (_003C_003Ec._003C_003E9__11_0 = _003C_003Ec._003C_003E9._003CDoClick_003Eb__11_0));
			}
		}
	}

	internal void Refresh()
	{
		SetTarget(currentTarget);
	}
}
