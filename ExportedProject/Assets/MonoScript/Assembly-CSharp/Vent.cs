using PowerTools;
using UnityEngine;

public class Vent : MonoBehaviour, IUsable
{
	public int Id;

	public Vent Left;

	public Vent Right;

	public Vent Center;

	public ButtonBehavior[] Buttons;

	public AnimationClip EnterVentAnim;

	public AnimationClip ExitVentAnim;

	private SpriteRenderer myRend;

	public Vector3 Offset = new Vector3(0f, 0.3636057f, 0f);

	public float UsableDistance
	{
		get
		{
			return 0.75f;
		}
	}

	public float PercentCool
	{
		get
		{
			return 0f;
		}
	}

	private void Start()
	{
		SetButtons(false);
		myRend = GetComponent<SpriteRenderer>();
	}

	public void SetButtons(bool enabled)
	{
		Vent[] array = new Vent[3] { Right, Left, Center };
		for (int i = 0; i < Buttons.Length; i++)
		{
			ButtonBehavior buttonBehavior = Buttons[i];
			if (enabled)
			{
				Vent vent = array[i];
				if ((bool)vent)
				{
					buttonBehavior.gameObject.SetActive(true);
					Vector3 localPosition = (vent.transform.position - base.transform.position).normalized * 0.7f;
					localPosition.y -= 0.08f;
					localPosition.z = -10f;
					buttonBehavior.transform.localPosition = localPosition;
					buttonBehavior.transform.LookAt2d(vent.transform);
				}
				else
				{
					buttonBehavior.gameObject.SetActive(false);
				}
			}
			else
			{
				buttonBehavior.gameObject.SetActive(false);
			}
		}
	}

	public float CanUse(GameData.PlayerInfo pc, out bool canUse, out bool couldUse)
	{
		float num = float.MaxValue;
		PlayerControl @object = pc.Object;
		couldUse = pc.IsImpostor && !pc.IsDead && (@object.CanMove || @object.inVent);
		canUse = couldUse;
		if (canUse)
		{
			num = Vector2.Distance(@object.GetTruePosition(), base.transform.position);
			canUse &= num <= UsableDistance;
		}
		return num;
	}

	public void SetOutline(bool on, bool mainTarget)
	{
		myRend.material.SetFloat("_Outline", on ? 1 : 0);
		myRend.material.SetColor("_OutlineColor", Color.red);
		myRend.material.SetColor("_AddColor", mainTarget ? Color.red : Color.clear);
	}

	public void ClickRight()
	{
		if ((bool)Right && PlayerControl.LocalPlayer.inVent)
		{
			DoMove(Right.transform.position);
			SetButtons(false);
			Right.SetButtons(true);
		}
	}

	public void ClickLeft()
	{
		if ((bool)Left && PlayerControl.LocalPlayer.inVent)
		{
			DoMove(Left.transform.position);
			SetButtons(false);
			Left.SetButtons(true);
		}
	}

	public void ClickCenter()
	{
		if ((bool)Center && PlayerControl.LocalPlayer.inVent)
		{
			DoMove(Center.transform.position);
			SetButtons(false);
			Center.SetButtons(true);
		}
	}

	private static void DoMove(Vector3 pos)
	{
		pos -= (Vector3)PlayerControl.LocalPlayer.Collider.offset;
		PlayerControl.LocalPlayer.NetTransform.RpcSnapTo(pos);
		if (Constants.ShouldPlaySfx())
		{
			SoundManager.Instance.PlaySound(PlayerControl.LocalPlayer.VentMoveSounds.Random(), false).pitch = FloatRange.Next(0.8f, 1.2f);
		}
	}

	public void Use()
	{
		bool canUse;
		bool couldUse;
		CanUse(PlayerControl.LocalPlayer.Data, out canUse, out couldUse);
		if (canUse)
		{
			PlayerControl localPlayer = PlayerControl.LocalPlayer;
			if (localPlayer.inVent)
			{
				localPlayer.MyPhysics.RpcExitVent(Id);
				SetButtons(false);
			}
			else
			{
				localPlayer.MyPhysics.RpcEnterVent(Id);
				SetButtons(true);
			}
		}
	}

	internal void EnterVent(PlayerControl pc)
	{
		if ((bool)EnterVentAnim)
		{
			GetComponent<SpriteAnim>().Play(EnterVentAnim);
			if (pc.AmOwner && Constants.ShouldPlaySfx())
			{
				SoundManager.Instance.StopSound(pc.VentEnterSound);
				SoundManager.Instance.PlaySound(pc.VentEnterSound, false).pitch = FloatRange.Next(0.8f, 1.2f);
			}
		}
	}

	internal void ExitVent(PlayerControl pc)
	{
		if ((bool)ExitVentAnim)
		{
			GetComponent<SpriteAnim>().Play(ExitVentAnim);
			if (pc.AmOwner && Constants.ShouldPlaySfx())
			{
				SoundManager.Instance.StopSound(pc.VentEnterSound);
				SoundManager.Instance.PlaySound(pc.VentEnterSound, false).pitch = FloatRange.Next(0.8f, 1.2f);
			}
		}
	}
}
