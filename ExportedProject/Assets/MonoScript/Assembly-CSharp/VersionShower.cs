using UnityEngine;

public class VersionShower : MonoBehaviour
{
	public TextRenderer text;

	public void Start()
	{
		string text = "v" + Application.version;
		text += "s";
		if (!DetectTamper.Detect())
		{
			text += "h";
		}
		this.text.Text = text;
		Screen.sleepTimeout = -1;
	}
}
