using UnityEngine;

public class VitalsMinigame : Minigame
{
	public VitalsPanel PanelPrefab;

	public Sprite VitalBgDead;

	public Sprite VitalBgDiscon;

	private VitalsPanel[] vitals;

	public IntRange BeatRange;

	public override void Begin(PlayerTask task)
	{
		base.Begin(task);
		vitals = new VitalsPanel[GameData.Instance.AllPlayers.Count];
		for (int i = 0; i < vitals.Length; i++)
		{
			GameData.PlayerInfo playerInfo = GameData.Instance.AllPlayers[i];
			VitalsPanel vitalsPanel = Object.Instantiate(PanelPrefab, base.transform);
			vitalsPanel.transform.localPosition = new Vector3((float)i * 0.6f + -2.7f, 0.2f, -1f);
			PlayerControl.SetPlayerMaterialColors(playerInfo.ColorId, vitalsPanel.PlayerImage);
			vitalsPanel.Text.Text = Palette.ShortColorNames[playerInfo.ColorId];
			if (playerInfo.Disconnected)
			{
				vitalsPanel.IsDiscon = true;
				vitalsPanel.Background.sprite = VitalBgDiscon;
				vitalsPanel.Cardio.gameObject.SetActive(false);
			}
			else if (playerInfo.IsDead)
			{
				vitalsPanel.IsDead = true;
				vitalsPanel.Background.sprite = VitalBgDead;
				vitalsPanel.Cardio.SetDead();
			}
			else
			{
				vitalsPanel.Cardio.Offset = IntRange.Next(0, 64);
				vitalsPanel.Cardio.beats = BeatRange.Next();
				vitalsPanel.Cardio.SetAlive();
			}
			vitals[i] = vitalsPanel;
		}
	}

	private void Update()
	{
		for (int i = 0; i < vitals.Length; i++)
		{
			VitalsPanel vitalsPanel = vitals[i];
			GameData.PlayerInfo playerInfo = GameData.Instance.AllPlayers[i];
			if (playerInfo.Disconnected && !vitalsPanel.IsDiscon)
			{
				vitalsPanel.IsDead = false;
				vitalsPanel.IsDiscon = true;
				vitalsPanel.Background.sprite = VitalBgDiscon;
				vitalsPanel.Cardio.gameObject.SetActive(false);
			}
			else if (playerInfo.IsDead && !vitalsPanel.IsDead)
			{
				vitalsPanel.IsDiscon = false;
				vitalsPanel.IsDead = true;
				vitalsPanel.Background.sprite = VitalBgDead;
				vitalsPanel.Cardio.SetDead();
			}
		}
	}
}
