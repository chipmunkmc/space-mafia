using UnityEngine;

public class VitalsPanel : MonoBehaviour
{
	public SpriteRenderer PlayerImage;

	public SpriteRenderer Background;

	public TextRenderer Text;

	public VertLineBehaviour Cardio;

	public bool IsDead;

	public bool IsDiscon;
}
