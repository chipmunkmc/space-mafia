using UnityEngine;

public class WaterStage : Minigame
{
	public float RefuelDuration = 5f;

	private Color darkRed = new Color32(90, 0, 0, byte.MaxValue);

	private Color red = new Color32(byte.MaxValue, 58, 0, byte.MaxValue);

	private Color green = Color.green;

	public SpriteRenderer redLight;

	public SpriteRenderer greenLight;

	public VerticalGauge srcGauge;

	public VerticalGauge destGauge;

	public AudioClip RefuelSound;

	private float timer;

	private bool isDown;

	private bool complete;

	public override void Begin(PlayerTask task)
	{
		base.Begin(task);
		timer = (float)(int)MyNormTask.Data[0] / 255f;
	}

	public void FixedUpdate()
	{
		if (complete)
		{
			return;
		}
		if (isDown && timer < 1f)
		{
			timer += Time.fixedDeltaTime / RefuelDuration;
			MyNormTask.Data[0] = (byte)Mathf.Min(255f, timer * 255f);
			if (timer >= 1f)
			{
				complete = true;
				if ((bool)greenLight)
				{
					greenLight.color = green;
				}
				if ((bool)redLight)
				{
					redLight.color = darkRed;
				}
				MyNormTask.Data[0] = 0;
				MyNormTask.NextStep();
			}
		}
		if ((bool)destGauge)
		{
			destGauge.value = timer;
		}
		if ((bool)srcGauge)
		{
			srcGauge.value = 1f - timer;
		}
	}

	public void Refuel()
	{
		if (complete)
		{
			base.transform.parent.GetComponent<Minigame>().Close();
			return;
		}
		isDown = !isDown;
		if ((bool)redLight)
		{
			redLight.color = (isDown ? red : darkRed);
		}
		if (isDown)
		{
			if (Constants.ShouldPlaySfx())
			{
				SoundManager.Instance.PlayDynamicSound("Refuel", RefuelSound, true, GetRefuelDynamics, true);
			}
		}
		else
		{
			SoundManager.Instance.StopSound(RefuelSound);
		}
	}

	private void GetRefuelDynamics(AudioSource player, float dt)
	{
		player.volume = 1f;
		if (MyNormTask.taskStep == 0)
		{
			player.pitch = Mathf.Lerp(0.75f, 1.25f, timer);
		}
		else
		{
			player.pitch = Mathf.Lerp(1.25f, 0.75f, timer);
		}
	}

	public override void Close()
	{
		SoundManager.Instance.StopSound(RefuelSound);
		if ((bool)Minigame.Instance)
		{
			Minigame.Instance.Close();
		}
	}
}
