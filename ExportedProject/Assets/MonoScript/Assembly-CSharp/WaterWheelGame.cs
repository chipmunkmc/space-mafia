using UnityEngine;

public class WaterWheelGame : Minigame
{
	public SpriteRenderer Wheel;

	public VerticalSpriteGauge WaterLevel;

	public SpriteRenderer Watertop;

	public int WheelScale = 4;

	public AudioClip FillStart;

	public AudioClip FillLoop;

	public AudioClip WheelTurn;

	private float Rate = 0.01f;

	private AudioSource fillSound;

	private bool grabbed;

	private float Water
	{
		get
		{
			return WaterLevel.Value;
		}
		set
		{
			WaterLevel.Value = value;
			MyNormTask.Data[base.ConsoleId] = (byte)(WaterLevel.Value * 255f);
		}
	}

	public override void Begin(PlayerTask task)
	{
		base.Begin(task);
		WaterLevel.Value = (float)(int)MyNormTask.Data[base.ConsoleId] / 255f;
		if (Constants.ShouldPlaySfx())
		{
			fillSound = SoundManager.Instance.PlaySound(FillStart, false);
		}
	}

	public void Update()
	{
		if (amClosing != 0)
		{
			return;
		}
		Water += Time.deltaTime * Rate;
		if ((bool)fillSound && !fillSound.isPlaying)
		{
			fillSound = SoundManager.Instance.PlaySound(FillLoop, true);
		}
		if ((bool)fillSound)
		{
			fillSound.volume = Mathf.Lerp(0f, 1f, Rate * 5f);
			fillSound.pitch = Mathf.Lerp(0.75f, 1.25f, Water);
		}
		if (grabbed)
		{
			Controller controller = DestroyableSingleton<PassiveButtonManager>.Instance.controller;
			Vector2 vector = Wheel.transform.position;
			float num = (controller.DragStartPosition - vector).AngleSigned(controller.DragPosition - vector);
			if (num > 180f)
			{
				num -= 360f;
			}
			if (num < -180f)
			{
				num += 360f;
			}
			num /= (float)WheelScale;
			Vector3 localEulerAngles = Wheel.transform.localEulerAngles;
			float num2 = Mathf.Clamp(localEulerAngles.z + num, 0.0001f, 358.99f);
			if (Mathf.Abs(localEulerAngles.z - num2) > 1f && Constants.ShouldPlaySfx())
			{
				AudioSource audioSource = SoundManager.Instance.PlaySound(WheelTurn, false);
				if (audioSource.timeSamples == 0)
				{
					audioSource.pitch = FloatRange.Next(0.9f, 1.1f);
				}
			}
			localEulerAngles.z = num2;
			Wheel.transform.localEulerAngles = localEulerAngles;
			Rate += num / (float)(360 * WheelScale);
			controller.ResetDragPosition();
		}
		else if (WaterLevel.Value > 0.95f)
		{
			MyNormTask.Data[base.ConsoleId] = 1;
			MyNormTask.NextStep();
			StartCoroutine(CoStartClose());
		}
		Vector3 localPosition = Watertop.transform.localPosition;
		localPosition.y = WaterLevel.TopY;
		Watertop.transform.localPosition = localPosition;
	}

	public void Grab()
	{
		grabbed = !grabbed;
	}

	public override void Close()
	{
		if ((bool)fillSound)
		{
			fillSound.Stop();
		}
		base.Close();
	}
}
