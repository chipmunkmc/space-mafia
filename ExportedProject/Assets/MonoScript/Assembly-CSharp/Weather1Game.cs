using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Weather1Game : Minigame
{
	private static int[] BarrierValidX = new int[10] { -10, -8, -6, -4, -2, 0, 2, 4, 6, 8 };

	private static int[] BarrierValidY = new int[3] { -2, 0, 2 };

	private const int MinX = -10;

	private const int MaxX = 8;

	private const int MinY = -3;

	private const int MaxY = 3;

	public Tilemap BarrierMap;

	public Tile fillTile;

	public Tile controlTile;

	public Tile barrierTile;

	private Controller control = new Controller();

	private bool inControl;

	private Vector3Int controlTilePos;

	private static Vector3Int[] Directions = new Vector3Int[4]
	{
		Vector3Int.up,
		Vector3Int.down,
		Vector3Int.left,
		Vector3Int.right
	};

	public void Start()
	{
		Vector3Int vector3Int = new Vector3Int(-9, 3, 0);
		HashSet<Vector3Int> hashSet = new HashSet<Vector3Int>();
		hashSet.Add(vector3Int);
		SolveMaze(vector3Int, hashSet);
		Matrix4x4 matrix4x = Matrix4x4.Rotate(Quaternion.Euler(0f, 0f, 90f));
		new List<Vector3Int>();
		Vector3Int vector3Int2 = default(Vector3Int);
		vector3Int2.x = -10;
		while (vector3Int2.x <= 8)
		{
			bool flag = vector3Int2.x % 2 == 0;
			vector3Int2.y = -3;
			while (vector3Int2.y <= 3)
			{
				bool flag2 = vector3Int2.y % 2 == 0;
				if (PointIsValid(vector3Int2) && !hashSet.Contains(vector3Int2) && flag == flag2 && BoolRange.Next(0.75f))
				{
					BarrierMap.SetTile(vector3Int2, barrierTile);
					if (flag)
					{
						BarrierMap.SetTransformMatrix(vector3Int2, matrix4x);
					}
				}
				vector3Int2.y++;
			}
			vector3Int2.x++;
		}
	}

	private bool SolveMaze(Vector3Int curPos, HashSet<Vector3Int> solution)
	{
		if (solution.Count > 50)
		{
			return false;
		}
		bool[] array = new bool[4];
		while (Contains(array, false))
		{
			int num = array.RandomIdx();
			while (array[num])
			{
				num = (num + 1) % array.Length;
			}
			array[num] = true;
			Vector3Int vector3Int = Directions[num] + curPos;
			if (PointIsValid(vector3Int) && solution.Add(vector3Int))
			{
				if (vector3Int.x == 7 && vector3Int.y == -3)
				{
					return true;
				}
				if (SolveMaze(vector3Int, solution))
				{
					return true;
				}
				solution.Remove(vector3Int);
			}
		}
		return false;
	}

	public void Update()
	{
		control.Update();
		if (control.AnyTouch)
		{
			Controller.TouchState touch = control.GetTouch(0);
			Vector3Int vector3Int = BarrierMap.WorldToCell(touch.Position);
			TileBase tile = BarrierMap.GetTile(vector3Int);
			if (touch.TouchStart)
			{
				if (tile == controlTile)
				{
					inControl = true;
					controlTilePos = vector3Int;
				}
			}
			else if (inControl && !tile && PointIsValid(vector3Int) && !AnythingBetween(controlTilePos, vector3Int))
			{
				FillLine(controlTilePos, vector3Int);
				controlTilePos = vector3Int;
				BarrierMap.SetTile(controlTilePos, controlTile);
			}
		}
		else if (control.AnyTouchUp)
		{
			control.GetTouch(0);
			if (!inControl)
			{
				return;
			}
			if (BarrierMap.GetTile(new Vector3Int(7, -3, 0)) == controlTile)
			{
				MyNormTask.NextStep();
				StartCoroutine(CoStartClose());
				return;
			}
			Vector3Int position = default(Vector3Int);
			position.x = -10;
			while (position.x <= 8)
			{
				position.y = -3;
				while (position.y <= 3)
				{
					if (BarrierMap.GetTile(position) == fillTile)
					{
						BarrierMap.SetTile(position, null);
					}
					position.y++;
				}
				position.x++;
			}
			BarrierMap.SetTile(controlTilePos, null);
			controlTilePos.x = -9;
			controlTilePos.y = 3;
			BarrierMap.SetTile(controlTilePos, controlTile);
		}
		else
		{
			inControl = false;
		}
	}

	private void FillLine(Vector3Int controlTilePos, Vector3Int touchCellPos)
	{
		Vector3Int position = controlTilePos;
		BarrierMap.SetTile(position, fillTile);
		if (controlTilePos.x == touchCellPos.x)
		{
			int num = (int)Mathf.Sign(touchCellPos.y - controlTilePos.y);
			while ((position.y += num) != touchCellPos.y)
			{
				BarrierMap.SetTile(position, fillTile);
			}
		}
		else if (controlTilePos.y == touchCellPos.y)
		{
			int num2 = (int)Mathf.Sign(touchCellPos.x - controlTilePos.x);
			while ((position.x += num2) != touchCellPos.x)
			{
				BarrierMap.SetTile(position, fillTile);
			}
		}
	}

	private bool AnythingBetween(Vector3Int controlTilePos, Vector3Int touchCellPos)
	{
		Vector3Int position = controlTilePos;
		if (controlTilePos.x == touchCellPos.x)
		{
			int num = (int)Mathf.Sign(touchCellPos.y - controlTilePos.y);
			while ((position.y += num) != touchCellPos.y)
			{
				if ((bool)BarrierMap.GetTile(position))
				{
					return true;
				}
			}
			return false;
		}
		if (controlTilePos.y == touchCellPos.y)
		{
			int num2 = (int)Mathf.Sign(touchCellPos.x - controlTilePos.x);
			while ((position.x += num2) != touchCellPos.x)
			{
				if ((bool)BarrierMap.GetTile(position))
				{
					return true;
				}
			}
			return false;
		}
		return true;
	}

	private bool PointIsValid(Vector3Int touchCellPos)
	{
		bool flag = touchCellPos.x % 2 == 0;
		bool result = touchCellPos.y % 2 == 0;
		if (touchCellPos.x <= 8 && touchCellPos.x >= -10 && touchCellPos.y <= 3 && touchCellPos.y >= -3)
		{
			if (flag)
			{
				return result;
			}
			return true;
		}
		return false;
	}

	private bool Contains<T>(T[] self, T item) where T : IComparable
	{
		for (int i = 0; i < self.Length; i++)
		{
			if (self[i].Equals(item))
			{
				return true;
			}
		}
		return false;
	}
}
