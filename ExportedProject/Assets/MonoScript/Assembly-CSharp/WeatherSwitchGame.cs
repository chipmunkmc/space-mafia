using UnityEngine;

public class WeatherSwitchGame : Minigame
{
	public static string[] ControlNames = new string[6] { "Node_CA", "Node_TB", "Node_IRO", "Node_PD", "Node_GI", "Node_MLG" };

	public WeatherControl[] Controls;

	private WeatherNodeTask WeatherTask;

	public AudioClip SwitchSound;

	public void Start()
	{
		for (int i = 0; i < Controls.Length; i++)
		{
			WeatherControl obj = Controls[i];
			obj.name = ControlNames[i];
			obj.Label.Text = ControlNames[i];
		}
	}

	public override void Begin(PlayerTask task)
	{
		base.Begin(task);
		WeatherTask = MyNormTask as WeatherNodeTask;
		Controls[WeatherTask.NodeId].SetInactive();
	}

	public void FlipSwitch(int i)
	{
		if (i == WeatherTask.NodeId)
		{
			if (Constants.ShouldPlaySfx())
			{
				SoundManager.Instance.PlaySound(SwitchSound, false);
			}
			WeatherTask.NextStep();
			Controls[WeatherTask.NodeId].SetActive();
			StartCoroutine(CoStartClose());
		}
	}
}
