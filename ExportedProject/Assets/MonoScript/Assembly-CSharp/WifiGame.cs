using System.Collections;
using System.Runtime.CompilerServices;
using UnityEngine;

public class WifiGame : Minigame
{
	private const int WaitDuration = 60;

	public SlideBar Slider;

	public TextRenderer StatusText;

	public SpriteRenderer[] Lights;

	public Sprite LightOn;

	public Sprite LightOff;

	public AudioClip SliderClick;

	private bool WifiOff;

	public override void Begin(PlayerTask task)
	{
		base.Begin(task);
		if (MyNormTask.TimerStarted == NormalPlayerTask.TimerState.NotStarted)
		{
			TurnOn(true);
		}
		else
		{
			TurnOff(true);
		}
	}

	public void Update()
	{
		if (MyNormTask.IsComplete)
		{
			return;
		}
		if (WifiOff)
		{
			if (MyNormTask.TimerStarted == NormalPlayerTask.TimerState.Finished)
			{
				StatusText.Text = DestroyableSingleton<TranslationController>.Instance.GetString(StringNames.WifiPleasePowerOn);
			}
			else if (MyNormTask.TimerStarted == NormalPlayerTask.TimerState.Started)
			{
				StatusText.Text = DestroyableSingleton<TranslationController>.Instance.GetString(StringNames.WifiPleaseReturnIn) + " " + Mathf.CeilToInt(MyNormTask.TaskTimer);
			}
		}
		if (!WifiOff && (double)Slider.Value < 0.1)
		{
			TurnOff();
		}
		else if (WifiOff && (double)Slider.Value > 0.9)
		{
			TurnOn();
		}
	}

	private void TurnOn(bool first = false)
	{
		if (Constants.ShouldPlaySfx() && !first)
		{
			SoundManager.Instance.PlaySound(SliderClick, false);
		}
		Slider.Value = 1f;
		Slider.UpdateValue();
		WifiOff = false;
		if (!first)
		{
			StopAllCoroutines();
		}
		StartCoroutine(RunLights(MyNormTask.TimerStarted == NormalPlayerTask.TimerState.Finished));
	}

	private void TurnOff(bool first = false)
	{
		if (Constants.ShouldPlaySfx() && !first)
		{
			SoundManager.Instance.PlaySound(SliderClick, false);
		}
		Slider.Value = 0f;
		Slider.UpdateValue();
		WifiOff = true;
		if (!first)
		{
			StopAllCoroutines();
		}
		Lights.ForEach(_003CTurnOff_003Eb__11_0);
		if (MyNormTask.TimerStarted == NormalPlayerTask.TimerState.NotStarted)
		{
			StatusText.Text = DestroyableSingleton<TranslationController>.Instance.GetString(StringNames.WifiPleaseReturnIn) + " " + 60;
			MyNormTask.TaskTimer = 60f;
			MyNormTask.TimerStarted = NormalPlayerTask.TimerState.Started;
		}
	}

	private IEnumerator RunLights(bool finishing)
	{
		if (!finishing)
		{
			StatusText.Text = DestroyableSingleton<TranslationController>.Instance.GetString(StringNames.WifiRebootRequired);
			Lights.ForEach(_003CRunLights_003Eb__12_0);
			Lights[1].sprite = LightOff;
			Lights[3].sprite = LightOff;
			Lights[4].sprite = LightOff;
			Lights[6].sprite = LightOff;
			yield return Effects.All(CoBlinkLight(Lights[2], 0.3f));
			yield break;
		}
		StatusText.Text = DestroyableSingleton<TranslationController>.Instance.GetString(StringNames.WifiPleaseWait);
		for (float timer = 0f; timer < 3f; timer += Time.deltaTime)
		{
			float num = timer / 3f;
			for (int i = 0; i < Lights.Length; i++)
			{
				float num2 = 0.75f * (float)i / (float)Lights.Length;
				float num3 = 0.75f * (float)(i + 1) / (float)Lights.Length;
				Lights[i].sprite = ((num > num2 && num < num3) ? LightOn : LightOff);
			}
			yield return null;
		}
		StatusText.Text = DestroyableSingleton<TranslationController>.Instance.GetString(StringNames.WifiRebootComplete);
		Lights.ForEach(_003CRunLights_003Eb__12_1);
		StartCoroutine(Effects.All(CoBlinkLight(Lights[3], 0.1f), CoBlinkLight(Lights[4], 0.09f), CoBlinkLight(Lights[5], 0.1f), CoBlinkLight(Lights[6], 0.05f), CoBlinkLight(Lights[7], 0.5f)));
		MyNormTask.NextStep();
		StartCoroutine(CoStartClose());
	}

	private IEnumerator CoBlinkLight(SpriteRenderer light, float delay)
	{
		while (true)
		{
			light.sprite = LightOn;
			yield return Effects.Wait(delay);
			light.sprite = LightOff;
			yield return Effects.Wait(delay);
		}
	}

	[CompilerGenerated]
	private void _003CTurnOff_003Eb__11_0(SpriteRenderer s)
	{
		s.sprite = LightOff;
	}

	[CompilerGenerated]
	private void _003CRunLights_003Eb__12_0(SpriteRenderer s)
	{
		s.sprite = LightOn;
	}

	[CompilerGenerated]
	private void _003CRunLights_003Eb__12_1(SpriteRenderer s)
	{
		s.sprite = LightOn;
	}
}
