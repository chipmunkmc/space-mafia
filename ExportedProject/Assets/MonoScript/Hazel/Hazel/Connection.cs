using System;
using System.Net;
using System.Threading;

namespace Hazel
{
	public abstract class Connection : IDisposable
	{
		public int TestLagMs = -1;

		public int TestDropRate;

		protected int testDropCount;

		protected ConnectionState _state;

		private ManualResetEvent connectWaitLock = new ManualResetEvent(false);

		public IPEndPoint EndPoint { get; protected set; }

		public IPMode IPMode { get; protected set; }

		public ConnectionStatistics Statistics { get; protected set; }

		public ConnectionState State
		{
			get
			{
				return _state;
			}
			protected set
			{
				_state = value;
				if (_state == ConnectionState.Connected)
				{
					connectWaitLock.Set();
				}
				else
				{
					connectWaitLock.Reset();
				}
			}
		}

		public event Action<DataReceivedEventArgs> DataReceived;

		public event EventHandler<DisconnectedEventArgs> Disconnected;

		protected Connection()
		{
			Statistics = new ConnectionStatistics();
			State = ConnectionState.NotConnected;
		}

		public abstract void Send(MessageWriter msg);

		public abstract void SendBytes(byte[] bytes, SendOption sendOption = SendOption.None);

		public abstract void Connect(byte[] bytes = null, int timeout = 5000);

		public abstract void ConnectAsync(byte[] bytes = null, int timeout = 5000);

		protected void InvokeDataReceived(MessageReader msg, SendOption sendOption)
		{
			Action<DataReceivedEventArgs> dataReceived = this.DataReceived;
			if (dataReceived != null)
			{
				dataReceived(new DataReceivedEventArgs(msg, sendOption));
			}
			else
			{
				msg.Recycle();
			}
		}

		protected void InvokeDisconnected(string e, MessageReader reader)
		{
			EventHandler<DisconnectedEventArgs> disconnected = this.Disconnected;
			if (disconnected != null)
			{
				DisconnectedEventArgs e2 = new DisconnectedEventArgs(e, reader);
				disconnected(this, e2);
			}
		}

		protected bool WaitOnConnect(int timeout)
		{
			return connectWaitLock.WaitOne(timeout);
		}

		public abstract void Disconnect(string reason, MessageWriter writer = null);

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				this.DataReceived = null;
				this.Disconnected = null;
				connectWaitLock.Dispose();
			}
		}
	}
}
