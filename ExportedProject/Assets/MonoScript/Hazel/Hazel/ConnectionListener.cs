using System;

namespace Hazel
{
	public abstract class ConnectionListener : IDisposable
	{
		public event Action<NewConnectionEventArgs> NewConnection;

		public abstract void Start();

		protected void InvokeNewConnection(MessageReader msg, Connection connection)
		{
			Action<NewConnectionEventArgs> newConnection = this.NewConnection;
			if (newConnection != null)
			{
				newConnection(new NewConnectionEventArgs(msg, connection));
			}
			else
			{
				msg.Recycle();
			}
		}

		public virtual void Close()
		{
			Dispose();
		}

		public void Dispose()
		{
			Dispose(true);
		}

		protected virtual void Dispose(bool disposing)
		{
			this.NewConnection = null;
		}
	}
}
