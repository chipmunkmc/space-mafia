using System.Threading;

namespace Hazel
{
	public class ConnectionStatistics
	{
		private int fragmentableMessagesSent;

		private int unreliableMessagesSent;

		private int reliableMessagesSent;

		private int fragmentedMessagesSent;

		private int acknowledgementMessagesSent;

		private int helloMessagesSent;

		private long dataBytesSent;

		private long totalBytesSent;

		private int unreliableMessagesReceived;

		private int reliableMessagesReceived;

		private int fragmentedMessagesReceived;

		private int acknowledgementMessagesReceived;

		private int pingMessagesReceived;

		private int helloMessagesReceived;

		private long dataBytesReceived;

		private long totalBytesReceived;

		private int messagesResent;

		public int MessagesSent
		{
			get
			{
				return UnreliableMessagesSent + ReliableMessagesSent + FragmentedMessagesSent + AcknowledgementMessagesSent + HelloMessagesSent;
			}
		}

		public int FragmentableMessagesSent
		{
			get
			{
				return fragmentableMessagesSent;
			}
		}

		public int UnreliableMessagesSent
		{
			get
			{
				return unreliableMessagesSent;
			}
		}

		public int ReliableMessagesSent
		{
			get
			{
				return reliableMessagesSent;
			}
		}

		public int FragmentedMessagesSent
		{
			get
			{
				return fragmentedMessagesSent;
			}
		}

		public int AcknowledgementMessagesSent
		{
			get
			{
				return acknowledgementMessagesSent;
			}
		}

		public int HelloMessagesSent
		{
			get
			{
				return helloMessagesSent;
			}
		}

		public long DataBytesSent
		{
			get
			{
				return Interlocked.Read(ref dataBytesSent);
			}
		}

		public long TotalBytesSent
		{
			get
			{
				return Interlocked.Read(ref totalBytesSent);
			}
		}

		public int MessagesReceived
		{
			get
			{
				return UnreliableMessagesReceived + ReliableMessagesReceived + FragmentedMessagesReceived + AcknowledgementMessagesReceived + helloMessagesReceived;
			}
		}

		public int UnreliableMessagesReceived
		{
			get
			{
				return unreliableMessagesReceived;
			}
		}

		public int ReliableMessagesReceived
		{
			get
			{
				return reliableMessagesReceived;
			}
		}

		public int FragmentedMessagesReceived
		{
			get
			{
				return fragmentedMessagesReceived;
			}
		}

		public int AcknowledgementMessagesReceived
		{
			get
			{
				return acknowledgementMessagesReceived;
			}
		}

		public int PingMessagesReceived
		{
			get
			{
				return pingMessagesReceived;
			}
		}

		public int HelloMessagesReceived
		{
			get
			{
				return helloMessagesReceived;
			}
		}

		public long DataBytesReceived
		{
			get
			{
				return Interlocked.Read(ref dataBytesReceived);
			}
		}

		public long TotalBytesReceived
		{
			get
			{
				return Interlocked.Read(ref totalBytesReceived);
			}
		}

		public int MessagesResent
		{
			get
			{
				return messagesResent;
			}
		}

		internal void LogUnreliableSend(int dataLength, int totalLength)
		{
			Interlocked.Increment(ref unreliableMessagesSent);
			Interlocked.Add(ref dataBytesSent, dataLength);
			Interlocked.Add(ref totalBytesSent, totalLength);
			if (totalLength > 1400)
			{
				Interlocked.Increment(ref fragmentableMessagesSent);
			}
		}

		internal void LogReliableSend(int dataLength, int totalLength)
		{
			Interlocked.Increment(ref reliableMessagesSent);
			Interlocked.Add(ref dataBytesSent, dataLength);
			Interlocked.Add(ref totalBytesSent, totalLength);
			if (totalLength > 1400)
			{
				Interlocked.Increment(ref fragmentableMessagesSent);
			}
		}

		internal void LogFragmentedSend(int dataLength, int totalLength)
		{
			Interlocked.Increment(ref fragmentedMessagesSent);
			Interlocked.Add(ref dataBytesSent, dataLength);
			Interlocked.Add(ref totalBytesSent, totalLength);
			if (totalLength > 1400)
			{
				Interlocked.Increment(ref fragmentableMessagesSent);
			}
		}

		internal void LogAcknowledgementSend(int totalLength)
		{
			Interlocked.Increment(ref acknowledgementMessagesSent);
			Interlocked.Add(ref totalBytesSent, totalLength);
		}

		internal void LogHelloSend(int totalLength)
		{
			Interlocked.Increment(ref helloMessagesSent);
			Interlocked.Add(ref totalBytesSent, totalLength);
		}

		internal void LogUnreliableReceive(int dataLength, int totalLength)
		{
			Interlocked.Increment(ref unreliableMessagesReceived);
			Interlocked.Add(ref dataBytesReceived, dataLength);
			Interlocked.Add(ref totalBytesReceived, totalLength);
		}

		internal void LogReliableReceive(int dataLength, int totalLength)
		{
			Interlocked.Increment(ref reliableMessagesReceived);
			Interlocked.Add(ref dataBytesReceived, dataLength);
			Interlocked.Add(ref totalBytesReceived, totalLength);
		}

		internal void LogFragmentedReceive(int dataLength, int totalLength)
		{
			Interlocked.Increment(ref fragmentedMessagesReceived);
			Interlocked.Add(ref dataBytesReceived, dataLength);
			Interlocked.Add(ref totalBytesReceived, totalLength);
		}

		internal void LogAcknowledgementReceive(int totalLength)
		{
			Interlocked.Increment(ref acknowledgementMessagesReceived);
			Interlocked.Add(ref totalBytesReceived, totalLength);
		}

		internal void LogPingReceive(int totalLength)
		{
			Interlocked.Increment(ref pingMessagesReceived);
			Interlocked.Add(ref totalBytesReceived, totalLength);
		}

		internal void LogHelloReceive(int totalLength)
		{
			Interlocked.Increment(ref helloMessagesReceived);
			Interlocked.Add(ref totalBytesReceived, totalLength);
		}

		internal void LogMessageResent()
		{
			Interlocked.Increment(ref messagesResent);
		}
	}
}
