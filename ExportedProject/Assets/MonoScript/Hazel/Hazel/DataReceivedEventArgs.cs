namespace Hazel
{
	public struct DataReceivedEventArgs
	{
		public readonly MessageReader Message;

		public readonly SendOption SendOption;

		public DataReceivedEventArgs(MessageReader msg, SendOption sendOption)
		{
			Message = msg;
			SendOption = sendOption;
		}
	}
}
