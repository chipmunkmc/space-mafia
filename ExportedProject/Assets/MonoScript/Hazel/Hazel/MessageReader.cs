using System;
using System.Runtime.CompilerServices;
using System.Text;

namespace Hazel
{
	public class MessageReader : IRecyclable
	{
		[Serializable]
		[CompilerGenerated]
		private sealed class _003C_003Ec
		{
			public static readonly _003C_003Ec _003C_003E9 = new _003C_003Ec();

			internal MessageReader _003C_002Ecctor_003Eb__33_0()
			{
				return new MessageReader();
			}
		}

		public static readonly ObjectPool<MessageReader> ReaderPool = new ObjectPool<MessageReader>(_003C_003Ec._003C_003E9._003C_002Ecctor_003Eb__33_0);

		public byte[] Buffer;

		public byte Tag;

		public int Length;

		public int Offset;

		private int _position;

		private int readHead;

		public int Position
		{
			get
			{
				return _position;
			}
			set
			{
				_position = value;
				readHead = value + Offset;
			}
		}

		public static MessageReader GetSized(int minSize)
		{
			MessageReader @object = ReaderPool.GetObject();
			if (@object.Buffer == null || @object.Buffer.Length < minSize)
			{
				@object.Buffer = new byte[minSize];
			}
			@object.Offset = 0;
			@object.Tag = byte.MaxValue;
			return @object;
		}

		public static MessageReader Get(byte[] buffer)
		{
			MessageReader @object = ReaderPool.GetObject();
			@object.Buffer = buffer;
			@object.Offset = 0;
			@object.Position = 0;
			@object.Length = buffer.Length;
			@object.Tag = byte.MaxValue;
			return @object;
		}

		public static MessageReader CopyMessageIntoParent(MessageReader source)
		{
			MessageReader sized = GetSized(source.Length + 3);
			System.Buffer.BlockCopy(source.Buffer, source.Offset - 3, sized.Buffer, 0, source.Length + 3);
			sized.Offset = 0;
			sized.Position = 0;
			sized.Length = source.Length + 3;
			return sized;
		}

		public static MessageReader Get(MessageReader source)
		{
			MessageReader sized = GetSized(source.Buffer.Length);
			System.Buffer.BlockCopy(source.Buffer, 0, sized.Buffer, 0, source.Buffer.Length);
			sized.Offset = source.Offset;
			sized._position = source._position;
			sized.readHead = source.readHead;
			sized.Length = source.Length;
			sized.Tag = source.Tag;
			return sized;
		}

		public static MessageReader Get(byte[] buffer, int offset)
		{
			if (offset + 3 > buffer.Length)
			{
				return null;
			}
			MessageReader @object = ReaderPool.GetObject();
			@object.Buffer = buffer;
			@object.Offset = offset;
			@object.Position = 0;
			@object.Length = @object.ReadUInt16();
			@object.Tag = @object.ReadByte();
			@object.Offset += 3;
			@object.Position = 0;
			return @object;
		}

		public MessageReader ReadMessage()
		{
			if (readHead + 3 > Buffer.Length)
			{
				return null;
			}
			MessageReader messageReader = new MessageReader();
			messageReader.Buffer = Buffer;
			messageReader.Offset = readHead;
			messageReader.Position = 0;
			messageReader.Length = messageReader.ReadUInt16();
			messageReader.Tag = messageReader.ReadByte();
			messageReader.Offset += 3;
			messageReader.Position = 0;
			Position += messageReader.Length + 3;
			return messageReader;
		}

		public void Recycle()
		{
			ReaderPool.PutObject(this);
		}

		public bool ReadBoolean()
		{
			return FastByte() != 0;
		}

		public sbyte ReadSByte()
		{
			return (sbyte)FastByte();
		}

		public byte ReadByte()
		{
			return FastByte();
		}

		public ushort ReadUInt16()
		{
			return (ushort)(FastByte() | (FastByte() << 8));
		}

		public short ReadInt16()
		{
			return (short)(FastByte() | (FastByte() << 8));
		}

		public uint ReadUInt32()
		{
			return (uint)(FastByte() | (FastByte() << 8) | (FastByte() << 16) | (FastByte() << 24));
		}

		public int ReadInt32()
		{
			return FastByte() | (FastByte() << 8) | (FastByte() << 16) | (FastByte() << 24);
		}

		public unsafe float ReadSingle()
		{
			float result = 0f;
			fixed (byte* ptr2 = &Buffer[readHead])
			{
				byte* ptr = (byte*)(&result);
				*ptr = *ptr2;
				ptr[1] = ptr2[1];
				ptr[2] = ptr2[2];
				ptr[3] = ptr2[3];
			}
			Position += 4;
			return result;
		}

		public string ReadString()
		{
			int num = ReadPackedInt32();
			string @string = Encoding.UTF8.GetString(Buffer, readHead, num);
			Position += num;
			return @string;
		}

		public byte[] ReadBytesAndSize()
		{
			int length = ReadPackedInt32();
			return ReadBytes(length);
		}

		public byte[] ReadBytes(int length)
		{
			byte[] array = new byte[length];
			Array.Copy(Buffer, readHead, array, 0, array.Length);
			Position += array.Length;
			return array;
		}

		public int ReadPackedInt32()
		{
			return (int)ReadPackedUInt32();
		}

		public uint ReadPackedUInt32()
		{
			bool flag = true;
			int num = 0;
			uint num2 = 0u;
			while (flag)
			{
				byte b = ReadByte();
				if (b >= 128)
				{
					flag = true;
					b = (byte)(b ^ 0x80u);
				}
				else
				{
					flag = false;
				}
				num2 |= (uint)(b << num);
				num += 7;
			}
			return num2;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private byte FastByte()
		{
			_position++;
			return Buffer[readHead++];
		}

		public unsafe static bool IsLittleEndian()
		{
			int num = 1;
			byte* ptr = (byte*)(&num);
			return *ptr == 1;
		}
	}
}
