using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Hazel
{
	public class MessageWriter : IRecyclable
	{
		[Serializable]
		[CompilerGenerated]
		private sealed class _003C_003Ec
		{
			public static readonly _003C_003Ec _003C_003E9 = new _003C_003Ec();

			internal MessageWriter _003C_002Ecctor_003Eb__40_0()
			{
				return new MessageWriter(BufferSize);
			}
		}

		public static int BufferSize = 64000;

		public static readonly ObjectPool<MessageWriter> WriterPool = new ObjectPool<MessageWriter>(_003C_003Ec._003C_003E9._003C_002Ecctor_003Eb__40_0);

		public byte[] Buffer;

		public int Length;

		public int Position;

		private Stack<int> messageStarts = new Stack<int>();

		public SendOption SendOption { get; private set; }

		public MessageWriter(byte[] buffer)
		{
			Buffer = buffer;
			Length = Buffer.Length;
		}

		public MessageWriter(int bufferSize)
		{
			Buffer = new byte[bufferSize];
		}

		public byte[] ToByteArray(bool includeHeader)
		{
			if (includeHeader)
			{
				byte[] array = new byte[Length];
				System.Buffer.BlockCopy(Buffer, 0, array, 0, Length);
				return array;
			}
			switch (SendOption)
			{
			case SendOption.Reliable:
			{
				byte[] array3 = new byte[Length - 3];
				System.Buffer.BlockCopy(Buffer, 3, array3, 0, Length - 3);
				return array3;
			}
			case SendOption.None:
			{
				byte[] array2 = new byte[Length - 1];
				System.Buffer.BlockCopy(Buffer, 1, array2, 0, Length - 1);
				return array2;
			}
			default:
				throw new NotImplementedException();
			}
		}

		public static MessageWriter Get(SendOption sendOption = SendOption.None)
		{
			MessageWriter @object = WriterPool.GetObject();
			@object.Clear(sendOption);
			return @object;
		}

		public bool HasBytes(int expected)
		{
			if (SendOption == SendOption.None)
			{
				return Length > 1 + expected;
			}
			return Length > 3 + expected;
		}

		public void StartMessage(byte typeFlag)
		{
			messageStarts.Push(Position);
			Position += 2;
			Write(typeFlag);
		}

		public void EndMessage()
		{
			int num = messageStarts.Pop();
			ushort num2 = (ushort)(Position - num - 3);
			Buffer[num] = (byte)num2;
			Buffer[num + 1] = (byte)(num2 >> 8);
		}

		public void CancelMessage()
		{
			Position = messageStarts.Pop();
			Length = Position;
		}

		public void Clear(SendOption sendOption)
		{
			messageStarts.Clear();
			SendOption = sendOption;
			Buffer[0] = (byte)sendOption;
			if (sendOption == SendOption.None || sendOption != SendOption.Reliable)
			{
				Length = (Position = 1);
			}
			else
			{
				Length = (Position = 3);
			}
		}

		public void Recycle()
		{
			Position = (Length = 0);
			WriterPool.PutObject(this);
		}

		public void CopyFrom(MessageReader target)
		{
			int srcOffset;
			int num;
			if (target.Tag == byte.MaxValue)
			{
				srcOffset = target.Offset;
				num = target.Length;
			}
			else
			{
				srcOffset = target.Offset - 3;
				num = target.Length + 3;
			}
			System.Buffer.BlockCopy(target.Buffer, srcOffset, Buffer, Position, num);
			Position += num;
			if (Position > Length)
			{
				Length = Position;
			}
		}

		public void Write(bool value)
		{
			Buffer[Position++] = (byte)(value ? 1u : 0u);
			if (Position > Length)
			{
				Length = Position;
			}
		}

		public void Write(sbyte value)
		{
			Buffer[Position++] = (byte)value;
			if (Position > Length)
			{
				Length = Position;
			}
		}

		public void Write(byte value)
		{
			Buffer[Position++] = value;
			if (Position > Length)
			{
				Length = Position;
			}
		}

		public void Write(short value)
		{
			Buffer[Position++] = (byte)value;
			Buffer[Position++] = (byte)(value >> 8);
			if (Position > Length)
			{
				Length = Position;
			}
		}

		public void Write(ushort value)
		{
			Buffer[Position++] = (byte)value;
			Buffer[Position++] = (byte)(value >> 8);
			if (Position > Length)
			{
				Length = Position;
			}
		}

		public void Write(uint value)
		{
			Buffer[Position++] = (byte)value;
			Buffer[Position++] = (byte)(value >> 8);
			Buffer[Position++] = (byte)(value >> 16);
			Buffer[Position++] = (byte)(value >> 24);
			if (Position > Length)
			{
				Length = Position;
			}
		}

		public void Write(int value)
		{
			Buffer[Position++] = (byte)value;
			Buffer[Position++] = (byte)(value >> 8);
			Buffer[Position++] = (byte)(value >> 16);
			Buffer[Position++] = (byte)(value >> 24);
			if (Position > Length)
			{
				Length = Position;
			}
		}

		public unsafe void Write(float value)
		{
			fixed (byte* ptr2 = &Buffer[Position])
			{
				byte* ptr = (byte*)(&value);
				*ptr2 = *ptr;
				ptr2[1] = ptr[1];
				ptr2[2] = ptr[2];
				ptr2[3] = ptr[3];
			}
			Position += 4;
			if (Position > Length)
			{
				Length = Position;
			}
		}

		public void Write(string value)
		{
			byte[] bytes = Encoding.UTF8.GetBytes(value);
			WritePacked(bytes.Length);
			Write(bytes);
		}

		public void WriteBytesAndSize(byte[] bytes)
		{
			WritePacked((uint)bytes.Length);
			Write(bytes);
		}

		public void WriteBytesAndSize(byte[] bytes, int length)
		{
			WritePacked((uint)length);
			Write(bytes, length);
		}

		public void WriteBytesAndSize(byte[] bytes, int offset, int length)
		{
			WritePacked((uint)length);
			Write(bytes, offset, length);
		}

		public void Write(byte[] bytes)
		{
			Array.Copy(bytes, 0, Buffer, Position, bytes.Length);
			Position += bytes.Length;
			if (Position > Length)
			{
				Length = Position;
			}
		}

		public void Write(byte[] bytes, int offset, int length)
		{
			Array.Copy(bytes, offset, Buffer, Position, length);
			Position += length;
			if (Position > Length)
			{
				Length = Position;
			}
		}

		public void Write(byte[] bytes, int length)
		{
			Array.Copy(bytes, 0, Buffer, Position, length);
			Position += length;
			if (Position > Length)
			{
				Length = Position;
			}
		}

		public void WritePacked(int value)
		{
			WritePacked((uint)value);
		}

		public void WritePacked(uint value)
		{
			do
			{
				byte b = (byte)(value & 0xFFu);
				if (value >= 128)
				{
					b = (byte)(b | 0x80u);
				}
				Write(b);
				value >>= 7;
			}
			while (value != 0);
		}

		public void Write(MessageWriter msg, bool includeHeader)
		{
			int num = 0;
			if (!includeHeader)
			{
				switch (msg.SendOption)
				{
				case SendOption.None:
					num = 1;
					break;
				case SendOption.Reliable:
					num = 3;
					break;
				}
			}
			Write(msg.Buffer, num, msg.Length - num);
		}

		public unsafe static bool IsLittleEndian()
		{
			int num = 1;
			byte* ptr = (byte*)(&num);
			return *ptr == 1;
		}
	}
}
