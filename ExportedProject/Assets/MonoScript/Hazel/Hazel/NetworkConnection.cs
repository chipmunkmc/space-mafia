using System;
using System.Net;

namespace Hazel
{
	public abstract class NetworkConnection : Connection
	{
		public EndPoint RemoteEndPoint { get; protected set; }

		public long GetIP4Address()
		{
			if (base.IPMode == IPMode.IPv4)
			{
				return ((IPEndPoint)RemoteEndPoint).Address.Address;
			}
			byte[] addressBytes = ((IPEndPoint)RemoteEndPoint).Address.GetAddressBytes();
			return BitConverter.ToInt64(addressBytes, addressBytes.Length - 8);
		}

		protected abstract bool SendDisconnect(MessageWriter writer);

		protected void DisconnectRemote(string reason, MessageReader reader)
		{
			if (SendDisconnect(null))
			{
				try
				{
					InvokeDisconnected(reason, reader);
				}
				catch
				{
				}
			}
			Dispose();
		}

		public override void Disconnect(string reason, MessageWriter writer = null)
		{
			if (SendDisconnect(writer))
			{
				try
				{
					InvokeDisconnected(reason, null);
				}
				catch
				{
				}
			}
			Dispose();
		}
	}
}
