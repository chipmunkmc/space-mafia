namespace Hazel
{
	public struct NewConnectionEventArgs
	{
		public readonly MessageReader HandshakeData;

		public readonly Connection Connection;

		public NewConnectionEventArgs(MessageReader handshakeData, Connection connection)
		{
			HandshakeData = handshakeData;
			Connection = connection;
		}
	}
}
