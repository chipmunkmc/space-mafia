using System;
using System.Collections.Concurrent;
using System.Threading;

namespace Hazel
{
	public sealed class ObjectPool<T> where T : IRecyclable
	{
		private int numberCreated;

		private readonly ConcurrentBag<T> pool = new ConcurrentBag<T>();

		private readonly ConcurrentDictionary<T, bool> inuse = new ConcurrentDictionary<T, bool>();

		private readonly Func<T> objectFactory;

		public int NumberCreated
		{
			get
			{
				return numberCreated;
			}
		}

		public int NumberInUse
		{
			get
			{
				return inuse.Count;
			}
		}

		public int NumberNotInUse
		{
			get
			{
				return pool.Count;
			}
		}

		internal ObjectPool(Func<T> objectFactory)
		{
			this.objectFactory = objectFactory;
		}

		internal T GetObject()
		{
			T result;
			if (!pool.TryTake(out result))
			{
				Interlocked.Increment(ref numberCreated);
				result = objectFactory();
			}
			if (!inuse.TryAdd(result, true))
			{
				throw new Exception("Duplicate pull " + typeof(T).Name);
			}
			return result;
		}

		internal void PutObject(T item)
		{
			bool value;
			if (inuse.TryRemove(item, out value))
			{
				pool.Add(item);
				return;
			}
			throw new Exception("Duplicate add " + typeof(T).Name);
		}
	}
}
