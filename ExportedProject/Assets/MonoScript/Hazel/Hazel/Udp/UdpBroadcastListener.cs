using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;

namespace Hazel.Udp
{
	public class UdpBroadcastListener : IDisposable
	{
		[CompilerGenerated]
		private sealed class _003C_003Ec__DisplayClass10_0
		{
			public IAsyncResult result;

			public UdpBroadcastListener _003C_003E4__this;

			internal void _003CStartListen_003Eb__0(object _)
			{
				_003C_003E4__this.HandleData(result);
			}
		}

		private Socket socket;

		private EndPoint endpoint;

		private Action<string> logger;

		private byte[] buffer = new byte[1024];

		private List<BroadcastPacket> packets = new List<BroadcastPacket>();

		public bool Running { get; private set; }

		public UdpBroadcastListener(int port, Action<string> logger = null)
		{
			this.logger = logger;
			socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
			socket.EnableBroadcast = true;
			socket.MulticastLoopback = false;
			endpoint = new IPEndPoint(IPAddress.Any, port);
			socket.Bind(endpoint);
		}

		public void StartListen()
		{
			if (Running)
			{
				return;
			}
			Running = true;
			try
			{
				_003C_003Ec__DisplayClass10_0 _003C_003Ec__DisplayClass10_ = new _003C_003Ec__DisplayClass10_0();
				_003C_003Ec__DisplayClass10_._003C_003E4__this = this;
				EndPoint remoteEP = new IPEndPoint(IPAddress.Any, 0);
				_003C_003Ec__DisplayClass10_.result = socket.BeginReceiveFrom(buffer, 0, buffer.Length, SocketFlags.None, ref remoteEP, HandleData, null);
				if (_003C_003Ec__DisplayClass10_.result.CompletedSynchronously)
				{
					ThreadPool.QueueUserWorkItem(_003C_003Ec__DisplayClass10_._003CStartListen_003Eb__0);
				}
			}
			catch (NullReferenceException)
			{
			}
			catch (Exception ex2)
			{
				Action<string> action = logger;
				if (action != null)
				{
					action("BroadcastListener: " + ex2);
				}
				Dispose();
			}
		}

		private void HandleData(IAsyncResult result)
		{
			Running = false;
			EndPoint endPoint = new IPEndPoint(IPAddress.Any, 0);
			int num;
			try
			{
				num = socket.EndReceiveFrom(result, ref endPoint);
			}
			catch (NullReferenceException)
			{
				return;
			}
			catch (Exception ex2)
			{
				Action<string> action = logger;
				if (action != null)
				{
					action("BroadcastListener: " + ex2);
				}
				Dispose();
				return;
			}
			if (num < 3 || buffer[0] != 4 || buffer[1] != 2)
			{
				StartListen();
				return;
			}
			IPEndPoint iPEndPoint = (IPEndPoint)endPoint;
			string @string = Encoding.UTF8.GetString(buffer, 2, num - 2);
			int hashCode = @string.GetHashCode();
			lock (packets)
			{
				bool flag = false;
				for (int i = 0; i < packets.Count; i++)
				{
					BroadcastPacket broadcastPacket = packets[i];
					if (broadcastPacket == null || broadcastPacket.Data == null)
					{
						packets.RemoveAt(i);
						i--;
					}
					else if (broadcastPacket.Data.GetHashCode() == hashCode && broadcastPacket.Sender.Equals(iPEndPoint))
					{
						packets[i].ReceiveTime = DateTime.Now;
						break;
					}
				}
				if (!flag)
				{
					packets.Add(new BroadcastPacket(@string, iPEndPoint));
				}
			}
			StartListen();
		}

		public BroadcastPacket[] GetPackets()
		{
			lock (packets)
			{
				BroadcastPacket[] result = packets.ToArray();
				packets.Clear();
				return result;
			}
		}

		public void Dispose()
		{
			if (socket != null)
			{
				try
				{
					socket.Shutdown(SocketShutdown.Both);
				}
				catch
				{
				}
				try
				{
					socket.Close();
				}
				catch
				{
				}
				try
				{
					socket.Dispose();
				}
				catch
				{
				}
				socket = null;
			}
		}
	}
}
