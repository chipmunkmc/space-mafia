using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Hazel.Udp
{
	public class UdpBroadcaster : IDisposable
	{
		private Socket socket;

		private byte[] data;

		private EndPoint endpoint;

		private Action<string> logger;

		public UdpBroadcaster(int port, Action<string> logger = null)
		{
			this.logger = logger;
			socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
			socket.EnableBroadcast = true;
			socket.MulticastLoopback = false;
			endpoint = new IPEndPoint(IPAddress.Broadcast, port);
		}

		public void SetData(string data)
		{
			int byteCount = Encoding.UTF8.GetByteCount(data);
			this.data = new byte[byteCount + 2];
			this.data[0] = 4;
			this.data[1] = 2;
			Encoding.UTF8.GetBytes(data, 0, data.Length, this.data, 2);
		}

		public void Broadcast()
		{
			if (data == null)
			{
				return;
			}
			try
			{
				socket.BeginSendTo(data, 0, data.Length, SocketFlags.None, endpoint, FinishSendTo, null);
			}
			catch (Exception ex)
			{
				Action<string> action = logger;
				if (action != null)
				{
					action("BroadcastListener: " + ex);
				}
			}
		}

		private void FinishSendTo(IAsyncResult evt)
		{
			try
			{
				socket.EndSendTo(evt);
			}
			catch (Exception ex)
			{
				Action<string> action = logger;
				if (action != null)
				{
					action("BroadcastListener: " + ex);
				}
			}
		}

		public void Dispose()
		{
			if (socket != null)
			{
				try
				{
					socket.Shutdown(SocketShutdown.Both);
				}
				catch
				{
				}
				try
				{
					socket.Close();
				}
				catch
				{
				}
				try
				{
					socket.Dispose();
				}
				catch
				{
				}
				socket = null;
			}
		}
	}
}
