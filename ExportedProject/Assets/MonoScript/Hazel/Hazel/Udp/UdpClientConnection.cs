using System;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Threading;

namespace Hazel.Udp
{
	public sealed class UdpClientConnection : UdpConnection
	{
		private Socket socket;

		private Timer reliablePacketTimer;

		public UdpClientConnection(IPEndPoint remoteEndPoint, IPMode ipMode = IPMode.IPv4)
		{
			base.EndPoint = remoteEndPoint;
			base.RemoteEndPoint = remoteEndPoint;
			base.IPMode = ipMode;
			if (base.IPMode == IPMode.IPv4)
			{
				socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
			}
			else
			{
				if (!Socket.OSSupportsIPv6)
				{
					throw new InvalidOperationException("IPV6 not supported!");
				}
				socket = new Socket(AddressFamily.InterNetworkV6, SocketType.Dgram, ProtocolType.Udp);
				socket.SetSocketOption(SocketOptionLevel.IPv6, SocketOptionName.IPv6Only, false);
			}
			reliablePacketTimer = new Timer(ManageReliablePacketsInternal, null, 100, -1);
		}

		~UdpClientConnection()
		{
			Dispose(false);
		}

		private void ManageReliablePacketsInternal(object state)
		{
			ManageReliablePackets();
			try
			{
				reliablePacketTimer.Change(100, -1);
			}
			catch
			{
			}
		}

		protected override void WriteBytesToConnection(byte[] bytes, int length)
		{
			WriteBytesToConnectionReal(bytes, length);
		}

		private void WriteBytesToConnectionReal(byte[] bytes, int length)
		{
			try
			{
				socket.BeginSendTo(bytes, 0, length, SocketFlags.None, base.RemoteEndPoint, HandleSendTo, null);
			}
			catch (NullReferenceException)
			{
			}
			catch (ObjectDisposedException)
			{
			}
			catch (SocketException ex3)
			{
				Disconnect("Could not send data as a SocketException occurred: " + ex3.Message);
			}
		}

		private void HandleSendTo(IAsyncResult result)
		{
			try
			{
				socket.EndSendTo(result);
			}
			catch (NullReferenceException)
			{
			}
			catch (ObjectDisposedException)
			{
			}
			catch (SocketException ex3)
			{
				Disconnect("Could not send data as a SocketException occurred: " + ex3.Message);
			}
		}

		public override void Connect(byte[] bytes = null, int timeout = 5000)
		{
			ConnectAsync(bytes, timeout);
			if (!WaitOnConnect(timeout))
			{
				Dispose();
				throw new HazelException("Connection attempt timed out.");
			}
		}

		public override void ConnectAsync(byte[] bytes = null, int timeout = 5000)
		{
			base.State = ConnectionState.Connecting;
			try
			{
				if (base.IPMode == IPMode.IPv4)
				{
					socket.Bind(new IPEndPoint(IPAddress.Any, 0));
				}
				else
				{
					socket.Bind(new IPEndPoint(IPAddress.IPv6Any, 0));
				}
			}
			catch (SocketException e)
			{
				base.State = ConnectionState.NotConnected;
				throw new HazelException("A SocketException occurred while binding to the port.", e);
			}
			try
			{
				StartListeningForData();
			}
			catch (ObjectDisposedException)
			{
				base.State = ConnectionState.NotConnected;
				return;
			}
			catch (SocketException e2)
			{
				Dispose();
				throw new HazelException("A SocketException occurred while initiating a receive operation.", e2);
			}
			SendHello(bytes, _003CConnectAsync_003Eb__9_0);
		}

		private void StartListeningForData()
		{
			MessageReader sized = MessageReader.GetSized(65535);
			try
			{
				socket.BeginReceive(sized.Buffer, 0, sized.Buffer.Length, SocketFlags.None, ReadCallback, sized);
			}
			catch
			{
				sized.Recycle();
			}
		}

		private void ReadCallback(IAsyncResult result)
		{
			MessageReader messageReader = (MessageReader)result.AsyncState;
			try
			{
				messageReader.Length = socket.EndReceive(result);
			}
			catch (SocketException ex)
			{
				messageReader.Recycle();
				Disconnect("Socket exception while reading data: " + ex.Message);
				return;
			}
			catch (Exception)
			{
				messageReader.Recycle();
				return;
			}
			if (messageReader.Length == 0)
			{
				messageReader.Recycle();
				Disconnect("Received 0 bytes");
				return;
			}
			try
			{
				StartListeningForData();
			}
			catch (SocketException ex3)
			{
				Disconnect("Socket exception during receive: " + ex3.Message);
			}
			catch (ObjectDisposedException)
			{
				return;
			}
			HandleReceive(messageReader, messageReader.Length);
		}

		protected override bool SendDisconnect(MessageWriter data = null)
		{
			lock (this)
			{
				if (_state == ConnectionState.NotConnected)
				{
					return false;
				}
				_state = ConnectionState.NotConnected;
			}
			byte[] array = UdpConnection.EmptyDisconnectBytes;
			if (data != null && data.Length > 0)
			{
				if (data.SendOption != 0)
				{
					throw new ArgumentException("Disconnect messages can only be unreliable.");
				}
				array = data.ToByteArray(true);
				array[0] = 9;
			}
			try
			{
				socket.SendTo(array, 0, array.Length, SocketFlags.None, base.RemoteEndPoint);
			}
			catch
			{
			}
			return true;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				SendDisconnect(null);
			}
			try
			{
				socket.Shutdown(SocketShutdown.Both);
			}
			catch
			{
			}
			try
			{
				socket.Close();
			}
			catch
			{
			}
			try
			{
				socket.Dispose();
			}
			catch
			{
			}
			reliablePacketTimer.Dispose();
			base.Dispose(disposing);
		}

		[CompilerGenerated]
		private void _003CConnectAsync_003Eb__9_0()
		{
			base.State = ConnectionState.Connected;
			InitializeKeepAliveTimer();
		}
	}
}
