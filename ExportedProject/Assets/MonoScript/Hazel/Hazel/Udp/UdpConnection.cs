using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;

namespace Hazel.Udp
{
	public abstract class UdpConnection : NetworkConnection
	{
		public class PingPacket : IRecyclable
		{
			[Serializable]
			[CompilerGenerated]
			private sealed class _003C_003Ec
			{
				public static readonly _003C_003Ec _003C_003E9 = new _003C_003Ec();

				internal PingPacket _003C_002Ecctor_003Eb__5_0()
				{
					return new PingPacket();
				}
			}

			private static readonly ObjectPool<PingPacket> PacketPool = new ObjectPool<PingPacket>(_003C_003Ec._003C_003E9._003C_002Ecctor_003Eb__5_0);

			public readonly Stopwatch Stopwatch = new Stopwatch();

			internal static PingPacket GetObject()
			{
				return PacketPool.GetObject();
			}

			public void Recycle()
			{
				Stopwatch.Stop();
				PacketPool.PutObject(this);
			}
		}

		public class Packet : IRecyclable
		{
			[Serializable]
			[CompilerGenerated]
			private sealed class _003C_003Ec
			{
				public static readonly _003C_003Ec _003C_003E9 = new _003C_003Ec();

				internal Packet _003C_002Ecctor_003Eb__15_0()
				{
					return new Packet();
				}
			}

			public static readonly ObjectPool<Packet> PacketPool = new ObjectPool<Packet>(_003C_003Ec._003C_003E9._003C_002Ecctor_003Eb__15_0);

			public ushort Id;

			private byte[] Data;

			private UdpConnection Connection;

			private int Length;

			public int NextTimeout;

			public volatile bool Acknowledged;

			public Action AckCallback;

			public int Retransmissions;

			public Stopwatch Stopwatch = new Stopwatch();

			internal static Packet GetObject()
			{
				return PacketPool.GetObject();
			}

			private Packet()
			{
			}

			internal void Set(ushort id, UdpConnection connection, byte[] data, int length, int timeout, Action ackCallback)
			{
				Id = id;
				Data = data;
				Connection = connection;
				Length = length;
				Acknowledged = false;
				NextTimeout = timeout;
				AckCallback = ackCallback;
				Retransmissions = 0;
				Stopwatch.Restart();
			}

			public int Resend()
			{
				UdpConnection connection = Connection;
				if (!Acknowledged && connection != null)
				{
					long elapsedMilliseconds = Stopwatch.ElapsedMilliseconds;
					if (elapsedMilliseconds >= connection.DisconnectTimeout)
					{
						Packet value;
						if (connection.reliableDataPacketsSent.TryRemove(Id, out value))
						{
							connection.Disconnect(string.Format("Reliable packet {0} (size={1}) was not ack'd after {2}ms ({3} resends)", value.Id, Length, elapsedMilliseconds, value.Retransmissions));
							value.Recycle();
						}
						return 0;
					}
					if (elapsedMilliseconds >= NextTimeout)
					{
						Retransmissions++;
						if (connection.ResendLimit != 0 && Retransmissions > connection.ResendLimit)
						{
							Packet value2;
							if (connection.reliableDataPacketsSent.TryRemove(Id, out value2))
							{
								connection.Disconnect(string.Format("Reliable packet {0} (size={1}) was not ack'd after {2} resends ({3}ms)", value2.Id, Length, value2.Retransmissions, elapsedMilliseconds));
								value2.Recycle();
							}
							return 0;
						}
						NextTimeout += (int)Math.Min((float)NextTimeout * connection.ResendPingMultiplier, 1000f);
						try
						{
							connection.WriteBytesToConnection(Data, Length);
							connection.Statistics.LogMessageResent();
							return 1;
						}
						catch (InvalidOperationException)
						{
							connection.Disconnect("Could not resend data as connection is no longer connected");
						}
					}
				}
				return 0;
			}

			public void Recycle()
			{
				Acknowledged = true;
				Connection = null;
				PacketPool.PutObject(this);
			}
		}

		public static readonly byte[] EmptyDisconnectBytes = new byte[1] { 9 };

		internal ConcurrentDictionary<ushort, PingPacket> activePingPackets = new ConcurrentDictionary<ushort, PingPacket>();

		private int keepAliveInterval = 1500;

		private volatile int pingsSinceAck;

		private Timer keepAliveTimer;

		public volatile int ResendTimeout;

		public volatile int ResendLimit;

		public volatile float ResendPingMultiplier = 2f;

		private int lastIDAllocated;

		internal ConcurrentDictionary<ushort, Packet> reliableDataPacketsSent = new ConcurrentDictionary<ushort, Packet>();

		private HashSet<ushort> reliableDataPacketsMissing = new HashSet<ushort>();

		private volatile ushort reliableReceiveLast;

		private volatile bool hasReceivedSomething;

		private object PingLock = new object();

		public float AveragePingMs = 500f;

		public volatile int DisconnectTimeout = 5000;

		public int KeepAliveInterval
		{
			get
			{
				return keepAliveInterval;
			}
			set
			{
				keepAliveInterval = value;
				ResetKeepAliveTimer();
			}
		}

		public int MissingPingsUntilDisconnect { get; set; } = 6;


		protected abstract void WriteBytesToConnection(byte[] bytes, int length);

		public override void Send(MessageWriter msg)
		{
			if (_state != ConnectionState.Connected)
			{
				throw new InvalidOperationException("Could not send data as this Connection is not connected. Did you disconnect?");
			}
			byte[] array = new byte[msg.Length];
			Buffer.BlockCopy(msg.Buffer, 0, array, 0, msg.Length);
			SendOption sendOption = msg.SendOption;
			if (sendOption == SendOption.Reliable)
			{
				ResetKeepAliveTimer();
				AttachReliableID(array, 1, array.Length);
				WriteBytesToConnection(array, array.Length);
				base.Statistics.LogReliableSend(array.Length - 3, array.Length);
			}
			else
			{
				WriteBytesToConnection(array, array.Length);
				base.Statistics.LogUnreliableSend(array.Length - 1, array.Length);
			}
		}

		public override void SendBytes(byte[] bytes, SendOption sendOption = SendOption.None)
		{
			HandleSend(bytes, (byte)sendOption);
		}

		protected void HandleSend(byte[] data, byte sendOption, Action ackCallback = null)
		{
			if (sendOption == 1 || sendOption == 8 || sendOption == 12)
			{
				ReliableSend(sendOption, data, ackCallback);
			}
			else
			{
				UnreliableSend(sendOption, data);
			}
		}

		protected internal void HandleReceive(MessageReader message, int bytesReceived)
		{
			ushort id;
			switch (message.Buffer[0])
			{
			case 1:
				ReliableMessageReceive(message, bytesReceived);
				break;
			case 10:
				AcknowledgementMessageReceive(message.Buffer, bytesReceived);
				message.Recycle();
				break;
			case 12:
				ProcessReliableReceive(message.Buffer, 1, out id);
				base.Statistics.LogHelloReceive(bytesReceived);
				message.Recycle();
				break;
			case 8:
				ProcessReliableReceive(message.Buffer, 1, out id);
				base.Statistics.LogHelloReceive(bytesReceived);
				break;
			case 9:
				message.Offset = 1;
				message.Position = 0;
				DisconnectRemote("The remote sent a disconnect request", message);
				message.Recycle();
				break;
			default:
				InvokeDataReceived(SendOption.None, message, 1, bytesReceived);
				base.Statistics.LogUnreliableReceive(bytesReceived - 1, bytesReceived);
				break;
			}
		}

		private void UnreliableSend(byte sendOption, byte[] data)
		{
			UnreliableSend(sendOption, data, 0, data.Length);
		}

		private void UnreliableSend(byte sendOption, byte[] data, int offset, int length)
		{
			byte[] array = new byte[length + 1];
			array[0] = sendOption;
			Buffer.BlockCopy(data, offset, array, array.Length - length, length);
			WriteBytesToConnection(array, array.Length);
			base.Statistics.LogUnreliableSend(length, array.Length);
		}

		private void InvokeDataReceived(SendOption sendOption, MessageReader buffer, int dataOffset, int bytesReceived)
		{
			buffer.Offset = dataOffset;
			buffer.Length = bytesReceived - dataOffset;
			buffer.Position = 0;
			InvokeDataReceived(buffer, sendOption);
		}

		protected void SendHello(byte[] bytes, Action acknowledgeCallback)
		{
			byte[] array;
			if (bytes == null)
			{
				array = new byte[1];
			}
			else
			{
				array = new byte[bytes.Length + 1];
				Buffer.BlockCopy(bytes, 0, array, 1, bytes.Length);
			}
			HandleSend(array, 8, acknowledgeCallback);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				DisposeKeepAliveTimer();
				DisposeReliablePackets();
			}
			base.Dispose(disposing);
		}

		protected void InitializeKeepAliveTimer()
		{
			keepAliveTimer = new Timer(_003CInitializeKeepAliveTimer_003Eb__23_0, null, keepAliveInterval, keepAliveInterval);
		}

		private void SendPing()
		{
			ushort num = (ushort)Interlocked.Increment(ref lastIDAllocated);
			byte[] array = new byte[3]
			{
				12,
				(byte)(num >> 8),
				(byte)num
			};
			PingPacket value;
			if (!activePingPackets.TryGetValue(num, out value))
			{
				value = PingPacket.GetObject();
				if (!activePingPackets.TryAdd(num, value))
				{
					throw new Exception("This shouldn't be possible");
				}
			}
			value.Stopwatch.Restart();
			WriteBytesToConnection(array, array.Length);
			base.Statistics.LogReliableSend(0, array.Length);
		}

		private void ResetKeepAliveTimer()
		{
			try
			{
				keepAliveTimer.Change(keepAliveInterval, keepAliveInterval);
			}
			catch
			{
			}
		}

		private void DisposeKeepAliveTimer()
		{
			if (keepAliveTimer != null)
			{
				keepAliveTimer.Dispose();
			}
			foreach (KeyValuePair<ushort, PingPacket> activePingPacket in activePingPackets)
			{
				PingPacket value;
				if (activePingPackets.TryRemove(activePingPacket.Key, out value))
				{
					value.Recycle();
				}
			}
		}

		internal int ManageReliablePackets()
		{
			int num = 0;
			if (reliableDataPacketsSent.Count > 0)
			{
				foreach (KeyValuePair<ushort, Packet> item in reliableDataPacketsSent)
				{
					Packet value = item.Value;
					try
					{
						num += value.Resend();
					}
					catch
					{
					}
				}
				return num;
			}
			return num;
		}

		private void AttachReliableID(byte[] buffer, int offset, int sendLength, Action ackCallback = null)
		{
			ushort num = (ushort)Interlocked.Increment(ref lastIDAllocated);
			buffer[offset] = (byte)(num >> 8);
			buffer[offset + 1] = (byte)num;
			Packet @object = Packet.GetObject();
			@object.Set(num, this, buffer, sendLength, (ResendTimeout > 0) ? ResendTimeout : ((int)Math.Min(AveragePingMs * ResendPingMultiplier, 300f)), ackCallback);
			if (!reliableDataPacketsSent.TryAdd(num, @object))
			{
				throw new Exception("That shouldn't be possible");
			}
		}

		public static int ClampToInt(float value, int min, int max)
		{
			if (value < (float)min)
			{
				return min;
			}
			if (value > (float)max)
			{
				return max;
			}
			return (int)value;
		}

		private void ReliableSend(byte sendOption, byte[] data, Action ackCallback = null)
		{
			ReliableSend(sendOption, data, 0, data.Length, ackCallback);
		}

		private void ReliableSend(byte sendOption, byte[] data, int offset, int length, Action ackCallback = null)
		{
			ResetKeepAliveTimer();
			byte[] array = new byte[length + 3];
			array[0] = sendOption;
			AttachReliableID(array, 1, array.Length, ackCallback);
			Buffer.BlockCopy(data, offset, array, array.Length - length, length);
			WriteBytesToConnection(array, array.Length);
			base.Statistics.LogReliableSend(length, array.Length);
		}

		private void ReliableMessageReceive(MessageReader message, int bytesReceived)
		{
			ushort id;
			if (ProcessReliableReceive(message.Buffer, 1, out id))
			{
				InvokeDataReceived(SendOption.Reliable, message, 3, bytesReceived);
			}
			else
			{
				message.Recycle();
			}
			base.Statistics.LogReliableReceive(message.Length - 3, message.Length);
		}

		private bool ProcessReliableReceive(byte[] bytes, int offset, out ushort id)
		{
			byte b = bytes[offset];
			byte b2 = bytes[offset + 1];
			id = (ushort)((b << 8) + b2);
			SendAck(b, b2);
			lock (reliableDataPacketsMissing)
			{
				ushort num = (ushort)(reliableReceiveLast - 32768);
				bool flag = ((num >= reliableReceiveLast) ? (id > reliableReceiveLast && id <= num) : (id > reliableReceiveLast || id <= num));
				if (flag || !hasReceivedSomething)
				{
					for (ushort num2 = (ushort)(reliableReceiveLast + 1); num2 < id; num2 = (ushort)(num2 + 1))
					{
						reliableDataPacketsMissing.Add(num2);
					}
					reliableReceiveLast = id;
					hasReceivedSomething = true;
				}
				else if (!reliableDataPacketsMissing.Remove(id))
				{
					return false;
				}
			}
			return true;
		}

		private void AcknowledgementMessageReceive(byte[] bytes, int bytesReceived)
		{
			pingsSinceAck = 0;
			ushort key = (ushort)((bytes[1] << 8) + bytes[2]);
			Packet value;
			PingPacket value2;
			if (reliableDataPacketsSent.TryRemove(key, out value))
			{
				float num = value.Stopwatch.ElapsedMilliseconds;
				Action ackCallback = value.AckCallback;
				if (ackCallback != null)
				{
					ackCallback();
				}
				value.Recycle();
				lock (PingLock)
				{
					AveragePingMs = Math.Max(50f, AveragePingMs * 0.7f + num * 0.3f);
				}
			}
			else if (activePingPackets.TryRemove(key, out value2))
			{
				float num2 = value2.Stopwatch.ElapsedMilliseconds;
				value2.Recycle();
				lock (PingLock)
				{
					AveragePingMs = Math.Max(50f, AveragePingMs * 0.7f + num2 * 0.3f);
				}
			}
			base.Statistics.LogReliableReceive(bytesReceived - 3, bytesReceived);
		}

		private void SendAck(byte byte1, byte byte2)
		{
			byte[] array = new byte[3] { 10, byte1, byte2 };
			try
			{
				WriteBytesToConnection(array, array.Length);
			}
			catch (InvalidOperationException)
			{
			}
		}

		private void DisposeReliablePackets()
		{
			foreach (KeyValuePair<ushort, Packet> item in reliableDataPacketsSent)
			{
				Packet value;
				if (reliableDataPacketsSent.TryRemove(item.Key, out value))
				{
					value.Recycle();
				}
			}
		}

		[CompilerGenerated]
		private void _003CInitializeKeepAliveTimer_003Eb__23_0(object o)
		{
			if (pingsSinceAck >= MissingPingsUntilDisconnect)
			{
				DisposeKeepAliveTimer();
				Disconnect(string.Format("Sent {0} pings that remote has not responded to.", pingsSinceAck));
				return;
			}
			try
			{
				pingsSinceAck++;
				SendPing();
			}
			catch
			{
			}
		}
	}
}
