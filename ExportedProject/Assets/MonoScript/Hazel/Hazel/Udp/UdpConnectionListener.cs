using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Hazel.Udp
{
	public class UdpConnectionListener : NetworkConnectionListener
	{
		public delegate bool AcceptConnectionCheck(byte[] input, out byte[] response);

		public const int BufferSize = 65535;

		public int MinConnectionLength;

		public AcceptConnectionCheck AcceptConnection;

		private Socket socket;

		private Action<string> Logger;

		private Timer reliablePacketTimer;

		private ConcurrentDictionary<EndPoint, UdpServerConnection> allConnections = new ConcurrentDictionary<EndPoint, UdpServerConnection>();

		public volatile int ActiveCallbacks;

		public int ConnectionCount
		{
			get
			{
				return allConnections.Count;
			}
		}

		public UdpConnectionListener(IPEndPoint endPoint, IPMode ipMode = IPMode.IPv4, Action<string> logger = null)
		{
			Logger = logger;
			base.EndPoint = endPoint;
			base.IPMode = ipMode;
			if (base.IPMode == IPMode.IPv4)
			{
				socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
			}
			else
			{
				if (!Socket.OSSupportsIPv6)
				{
					throw new HazelException("IPV6 not supported!");
				}
				socket = new Socket(AddressFamily.InterNetworkV6, SocketType.Dgram, ProtocolType.Udp);
				socket.SetSocketOption(SocketOptionLevel.IPv6, SocketOptionName.IPv6Only, false);
			}
			socket.ReceiveBufferSize = 65535;
			socket.SendBufferSize = 65535;
			reliablePacketTimer = new Timer(ManageReliablePackets, null, 100, -1);
		}

		~UdpConnectionListener()
		{
			Dispose(false);
		}

		private void ManageReliablePackets(object state)
		{
			foreach (KeyValuePair<EndPoint, UdpServerConnection> allConnection in allConnections)
			{
				allConnection.Value.ManageReliablePackets();
			}
			try
			{
				reliablePacketTimer.Change(100, -1);
			}
			catch
			{
			}
		}

		public override void Start()
		{
			try
			{
				socket.Bind(base.EndPoint);
			}
			catch (SocketException e)
			{
				throw new HazelException("Could not start listening as a SocketException occurred", e);
			}
			StartListeningForData();
		}

		private void StartListeningForData()
		{
			EndPoint remoteEP = base.EndPoint;
			MessageReader messageReader = null;
			try
			{
				messageReader = MessageReader.GetSized(65535);
				if (socket.BeginReceiveFrom(messageReader.Buffer, 0, messageReader.Buffer.Length, SocketFlags.None, ref remoteEP, ReadCallback, messageReader).CompletedSynchronously)
				{
					Logger("Operation completed synchronously");
				}
			}
			catch (SocketException ex)
			{
				if (messageReader != null)
				{
					messageReader.Recycle();
				}
				Action<string> logger = Logger;
				if (logger != null)
				{
					logger("Socket Ex in StartListening: " + ex.Message);
				}
				Thread.Sleep(10);
				StartListeningForData();
			}
			catch (Exception ex2)
			{
				messageReader.Recycle();
				Action<string> logger2 = Logger;
				if (logger2 != null)
				{
					logger2("Stopped due to: " + ex2.Message);
				}
			}
		}

		private void ReadCallback(IAsyncResult result)
		{
			Interlocked.Increment(ref ActiveCallbacks);
			MessageReader messageReader = (MessageReader)result.AsyncState;
			EndPoint endPoint = new IPEndPoint((base.IPMode == IPMode.IPv4) ? IPAddress.Any : IPAddress.IPv6Any, 0);
			int num;
			try
			{
				num = socket.EndReceiveFrom(result, ref endPoint);
				messageReader.Offset = 0;
				messageReader.Length = num;
			}
			catch (SocketException ex)
			{
				messageReader.Recycle();
				Action<string> logger = Logger;
				if (logger != null)
				{
					logger("Socket Ex in ReadCallback: " + ex.Message);
				}
				Thread.Sleep(10);
				StartListeningForData();
				Interlocked.Decrement(ref ActiveCallbacks);
				return;
			}
			catch (Exception ex2)
			{
				messageReader.Recycle();
				Action<string> logger2 = Logger;
				if (logger2 != null)
				{
					logger2("Stopped due to: " + ex2.Message);
				}
				Interlocked.Decrement(ref ActiveCallbacks);
				return;
			}
			if (num == 0)
			{
				messageReader.Recycle();
				Action<string> logger3 = Logger;
				if (logger3 != null)
				{
					logger3("Received 0 bytes");
				}
				Thread.Sleep(10);
				StartListeningForData();
				Interlocked.Decrement(ref ActiveCallbacks);
				return;
			}
			StartListeningForData();
			bool flag = true;
			bool flag2 = messageReader.Buffer[0] == 8;
			bool flag3 = flag2 && messageReader.Length >= MinConnectionLength;
			UdpServerConnection value;
			if (!allConnections.TryGetValue(endPoint, out value))
			{
				lock (allConnections)
				{
					if (!allConnections.TryGetValue(endPoint, out value))
					{
						if (!flag3)
						{
							messageReader.Recycle();
							Interlocked.Decrement(ref ActiveCallbacks);
							return;
						}
						byte[] response;
						if (AcceptConnection != null && !AcceptConnection(messageReader.Buffer, out response))
						{
							messageReader.Recycle();
							SendData(response, response.Length, endPoint);
							Interlocked.Decrement(ref ActiveCallbacks);
							return;
						}
						flag = false;
						value = new UdpServerConnection(this, (IPEndPoint)endPoint, base.IPMode);
						if (!allConnections.TryAdd(endPoint, value))
						{
							throw new HazelException("Failed to add a connection. This should never happen.");
						}
					}
				}
			}
			value.HandleReceive(messageReader, num);
			if (!flag)
			{
				messageReader.Offset = 4;
				messageReader.Length = num - 4;
				messageReader.Position = 0;
				InvokeNewConnection(messageReader, value);
			}
			else if (flag3 || (!flag3 && flag2))
			{
				messageReader.Recycle();
			}
			Interlocked.Decrement(ref ActiveCallbacks);
		}

		internal void SendData(byte[] bytes, int length, EndPoint endPoint)
		{
			if (length > bytes.Length)
			{
				return;
			}
			try
			{
				socket.BeginSendTo(bytes, 0, length, SocketFlags.None, endPoint, SendCallback, null);
			}
			catch (SocketException e)
			{
				throw new HazelException("Could not send data as a SocketException occurred.", e);
			}
			catch (ObjectDisposedException)
			{
			}
		}

		private void SendCallback(IAsyncResult result)
		{
			try
			{
				socket.EndSendTo(result);
			}
			catch
			{
			}
		}

		internal void SendDataSync(byte[] bytes, int length, EndPoint endPoint)
		{
			try
			{
				socket.SendTo(bytes, 0, length, SocketFlags.None, endPoint);
			}
			catch
			{
			}
		}

		internal void RemoveConnectionTo(EndPoint endPoint)
		{
			UdpServerConnection value;
			allConnections.TryRemove(endPoint, out value);
		}

		protected override void Dispose(bool disposing)
		{
			foreach (KeyValuePair<EndPoint, UdpServerConnection> allConnection in allConnections)
			{
				allConnection.Value.Dispose();
			}
			try
			{
				socket.Shutdown(SocketShutdown.Both);
			}
			catch
			{
			}
			try
			{
				socket.Close();
			}
			catch
			{
			}
			try
			{
				socket.Dispose();
			}
			catch
			{
			}
			reliablePacketTimer.Dispose();
			base.Dispose(disposing);
		}
	}
}
