namespace Hazel.Udp
{
	public enum UdpSendOption : byte
	{
		Hello = 8,
		Ping = 12,
		Disconnect = 9,
		Acknowledgement = 10,
		Fragment = 11
	}
}
