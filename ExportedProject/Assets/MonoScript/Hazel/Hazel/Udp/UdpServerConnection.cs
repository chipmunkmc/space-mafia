using System;
using System.Net;

namespace Hazel.Udp
{
	internal sealed class UdpServerConnection : UdpConnection
	{
		public UdpConnectionListener Listener { get; private set; }

		internal UdpServerConnection(UdpConnectionListener listener, IPEndPoint endPoint, IPMode IPMode)
		{
			Listener = listener;
			base.RemoteEndPoint = endPoint;
			base.EndPoint = endPoint;
			base.IPMode = IPMode;
			base.State = ConnectionState.Connected;
			InitializeKeepAliveTimer();
		}

		protected override void WriteBytesToConnection(byte[] bytes, int length)
		{
			Listener.SendData(bytes, length, base.RemoteEndPoint);
		}

		public override void Connect(byte[] bytes = null, int timeout = 5000)
		{
			throw new InvalidOperationException("Cannot manually connect a UdpServerConnection, did you mean to use UdpClientConnection?");
		}

		public override void ConnectAsync(byte[] bytes = null, int timeout = 5000)
		{
			throw new InvalidOperationException("Cannot manually connect a UdpServerConnection, did you mean to use UdpClientConnection?");
		}

		protected override bool SendDisconnect(MessageWriter data = null)
		{
			lock (this)
			{
				if (_state != ConnectionState.Connected)
				{
					return false;
				}
				_state = ConnectionState.NotConnected;
			}
			byte[] array = UdpConnection.EmptyDisconnectBytes;
			if (data != null && data.Length > 0)
			{
				if (data.SendOption != 0)
				{
					throw new ArgumentException("Disconnect messages can only be unreliable.");
				}
				array = data.ToByteArray(true);
				array[0] = 9;
			}
			try
			{
				Listener.SendDataSync(array, array.Length, base.RemoteEndPoint);
			}
			catch
			{
			}
			return true;
		}

		protected override void Dispose(bool disposing)
		{
			Listener.RemoveConnectionTo(base.RemoteEndPoint);
			if (disposing)
			{
				SendDisconnect(null);
			}
			base.Dispose(disposing);
		}
	}
}
